﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SPR.UI.App.Tools
{
    public class WindowSettings
    {
        [XmlAttribute]
        public double Width { get; set; }
        [XmlAttribute]
        public double Height { get; set; }
        [XmlAttribute]
        public double Top { get; set; }
        [XmlAttribute]
        public double Left { get; set; }
        [XmlAttribute]
        public bool Maximized { get; set; }
        [XmlAttribute]
        public double TestPanelWidth { get; set; }
    }
}
