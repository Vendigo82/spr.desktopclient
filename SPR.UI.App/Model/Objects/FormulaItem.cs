﻿using SPR.DAL.Enums;
using SPR.UI.App.DataContract;
using System;

namespace SPR.UI.App.Model.Objects
{
    [Obsolete]
    public class FormulaItem : IFormulaItem
    {
        public long Id { get; set; }

        public Guid Guid { get; set; }

        public FormulaType Type { get; set; }

        public string Name { get; set; }
    }
}
