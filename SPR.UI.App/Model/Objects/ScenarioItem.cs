﻿using SPR.UI.App.DataContract;
using System;

namespace SPR.UI.App.Model.Objects
{
    [Obsolete]
    public class ScenarioItem : IScenarioItem
    {
        public long Id { get; set; }

        public Guid Guid { get; set; }

        public string Name { get; set; }
    }
}
