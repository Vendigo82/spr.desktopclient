﻿using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.App.Services.WebService;
using SPR.UI.WebService.DataContract.Configuration;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Model.Main
{
    public class MainWndModel : IMainWndModel
    {
        private readonly ITypeResolver typeResolver;

        private readonly IInstancesSource instancesSource;
        private readonly IWebServiceConfigWritter webServiceSettings;
        private readonly ISettingsWritter _settings;

        public MainWndModel(
            ITypeResolver typeResolver,
            IAppVersionProvider version,
            IInstancesSource instancesSource,
            ISettingsWritter settings,
            IWebServiceConfigWritter webServiceSettings) 
        {
            this.typeResolver = typeResolver;
            AppVersion = version;
            this.instancesSource = instancesSource;
            _settings = settings;
            this.webServiceSettings = webServiceSettings;
        }

        public IEnumerable<InstanceSettingsModel> GetInstances() => instancesSource.GetInstances();

        public void SetInstance(InstanceSettingsModel instance)
        {
            webServiceSettings.SetValues(instance.BaseUrl, instance.Timeout);
        }

        public async Task InitializeAsync(CancellationToken ct) 
        {
            var ds = typeResolver.Resolve<IConfigurationSource>();
            InstanceResponse resp = await ds.GetInstanceAsync(CancellationToken.None);
            InstanceName = resp.Name;
            _settings.SetTestModuleUrl(GetAbsoluteTestServiceUrl(resp.TestServiceUrl));
            _settings.SetProfile(resp.UsingProfileWebsite, resp.ProfileUrl);

            if (resp.DocumentationSettings != null)
            {
                IsDocumentationEnabled = true;
                _settings.SetDocumentationService(new DocumentationServiceSettings(
                    resp.DocumentationSettings.BaseUrl
                    ));
            }
        }

        public IAppVersionProvider AppVersion { get; }

        public string InstanceName { get; private set; }

        public string ServerUrl => string.IsNullOrEmpty(webServiceSettings.BaseUrl) ? string.Empty : new Uri(webServiceSettings.BaseUrl).Host;

        public bool IsDocumentationEnabled { get; private set; }

        private string GetAbsoluteTestServiceUrl(string settingsUrl)
        {
            if (string.IsNullOrEmpty(settingsUrl))
                return settingsUrl;

            try
            {
                var uri = new Uri(settingsUrl);
                if (uri.IsAbsoluteUri)
                    return settingsUrl;
                else
                    return CombineUrl(settingsUrl);
            } 
            catch
            {
                return CombineUrl(settingsUrl);
            }            
        }

        private string CombineUrl(string settingsUrl) => webServiceSettings.BaseUrl.TrimEnd('/') + "/" + settingsUrl.TrimStart('/').TrimEnd('/');
    }
}
