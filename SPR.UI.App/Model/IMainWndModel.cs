﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;

namespace SPR.UI.App.Model
{
    public struct CheckAppVersionResult
    {
        public CheckAppVersionResult(bool result, int minVersion, int maxVersion) {
            Result = result;
            MinVersion = minVersion;
            MaxVersion = maxVersion;
        }

        public bool Result { get; }

        public int MinVersion { get; }

        public int MaxVersion { get; }
    }

    /// <summary>
    /// Model for main window
    /// </summary>
    public interface IMainWndModel
    {        
        /// <summary>
        /// Check app version compability
        /// </summary>
        /// <returns>true if app version is supported</returns>
        Task InitializeAsync(CancellationToken ct);

        IEnumerable<InstanceSettingsModel> GetInstances();
        void SetInstance(InstanceSettingsModel instance);

        /// <summary>
        /// Returns application version provider
        /// </summary>
        IAppVersionProvider AppVersion { get; }

        /// <summary>
        /// Returns server instance name. Available after execute CheckAppVersion
        /// </summary>
        string InstanceName { get; }

        /// <summary>
        /// Server Url
        /// </summary>
        string ServerUrl { get; }

        bool IsDocumentationEnabled { get; }
    }
}
