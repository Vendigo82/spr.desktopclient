﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Model.Dir
{
    /// <summary>
    /// Database system special object type
    /// </summary>
    public class DatabaseSystemModel : KeyDescriptionModel
    {
        public DatabaseSystemModel(KeyDescriptionModel model)
        {
            Key = model.Key;
            Descr = model.Descr;
        }
    }
}
