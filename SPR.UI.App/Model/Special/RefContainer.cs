﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Model.Special
{
    /// <summary>
    /// Contains object of type <typeparamref name="T"/> and call <see cref="_itemChanged"/> when reference was changed
    /// </summary>
    /// <typeparam name="T">holding object type</typeparam>
    public struct RefContainer<T> where T : class
    {
        /// <summary>
        /// Call when reference to item was changed
        /// </summary>
        private Action<T> _itemChanged;

        /// <summary>
        /// Holding reference
        /// </summary>
        private T _value;

        /// <summary>
        /// Initialize holder with item
        /// </summary>
        /// <param name="item">holding item. Can be null</param>
        public RefContainer(Action<T> onChanged)
        {
            _value = null;
            _itemChanged = onChanged;
        }

        /// <summary>
        /// Initialize holder with item
        /// </summary>
        /// <param name="item">holding item. Can be null</param>
        public RefContainer(T item, Action<T> onChanged)
        {
            _value = item;
            _itemChanged = onChanged;
        }

        /// <summary>
        /// Holding item. Call <see cref="_itemChanged"/> when asign reference to another item 
        /// </summary>
        public T Value { 
            get => _value;
            set {
                if (!ReferenceEquals(Value, value)) {
                    _value = value;
                    _itemChanged?.Invoke(_value);
                }
            }
        }

        /// <summary>
        /// Converts item holder to item
        /// </summary>
        /// <param name="h"></param>
        public static implicit operator T(RefContainer<T> h) => h.Value;        
    }
}
