﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Scenarios
{
    /// <summary>
    /// Interaction logic for ScenarioList.xaml
    /// </summary>
    public partial class ScenarioList : UserControl
    {
        public ScenarioList()
        {
            SlimMode = false;
            InitializeComponent();
            Grid.SelectionChanged += Grid_SelectionChanged;
        }        

        #region ItemsSource
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            nameof(ItemsSource), typeof(IEnumerable), typeof(ScenarioList),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(ItemsSourceChanged)));

        public IEnumerable ItemsSource {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        private static void ItemsSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            ScenarioList obj = (ScenarioList)o;
            obj.Grid.ItemsSource = args.NewValue as IEnumerable;
        }
        #endregion

        #region SelectedItem
        public static DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            nameof(SelectedItem), typeof(object), typeof(ScenarioList),
            new FrameworkPropertyMetadata(null,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                new PropertyChangedCallback(SelectedItemChanged)));

        public object SelectedItem {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        private static void SelectedItemChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            ScenarioList obj = (ScenarioList)o;
            obj.Grid.SelectedItem = args.NewValue;
        }

        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedItem = Grid.SelectedItem;
        }
        #endregion

        #region ActionCommand
        public static readonly DependencyProperty ActionCommandProperty = DependencyProperty.Register(
            nameof(ActionCommand), typeof(ICommand), typeof(ScenarioList), new UIPropertyMetadata(null));

        public ICommand ActionCommand {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ActionCommand != null && Grid.SelectedItem != null && ActionCommand.CanExecute(Grid.SelectedItem))
                ActionCommand.Execute(Grid.SelectedItem);
        }
        #endregion

        #region SlimMode
        public static DependencyProperty SlimModeProperty = DependencyProperty.Register(
            nameof(SlimMode), typeof(bool), typeof(ScenarioList),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(SlimModeChanged)));

        public bool SlimMode {
            get => (bool)GetValue(SlimModeProperty);
            set => SetValue(SlimModeProperty, value);
        }

        private static void SlimModeChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            ScenarioList obj = (ScenarioList)o;
            var vis = ((bool)args.NewValue) ? Visibility.Collapsed : Visibility.Visible;
            obj.colQuota.Visibility = vis;
            obj.colHasFilter.Visibility = vis;
            obj.colPriority.Visibility = vis;
            obj.colStepReject.Visibility = vis;
            obj.colStepSuccess.Visibility = vis;
        }
        #endregion

        #region AutoWidth
        public static DependencyProperty AutoWithProperty = DependencyProperty.Register(
            nameof(AutoWidth), typeof(bool), typeof(ScenarioList),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(AutoWidthChanged)));

        public bool AutoWidth {
            get => (bool)GetValue(AutoWithProperty);
            set => SetValue(AutoWithProperty, value);
        }

        private static void AutoWidthChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            ScenarioList obj = (ScenarioList)o;
            var value = (bool)args.NewValue;
            obj.colName.Width = ColumnWidth(80);
            obj.colStep.Width = ColumnWidth(20);
            obj.colStepSuccess.Width = ColumnWidth(20);
            obj.colStepReject.Width = ColumnWidth(20);

            DataGridLength ColumnWidth(double weight)
                => value ? DataGridLength.Auto : new DataGridLength(weight, DataGridLengthUnitType.Star);
        }
        #endregion
    }
}
