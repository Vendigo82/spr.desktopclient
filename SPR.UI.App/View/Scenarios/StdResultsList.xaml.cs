﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Scenarios
{
    /// <summary>
    /// Interaction logic for StdResultsList.xaml
    /// </summary>
    public partial class StdResultsList : UserControl
    {
        public StdResultsList()
        {            
            InitializeComponent();            
        }

        #region ItemsSource
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            nameof(ItemsSource), typeof(IEnumerable), typeof(StdResultsList),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(ItemsSourceChanged)));

        public IEnumerable ItemsSource {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        private static void ItemsSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var obj = (StdResultsList)o;
            obj.Grid.ItemsSource = args.NewValue as IEnumerable;
        }
        #endregion

        #region SelectedItem
        public static DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            nameof(SelectedItem), typeof(object), typeof(StdResultsList),
            new FrameworkPropertyMetadata(null,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                new PropertyChangedCallback(SelectedItemChanged)));

        public object SelectedItem {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        private static void SelectedItemChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var obj = (StdResultsList)o;
            obj.Grid.SelectedItem = args.NewValue;
        }

        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedItem = Grid.SelectedItem;
        }
        #endregion

        #region ActionCommand
        public static readonly DependencyProperty ActionCommandProperty = DependencyProperty.Register(
            nameof(ActionCommand), typeof(ICommand), typeof(StdResultsList), new UIPropertyMetadata(null));

        public ICommand ActionCommand {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ActionCommand != null && ActionCommand.CanExecute(Grid.SelectedItem))
                ActionCommand.Execute(Grid.SelectedItem);
        }
        #endregion

    }
}
