﻿using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Scenarios
{
    /// <summary>
    /// Interaction logic for ScenarioMainTabsView.xaml
    /// </summary>
    public partial class ScenarioMainTabsView : UserControl
    {
        public ScenarioMainTabsView(DataObjectTabsVM<ScenarioModel> vm)
        {
            DataContext = vm;
            InitializeComponent();
            Tabs.SelectionChanged += Tabs_SelectionChanged;
            MainTab.DataContext = vm.MainVM;
            CheckupsTab.DataContext = vm.MainVM;
            TestTab.DataContext = vm.TestVM;
            JournalTab.DataContext = vm.JournalVM;
            //UsageTab.DataContext = vm.UsageVM;
        }

        private void Tabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var vm = (DataObjectTabsVM<ScenarioModel>)DataContext;

            if (Tabs.SelectedItem == MainTab || Tabs.SelectedItem == CheckupsTab)
                vm.SelectedTab = DataObjectTabsVM<ScenarioModel>.TabKind.Main;
            else if (Tabs.SelectedItem == TestTab)
                vm.SelectedTab = DataObjectTabsVM<ScenarioModel>.TabKind.Test;
            else if (Tabs.SelectedItem == JournalTab)
                vm.SelectedTab = DataObjectTabsVM<ScenarioModel>.TabKind.Journal;
            //else if (Tabs.SelectedItem == UsageTab)
            //    vm.SelectedTab = DataObjectTabsVM<ScenarioModel>.TabKind.Usage;
        }
    }
}
