﻿using System;

namespace SPR.UI.App.View
{
    /// <summary>
    /// Tells to tabs system that's view contains busy indecator and busy indicator should not be provide by outside
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ViewBusyIndicatorAttribute : Attribute
    {
    }
}
