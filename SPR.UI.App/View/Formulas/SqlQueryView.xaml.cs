﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Formulas
{
    /// <summary>
    /// Interaction logic for SqlQueryView.xaml
    /// </summary>
    public partial class SqlQueryView : UserControl
    {
        public SqlQueryView()
        {
            InitializeComponent();
        }
    }
}
