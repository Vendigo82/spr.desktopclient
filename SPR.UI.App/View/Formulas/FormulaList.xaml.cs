﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SPR.UI.App.DataContract;

namespace SPR.UI.App.View.Formulas
{
    /// <summary>
    /// Interaction logic for FormulaList.xaml
    /// </summary>
    public partial class FormulaList : UserControl
    {
        public FormulaList() {
            InitializeComponent();
            Grid.SelectionChanged += Grid_SelectionChanged;
        }

        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            SelectedItem = Grid.SelectedItem;
            if (Grid.SelectedItem != null) {
                Grid.UpdateLayout();
                Grid.ScrollIntoView(Grid.SelectedItem);
            }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            if (Grid.SelectedItem != null) {
                if (ActionCommand != null && ActionCommand.CanExecute(Grid.SelectedItem))
                    ActionCommand.Execute(Grid.SelectedItem);
            }
        }

        #region ItemsSource
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            nameof(ItemsSource), typeof(IEnumerable), typeof(FormulaList),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(ItemsSourceChanged)));

        public IEnumerable ItemsSource {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        private static void ItemsSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            FormulaList obj = (FormulaList)o;
            obj.Grid.ItemsSource = args.NewValue as IEnumerable;
        }
        #endregion

        #region SelectedItem
        public static DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            nameof(SelectedItem), typeof(object), typeof(FormulaList),
            new FrameworkPropertyMetadata(null, 
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                new PropertyChangedCallback(SelectedItemChanged)));

        public object SelectedItem {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        private static void SelectedItemChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            FormulaList obj = (FormulaList)o;
            obj.Grid.SelectedItem = args.NewValue;
        }
        #endregion

        #region SelectedValue
        public static DependencyProperty SelectedValueProperty = DependencyProperty.Register(
            nameof(SelectedValue), typeof(object), typeof(FormulaList),
            new FrameworkPropertyMetadata(null,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                new PropertyChangedCallback(SelectedValueChanged)));

        public object SelectedValue {
            get => GetValue(SelectedValueProperty);
            set => SetValue(SelectedValueProperty, value);
        }

        private static void SelectedValueChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            FormulaList obj = (FormulaList)o;
            obj.Grid.SelectedValue = args.NewValue;
        }
        #endregion

        #region SelectedValuePath
        public static DependencyProperty SelectedValuePathProperty = DependencyProperty.Register(
            nameof(SelectedValuePath), typeof(object), typeof(FormulaList),
            new FrameworkPropertyMetadata(null,
                new PropertyChangedCallback(SelectedValuePathChanged)));

        public object SelectedValuePath {
            get => GetValue(SelectedValuePathProperty);
            set => SetValue(SelectedValuePathProperty, value);
        }

        private static void SelectedValuePathChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            FormulaList obj = (FormulaList)o;
            obj.Grid.SelectedValuePath = (string)args.NewValue;
        }
        #endregion

        #region ActionCommand
        public static readonly DependencyProperty ActionCommandProperty = DependencyProperty.Register(
            nameof(ActionCommand), typeof(ICommand), typeof(FormulaList), new UIPropertyMetadata(null));

        public ICommand ActionCommand {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }
        #endregion

        #region AutoWidth
        public static DependencyProperty AutoWithProperty = DependencyProperty.Register(
            nameof(AutoWidth), typeof(bool), typeof(FormulaList),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(AutoWidthChanged)));

        public bool AutoWidth {
            get => (bool)GetValue(AutoWithProperty);
            set => SetValue(AutoWithProperty, value);
        }

        private static void AutoWidthChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            FormulaList obj = (FormulaList)o;
            var value = (bool)args.NewValue;
            obj.colName.Width = ColumnWidth(80);

            DataGridLength ColumnWidth(double weight)
                => value ? DataGridLength.Auto : new DataGridLength(weight, DataGridLengthUnitType.Star);
        }
        #endregion
    }
}
