﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Formulas
{
    using System.Windows.Markup;
    using VM.Formulas;

    /// <summary>
    /// Interaction logic for FormulaBodyView.xaml
    /// </summary>
    [ContentProperty(nameof(MainContent))]
    public partial class FormulaBodyView : UserControl
    {
        public FormulaBodyView() {
            InitializeComponent();
            DataContextChanged += FormulaBodyView_DataContextChanged;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e) {
            base.OnPropertyChanged(e);
            if (e.Property.Name == nameof(DataContext) && e.NewValue != null) {
                if (DataContext.TryGetPropertyValue<bool>(nameof(FormulaVM.IsExistsMain), out bool value) && value == false)
                    DescriptionExpander.IsExpanded = true;
                else
                    DescriptionExpander.IsExpanded = false;
            }
        }

        private void DescriptionExpander_Expanded(object sender, RoutedEventArgs e) {
            HeaderContent.Visibility = DescriptionExpander.IsExpanded ? Visibility.Hidden : Visibility.Visible;
        }

        private void FormulaBodyView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (FormulaPartialContentPresenter.Content is Control ctrl)
                ctrl.DataContext = e.NewValue;
        }

        #region Main content
        public static DependencyProperty MainContentProperty = DependencyProperty.Register(
            nameof(MainContent), typeof(UIElement), typeof(FormulaBodyView),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(MainContentChanged)));

        public UIElement MainContent {
            get => (UIElement)GetValue(MainContentProperty);
            set => SetValue(MainContentProperty, value);
        }

        private static void MainContentChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var obj = (FormulaBodyView)o;
            obj.FormulaPartialContentPresenter.Content = args.NewValue as UIElement;
            if (args.NewValue is Control ui)
                ui.DataContext = obj.DataContext;
        }
        #endregion

    }
}
