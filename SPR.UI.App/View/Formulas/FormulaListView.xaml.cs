﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SPR.UI.App.VM.Formulas;

namespace SPR.UI.App.View.Formulas
{
    /// <summary>
    /// Interaction logic for FormulaListView.xaml
    /// </summary>
    [ViewBusyIndicator]
    public partial class FormulaListView : UserControl
    {
        public FormulaListView(FormulaListVM vm) {
            InitializeComponent();
            DataContext = vm;
        }

        //public ICommand ActionCommand { 
        //    get => FormulaList.ActionCommand; 
        //    set => FormulaList.ActionCommand = value; }

        #region ActionCommand
        public static readonly DependencyProperty ActionCommandProperty = DependencyProperty.Register(
            nameof(ActionCommand), typeof(ICommand), typeof(FormulaListView), new UIPropertyMetadata(null));

        public ICommand ActionCommand {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }
        #endregion

    }
}
