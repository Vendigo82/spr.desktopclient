﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Formulas
{
    using SPR.UI.App.VM.Base;
    using SPR.UI.WebService.DataContract.Formula;
    using VM.Formulas;

    /// <summary>
    /// Interaction logic for FormulaMainView.xaml
    /// </summary>
    public partial class FormulaMainView : UserControl
    {
        public FormulaMainView(DataObjectTabsVM<FormulaModel> vm, Control partialView) 
        {
            DataContext = vm;
            InitializeComponent();
            Tabs.SelectionChanged += Tabs_SelectionChanged;
            MainTab.DataContext = vm.MainVM;
            TestTab.DataContext = vm.TestVM;
            JournalTab.DataContext = vm.JournalVM;
            UsageTab.DataContext = vm.UsageVM;

            BodyView.FormulaPartialContentPresenter.Content = partialView;            
        }

        private void Tabs_SelectionChanged(object sender, SelectionChangedEventArgs e) 
        {
            var vm = (DataObjectTabsVM<FormulaModel>)DataContext;

            if (Tabs.SelectedItem == MainTab)
                vm.SelectedTab = DataObjectTabsVM<FormulaModel>.TabKind.Main;
            else if (Tabs.SelectedItem == TestTab)
                vm.SelectedTab = DataObjectTabsVM<FormulaModel>.TabKind.Test;
            else if (Tabs.SelectedItem == JournalTab)
                vm.SelectedTab = DataObjectTabsVM<FormulaModel>.TabKind.Journal;
            else if (Tabs.SelectedItem == UsageTab)
                vm.SelectedTab = DataObjectTabsVM<FormulaModel>.TabKind.Usage;
        }
    }
}
