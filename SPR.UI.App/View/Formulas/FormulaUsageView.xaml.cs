﻿using System.Windows.Controls;

namespace SPR.UI.App.View.Formulas
{
    /// <summary>
    /// Interaction logic for FormulaUsageView.xaml
    /// </summary>
    public partial class FormulaUsageView : UserControl
    {
        public FormulaUsageView()
        {
            InitializeComponent();
        }
    }    
}
