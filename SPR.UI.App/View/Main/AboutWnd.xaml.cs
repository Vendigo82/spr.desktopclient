﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using SPR.UI.App.VM.Main;

namespace SPR.UI.App.View.Main
{
    /// <summary>
    /// Interaction logic for AboutWnd.xaml
    /// </summary>
    public partial class AboutWnd : Window
    {
        public AboutWnd(AboutVM vm) {
            DataContext = vm;
            InitializeComponent();
        }
    }
}
