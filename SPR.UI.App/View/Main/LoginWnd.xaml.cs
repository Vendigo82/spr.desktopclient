﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using SPR.UI.App.VM.Main;

namespace SPR.UI.App.View.Main
{
    /// <summary>
    /// Interaction logic for LoginWnd.xaml
    /// </summary>
    public partial class LoginWnd : Window
    {
        public LoginWnd(LoginVM vm) {
            DataContext = vm;
            InitializeComponent();
            UsernameBox.Focus();
        }

        public LoginVM LoginVM => (LoginVM)DataContext;
    }
}
