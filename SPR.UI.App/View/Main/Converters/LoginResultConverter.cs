﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

using SPR.UI.App.Services;
using SPR.UI.App.VM.Main;

namespace SPR.UI.App.View.Main.Converters
{
    /// <summary>
    /// Get title for values of <see cref="LoginVM.LoginResult"/>
    /// </summary>
    public class LoginResultConverter : IValueConverter
    {
        private const string PREFIX = "AuthWnd.LoginResult.";

        private readonly IResourceStringProvider _stringProvider;

        public LoginResultConverter() {
            _stringProvider = (Application.Current as App).TypeResolver.Resolve<IResourceStringProvider>();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is LoginVM.LoginResult lr) {
                if (lr == LoginVM.LoginResult.Error || lr == LoginVM.LoginResult.Failed)
                    return _stringProvider.GetString(string.Concat(PREFIX, lr.ToString())) ?? lr.ToString();
                else
                    return string.Empty;
            } else
                throw new InvalidCastException($"Can't convert {value.GetType().Name} to {typeof(LoginVM.LoginResult)}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
