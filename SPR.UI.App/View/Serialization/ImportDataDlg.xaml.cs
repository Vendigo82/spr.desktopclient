﻿using SPR.UI.App.Abstractions.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Serialization
{
    /// <summary>
    /// Interaction logic for ImportDataDlg.xaml
    /// </summary>
    public partial class ImportDataDlg : Window
    {
        private readonly IWindowFactory wndFactory;

        public ImportDataDlg(IWindowFactory wndFactory)
        {
            this.wndFactory = wndFactory;
            InitializeComponent();
            
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (DialogResult == true)
                wndFactory.ShowInformationDialog("Data imported successfully");
            base.OnClosing(e);
        }

    }
}
