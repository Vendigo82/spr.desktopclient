﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Serialization
{
    /// <summary>
    /// Interaction logic for ExportDlg.xaml
    /// </summary>
    public partial class ExportDlg : Window
    {
        private const string DataContentPropertyName = "DataContent";

        public ExportDlg()
        {
            InitializeComponent();

            DataContextChanged += ExportDlg_DataContextChanged;            
        }

        private void ExportDlg_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is INotifyPropertyChanged p)
                p.PropertyChanged += ContextPropertyChanged;
        }

        private void ContextPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == DataContentPropertyName) {
                SaveContent();
            }
        }

        private void SaveContent()
        {
            var content = DataContext.GetPropertyValue<string>(DataContentPropertyName);

            SaveFileDialog dlg = new SaveFileDialog() {
                FileName = DataContext.GetPropertyValue<string>("FileName"),
                Filter = "Json files (.json)|*.json",
                DefaultExt = ".json",
                CheckPathExists = true,
                //Title = "Сохранение"
            };
            if (dlg.ShowDialog() == true) {
                File.WriteAllText(dlg.FileName, content, Encoding.UTF8);
                DialogResult = true;
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext.TryGetPropertyValue<ICommand>("CancelCmd", out var cmd) && cmd != null)
                cmd.Execute(null);
            DialogResult = false;
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            SaveContent();
        }
    }
}
