﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Journal
{
    /// <summary>
    /// Interaction logic for JournalGridView.xaml
    /// </summary>
    public partial class JournalGridView : UserControl
    {
        public JournalGridView()
        {
            InitializeComponent();
        }

        #region ShowObjectColumns property
        public static DependencyProperty ShowObjectColumnsProperty = DependencyProperty.Register(
            nameof(ShowObjectColumns), typeof(bool), typeof(JournalGridView),
            new FrameworkPropertyMetadata(true, new PropertyChangedCallback(ShowObjectColumnsChanged))
            );

        public bool ShowObjectColumns {
            get => (bool)GetValue(ShowObjectColumnsProperty);
            set => SetValue(ShowObjectColumnsProperty, value);
        }

        private static void ShowObjectColumnsChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var obj = (JournalGridView)o;
            var value = (bool)args.NewValue;
            var vis = value ? Visibility.Visible : Visibility.Collapsed;
            obj.ColumnGuid.Visibility = vis;
            obj.ColumnObjectType.Visibility = vis;
            obj.ColumnObjectCode.Visibility = vis;
            obj.ColumnObjectName.Visibility = vis;
        }
        #endregion

        #region ShowCommentColumn
        public static DependencyProperty ShowCommentColumnProperty = DependencyProperty.Register(
            nameof(ShowCommentColumn), typeof(bool), typeof(JournalGridView),
            new FrameworkPropertyMetadata(true, new PropertyChangedCallback(ShowCommentColumnChanged))
            );

        public bool ShowCommentColumn {
            get => (bool)GetValue(ShowCommentColumnProperty);
            set => SetValue(ShowCommentColumnProperty, value);
        }

        private static void ShowCommentColumnChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var obj = (JournalGridView)o;
            var value = (bool)args.NewValue;
            obj.ColumnComment.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion
    }
}
