﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Journal
{
    /// <summary>
    /// Interaction logic for ConfirmActionWnd.xaml
    /// </summary>
    public partial class ConfirmActionWnd : Window
    {
        public ConfirmActionWnd() {
            InitializeComponent();
        }

        private void BtnAccept_Click(object sender, RoutedEventArgs e) {
            DialogResult = true;
        }
    }
}
