﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;

namespace SPR.UI.App.View {
    using Abstractions.Tests;
    using Services.Events;
    using Tools;
    using VM.Main;
    using View.Tests;
    using SPR.WebService.TestModule.DataContract;
    using SPR.UI.App.DataContract;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable {
        private double _rightPanelOriginWidth;
        private double _rightPanelCollapsedWidth;
        private bool _isTestPanelVisible;

        public MainWindow(MainVM dataContext, TestResultsView testResultsView, INotificationEventsService events) {            
            DataContext = dataContext;
            InitializeComponent();

            this.Loaded += MainWindow_Loaded;

            //initialize test result panel
            TestResultsContainer.Content = testResultsView;
            SetRightPanelVisibilityStatus(false, setWidth: false);

            try {
                if (File.Exists("windowsettings.xml")) {
                    XmlSerializer xml = new XmlSerializer(typeof(WindowSettings));

                    using (FileStream file = new FileStream("windowsettings.xml", FileMode.Open)) {
                        WindowSettings ws = (WindowSettings)xml.Deserialize(file);
                        file.Close();

                        Width = ws.Width;
                        Height = ws.Height;
                        Left = ws.Left;
                        Top = ws.Top;
                        if (ws.Maximized)
                            WindowState = WindowState.Maximized;
                        _rightPanelOriginWidth = ws.TestPanelWidth;
                    }
                }
            } catch { 
            }

            events.TestComplete += (s, e) => {
                if (!_isTestPanelVisible)
                    SetRightPanelVisibilityStatus(true);
            };
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e) {
            _rightPanelOriginWidth = Math.Min(ActualWidth - MainGrid.ColumnDefinitions[0].MinWidth, 
                Math.Max(_rightPanelOriginWidth, Math.Min(250, ActualWidth * 0.3)));
            _rightPanelCollapsedWidth = TestPanelExpander.DesiredSize.Width; //OpenRightPanelBtn.ActualWidth
            //    + (OpenRightPanelBtn.PointToScreen(new Point(0, 0)).X - RightPanel.PointToScreen(new Point(0, 0)).X) * 2;
            MainGrid.ColumnDefinitions[2].MinWidth = _rightPanelCollapsedWidth;
            MainGrid.ColumnDefinitions[2].Width = new GridLength(_rightPanelCollapsedWidth);
        }

        public void Dispose() { 
            DataContext.DisposeIf();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            if (TestPanelExpander.IsExpanded)
                _rightPanelOriginWidth = MainGrid.ColumnDefinitions[2].Width.Value;
        }

        private void Window_Closed(object sender, EventArgs e) {
            Dispose();
            try {
                WindowSettings ws = new WindowSettings() {
                    Height = this.Height,
                    Width = this.Width,
                    Left = this.Left,
                    Top = this.Top,
                    Maximized = this.WindowState == WindowState.Maximized,
                    TestPanelWidth = _rightPanelOriginWidth
                };
                XmlSerializer xml = new XmlSerializer(typeof(WindowSettings));
                using (FileStream file = new FileStream("windowsettings.xml", FileMode.Create)) {
                    xml.Serialize(file, ws);
                    file.Close();
                }
            } catch { }
        }        

        private void CloseTabExecuted(object sender, ExecutedRoutedEventArgs e) {
            if ((e.Parameter as ITabItemVM).DataContext != null) {
                PropertyInfo prop = (e.Parameter as ITabItemVM).DataContext.GetType().GetProperty("IsChanged");
                if (prop != null && (bool)prop.GetValue((e.Parameter as ITabItemVM).DataContext)) {
                    if (MessageBox.Show(this, "Данные не сохранены, всё равно закрыть?", "Закрытие", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                        return;
                }
            }
            (TabContainer.ItemsSource as IList).Remove(e.Parameter);
        }

        #region Test result panel functionality
        /// <summary>
        /// Expand or collapse test results panel
        /// </summary>
        /// <param name="open"></param>
        private void SetRightPanelVisibilityStatus(bool open, bool setWidth = true) {
            TestResultsContainer.Visibility = open ? Visibility.Visible : Visibility.Collapsed;
            TestPanelExpander.IsExpanded = open;
            if (!open)
                _rightPanelOriginWidth = MainGrid.ColumnDefinitions[2].Width.Value;
            if (setWidth)
                MainGrid.ColumnDefinitions[2].Width = new GridLength(open ? _rightPanelOriginWidth : _rightPanelCollapsedWidth);
            Splitter.Visibility = open ? Visibility.Visible : Visibility.Hidden;
            _isTestPanelVisible = open;
        }

        /// <summary>
        /// Implementation of <see cref="ITestResultsAcceptor"/>
        /// </summary>
        /// <param name="formulaId"></param>
        /// <param name="response"></param>
        public void PushResult(long _, ShownObjectEnum __, TestFormulaResponse response) {
            if (!_isTestPanelVisible)
                SetRightPanelVisibilityStatus(true);
        }

        /// <summary>
        /// Called when test panel expanded or collapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TestPanelExpander_Collapsed(object sender, RoutedEventArgs e) {
            SetRightPanelVisibilityStatus(TestPanelExpander.IsExpanded);
        }
        #endregion

        #region RegisterCommandBindings
        public static DependencyProperty RegisterCommandBindingsProperty = 
            DependencyProperty.RegisterAttached(
                "RegisterCommandBindings", 
                typeof(CommandBindingCollection), 
                typeof(MainWindow), 
                new PropertyMetadata(null, OnRegisterCommandBindingChanged));

        public static void SetRegisterCommandBindings(UIElement element, CommandBindingCollection value) {
            if (element != null)
                element.SetValue(RegisterCommandBindingsProperty, value);
        }
        public static CommandBindingCollection GetRegisterCommandBindings(UIElement element) {
            return (element != null ? (CommandBindingCollection)element.GetValue(RegisterCommandBindingsProperty) : null);
        }
        private static void OnRegisterCommandBindingChanged (DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            UIElement element = sender as UIElement;
            if (element == null) return;

            var bindings = (e.NewValue as CommandBindingCollection);
            if (bindings != null) {
                // clear the collection first
                //element.CommandBindings.Clear();
                element.CommandBindings.AddRange(bindings);
            }

        }
        #endregion
    }
}
