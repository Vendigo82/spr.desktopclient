﻿using SPR.UI.WebService.DataContract.Configuration;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SPR.UI.App.View.Monitoring
{
    /// <summary>
    /// Interaction logic for TransactionsFilterView.xaml
    /// </summary>
    public partial class TransactionsFilterView : UserControl
    {
        public TransactionsFilterView()
        {
            InitializeComponent();
        }

        private readonly ICollection<TextBox> additionalFilters = new List<TextBox>();

        public TextBox AddColumn(TransactionColumnModel columnModel)
        {
            var panel = new StackPanel() { Orientation = Orientation.Horizontal };
            panel.Children.Add(new Label() { Content = columnModel.Title, VerticalAlignment = VerticalAlignment.Center });

            var textBox = new TextBox() { Width = 120, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(0, 0, 20, 0), Tag = columnModel.DbName };
            panel.Children.Add(textBox);
            additionalFilters.Add(textBox);

            FilterPanel.Children.Insert(FilterPanel.Children.Count - 1, panel);

            return textBox;
        }

        private void ButtonFilterApply_Click(object sender, RoutedEventArgs e)
        {
            var values = new List<KeyValuePair<string, string>>();
            foreach (var edit in additionalFilters)
            {
                if (!string.IsNullOrEmpty(edit.Text))
                    values.Add(new KeyValuePair<string, string>((string)edit.Tag, edit.Text));
            }

            if (DataContext.TryGetPropertyValue<ICommand>("ApplyFilterCmd", out var command))
                command.Execute(values);
        }

        private void ButtonFilterClear_Click(object sender, RoutedEventArgs e)
        {
            foreach (var edit in additionalFilters)
                edit.Text = string.Empty;

            if (DataContext.TryGetPropertyValue<ICommand>("ResetFilterCmd", out var command))
                command.Execute(null);
        }
    }
}
