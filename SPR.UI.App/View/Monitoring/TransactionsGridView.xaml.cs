﻿using SPR.UI.App.Converters.Monitoring;
using SPR.UI.App.VM.Monitoring;
using SPR.UI.WebService.DataContract.Configuration;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;

namespace SPR.UI.App.View.Monitoring
{
    /// <summary>
    /// Interaction logic for TransactionsGridView.xaml
    /// </summary>
    public partial class TransactionsGridView : UserControl
    {
        public TransactionsGridView()
        {
            InitializeComponent();
        }

        public DataGridColumn AddColumn(TransactionColumnModel columnModel)
        {
            var column = new DataGridTextColumn
            {
                Header = columnModel.Title,
                Binding = new Binding()
                {
                    Converter = new TransactionAdditionalDataConverter(),
                    ConverterParameter = columnModel.DbName,
                },
                Width = DataGridLength.Auto,
            };

            if (columnModel.Position != null)
                TransGrid.Columns.Insert(columnModel.Position.Value, column);
            else
                TransGrid.Columns.Add(column);

            return column;
        }

        private void TransGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            var context = DataContext as TransactionsVM;
            if (context != null && TransGrid.SelectedCells.Any())
                context.SelectedItem = TransGrid.SelectedCells.FirstOrDefault().Item as TransactionModel;
        }
    }
}
