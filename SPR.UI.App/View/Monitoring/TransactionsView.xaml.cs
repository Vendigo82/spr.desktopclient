﻿using SPR.UI.WebService.DataContract.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SPR.UI.App.View.Monitoring
{
    /// <summary>
    /// Interaction logic for TransactionsView.xaml
    /// </summary>
    public partial class TransactionsView : UserControl
    {
        private readonly Dictionary<DataGridColumn, TextBox> gridColumnToSearchFieldsMapping = new Dictionary<DataGridColumn, TextBox>();

        public TransactionsView()
        {
            InitializeComponent();
            DataContextChanged += TransactionsView_DataContextChanged;
            SizeChanged += TransactionsView_SizeChanged;
            Grid.TransGrid.SelectedCellsChanged += TransGrid_SelectedCellsChanged;
            FilterPanel.SelectedCellToFilterBtn.Click += SelectedCellToFilterBtn_Click;

            AddFilterColumnsToMapping();
            TransGrid_SelectedCellsChanged(null, null);
        }

        private void AddFilterColumnsToMapping()
        {
            gridColumnToSearchFieldsMapping.Add(Grid.StepColumn, FilterPanel.FilterStepTextBox);
            gridColumnToSearchFieldsMapping.Add(Grid.IpAddressColumn, FilterPanel.FilterIpAddressTextBox);
            gridColumnToSearchFieldsMapping.Add(Grid.ScenarioIdColumn, FilterPanel.FilterScenarioIdTextBox);
            gridColumnToSearchFieldsMapping.Add(Grid.NextStepColumn, FilterPanel.FilterNextStepTextBox);
        }

        private void SelectedCellToFilterBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectedCell = Grid.TransGrid.SelectedCells.FirstOrDefault();
            if (selectedCell == default)
                return;

            if (!gridColumnToSearchFieldsMapping.TryGetValue(selectedCell.Column, out var filterTextBox))
                return;

            if (!selectedCell.Column.GetCellContent(selectedCell.Item).TryGetPropertyValue<string>("Text", out var cellValue))
                return;

            filterTextBox.Text = cellValue;           
        }

        private void TransGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            FilterPanel.SelectedCellToFilterBtn.IsEnabled = IsSelectedCellToFilterEnabled();
        }

        private bool IsSelectedCellToFilterEnabled()
        {
            var selectedCell = Grid.TransGrid.SelectedCells.FirstOrDefault();
            if (selectedCell == default)
                return false;

            return gridColumnToSearchFieldsMapping.TryGetValue(selectedCell.Column, out var _);
        }

        private void TransactionsView_SizeChanged(object sender, SizeChangedEventArgs e)
        {                        
            ErrorsGrid.MaxHeight = Grid.ActualHeight * 0.5;
        }

        private void TransactionsView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null && e.OldValue is INotifyPropertyChanged npcOld)
                npcOld.PropertyChanged -= DataContext_PropertyChanged;
            if (e.NewValue is INotifyPropertyChanged npc)
                npc.PropertyChanged += DataContext_PropertyChanged;
        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            const string PropertyName = "AdditionalColums";

            if (e.PropertyName != PropertyName)
                return;

            if (DataContext.TryGetPropertyValue<IEnumerable<TransactionColumnModel>>(PropertyName, out var columnModelsList))
            {
                foreach (var columnModel in columnModelsList)
                {
                    var gridColumn = Grid.AddColumn(columnModel);
                    if (columnModel.EnableFilter)
                    {
                        var filterTextBox = FilterPanel.AddColumn(columnModel);
                        gridColumnToSearchFieldsMapping.Add(gridColumn, filterTextBox);
                    }
                }
            }
            if (DataContext is INotifyPropertyChanged npc)
                npc.PropertyChanged -= DataContext_PropertyChanged;

        }
        
    }
}
