﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Dirs
{
    /// <summary>
    /// Interaction logic for SelectListItemDlg.xaml
    /// </summary>
    [Obsolete]
    public partial class SelectListItemDlg : Window
    {
        public SelectListItemDlg(IEnumerable<object> list)
        {
            Owner = Application.Current.MainWindow;
            DataContext = list;
            InitializeComponent();            
        }

        public object SelectedItem {
            get => List.SelectedItem;
            set {
                List.SelectedItem = value;
                if (List.SelectedItem != null)
                    List.ScrollIntoView(List.SelectedItem);
            }
        }
        public object SelectedID {
            get => List.SelectedValue;
            set {
                List.SelectedValue = value;
                if (List.SelectedItem != null)
                    List.ScrollIntoView(List.SelectedItem);
            }
        }

        //private void ButtonSelect_Click(object sender, RoutedEventArgs e) {
        //    if (Grid.SelectedItem != null) {
        //        DialogResult = true;
        //        Close();
        //    }
        //}

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            //   ButtonSelect_Click(sender, null);
            if (BtnAccept.IsEnabled) {
                ButtonAutomationPeer peer = new ButtonAutomationPeer(BtnAccept);
                IInvokeProvider invokeProv = peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
                invokeProv.Invoke();
            }
        }
    }
}
