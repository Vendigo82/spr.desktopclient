﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Dirs
{
    using Services;
    using SPR.UI.App.Abstractions.UI;

    /// <summary>
    /// Interaction logic for DirGoogleBQConnectionView.xaml
    /// </summary>
    public partial class DirGoogleBQConnectionView : UserControl
    {
        private const string DIALOG_TITLE_KEY = "DirGoogleBQConnectionTab.SelectCredentialsBtn.DialogTitle";
        private readonly IResourceStringProvider _resources;
        private readonly IWindowFactory _wndFactory;

        public DirGoogleBQConnectionView(VM.Dirs.DirGoogleBQConnectionVM vm, 
            IResourceStringProvider resources,
            IWindowFactory wndFactory) {
            DataContext = vm;
            _resources = resources;
            _wndFactory = wndFactory;
            InitializeComponent();
        }

        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Grid.SelectedItem != null)
            {
                try
                {
                    Grid.UpdateLayout();
                    Grid.ScrollIntoView(Grid.SelectedItem);
                }
                catch (InvalidOperationException) { }
            }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            if (Grid.SelectedItem != null) {
                if (ActionCommand != null && ActionCommand.CanExecute(Grid.SelectedValue))
                    ActionCommand.Execute(Grid.SelectedValue);
            }
        }

        private void OpenCredentialsFileButton_Click(object sender, RoutedEventArgs e) {
            var item = Grid.SelectedItem;
            if (item != null)
                LoadCredentialsFromFile(item);
        }

        private void LoadCredentialsFromFile(object item) {
            OpenFileDialog dlg = new OpenFileDialog() {
                Filter = "Json files (.json)|*.json",
                DefaultExt = ".json",
                CheckFileExists = true,
                CheckPathExists = true,
                Multiselect = false,
                Title = _resources.GetString(DIALOG_TITLE_KEY)
            };
            if (dlg.ShowDialog() == true) {
                try {
                    string text = File.ReadAllText(dlg.FileName);
                    item.TrySetPropertyValue("Credentials", text);
                } catch (IOException e) {
                    _wndFactory.ShowErrorDialog(e.Message);
                }
            }
        }

        #region IsReadOnly property
        public static DependencyProperty IsReadOnlyProperty = DependencyProperty.Register(
            nameof(IsReadOnly), typeof(bool), typeof(DirGoogleBQConnectionView),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(IsReadOnlyChanged))
            );

        public bool IsReadOnly {
            get => (bool)GetValue(IsReadOnlyProperty);
            set => SetValue(IsReadOnlyProperty, value);
        }

        private static void IsReadOnlyChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            DirGoogleBQConnectionView obj = (DirGoogleBQConnectionView)o;
            obj.Grid.IsReadOnly = (bool)args.NewValue;
        }
        #endregion

        #region ActionCommand
        public static readonly DependencyProperty ActionCommandProperty = DependencyProperty.Register(
            nameof(ActionCommand), typeof(ICommand), typeof(DirGoogleBQConnectionView), new UIPropertyMetadata(null));

        public ICommand ActionCommand {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }
        #endregion

    }
}
