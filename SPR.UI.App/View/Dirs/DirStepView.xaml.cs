﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SPR.UI.App.View.Dirs
{
    /// <summary>
    /// Interaction logic for DirStepView.xaml
    /// </summary>
    public partial class DirStepView : UserControl
    {
        public DirStepView(VM.Dirs.DirStepVM vm)
        {
            DataContext = vm;
            InitializeComponent();
        }

        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Grid.SelectedItem != null)
            {
                try
                {
                    Grid.UpdateLayout();
                    Grid.ScrollIntoView(Grid.SelectedItem);
                }
                catch (InvalidOperationException) { }
            }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.SelectedItem != null)
            {
                if (ActionCommand != null && ActionCommand.CanExecute(Grid.SelectedValue))
                    ActionCommand.Execute(Grid.SelectedValue);
            }
        }

        #region IsReadOnly property
        public static DependencyProperty IsReadOnlyProperty = DependencyProperty.Register(
            nameof(IsReadOnly), typeof(bool), typeof(DirStepView),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(IsReadOnlyChanged))
            );

        public bool IsReadOnly {
            get => (bool)GetValue(IsReadOnlyProperty);
            set => SetValue(IsReadOnlyProperty, value);
        }

        private static void IsReadOnlyChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            DirStepView obj = (DirStepView)o;
            obj.Grid.IsReadOnly = (bool)args.NewValue;
        }
        #endregion

        #region ActionCommand
        public static readonly DependencyProperty ActionCommandProperty = DependencyProperty.Register(
            nameof(ActionCommand), typeof(ICommand), typeof(DirStepView), new UIPropertyMetadata(null));

        public ICommand ActionCommand {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }
        #endregion
    }
}
