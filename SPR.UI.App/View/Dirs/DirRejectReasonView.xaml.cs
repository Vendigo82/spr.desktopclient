﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using SPR.UI.App.VM.Dirs;

namespace SPR.UI.App.View.Dirs
{
    /// <summary>
    /// Interaction logic for DirRejectReasonView.xaml
    /// </summary>
    public partial class DirRejectReasonView : UserControl
    {
        public DirRejectReasonView(DirRejectReasonVM vm) {
            DataContext = vm;
            InitializeComponent();
        }

        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Grid.SelectedItem != null)
            {
                try
                {
                    Grid.UpdateLayout();
                    Grid.ScrollIntoView(Grid.SelectedItem);
                }
                catch (InvalidOperationException) { }
            }
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            if (Grid.SelectedItem != null) {
                if (ActionCommand != null && ActionCommand.CanExecute(Grid.SelectedValue))
                    ActionCommand.Execute(Grid.SelectedValue);
            }
        }

        #region IsReadOnly property
        public static DependencyProperty IsReadOnlyProperty = DependencyProperty.Register(
            nameof(IsReadOnly), typeof(bool), typeof(DirRejectReasonView),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(IsReadOnlyChanged))
            );

        public bool IsReadOnly {
            get => (bool)GetValue(IsReadOnlyProperty);
            set => SetValue(IsReadOnlyProperty, value);
        }

        private static void IsReadOnlyChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            DirRejectReasonView obj = (DirRejectReasonView)o;
            obj.Grid.IsReadOnly = (bool)args.NewValue;
        }
        #endregion

        #region ActionCommand
        public static readonly DependencyProperty ActionCommandProperty = DependencyProperty.Register(
            nameof(ActionCommand), typeof(ICommand), typeof(DirRejectReasonView), new UIPropertyMetadata(null));

        public ICommand ActionCommand {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }
        #endregion
    }
}
