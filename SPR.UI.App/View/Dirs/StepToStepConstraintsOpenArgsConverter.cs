﻿using SPR.UI.App.VM.Dirs;
using SPR.UI.App.Windows.StepConstraints;
using SPR.UI.Core.UseCases.StepConstraints;
using System;
using System.Globalization;
using System.Windows.Data;

namespace SPR.UI.App.View.Dirs
{
    internal class StepToStepConstraintsOpenArgsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DirStepVM.ItemWrapper item)
                return new StepConstraintsOpenArgs
                {
                    StepName = item.Name,
                    Type = StepConstraintType.PostConditions
                };
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
