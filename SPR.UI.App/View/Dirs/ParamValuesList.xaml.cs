﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Dirs
{
    /// <summary>
    /// Interaction logic for ParamValuesList.xaml
    /// </summary>
    public partial class ParamValuesList : UserControl
    {
        public ParamValuesList() {
            InitializeComponent();
        }

        #region ItemsSource
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            nameof(ItemsSource), typeof(IEnumerable), typeof(ParamValuesList),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(ItemsSourceChanged)));

        public IEnumerable ItemsSource {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        private static void ItemsSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            ParamValuesList obj = (ParamValuesList)o;
            obj.Grid.ItemsSource = args.NewValue as IEnumerable;
        }
        #endregion

        //private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
        //    SelectedItem = Grid.SelectedItem;
        //}
    }
}
