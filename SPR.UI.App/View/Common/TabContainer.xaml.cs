﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for TabContainer.xaml
    /// </summary>
    public partial class TabContainer : UserControl
    {
        public TabContainer(UIElement content) {
            if (content is Control ctrl)
                DataContext = ctrl.DataContext;
            InitializeComponent();
            MainContent.Content = content;
        }

        public TabContainer() {
            //if (content is Control ctrl)
            //    DataContext = ctrl.DataContext;
            InitializeComponent();
            //MainContent.Content = content;
        }      
    }
}
