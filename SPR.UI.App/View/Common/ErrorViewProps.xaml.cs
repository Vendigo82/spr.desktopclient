﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for ErrorViewProps.xaml
    /// </summary>
    public partial class ErrorViewProps : UserControl
    {
        public ErrorViewProps()
        {
            InitializeComponent();
        }

        #region HasError
        public static DependencyProperty HasErrorProperty = DependencyProperty.Register(
            nameof(HasError), typeof(bool), typeof(ErrorViewProps));

        public bool HasError {
            get => (bool)GetValue(HasErrorProperty);
            set => SetValue(HasErrorProperty, value);
        }
        #endregion

        #region ErrorMessage
        public static DependencyProperty ErrorMessageProperty = DependencyProperty.Register(
            nameof(ErrorMessage), typeof(string), typeof(ErrorViewProps));

        public string ErrorMessage {
            get => (string)GetValue(HasErrorProperty);
            set => SetValue(HasErrorProperty, value);
        }
        #endregion
    }
}
