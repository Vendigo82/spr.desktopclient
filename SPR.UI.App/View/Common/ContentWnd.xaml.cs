﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for ContentWnd.xaml
    /// </summary>
    public partial class ContentWnd : Window
    {
        public ContentWnd(UIElement content) {
            InitializeComponent();
            MainContent.Content = content;
            AcceptVisibility = false;
            AcceptBtn.Visibility = Visibility.Collapsed;
        }

        public static DependencyProperty AcceptVisibilityProperty = DependencyProperty.Register(
            nameof(AcceptVisibility), typeof(bool), typeof(ContentWnd),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(AcceptVisibilityChanged))
            );

        public bool AcceptVisibility {
            get => (bool)GetValue(AcceptVisibilityProperty);
            set => SetValue(AcceptVisibilityProperty, value);
        }

        private static void AcceptVisibilityChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            ContentWnd obj = (ContentWnd)o;
            obj.AcceptBtn.Visibility = ((bool)args.NewValue) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void AcceptBtn_Click(object sender, RoutedEventArgs e) {
            DialogResult = true;            
        }
    }
}
