﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for BusyIndicatorWrapper.xaml
    /// </summary>
    [ContentProperty(nameof(MainContent))]
    public partial class BusyIndicatorWrapper : UserControl
    {
        public BusyIndicatorWrapper(UIElement content)
        {
            if (content is Control ctrl)
                DataContext = ctrl.DataContext;
            InitializeComponent();
            MainContentPresenter.Content = content;
        }

        public BusyIndicatorWrapper()
        {
            InitializeComponent();
        }

        #region Main content
        public static DependencyProperty MainContentProperty = DependencyProperty.Register(
            nameof(MainContent), typeof(UIElement), typeof(BusyIndicatorWrapper),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(MainContentChanged)));

        public UIElement MainContent {
            get => (UIElement)GetValue(MainContentProperty);
            set => SetValue(MainContentProperty, value);
        }

        private static void MainContentChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var obj = (BusyIndicatorWrapper)o;
            obj.MainContentPresenter.Content = args.NewValue as UIElement;
            if (args.NewValue is Control ui && obj.DataContext != null)
                ui.DataContext = obj.DataContext;
        }
        #endregion
    }
}
