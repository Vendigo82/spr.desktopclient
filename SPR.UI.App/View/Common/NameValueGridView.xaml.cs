﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for NameValueGridView.xaml
    /// </summary>
    public partial class NameValueGridView : UserControl
    {
        public NameValueGridView()
        {
            InitializeComponent();
        }

        #region ItemsSource
        public static DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            nameof(ItemsSource), typeof(IEnumerable), typeof(NameValueGridView),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(ItemsSourceChanged)));

        public IEnumerable ItemsSource {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        private static void ItemsSourceChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            NameValueGridView obj = (NameValueGridView)o;
            obj.ParamsGrid.ItemsSource = args.NewValue as IEnumerable;
        }
        #endregion
    }
}
