﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for BusyContentWnd.xaml
    /// </summary>
    public partial class BusyContentWnd : Window
    {
        public BusyContentWnd(UIElement listControl)
        {
            InitializeComponent();

            ContainerView.MainContent.Content = listControl;
            SizeChanged += BusyContentWnd_SizeChanged;
        }

        private void BusyContentWnd_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.HeightChanged)
                Top -= (e.NewSize.Height - e.PreviousSize.Height) / 2;
            if (e.WidthChanged)
                Left -= (e.NewSize.Width - e.PreviousSize.Width) / 2;
        }
    }
}
