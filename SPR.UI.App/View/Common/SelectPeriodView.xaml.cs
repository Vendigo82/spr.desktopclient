﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for SelectPeriodView.xaml
    /// </summary>
    public partial class SelectPeriodView : UserControl
    {
        public SelectPeriodView()
        {
            InitializeComponent();
        }

        private void DropCalendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            DateCombo.Text = GetSelectedRange();
        }

        private void DateCombo_DropDownClosed(object sender, EventArgs e)
        {
            DateCombo.Text = GetSelectedRange();
            SelectedPeriod = CalendarSelectedPeriod;
            //SelectedPeriodProperty.
        }

        private string GetSelectedRange()
        {
            var tuple = CalendarSelectedPeriod;
            if (tuple == null)
                return string.Empty;

            if (tuple.Item1 == tuple.Item2)
                return tuple.Item1.ToString("d");

            return tuple.Item1.ToString("d") + " - " + tuple.Item2.ToString("d");
        }

        private Tuple<DateTime, DateTime> CalendarSelectedPeriod
        {
            get {
                if (DropCalendar.SelectedDates.Count == 1)
                    return Tuple.Create(DropCalendar.SelectedDates.First(), DropCalendar.SelectedDates.First());

                if (DropCalendar.SelectedDates.Count != 0) {
                    var d1 = DropCalendar.SelectedDates.First();
                    var d2 = DropCalendar.SelectedDates.Last();
                    return d1 < d2 ? Tuple.Create(d1, d2) : Tuple.Create(d2, d1);
                }

                return null;
            }
        }

        #region DateRange
        public static DependencyProperty SelectedPeriodProperty = DependencyProperty.Register(
            nameof(SelectedPeriod), typeof(Tuple<DateTime, DateTime>), typeof(SelectPeriodView),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(SelectedPeriodChanged))
            );

        public Tuple<DateTime, DateTime> SelectedPeriod {
            get => (Tuple<DateTime, DateTime>)GetValue(SelectedPeriodProperty);
            set => SetValue(SelectedPeriodProperty, value);
        }

        private static void SelectedPeriodChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var obj = (SelectPeriodView)o;
            var value = (Tuple<DateTime, DateTime>)args.NewValue;

            obj.DropCalendar.SelectedDates.Clear();
            obj.DropCalendar.SelectedDates.AddRange(value.Item1, value.Item2);
        }
        #endregion

        private void TodayButton_Click(object sender, RoutedEventArgs e)
        {
            var dt = DateTime.Now.Date;
            SelectedPeriod = new Tuple<DateTime, DateTime>(dt, dt);
        }

        private void YesterdayButton_Click(object sender, RoutedEventArgs e)
        {
            var dt = DateTime.Now.Date - TimeSpan.FromDays(1);
            SelectedPeriod = new Tuple<DateTime, DateTime>(dt, dt);
        }

        private void WeekButton_Click(object sender, RoutedEventArgs e)
        {
            var dt = DateTime.Now.Date;
            SelectedPeriod = new Tuple<DateTime, DateTime>(dt - TimeSpan.FromDays(6), dt);
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            var dt = CalendarSelectedPeriod;
            if (dt == null)
                return;

            var ts = dt.Item2 - dt.Item1 + TimeSpan.FromDays(1);
            SelectedPeriod = Tuple.Create(dt.Item1 - ts, dt.Item2 - ts);
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            var dt = CalendarSelectedPeriod;
            if (dt == null)
                return;

            var ts = dt.Item2 - dt.Item1 + TimeSpan.FromDays(1);
            SelectedPeriod = Tuple.Create(dt.Item1 + ts, dt.Item2 + ts);
        }
    }
}
