﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for MainDraftArhiveViewFrame.xaml
    /// </summary>
    [ContentProperty(nameof(MainContent))]
    public partial class MainDraftArhiveViewFrame : UserControl
    {
        public MainDraftArhiveViewFrame()
        {
            InitializeComponent();
            DataContextChanged += MainDraftArhiveViewFrame_DataContextChanged;
        }

        private void MainDraftArhiveViewFrame_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (MainContentPresenter.Content is Control ctrl)
                ctrl.DataContext = e.NewValue;
        }

        #region Main content
        public static DependencyProperty MainContentProperty = DependencyProperty.Register(
            nameof(MainContent), typeof(UIElement), typeof(MainDraftArhiveViewFrame),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(MainContentChanged)));

        public UIElement MainContent {
            get => (UIElement)GetValue(MainContentProperty);
            set => SetValue(MainContentProperty, value);
        }

        private static void MainContentChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var obj = (MainDraftArhiveViewFrame)o;
            obj.MainContentPresenter.Content = args.NewValue as UIElement;
            if (args.NewValue is Control ui)
                ui.DataContext = obj.DataContext;
        }
        #endregion
    }
}
