﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for SelectArchivedVersionView.xaml
    /// </summary>
    public partial class SelectArchivedVersionView : UserControl
    {
        public SelectArchivedVersionView() {
            InitializeComponent();
        }

        public int? SelectedVersion {
            get => Grid.SelectedItem != null ? (int)Grid.SelectedValue : (int?)null;
        }

        public int? SelectedValue {
            get => SelectedVersion;
            set => Grid.SelectedValue = value;
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            if (Grid.SelectedItem != null) {
                if (ActionCommand != null && ActionCommand.CanExecute(SelectedValue))
                    ActionCommand.Execute(SelectedValue);
            }
        }

        #region ActionCommand
        public static readonly DependencyProperty ActionCommandProperty = DependencyProperty.Register(
            nameof(ActionCommand), typeof(ICommand), typeof(SelectArchivedVersionView), new UIPropertyMetadata(null));

        public ICommand ActionCommand {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }
        #endregion
    }
}
