﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.View.Common
{
    /// <summary>
    /// Interaction logic for SelectItemDlg.xaml
    /// </summary>
    public partial class SelectItemDlg : Window
    {
        public SelectItemDlg(UIElement listControl)
        {
            InitializeComponent();

            if (listControl.GetType().GetCustomAttribute<ViewBusyIndicatorAttribute>() == null) {
                ContainerView.MainContent.Content = listControl as UserControl;
                ContentView.Visibility = Visibility.Collapsed;
            } else {
                ContentView.Content = listControl;
                ContainerView.Visibility = Visibility.Collapsed;
            }

            if ((listControl is Control ctrl) && (ctrl.DataContext is INotifyPropertyChanged npc))
                npc.PropertyChanged += PropertyChanged;

            listControl.TrySetPropertyValue("IsReadOnly", true);

            listControl.TrySetCommandDelegate("ActionCommand", () => {
                if (SelectedItem != null)
                    DialogResult = true;
            });

            SizeChanged += SelectItemDlg_SizeChanged;            
        }

        private void SelectItemDlg_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Width < e.PreviousSize.Width || e.NewSize.Height < e.PreviousSize.Height) {
                MinWidth = e.PreviousSize.Width;
                MinHeight = e.PreviousSize.Height;
            }
            if (e.HeightChanged)
                Top -= (e.NewSize.Height - e.PreviousSize.Height) / 2;
            if (e.WidthChanged)
                Left -= (e.NewSize.Width - e.PreviousSize.Width) / 2;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e) {
            if (e.Property.Name == nameof(DataContext)) {
                if (DataContext is INotifyPropertyChanged npc)
                    npc.PropertyChanged += PropertyChanged;
            }
            base.OnPropertyChanged(e);
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(SelectedValue))
                SelectedValue = InnerSelectedValue;

            if (e.PropertyName == nameof(SelectedItem))
                SelectedItem = InnerSelectedItem;
        }

        #region SelectedValue property
        public static DependencyProperty SelectedValueProperty = DependencyProperty.Register(
            nameof(SelectedValue), typeof(object), typeof(SelectItemDlg),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(SelectedValueChanged))
            );

        public object SelectedValue {
            get => (object)GetValue(SelectedValueProperty);
            set => SetValue(SelectedValueProperty, value);
        }

        private static void SelectedValueChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            SelectItemDlg obj = (SelectItemDlg)o;
            if (!args.NewValue?.Equals(obj.InnerSelectedValue) ?? true)
                obj.InnerSelectedValue = args.NewValue;
        }

        private object InnerSelectedValue {
            get => SelectedValuePropertyInfo.GetValue(DataContext);
            set => SelectedValuePropertyInfo.SetValue(DataContext, value);
        }

        private PropertyInfo SelectedValuePropertyInfo => DataContext.GetType().GetProperty(nameof(SelectedValue))
            ?? throw new InvalidOperationException($"Property {nameof(SelectedValue)} not found in DataContext ({DataContext.GetType().Name})");

        #endregion

        #region SelectedItem property
        public static DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            nameof(SelectedItem), typeof(object), typeof(SelectItemDlg),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(SelectedItemChanged))
            );

        public object SelectedItem {
            get => (object)GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        private static void SelectedItemChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            SelectItemDlg obj = (SelectItemDlg)o;
            if (!args.NewValue?.Equals(obj.InnerSelectedItem) ?? true)
                obj.InnerSelectedItem = args.NewValue;
        }

        private object InnerSelectedItem {
            get => SelectedItemPropertyInfo.GetValue(DataContext);
            set => SelectedItemPropertyInfo.SetValue(DataContext, value);
        }

        private PropertyInfo SelectedItemPropertyInfo => DataContext.GetType().GetProperty(nameof(SelectedItem))
            ?? throw new InvalidOperationException($"Property {nameof(SelectedItem)} not found in DataContext ({DataContext.GetType().Name})");

        #endregion

        private void ButtonAccept_Click(object sender, RoutedEventArgs e) {
            DialogResult = true;
        }
    }
}
