﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataModel
{
    public interface IItemContainer<T>
    {
        T Item { get; }
    }
}
