﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions
{
    public enum LogLevel
    {
        Debug = 1,
        Information = 2,
        Warning = 3,
        Error = 4,
        Critical = 5,
    }

    public interface ILogger
    {
        bool IsEnabled(LogLevel logLevel);
        void Log(LogLevel logLevel, string message, Exception exception);
    }

    public static class ILoggerExtentions
    {
        public static void LogDebug(this ILogger logger, string msg) => logger.Log(LogLevel.Debug, msg, null);
        public static void LogInfo(this ILogger logger, string msg) => logger.Log(LogLevel.Information, msg, null);
        public static void LogWarn(this ILogger logger, string msg) => logger.Log(LogLevel.Warning, msg, null);
        public static void LogWarn(this ILogger logger, string msg, Exception e) => logger.Log(LogLevel.Warning, msg, e);
        public static void LogError(this ILogger logger, string msg) => logger.Log(LogLevel.Error, msg, null);
        public static void LogError(this ILogger logger, string msg, Exception e) => logger.Log(LogLevel.Error, msg, e);
        public static void LogFatal (this ILogger logger, string msg) => logger.Log(LogLevel.Critical, msg, null);
        public static void LogFatal(this ILogger logger, string msg, Exception e) => logger.Log(LogLevel.Critical, msg, e);
    }
}
