﻿using SPR.WebService.TestModule.DataContract;
using SPR.UI.App.DataContract;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SPR.UI.App.Abstractions.Tests
{

    /// <summary>
    /// Provide test results acceptor
    /// </summary>
    public interface ITestResultsAcceptor
    {
        /// <summary>
        /// Add test formula result
        /// </summary>
        void PushResult(long formulaId, ShownObjectEnum shownObject, TestFormulaResponse response, 
            IEnumerable<KeyValuePair<string, object>> additionalData = null);

        /// <summary>
        /// Add test scenario result
        /// </summary>
        /// <param name="scenarioCode"></param>
        /// <param name="shownObject"></param>
        /// <param name="response"></param>
        void PushResult(long scenarioCode, ShownObjectEnum shownObject, TestResponse<ScenarioResultDto> response);

        /// <summary>
        /// Start testings object
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="id"></param>
        /// <param name="shownObject"></param>
        void StartTesting(ObjectType objectType, long id, ShownObjectEnum shownObject);

        /// <summary>
        /// Testing request was failed wth error
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="id"></param>
        /// <param name="shownObject"></param>
        /// <param name="e"></param>
        void Cancel(ObjectType objectType, long id, ShownObjectEnum shownObject);
    }
}
