﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

namespace SPR.UI.App.Abstractions.Tests
{
    /// <summary>
    /// Provide input data for testing module
    /// </summary>
    public interface ITestInputDataProvider
    {
        string GetInputData();
    }
}
