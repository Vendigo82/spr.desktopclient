﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.Tests
{
    using DataContract;
    using SPR.UI.App.VM.Formulas;

    /// <summary>
    /// Contains main information about tested item
    /// </summary>
    public interface ITestItemAccessor<T>
    {
        /// <summary>
        /// Item's id
        /// </summary>
        long Id { get; }

        /// <summary>
        /// Item's guid
        /// </summary>
        Guid Guid { get; }

        /// <summary>
        /// Draft item
        /// </summary>
        T Item { get; }    
        
        /// <summary>
        /// Which type of item testing
        /// </summary>
        ShownObjectEnum ShownObject { get; }
    }

    public interface ITestItemAccessorCreator<T>
    {
        ITestItemAccessor<T> Create(FormulaVM vm);
    }
}
