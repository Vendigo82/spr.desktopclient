﻿using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    /// <summary>
    /// Provide data source for standard results list
    /// </summary>
    public interface IStdResultsListSource : IDataListSourceExt<StdResultSlimModel>, IDeleteOperation<StdResultSlimModel>
    {
        /// <summary>
        /// Set specific std result as default
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task SetDefaultAsync(StdResultSlimModel model, CancellationToken ct);        
    }
}
