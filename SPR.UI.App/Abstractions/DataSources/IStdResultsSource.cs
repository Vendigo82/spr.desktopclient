﻿using System;
using System.Threading;
using System.Threading.Tasks;

using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.UI.App.Abstractions.DataSources
{
    /// <summary>
    /// Provide standard results item management
    /// </summary>
    public interface IStdResultsSource
    {
        /// <summary>
        /// Get item by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<StdResultModel> GetItemAsync(Guid id, CancellationToken ct = default);

        /// <summary>
        /// Get item usage
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<ItemUsageModel> GetUsageAsync(Guid id, CancellationToken ct = default);

        /// <summary>
        /// Save or update item
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<StdResultModel> SaveAsync(StdResultModel model, CancellationToken ct = default);

        /// <summary>
        /// Set std result as default
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task SetDefault(Guid id, CancellationToken ct = default);
    }
}
