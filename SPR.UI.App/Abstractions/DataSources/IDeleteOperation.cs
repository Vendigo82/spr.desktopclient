﻿using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IDeleteOperation<T>
    {
        /// <summary>
        /// Delete item from list
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<bool> DeleteAsync(T item, CancellationToken ct = default);
    }
}
