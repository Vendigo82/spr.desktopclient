﻿using System.Threading;
using System.Threading.Tasks;

using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.UI.App.Abstractions.DataSources
{
    /// <summary>
    /// Extended data list source
    /// </summary>
    public interface IDataListSourceExt<T> : IDataListSource<StdResultSlimModel>
    {
        /// <summary>
        /// get item's usage
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<ItemUsageModel> GetUsageAsync(T item, CancellationToken ct = default);
    }
}
