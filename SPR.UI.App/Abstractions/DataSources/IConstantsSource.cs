﻿using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IConstantsSource
    {
        /// <summary>
        /// Load list of constants
        /// </summary>
        /// <param name="token"></param>
        /// <param name="maxCount"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<Tuple<IEnumerable<ParamModel>, string>> GetItemsAsync(string token, int? maxCount, CancellationToken ct = default);

        /// <summary>
        /// Perform bulk update
        /// </summary>
        /// <param name="items"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task BulkUpdateAsync(IEnumerable<BunchUpdateItemModel<string, ParamModel>> items, CancellationToken ct = default);
    }
}
