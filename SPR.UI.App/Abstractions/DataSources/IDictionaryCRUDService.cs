﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Abstractions.DataSources
{
    /// <summary>
    /// Dictionary provider
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDictionaryCRUDService<T> : IDataListSource<T> where T : class
    {
        /// <summary>
        /// Name of the dictionary
        /// </summary>
        string DictionaryName { get; }

        /// <summary>
        /// Get item with usage information
        /// </summary>
        /// <param name="key">item's key</param>
        /// <param name="ct"></param>
        /// <returns>item with usage info</returns>
        Task<ItemInfoModel<T>> LoadItemInfoAsync(int id, CancellationToken ct = default);

        /// <summary>
        /// Update dictionary records from <paramref name="items"/> and returns all dictionary records
        /// </summary>
        /// <param name="items">Items for insert, update, delete</param>
        /// <returns>all dictionary items</returns>
        Task<IEnumerable<T>> UpdateAsync(IEnumerable<BunchUpdateItemModel<T>> items, CancellationToken ct = default);
    }
}
