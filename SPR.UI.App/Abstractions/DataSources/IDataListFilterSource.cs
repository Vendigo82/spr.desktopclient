﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IDataListFilterSource<TItem, TFilter> : IDataListSource<TItem>
    {
        /// <summary>
        /// Load items with filter
        /// </summary>
        Task<IEnumerable<TItem>> LoadItemsAsync(TFilter filter, CancellationToken ct = default);
    }
}
