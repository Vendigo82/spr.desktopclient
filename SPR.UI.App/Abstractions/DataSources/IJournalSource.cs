﻿using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Journal;
using SPR.UI.App.DataModel;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IJournalSource<T> where T : JournalRecordSlimModel
    {
        /// <summary>
        /// Load previous journal records
        /// </summary>
        /// <param name="nextId"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<PartialListContainer<T>> LoadAsync(JournalFilter filter, long? currentId, bool fresh, CancellationToken ct = default);
    }
}
