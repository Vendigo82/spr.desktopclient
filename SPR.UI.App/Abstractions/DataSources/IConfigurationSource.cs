﻿using SPR.UI.WebService.DataContract.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    /// <summary>
    /// Provide access to SPR data
    /// </summary>
    public interface IConfigurationSource
    {
        /// <summary>
        /// Current instance configuration
        /// </summary>
        Task<InstanceResponse> GetInstanceAsync(CancellationToken ct);
    }
}
