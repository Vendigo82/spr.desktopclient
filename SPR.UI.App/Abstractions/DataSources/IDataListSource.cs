﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IDataListSource<T>
    {
        /// <summary>
        /// Load all items
        /// </summary>
        Task<IEnumerable<T>> LoadItemsAsync(CancellationToken ct = default);

        /// <summary>
        /// Load all items if necessary and returns it
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetItemsAsync(CancellationToken ct = default);        
    }
}
