﻿using SPR.UI.Core.Shared;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IScenarioSource : IDataSource<ScenarioModel>
    {
        Task ActivateAsync(Guid id, bool enable);
    }
}
