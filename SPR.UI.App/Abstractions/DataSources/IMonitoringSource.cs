﻿using SPR.UI.App.DataModel;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Configuration;
using SPR.UI.WebService.DataContract.Monitoring;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IMonitoringSource
    {
        /// <summary>
        /// Load information about additional transaction columns
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<IEnumerable<TransactionColumnModel>> LoadAdditionalColumnsInfoAsync(CancellationToken ct = default);

        /// <summary>
        /// Load transactions records in descending order
        /// </summary>
        /// <param name="filter">transactions filter</param>
        /// <param name="currentId">last transaction id (newest or oldest)</param>
        /// <param name="fresh">loading new records or old records</param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<PartialListContainer<TransactionModel>> LoadAsync(TransactionsFilter filter, long? currentId, bool fresh, CancellationToken ct = default);

        /// <summary>
        /// Reload finished transactions
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<IEnumerable<TransactionModel>> ReloadAsync(IEnumerable<long> ids, CancellationToken ct = default);

        /// <summary>
        /// Load transactions data
        /// </summary>
        /// <param name="transactionid"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<TransactionDataResponseModel> LoadDataAsync(long transactionId, CancellationToken ct = default);

        /// <summary>
        /// Load calculation results for transaction
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<IEnumerable<CalculationResultModel>> LoadCalculationResulsAsync(long transactionId, CancellationToken ct = default);

        /// <summary>
        /// Load list of errors
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="currentId"></param>
        /// <param name="fresh"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<PartialListContainer<TransactionErrorModel>> LoadErrorsAsync(PeriodFilter filter, long? currentId, bool fresh, CancellationToken ct = default);

        /// <summary>
        /// Load summary
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<IEnumerable<SummaryRecordModel>> LoadSummaryAsync(PeriodFilter filter, CancellationToken ct = default);

        /// <summary>
        /// Load summary
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<IEnumerable<SummaryErrorModel>> LoadSummaryErrorsAsync(PeriodFilter filter, CancellationToken ct = default);
    }
}
