﻿using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    /// <summary>
    /// Full text formula search
    /// </summary>
    public interface IFullTextSearchSource
    {
        /// <summary>
        /// Search formulas which include given string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        Task<IEnumerable<FormulaBriefModel>> SearchAsync(string text, CancellationToken ct = default);

        /// <summary>
        /// Load search options
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<FullTextSearchOptionsModel> GetOptionsAsync(CancellationToken ct = default);

        /// <summary>
        /// Get fragment of formula which include given string
        /// </summary>
        /// <param name="formulaId"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        Task<IEnumerable<string>> GetFormulaFragments(Guid formulaId, string text, CancellationToken ct = default);
    }
}
