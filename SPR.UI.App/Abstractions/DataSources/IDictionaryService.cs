﻿using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IDictionaryService<TKey, T> : IDataListSource<T>
        where T : class
    {
        /// <summary>
        /// Get single item
        /// </summary>
        /// <param name="key"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<T> GetItemAsync(TKey key, CancellationToken ct);
    }
}
