﻿using SPR.UI.WebService.DataContract;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface IDictionarySource
    {
        /// <summary>
        /// Get usage information for item with id=<paramref name="id"/> from dictionary <paramref name="dictionaryName"/>
        /// </summary>
        /// <param name="dictionaryName">name of the dictionary</param>
        /// <param name="id">item's identity</param>
        /// <param name="ct"></param>
        /// <returns>usage info or null</returns>
        Task<ItemUsageModel> DirItemInfo(string dictionaryName, string id, CancellationToken ct = default);
    }
}
