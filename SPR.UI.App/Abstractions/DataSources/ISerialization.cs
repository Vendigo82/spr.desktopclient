﻿using SPR.UI.App.DataContract;
using SPR.UI.WebService.DataContract.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.DataSources
{
    public interface ISerialization
    {
        Task<string> SerializeFormulaAsync(Guid formulaGuid, CancellationToken ct = default);

        Task<string> SerializeScenarioAsync(Guid scenarioGuid, CancellationToken ct = default);

        Task<ExistanceResponseModel> CheckExistanceAsync(string data, CancellationToken ct = default);

        Task<ImportResultModel> ImportAsync(string data, ImportSettingsModel settings, string comment, CancellationToken ct = default);

        Task<ImportObjectResultModel> ShallowCopyAsync(ObjectType type, Guid guid, CancellationToken ct = default);

        Task<ImportResultModel> DeepCopyAsync(ObjectType type, Guid guid, CancellationToken ct = default);
    }
}
