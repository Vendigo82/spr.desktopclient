﻿using System;
using System.Collections.Generic;

namespace SPR.UI.App.Abstractions.DataSources
{
    public class InstanceSettingsModel
    {
        public string Name { get; set; }

        public string BaseUrl { get; set; }

        public TimeSpan? Timeout { get; set; }
    }

    public interface IInstancesSource
    {
        IEnumerable<InstanceSettingsModel> GetInstances();
    }
}
