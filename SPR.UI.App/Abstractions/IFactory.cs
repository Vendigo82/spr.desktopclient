﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions
{
    public interface IFactory<T> where T : class
    {
        T Create(string name);

        T Create(string name, IEnumerable<KeyValuePair<Type, object>> param);
    }

    public static class IFactoryExtensions
    {
        /// <summary>
        /// Resolve type with overriding constructor argument of specific type
        /// </summary>
        /// <typeparam name="T">Resolving value type</typeparam>
        /// <typeparam name="TArg1">constructor argument's type</typeparam>
        /// <param name="tr"></param>
        /// <param name="name">Name of the object. Can be null</param>
        /// <param name="param">constructor argument's value </param>
        /// <returns></returns>
        public static T Create<T, TArg1>(this IFactory<T> tr, string name, TArg1 param) where T : class
        {
            var pair = new KeyValuePair<Type, object>(typeof(TArg1), param);
            return tr.Create(name, new[] { pair });
        }

        public static T Create<T, TArg1, TArg2>(this IFactory<T> tr, string name, TArg1 param1, TArg2 param2) where T : class
        {
            return tr.Create(name, new KeyValuePair<Type, object>[] {
                new KeyValuePair<Type, object>(typeof(TArg1), param1),
                new KeyValuePair<Type, object>(typeof(TArg2), param2),
            });
        }

        public static T Create<T, TArg1, TArg2, TArg3>(this IFactory<T> tr, string name, TArg1 param1, TArg2 param2, TArg3 param3) where T : class
        {
            return tr.Create(name, new KeyValuePair<Type, object>[] {
                new KeyValuePair<Type, object>(typeof(TArg1), param1),
                new KeyValuePair<Type, object>(typeof(TArg2), param2),
                new KeyValuePair<Type, object>(typeof(TArg3), param3),
            });
        }
    }
}
