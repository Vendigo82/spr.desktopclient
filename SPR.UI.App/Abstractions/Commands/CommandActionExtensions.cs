﻿using System;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;
using SPR.UI.App.Services;
using SPR.UI.App.Abstractions.UI;

namespace SPR.UI.App.Abstractions.Commands
{
    public static class CommandActionExtensions
    {
        public static ICommand CreateRelayCommand(this ICommandAction action, ITypeResolver resolver)
            => new RelayCommand(() => ErrorHandler(() => action.Action(), resolver));

        public static ICommand CreateRelayCommand<T>(this ICommandAction<T> action, ITypeResolver resolver)
            => new RelayCommand<object>((o) => ErrorHandler(() => {
                if (o is T t)
                    action.Action(t);
                else
                    throw new InvalidCastException($"Action '{action.GetType().Name}' argument's type error; expected argument of type {typeof(T).Name}, but received {o.GetType().Name}");
            }, resolver));

        public static ICommand CreateRelayCommand<T1, T2>(this ICommandAction<T1, T2> action, ITypeResolver resolver)
            => new RelayCommand<object>((o) => ErrorHandler(() => {
                if (o is Tuple<object, object> tuple)
                    action.Action((T1)tuple.Item1, (T2)tuple.Item2);
                else
                    throw new InvalidCastException($"Error on action type '{action.GetType().Name}': expected argument of type '{typeof(Tuple<T1, T2>).Name}', but received {o.GetType().Name}");
            }, resolver));

        private static void ErrorHandler(Action action, ITypeResolver typeResolver)
        {
            try {
                action();
            } catch (Exception e) {
                typeResolver.Resolve<ILogger>().LogError("Perform command error", e);
                typeResolver.Resolve<IWindowFactory>().ShowErrorDialog($"Internal error: {e.Message}");
            }
        }
    }
}
