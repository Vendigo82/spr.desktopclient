﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.Commands
{
    public interface ICommandAction
    {
        void Action();
    }

    public interface ICommandAction<T>
    {
        void Action(T arg);
    }

    public interface ICommandAction<T1, T2>
    {
        void Action(T1 arg1, T2 arg2);
    }
}
