﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using SPR.UI.WebService.DataContract.Profile;

namespace SPR.UI.App.Abstractions.WebService
{
    public enum ChangePasswordResultType
    {
        Success,

        InvalidNewPassword,

        InvalidOldPassword
    }

    public class ChangePasswordResult
    {
        public ChangePasswordResultType Result { get; set; }

        public string ReasonCode { get; set; }
    }

    /// <summary>
    /// Provide access to user's profile
    /// </summary>
    public interface IProfileService
    {
        /// <summary>
        /// Current user profile information
        /// </summary>
        Task<Profile> Profile(CancellationToken ct);

        /// <summary>
        /// Edit current user profile
        /// </summary>
        /// <param name="name">users name</param>
        /// <param name="ct"></param>
        /// <returns>Updated profile</returns>
        Task<Profile> Edit(string name, CancellationToken ct);

        /// <summary>
        /// Change user's password
        /// </summary>
        Task<ChangePasswordResult> ChangePassword(string oldpsw, string newpsw, CancellationToken ct);
    }
}
