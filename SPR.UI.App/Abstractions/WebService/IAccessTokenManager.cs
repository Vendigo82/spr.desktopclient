﻿
namespace SPR.UI.App.Abstractions.WebService
{
    public interface IAccessTokenManager
    {
        void SetToken(string token);
    }
}
