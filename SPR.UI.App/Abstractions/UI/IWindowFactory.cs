﻿using SPR.DAL.Enums;
using SPR.UI.App.Abstractions.VM;
using System;
using System.Windows;
using System.Windows.Controls;

namespace SPR.UI.App.Abstractions.UI
{
    /// <summary>
    /// Create windows and views
    /// </summary>
    public interface IWindowFactory
    {
        /// <summary>
        /// Returns application main window
        /// </summary>
        Window MainWindow { get; }

        /// <summary>
        /// Create window by name
        /// </summary>
        /// <param name="name">Window name</param>
        /// <returns></returns>
        Window CreateWindow(string name);

        Window CreateWindow(string name, string contentName);

        /// <summary>
        /// Create view by name
        /// </summary>
        /// <param name="name">View name</param>
        /// <returns></returns>
        Control CreateView(string name);

        /// <summary>
        /// Create view model by name
        /// </summary>
        /// <param name="name">name of the ViewModel</param>
        /// <returns></returns>
        ICommonVM CreateVM(string name);

        ICommonVM CreateVM<T>(string name, T constructorParam);

        /// <summary>
        /// Create view for formula
        /// </summary>
        /// <param name="id">Formula id</param>
        /// <param name="type">Type of the formula</param>
        /// <returns></returns>
        Control CreateFormulaView(FormulaType type, Guid? id);

        /// <summary>
        /// Create view for scenarios
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Control CreateScenarioView(Guid? id);

        /// <summary>
        /// Create view for standard results
        /// </summary>
        /// <param name="id">Standard results' id</param>
        /// <returns></returns>
        Control CreateStdResultsView(Guid? id);

        /// <summary>
        /// Create tab container with content
        /// May be not this method needed, just create View with tabcontainer together
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        Control CreateTabContainer(UIElement content);        

        /// <summary>
        /// Show message box with text and single button "OK"
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        void ShowInformationDialog(string text, string caption = null);

        /// <summary>
        /// Show message box with confirm query
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        bool ShowConfirmDialog(string text, string caption = null);

        /// <summary>
        /// Show message box with error text
        /// </summary>
        /// <param name="error"></param>
        void ShowErrorDialog(string error);
    }

    public static class IWindowFactoryExtentions
    {
        /// <summary>
        /// Create window and set Owner property to MainWindow
        /// </summary>
        /// <param name="factory"></param>
        /// <param name="name">Window name</param>
        /// <returns></returns>
        public static Window CreateWindowOwned(this IWindowFactory factory, string name) {
            Window wnd = factory.CreateWindow(name);
            wnd.Owner = factory.MainWindow;
            if (wnd.Owner != null) {
                wnd.MaxHeight = wnd.Owner.Height;
                wnd.MaxWidth = wnd.Owner.Width;
            }
            return wnd;
        }

        public static Window CreateWindowOwned(this IWindowFactory factory, string name, string contentName) {
            Window wnd = factory.CreateWindow(name, contentName);
            wnd.Owner = factory.MainWindow;
            if (wnd.Owner != null) {
                wnd.MaxHeight = wnd.Owner.Height;
                wnd.MaxWidth = wnd.Owner.Width;
            }
            return wnd;
        }

        public static Window CreateWindowVM(this IWindowFactory factory, string name)
        {
            var context = factory.CreateVM(name);
            var wnd = factory.CreateWindow(name);
            wnd.DataContext = context;
            wnd.Owner = factory.MainWindow;

            if (context is IDataInitializer di)
                di.Initialize(true);

            return wnd;
        }

        public static bool ShowSelectItemDialog<T>(this IWindowFactory factory, string wndName, bool byValue, out T value)
        {
            var wnd = factory.CreateWindowOwned(wndName);
            if (wnd.ShowDialog() == true) {
                if (byValue)
                    value = (T)wnd.GetPropertyValue<object>("SelectedValue");
                else
                    value = (T)wnd.GetPropertyValue<object>("SelectedItem");
                return true;
            } else
                value = default;

            return false;
        }
    }
}
