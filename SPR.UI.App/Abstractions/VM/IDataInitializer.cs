﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.VM
{
    /// <summary>
    /// provide interface for data initialization
    /// </summary>
    public interface IDataInitializer
    {
        /// <summary>
        /// Perform initialization
        /// </summary>
        /// <param name="fresh">need to refresh data</param>
        /// <returns></returns>
        Task Initialize(bool fresh = false);
    }
}
