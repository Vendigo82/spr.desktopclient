﻿using System;

namespace SPR.UI.App.Abstractions.VM
{
    public interface IDataObjectVMFactory<T> where T : class
    {
        ITestVM CreateTestVM(IDataObjectVM<T> vm);

        ICommonVM CreateJournalVM(Guid guid);

        ICommonVM CreateUsageVM(Guid guid);
    }
}
