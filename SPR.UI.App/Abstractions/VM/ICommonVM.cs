﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace SPR.UI.App.Abstractions.VM
{
    public interface ISelectedItemAccessor<T>
    {
        T SelectedItem { get; }
    }

    /// <summary>
    /// Interface for common VM 
    /// </summary>
    public interface ICommonVM : INotifyPropertyChanged
    {
        /// <summary>
        /// VM busy now
        /// </summary>
        bool IsBusy { get; }

        /// <summary>
        /// VM's object was changed
        /// </summary>
        bool IsChanged { get; }

        /// <summary>
        /// Save command
        /// </summary>
        ICommand SaveCmd { get; }

        /// <summary>
        /// Refresh command
        /// </summary>
        ICommand RefreshCmd { get; }

        /// <summary>
        /// There was been error
        /// </summary>
        bool HasError { get; }

        /// <summary>
        /// Last error
        /// </summary>
        Exception Error { get; }

        /// <summary>
        /// Last error message
        /// </summary>
        string ErrorMessage { get; }
    }
}
