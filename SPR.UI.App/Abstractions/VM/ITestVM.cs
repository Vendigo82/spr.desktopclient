﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SPR.UI.App.Abstractions.VM
{
    /// <summary>
    /// Interface for test object VM
    /// </summary>
    public interface ITestVM : ICommonVM
    {
        ICommand TestCmd { get; }
    }
}
