﻿using SPR.UI.App.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Abstractions.VM
{
    public interface IDataObjectVM<TItem> : ICommonVM, DataContract.ISPRItem where TItem : class
    {
        TItem Item { get; }

        /// <summary>
        /// Current selected object
        /// </summary>
        ShownObjectEnum ShownObject { get; }
    }
}
