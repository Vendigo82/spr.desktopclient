﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.VM;
using SPR.UI.App.VM.Base;
using SPR.UI.Core.UseCases.StepConstraints;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.UI.App.Windows.StepConstraints
{

    public class StepConstraintsVM : BusyVMBase
    {
        private readonly IStepConstraintsRepository repository;
        private readonly StepConstraintsOpenArgs args;

        private int version = 0;

        public StepConstraintsVM(StepConstraintsOpenArgs args, IStepConstraintsRepository repository, ILogger logger)
            : base(logger, changeable: true, refreshable: true, resetIsChangedOnRefresh: true, refreshAfterSave: true)
        {
            this.repository = repository;
            this.args = args;
            Rows.CollectionChanged += RowsCollectionChanged;
        }

        public int Version => version;

        public string StepName => args.StepName;

        public ObservableCollection<ItemWrapper> Rows { get; } = new ObservableCollection<ItemWrapper>();

        protected override async Task Refresh()
        {
            var model = await repository.LoadLatestsAsync(args.StepName, args.Type, ct);
            version = model.Header.Version;

            Rows.Clear();
            foreach (var item in model.Items)
            {
                var wrapper = new ItemWrapper(item);
                wrapper.PropertyChanged += ItemChanged;
                Rows.Add(wrapper);
            }
        }

        protected override async Task Save()
        {
            var model = new StepConstraintsModel
            {
                Header = new StepConstraintsHeaderModel { Version = version },
                Items = Rows.Select(i => i.Item).ToArray()
            };

            await repository.SaveAsync(args.StepName, args.Type, model);
        }

        private void RowsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var i in e.NewItems.Cast<ItemWrapper>())
                {
                    IsChanged = true;
                    i.PropertyChanged += ItemChanged;
                }
            }

            if (e.OldItems != null)
            {
                foreach (var i in e.OldItems.Cast<ItemWrapper>())
                {
                    IsChanged = true;
                    i.PropertyChanged -= ItemChanged;
                }
            }
        }

        private void ItemChanged(object sender, PropertyChangedEventArgs e)
        {
            IsChanged = true;
        }

        public class ItemWrapper : NotifyPropertyChangedBase
        {
            private readonly StepConstraintItemModel item;

            public ItemWrapper(StepConstraintItemModel item)
            {
                this.item = item;
            }

            public ItemWrapper()
            {
                item = new StepConstraintItemModel()
                {
                    Expression = string.Empty,
                    Message = string.Empty
                };
            }

            public StepConstraintItemModel Item => item;

            public bool Error { get => item.Error; set { item.Error = value; NotifyAllPropertiesChanged(); } }

            public string Expression { get => item.Expression; set { item.Expression = value; NotifyAllPropertiesChanged(); } }

            public string Message { get => item.Message; set { item.Message = value; NotifyPropertyChanged(); } }
        }
    }
}
