﻿using SPR.UI.Core.UseCases.StepConstraints;

namespace SPR.UI.App.Windows.StepConstraints
{
    public class StepConstraintsOpenArgs
    {
        public string StepName { get; set; } 

        public StepConstraintType Type { get; set; }
    }
}
