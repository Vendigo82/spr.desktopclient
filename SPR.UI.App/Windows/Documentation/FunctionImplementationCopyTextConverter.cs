﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace SPR.UI.App.Windows.Documentation
{
    internal class FunctionImplementationCopyTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DocFunctionsListVM.FunctionImplementationViewModel vm)
                return $"{vm.SystemName}({string.Join(", ", vm.Args.Select(i => $"<{i.Name}:{i.Type}>"))})";

            throw new NotSupportedException($"FunctionImplementationSignatureConverter only support {nameof(DocFunctionsListVM.FunctionImplementationViewModel)} type");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
