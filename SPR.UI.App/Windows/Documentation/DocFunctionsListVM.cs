﻿using SPR.DocClient;
using SPR.DocClient.Models;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Services;
using SPR.UI.App.VM.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.UI.App.Windows.Documentation
{
    public class DocFunctionsListVM : BusyVMBase
    {
        private readonly IDocRepository repository;
        private FunctionName selectedFunctionName = FunctionName.Empty;
        private GroupName selectedGroupName = GroupName.Empty;
        private FunctionInfoViewModel selectedFunctionInfo = null;

        public DocFunctionsListVM(
            IDocRepository repository,
            ILogger logger) 
            : base(logger, changeable: false, refreshable: true, resetIsChangedOnRefresh: false)
        {
            this.repository = repository;
        }

        public ObservableCollection<FunctionBriefInfo> Functions { get; } = new ObservableCollection<FunctionBriefInfo>();

        public ObservableCollection<GroupInfo> Groups { get; } = new ObservableCollection<GroupInfo>();
        
        public FunctionName SelectedFunctionName
        {
            get => selectedFunctionName;
            set
            {
                selectedFunctionName = value ?? FunctionName.Empty;
                _ = PerformBackgroudOperation(() => LoadFunctionAsync());
            }
        }

        public GroupName SelectedGroupName
        {
            get => selectedGroupName;
            set
            {
                selectedGroupName = value ?? GroupName.Empty;
                NotifyPropertyChanged();
                _ = PerformBusyOperation(ReloadFunctionsAsync);
            }
        }

        public FunctionInfoViewModel SelectedFunctionInfo { get => selectedFunctionInfo; private set { selectedFunctionInfo = value; NotifyPropertyChanged(); } }

        private bool loadingContent = false;
        public bool LoadingContent { get => loadingContent; private set { loadingContent = value; NotifyPropertyChanged(); } }

        protected override async Task Refresh()
        {
            var taskGroups = repository.GetGroupsAsync();
            var taskFunctionsList = repository.GetFunctionsAsync();
            await Task.WhenAll(taskGroups, taskFunctionsList);

            FillFunctions(taskFunctionsList.Result.Items);
            FillGroups(taskGroups.Result.Items);
        }

        private async Task ReloadFunctionsAsync()
        {
            var functions = await repository.GetFunctionsAsync(new FunctionsFilter { Group = selectedGroupName });
            FillFunctions(functions.Items);
        }

        private void FillFunctions(IEnumerable<FunctionBriefInfo> functionsList)
        {
            Functions.Clear();
            foreach (var i in functionsList)
                Functions.Add(i);
        }

        private void FillGroups(IEnumerable<GroupInfo> groups)
        {
            Groups.Clear();
            Groups.Add(new GroupInfo(GroupName.Empty, new Title("*")));
            foreach (var g in groups)
                Groups.Add(g);
        }

        private async Task LoadFunctionAsync()
        {
            if (selectedFunctionName == FunctionName.Empty)
                return;

            try
            {
                LoadingContent = true;
                var info = await repository.GetFunctionAsync(selectedFunctionName);

                SelectedFunctionInfo = new FunctionInfoViewModel
                {
                    Title = info.Title,
                    SystemName = info.SystemName,
                    Implementations = info.Implementations.Select(i => new FunctionImplementationViewModel
                    {
                        Args = i.Args,
                        Description = i.Description,
                        Return = i.Return,
                        SystemName = info.SystemName
                    })
                };
            } finally
            {
                LoadingContent = false;
            }
        }

        public class FunctionInfoViewModel
        {
            public FunctionName SystemName { get; set; }

            public Title Title { get; set; }
           
            public IEnumerable<FunctionImplementationViewModel> Implementations { get; set; }
        }

        public class FunctionImplementationViewModel : FunctionImplementationInfo
        {
            public FunctionName SystemName { get; set; }
        }
    }
}
