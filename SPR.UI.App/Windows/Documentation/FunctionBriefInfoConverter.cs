﻿using SPR.DocClient.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace SPR.UI.App.Windows.Documentation
{
    public class FunctionBriefInfoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is FunctionBriefInfo info)
                return $"{info.SystemName} - {info.Title}";
            else
                throw new NotSupportedException($"FunctionBriefInfoConverter supports only {nameof(FunctionBriefInfo)} type");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
