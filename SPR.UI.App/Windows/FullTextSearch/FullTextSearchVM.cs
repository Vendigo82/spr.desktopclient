﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.VM;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.Windows.FullTextSearch
{
    public class FullTextSearchVM : BusyVMBase
    {
        private readonly IFullTextSearchSource _searchSource;
        private string _text = string.Empty;
        private int _minLength;
        private bool _settingsLoaded;

        public FullTextSearchVM(IFullTextSearchSource searchSource, ILogger logger) 
            : base(logger, changeable: false, refreshable: false, resetIsChangedOnRefresh: false)
        {
            _searchSource = searchSource ?? throw new ArgumentNullException(nameof(searchSource));
            SearchCmd = new AsyncCommand(() => PerformBusyOperation(SearchRoutineAsync), () => IsBusy == false && _text.Length >= _minLength, ErrorHandler);
        }

        /// <summary>
        /// Searching fragment
        /// </summary>
        public string Text { 
            get => _text; 
            set { 
                _text = value; 
                NotifyPropertyChanged();
                RaiseCommandCanExecuteChanged();
            } 
        }

        /// <summary>
        /// Search text minimal length
        /// </summary>
        public int MinLength { get => _minLength; private set { _minLength = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// Is <see cref="MinLength"/> was loaded
        /// </summary>
        public bool SettingsLoaded { get => _settingsLoaded; private set { _settingsLoaded = value; NotifyPropertyChanged(); } }

        /// <summary>
        ///  List of formulas
        /// </summary>
        public ObservableCollection<ItemWrapper> Items { get; } = new ObservableCollection<ItemWrapper>();

        /// <summary>
        /// Search button
        /// </summary>
        public ICommand SearchCmd { get; }

        private async Task SearchRoutineAsync()
        {
            var searchingText = _text;
            var list = await _searchSource.SearchAsync(searchingText);
            Items.Clear();

            var items = list.Select(i => new ItemWrapper(i, searchingText, Enumerable.Empty<string>())).ToArray();

            foreach (var item in items)
                Items.Add(item);

            if (items.Any()) {
                var _ = LoadFragmentsAsync();
            }

            // load code fragments for all formulas
            Task LoadFragmentsAsync()
            {
                var tasks = new List<Task>();
                foreach (var item in items) {
                    tasks.Add(LoadFormulaFragmentAsync(item));
                }

                return Task.WhenAll(tasks);
            }            
        }

        /// <summary>
        /// load code fragments for single formula
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private async Task LoadFormulaFragmentAsync(ItemWrapper item)
        {
            try {
                item.Loading = true;
                item.ErrorMessage = null;

                var fragments = await _searchSource.GetFormulaFragments(item.Formula.Guid, item.Text, ct);

                item.Fragments = fragments;
            } catch (Exception e) {
                _logger.LogError("Load formula fragments error", e);
                item.ErrorMessage = e.Message;
            } finally {
                item.Loading = false;
            }
        }

        protected override Task Refresh()
        {
            var _ = LoadOptionsAsync();
            return Task.CompletedTask;            
        }

        private async Task LoadOptionsAsync()
        {
            var response = await _searchSource.GetOptionsAsync();
            MinLength = response.MinLength;
            SettingsLoaded = true;
            RaiseCommandCanExecuteChanged();
        }

        private void RaiseCommandCanExecuteChanged()
        {
            ((AsyncCommand)SearchCmd).RaiseCanExecuteChanged();
        }

        public class ItemWrapper : NotifyPropertyChangedBase
        {
            private string _error;
            private bool _loading;
            private IEnumerable<string> _fragments;

            public ItemWrapper(FormulaBriefModel formula, string text, IEnumerable<string> fragments)
            {
                Formula = formula ?? throw new ArgumentNullException(nameof(formula));
                Text = text ?? throw new ArgumentNullException(nameof(text));
                Fragments = fragments ?? throw new ArgumentNullException(nameof(fragments));
                Loading = true;
                Fragments = new[] { "z", "b" };
            }

            public FormulaBriefModel Formula { get; }

            public string Text { get; }
            
            public bool Loading { get => _loading; set { _loading = value; NotifyPropertyChanged(); } }

            public IEnumerable<string> Fragments { get => _fragments; set { _fragments = value; NotifyPropertyChanged(); } }
            
            public bool HasError => _error != null;

            public string ErrorMessage { get => _error; set { _error = value; NotifyPropertyChanged(); NotifyPropertyChanged(nameof(_error)); } }
        }
    }
}
