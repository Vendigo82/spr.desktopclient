﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SPR.UI.App.Windows.Main.SelectInstanceDlg
{
    /// <summary>
    /// Interaction logic for SelectInstanceDlg.xaml
    /// </summary>
    public partial class SelectInstanceDlg : Window
    {
        public SelectInstanceDlg()
        {
            InitializeComponent();
        }

        private void Button_OkClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;            
        }
    }
}
