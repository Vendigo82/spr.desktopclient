﻿using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Services.WebService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;

namespace SPR.UI.App.Windows.Main.SelectInstanceDlg
{
    public class SelectInstanceVM : ICommonVM, ISelectedItemAccessor<InstanceSettingsModel>
    {
        public SelectInstanceVM(IInstancesSource source)
        {
            Items = source.GetInstances();
            SelectedItem = Items.FirstOrDefault();
        }

        public IEnumerable<InstanceSettingsModel> Items { get; }

        public InstanceSettingsModel SelectedItem { get; set; }

        #region ICommonVM
        public bool IsBusy => false;

        public bool IsChanged => false;

        public ICommand SaveCmd => null;

        public ICommand RefreshCmd => null;

        public bool HasError => false;

        public Exception Error => null;

        public string ErrorMessage => null;

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
