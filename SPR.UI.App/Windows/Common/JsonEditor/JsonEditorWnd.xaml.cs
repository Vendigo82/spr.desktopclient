﻿using System.Windows;

namespace SPR.UI.App.Windows.Common.JsonEditor
{
    /// <summary>
    /// Interaction logic for JsonEditorWnd.xaml
    /// </summary>
    public partial class JsonEditorWnd : Window
    {
        public JsonEditorWnd()
        {
            InitializeComponent();
            TextBox.Focus();
            this.SizeChanged += JsonEditorWnd_SizeChanged;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void JsonEditorWnd_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Width < e.PreviousSize.Width || e.NewSize.Height < e.PreviousSize.Height)
            {
                MinWidth = e.PreviousSize.Width;
                MinHeight = e.PreviousSize.Height;
            }
            if (e.HeightChanged)
                Top -= (e.NewSize.Height - e.PreviousSize.Height) / 2;
            if (e.WidthChanged)
                Left -= (e.NewSize.Width - e.PreviousSize.Width) / 2;
        }
    }
}
