﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SPR.UI.App.Windows.Base;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.Windows.Common.JsonEditor
{
    public class JsonEditorVM : StaticVMBase
    {
        private string value;

        public JsonEditorVM(string value)
        {
            BeautifyCmd = new RelayCommand(JsonBeautify);
            CompressCmd = new RelayCommand(JsonCompress);
            this.value = value;
            ValidateJson();            
        }

        public string Value
        {
            get => value;
            set { this.value = value; ValidateJson(); NotifyPropertyChanged(); }
        }

        public bool IsValidJson { get; private set; }

        public string ValidateJsonError { get; private set; }

        public ICommand BeautifyCmd { get; }

        public ICommand CompressCmd { get; }

        private void ValidateJson()
        {
            if (string.IsNullOrEmpty(value))
            {
                UpdateValidJsonProperties(true);
                return;
            }

            try
            {
                var _ = JToken.Parse(value);
                UpdateValidJsonProperties(true);
            }
            catch (JsonException e)
            {
                UpdateValidJsonProperties(false, e.Message);
            }
        }

        private void UpdateValidJsonProperties(bool valid, string error = null)
        {
            IsValidJson = valid;
            ValidateJsonError = valid ? string.Empty : error;

            NotifyPropertyChanged(nameof(IsValidJson));
            NotifyPropertyChanged(nameof(ValidateJsonError));
        }

        private void JsonBeautify() => JsonFormat(Formatting.Indented);

        private void JsonCompress() => JsonFormat(Formatting.None);

        private void JsonFormat(Formatting formatting)
        {
            try
            {

                Value = JToken.Parse(Value).ToString(formatting);
            }
            catch
            {
            }
        }
    }
}
