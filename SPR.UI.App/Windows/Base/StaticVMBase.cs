﻿using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.VM;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace SPR.UI.App.Windows.Base
{
    public class StaticVMBase : NotifyPropertyChangedBase, ICommonVM
    {
        public bool IsBusy => false;

        public bool IsChanged => false;

        public ICommand SaveCmd => null;

        public ICommand RefreshCmd => null;

        public bool HasError => false;

        public Exception Error => null;

        public string ErrorMessage => null;
    }
}
