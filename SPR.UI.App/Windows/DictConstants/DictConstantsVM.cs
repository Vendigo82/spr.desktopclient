﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.VM;
using SPR.UI.App.VM.Base;
using SPR.UI.App.VM.Common;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Enums;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.Windows.DictConstants
{
    public class DictConstantsVM : BusyVMBase
    {
        private readonly IConstantsSource _service;

        /// <summary>
        /// list of items which loaded from server
        /// </summary>
        private IEnumerable<string> _loadedItems = null;        

        private ItemWrapper _selectedItem = null;

        public DictConstantsVM(IConstantsSource service, ILogger logger)
            : base(logger, changeable: true, refreshable: true, resetIsChangedOnRefresh: true)
        {
            _service = service;
            AddCmd = new RelayCommand(AddRoutine, () => !IsBusy);
            DeleteCmd = new RelayCommand(() => Items.Remove(SelectedItem), () => IsBusy == false && SelectedItem != null);
            RemoveFromDeletingCmd = new RelayCommand<ItemWrapper>(RemoveFromDeleting);

            Items.CollectionChanged += Items_CollectionChanged;
        }

        public ICommand AddCmd { get; }

        public ICommand DeleteCmd { get; }

        protected override async Task Refresh()
        {
            var items = new List<ParamModel>();
            string token = null;
            do {
                var response = await _service.GetItemsAsync(token, null, ct);
                items.AddRange(response.Item1);
                token = response.Item2;
            } while (token != null);
            
            _loadedItems = items.Select(i => i.Name).ToList();
            Items.Clear();
            DeletingItems.Clear();

            foreach (var item in items)
                Items.Add(new ItemWrapper(item));
            SubscribeCollectionSetIsChanged(Items);
        }

        protected override async Task Save()
        {
            foreach (var item in Items)
                item.Error = null;

            var existedItems = _loadedItems;
            var currentItems = Items.ToDictionary(i => i.Name);

            var itemsToDelete = existedItems.Where(i => !currentItems.ContainsKey(i)).Select(i => i).ToList();
            var itemsToInsert = Items.Where(i => !existedItems.Contains(i.Name)).ToList();
            var itemsToUpdate = Items.Where(i => i.IsChanged).Where(i => existedItems.Contains(i.Name)).ToList();
            
            if (!(itemsToDelete.Any() || itemsToInsert.Any() || itemsToUpdate.Any())) {
                DeletingItems.Clear();
                return;
            }

            var delete = itemsToDelete.Select(i => new BunchUpdateItemModel<string, ParamModel> {
                Id = i,
                Operation = BunchOperation.Delete
            });
            var insert = itemsToInsert.Select(i => new BunchUpdateItemModel<string, ParamModel> {
                Id = i.Name, 
                Item = i.Item, 
                Operation = BunchOperation.Insert
            });
            var update = itemsToUpdate.Select(i => new BunchUpdateItemModel<string, ParamModel> {
                Id = i.Name,
                Item = i.Item,
                Operation = BunchOperation.Update
            });

            try {
                await _service.BulkUpdateAsync(delete.Concat(insert).Concat(update), ct);

                _loadedItems = currentItems.Select(i => i.Value.Item).Select(i => i.Name).ToList();
                foreach (var item in Items)
                    item.Reset();

                DeletingItems.Clear();
            } catch (Exceptions.UpdateItemsException e) {
                foreach (var err in e.Errors) {
                    var item = Items.FirstOrDefault(i => i.Name == err.Key) ?? DeletingItems.FirstOrDefault(i => i.Name == err.Key);
                    if (item != null)
                        item.Error = err.Value;
                    else                        
                        throw;                    
                }
            }
        }

        private void AddRoutine()
        {
            var item = new ItemWrapper(new ParamModel { Value = string.Empty });
            Items.Add(item);
            SelectedItem = item; 
        }

        public ObservableCollection<ItemWrapper> Items { get; } = new ObservableCollection<ItemWrapper>();

        public ObservableCollection<ItemWrapper> DeletingItems { get; } = new ObservableCollection<ItemWrapper>();

        public ICommand RemoveFromDeletingCmd { get; }

        private void RemoveFromDeleting(ItemWrapper item)
        {
            if (DeletingItems.Remove(item) && Items.IndexOf(item) < 0) {
                Items.Add(item);
            }
        }

        public ItemWrapper SelectedItem { 
            get => _selectedItem; 
            set { 
                _selectedItem = value; 
                NotifyPropertyChanged();
                (DeleteCmd as RelayCommand).RaiseCanExecuteChanged();
            } 
        }

        public class ItemWrapper : NotifyPropertyChangedBase
        {
            private readonly ParamModel _item;
            private bool _isChanged = false;
            private string _error = null;

            public ItemWrapper()
            {
                _isChanged = true;
                _item = new ParamModel();
            }

            public ItemWrapper(ParamModel item)
            {
                _item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public ParamModel Item => _item;

            public string Name { 
                get => _item.Name; 
                set {
                    _item.Name = value;
                    IsChanged = true;
                    NotifyPropertyChanged(); 
                } 
            }

            public string Value { 
                get => _item.Value; 
                set {
                    _item.Value = value;
                    IsChanged = true;
                    NotifyPropertyChanged(); 
                } 
            }

            public SPRDataType Type { 
                get => _item.Type; 
                set { 
                    _item.Type = value;
                    IsChanged = true;
                    NotifyPropertyChanged();                     
                } 
            }

            public bool IsChanged { get => _isChanged; set { _isChanged = value; NotifyPropertyChanged(); } }

            public string Error { get => _error; set { _error = value; NotifyPropertyChanged(); } }

            public void Reset() => IsChanged = false;

            public ICommand RevokeDeleteCmd { get; set; }
        }

        private void ListItemChanged(object sender, PropertyChangedEventArgs args) => IsChanged = true;        

        /// <summary>
        /// Called when collection of items was changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<ItemWrapper>()) {
                    i.PropertyChanged += ListItemChanged;
                    i.RevokeDeleteCmd = RemoveFromDeletingCmd;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<ItemWrapper>()) {
                    i.PropertyChanged -= ListItemChanged;
                    DeletingItems.Add(i);
                }
            }

            if (e.NewItems != null || e.OldItems != null)
                IsChanged = true;
        }
    }
}
