﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebClient.DataTypes
{
    /// <summary>
    /// Formula's filter
    /// </summary>
    public class FormulaFilter
    {
        /// <summary>
        /// Filter by name fraction
        /// </summary>
        public string NamePart { get; set; }

        /// <summary>
        /// Filter by code
        /// </summary>
        public long? Code { get; set; }

        /// <summary>
        /// Filter by formula type
        /// </summary>
        public DAL.Enums.FormulaType? FormulaType { get; set; }

        /// <summary>
        /// Filter by checkup flag
        /// </summary>
        public bool? CanBeCheckup { get; set; }

        /// <summary>
        /// Formula not used anywhere
        /// </summary>
        public bool? NotUsed { get; set; }
    }
}
