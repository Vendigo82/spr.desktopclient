﻿using System;

namespace SPR.UI.WebClient.DataTypes
{
    public class PeriodFilter
    {
        /// <summary>
        /// Period of time
        /// </summary>
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// Period of time
        /// </summary>
        public DateTime? DateTo { get; set; }

    }
}
