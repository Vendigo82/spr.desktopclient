﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebClient.DataTypes
{
    public class Pagination
    {
        public int? Count { get; set; }

        public long? StartFromId { get; set; }
    }
}
