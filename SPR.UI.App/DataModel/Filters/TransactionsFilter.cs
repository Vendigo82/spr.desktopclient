﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebClient.DataTypes
{
    public class TransactionsFilter : PeriodFilter
    {
        /// <summary>
        /// With errors
        /// </summary>
        public bool? WithError { get; set; }

        /// <summary>
        /// Input step
        /// </summary>
        public string Step { get; set; }

        /// <summary>
        /// Request's ip-address
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Scenario code
        /// </summary>
        public long? ScenarioCode { get; set; }

        /// <summary>
        /// Additional filter values
        /// </summary>
        public IEnumerable<KeyValuePair<string, string>> Values { get; set; }
    }
}
