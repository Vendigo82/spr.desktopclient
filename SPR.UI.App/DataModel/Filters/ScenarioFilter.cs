﻿
namespace SPR.UI.WebClient.DataTypes
{
    /// <summary>
    /// Scenarios filter
    /// </summary>
    public class ScenarioFilter
    {
        /// <summary>
        /// Filter by name fraction
        /// </summary>
        public string NamePart { get; set; }

        /// <summary>
        /// Filter by code
        /// </summary>
        public long? Code { get; set; }

        /// <summary>
        /// Filter by step
        /// </summary>
        public string Step { get; set; }
    }
}
