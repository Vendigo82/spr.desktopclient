﻿using SPR.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebClient.DataTypes
{
    public class JournalFilter : PeriodFilter
    {
        public SerializedObjectType? ObjectType { get; set; }

        public Guid? Guid { get; set; }
    }
}
