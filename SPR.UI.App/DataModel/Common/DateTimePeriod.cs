﻿using SPR.UI.WebClient.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.DataModel.Common
{
    /// <summary>
    /// Contains period of time
    /// </summary>
    public class DateTimePeriod
    {
        private Tuple<DateTime, DateTime> period;
        //DateTime first;
        //DateTime second;

        public DateTimePeriod()
        {
            Reset();
        }

        /// <summary>
        /// Fill filter data with period
        /// </summary>
        /// <param name="filter"></param>
        public void FillFilter(PeriodFilter filter)
        {
            if (period != null) {
                filter.DateFrom = DateFrom;
                filter.DateTo = DateTo;
            }
        }

        /// <summary>
        /// Access to period
        /// </summary>
        public Tuple<DateTime, DateTime> Period => period;

        public DateTime DateFrom => period.Item1 < period.Item2 ? period.Item1 : period.Item2;
        public DateTime DateTo => period.Item1 < period.Item2 ? period.Item2 : period.Item1;

        /// <summary>
        /// Reset period to default value 
        /// </summary>
        /// <returns>True if period was changed</returns>
        public bool Reset()
        {
            var dt = DateTime.Now.Date;
            var newPeriod = new Tuple<DateTime, DateTime>(dt - TimeSpan.FromDays(6), dt + TimeSpan.FromDays(1));
            return Set(newPeriod);
        }

        /// <summary>
        /// Set new period
        /// </summary>
        /// <param name="value">New period value</param>
        /// <returns>True if period was changed</returns>
        public bool Set(Tuple<DateTime, DateTime> value)
        {
            bool update = false;
            if (period != null && value != null) {
                if (!period.Equals(value))
                    update = true;
            } else if (period != null || value != null)
                update = true;

            if (update)
                period = value;

            return update;
        }                    
    }
}
