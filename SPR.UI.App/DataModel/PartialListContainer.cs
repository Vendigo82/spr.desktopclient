﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.DataModel
{
    public class PartialListContainer<T>
    {
        public bool Exhausted { get; set; }

        public IEnumerable<T> Items { get; set; }

        //public long? NextRequest
    }
}
