﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebClient
{
    public static class DictionaryNames
    {
        public const string Step = "step";
        public const string RejectReason = "rejectreason";
        public const string Server = "server";
        public const string SqlServer = "sqlserver";
        public const string GoogleBQ = "googlebq";
    }
}
