﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SPR.UI.App.DataContract
{
    public class DateTimePeriod
    {
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
    }

    public interface IPeriodVM : INotifyPropertyChanged
    {
        DateTime DateBegin { get; set; }
        DateTime DateEnd { get; set; }

        DateTimePeriod Period { get; }

        ICommand RefreshCmd { get; }
    }

    public static class IPeriodVMExtentions
    {
        public static void SetPeriod(this IPeriodVM vm, DateTime begin, DateTime end) {
            vm.DateBegin = begin;
            vm.DateEnd = end;
            vm.RefreshCmd.Execute(null);            
        }
    }
}
