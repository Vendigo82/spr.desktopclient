﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.DataContract
{
    public enum ShownObjectEnum
    {
        Main,
        Draft,
        Archive
    }
}
