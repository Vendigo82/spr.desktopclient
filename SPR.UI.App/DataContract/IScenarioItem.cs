﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.DataContract
{
    /// <summary>
    /// Scenario item, temporary interface. Will be deleted
    /// </summary>
    public interface IScenarioItem
    {
        /// <summary>
        /// Scenario or draft code
        /// </summary>
        long Id { get; }

        Guid Guid { get; }

        string Name { get; }
    }
}
