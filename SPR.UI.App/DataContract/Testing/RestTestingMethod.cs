﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.DataContract.Testing
{
    /// <summary>
    /// Testing REST formula methods
    /// </summary>
    public enum RestTestingMethod
    {
        /// <summary>
        /// Perform http request on server side
        /// </summary>
        ServerSide,

        /// <summary>
        /// Perform http request on local side
        /// </summary>
        LocalSide,

        /// <summary>
        /// View http request without sending it
        /// </summary>
        MakeRequest
    }
}
