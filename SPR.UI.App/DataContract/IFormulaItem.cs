﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.DAL.Enums;

namespace SPR.UI.App.DataContract
{
    /// <summary>
    /// Formula information, temporary interface. Will be deleted
    /// </summary>
    public interface IFormulaItem
    {
        long Id { get; }

        /// <summary>
        /// Formula guid
        /// </summary>
        Guid Guid { get; }

        /// <summary>
        /// Formula type
        /// </summary>
        FormulaType Type { get; }

        string Name { get; }
    }
}
