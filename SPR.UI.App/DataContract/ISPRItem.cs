﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.DataContract
{
    public interface ISPRItem : INotifyPropertyChanged
    {
        long ID { get; }
        int Ver { get; }
        Guid Guid { get; }
        string Name { get; }
    }
}
