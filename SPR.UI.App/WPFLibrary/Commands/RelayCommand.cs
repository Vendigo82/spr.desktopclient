﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VenSoft.WPFLibrary.Commands
{
    public class RelayCommand : ICommand {
        private readonly Action _execteMethod;
        private readonly Func<bool> _canExecuteMethod;

        public RelayCommand(Action execute, Func<bool> canExecute = null) {
            _execteMethod = execute;
            _canExecuteMethod = canExecute;
        }

        public bool CanExecute(object parameter) => _canExecuteMethod?.Invoke() ?? true;

        public event EventHandler CanExecuteChanged {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public void Execute(object parameter) => _execteMethod();

        public void RaiseCanExecuteChanged() {
            CommandManager.InvalidateRequerySuggested();
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _executeMethod;
        private readonly Func<T, bool> _canExecuteMethod;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null) {
            _executeMethod = execute;
            _canExecuteMethod = canExecute;
        }

        public bool CanExecute(object parameter) => _canExecuteMethod?.Invoke((T) parameter) ?? true;

        public event EventHandler CanExecuteChanged {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public void Execute(object parameter) => _executeMethod((T)parameter);

        public void RaiseCanExecuteChanged() {
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
