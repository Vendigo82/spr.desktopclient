﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VenSoft.WPFLibrary.Commands
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }

    public abstract class AsyncCommandBase : IAsyncCommand
    {
        private Action<Exception> _errorHandler;
        private bool _isExecuting = false;

        protected AsyncCommandBase(Action<Exception> errorHandler) {
            _errorHandler = errorHandler;
        }

        public bool CanExecute(object parameter) => !_isExecuting && CanExecuteCommand(parameter);

        protected abstract bool CanExecuteCommand(object parameter);

        public abstract Task ExecuteAsync(object parameter);

        public async void Execute(object parameter) {
            if (CanExecute(parameter)) {
                try {
                    _isExecuting = true;
                    await ExecuteAsync(parameter);
                } catch (Exception e) {
                    _errorHandler?.Invoke(e);
                } finally {
                    _isExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        public event EventHandler CanExecuteChanged {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void RaiseCanExecuteChanged() {
            CommandManager.InvalidateRequerySuggested();
        }
    }

    public class AsyncCommand : AsyncCommandBase
    {
        private readonly Func<Task> _execute;
        private readonly Func<bool> _canExecute;
        

        public AsyncCommand(Func<Task> execute, Func<bool> canExecute = null, Action<Exception> errorHandler = null)
            : base (errorHandler){
            _execute = execute;
            _canExecute = canExecute;
        }

        protected override bool CanExecuteCommand(object parameter) => _canExecute?.Invoke() ?? true;

        public override async Task ExecuteAsync(object parameter) => await _execute();
    }

    public class AsyncCommand<T> : AsyncCommandBase
    {
        private readonly Func<T, Task> _execute;
        private readonly Func<T, bool> _canExecute;

        public AsyncCommand(Func<T, Task> execute, Func<T, bool> canExecute = null, Action<Exception> errorHandler = null) 
            : base(errorHandler){
            _execute = execute;
            _canExecute = canExecute;
        }

        protected override bool CanExecuteCommand(object parameter) => (_canExecute?.Invoke((T)parameter) ?? true);

        public override async Task ExecuteAsync(object parameter) => await _execute((T)parameter);
    }    
}
