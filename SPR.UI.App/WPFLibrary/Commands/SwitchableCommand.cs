﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace VenSoft.WPFLibrary.Commands
{
    /// <summary>
    /// Command which can switch to anouther command
    /// </summary>
    public class SwitchableCommand : ICommand
    {
        private ICommand _command = null;

        public SwitchableCommand() {
        }

        public SwitchableCommand(ICommand command) {
            _command = command;
        }

        public ICommand Command { get => _command; set { SetCommand(value); } }

        public event EventHandler CanExecuteChanged {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter) => _command != null ? _command.CanExecute(parameter) : false;

        public async void Execute(object parameter) {
            if (_command == null)
                return;

            if (_command is IAsyncCommand asyncCommand)
                await asyncCommand.ExecuteAsync(parameter);
            else
                _command.Execute(parameter);
        }

        private void SetCommand(ICommand cmd) {
            if (cmd == _command)
                return;

            if (_command != null)
                _command.CanExecuteChanged -= _command_CanExecuteChanged;

            _command = cmd;

            if (_command != null)
                _command.CanExecuteChanged += _command_CanExecuteChanged;

            RaiseCanExecuteChanged();
        }

        private void _command_CanExecuteChanged(object sender, EventArgs e) {
            //RaiseCanExecuteChanged();
        }

        protected void RaiseCanExecuteChanged() {
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
