﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace VenSoft.WPFLibrary.MarkupExtentions
{
    public class EnumCollectionExtExtension : MarkupExtension
    {
        public Type EnumType { get; set; }

        public IValueConverter Converter { get; set; }

        public bool WithNull { get; set; } = false;

        public string NullCaption { get; set; } = "null";

        public override object ProvideValue(IServiceProvider _) {
            if (EnumType != null) {
                var list = CreateEnumValueList(EnumType, Converter);
                if (WithNull)
                    list.Insert(0, new KeyValuePair<object, object>(null, NullCaption));
                return list;
            }
            return default;
        }

        private static List<object> CreateEnumValueList(Type enumType, IValueConverter converter) {
            return Enum
                .GetValues(enumType)
                .Cast<object>()
                .Select(i => new KeyValuePair<object, object>(i, converter.Convert(i, null, null, null)))
                .Cast<object>()
                .ToList();            
        }
    }
}
