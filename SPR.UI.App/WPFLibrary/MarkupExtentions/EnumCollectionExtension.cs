﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace VenSoft.WPFLibrary.MarkupExtentions
{
    public class EnumCollectionExtension : MarkupExtension
    {
        public Type EnumType { get; set; }

        public IValueConverter Converter { get; set; }

        public override object ProvideValue(IServiceProvider _) {
            if (EnumType != null)
                return CreateEnumValueList(EnumType, Converter);
            return default;
        }

        private static List<object> CreateEnumValueList(Type enumType, IValueConverter converter) {
            return Enum.GetNames(enumType)
                .Select(name => Enum.Parse(enumType, name))                
                .ToList();
        }
    }
}
