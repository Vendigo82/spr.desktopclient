﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;

using VenSoft.WPFLibrary.Commands;
using SPR.DAL.Enums;
using SPR.UI.WebService.DataContract;
using SPR.UI.App.Abstractions;
using SPR.UI.App.DataContract;
using SPR.UI.App.Model.Special;
using SPR.UI.App.Services;
using SPR.UI.App.VM.Common;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.Core.Shared;

namespace SPR.UI.App.VM.Base
{
    public abstract class DataObjectVM<TItem> : BusyVMBase, IDataObjectVM<TItem> where TItem : class
    {
        /// <summary>
        /// Code property
        /// </summary>
        private static readonly PropertyInfo _piCode;

        private readonly IDataSource<TItem> _source;
        private readonly IConfirmActionCommentProvider _commentProvider;

        /// <summary>
        /// working version of object
        /// </summary>
        private TItem _main = null;

        /// <summary>
        /// Draft version of object
        /// </summary>
        private RefContainer<TItem> _draft;

        /// <summary>
        /// Archive version
        /// </summary>
        private TItem _archive = null;

        /// <summary>
        /// Current selected object
        /// </summary>
        private ShownObjectEnum? _shownObject = null;

        /// <summary>
        /// Type of object
        /// </summary>
        protected SerializedObjectType ObjectType { get; }

        /// <summary>
        /// New created item, is not saved yet
        /// </summary>
        protected bool IsNew { get; private set; }

        /// <summary>
        /// Current selected archive version number
        /// </summary>
        private int? _selectedArchiveVersion = null;

        static DataObjectVM() {
            _piCode = typeof(TItem).GetProperty("Code");            
        }

        public DataObjectVM(
            Guid? guid,
            SerializedObjectType objectType,
            IDataSource<TItem> source, 
            ILogger logger,
            IConfirmActionCommentProvider commentProvider) 
            : base(logger: logger, resetIsChangedOnRefresh: false) 
        {
            if (guid.HasValue) {
                Guid = guid.Value;
                IsNew = false;
            } else {
                Guid = Guid.NewGuid();
                IsNew = true;
            }
            ObjectType = objectType;
            _source = source;
            _commentProvider = commentProvider;
            _draft = new RefContainer<TItem>((s) => {
                (ClearDraftCmd as RelayCommand).RaiseCanExecuteChanged();
            });            

            ClearDraftCmd = new RelayCommand(ClearDraft, () => _draft.Value != null);
            RestoreCmd = new AsyncCommand(
                () => PerformBusyOperation(RestoreRoutineAsync, true), 
                errorHandler: ErrorHandler);

            PropertyChanged += DataObjectVM_PropertyChanged;
        }

        /// <summary>
        /// Object's id
        /// </summary>
        public Guid Guid { get; } //Item?.Guid ?? Guid.Empty;

        /// <summary>
        /// Object's code
        /// </summary>
        public long? Code => GetItemCode();

        /// <summary>
        /// Selected version of item
        /// </summary>
        public TItem Item {
            get {
                if (_shownObject == null)
                    return null;

                switch (_shownObject.Value) {
                    case ShownObjectEnum.Main: return _main;
                    case ShownObjectEnum.Draft: return _draft;
                    case ShownObjectEnum.Archive: return _archive;
                    default: throw new InvalidOperationException();
                }
            }
        }

        /// <summary>
        /// Main item
        /// </summary>
        public TItem MainItem => _main;

        /// <summary>
        /// Current selected object
        /// </summary>
        public ShownObjectEnum ShownObject {
            get => _shownObject ?? ShownObjectEnum.Main;
            set {
                if (IsNew) {
                    _shownObject = ShownObjectEnum.Draft;
                    return;
                } 

                switch (value) {
                    case ShownObjectEnum.Main:
                        _shownObject = ShownObjectEnum.Main;
                        if (_main == null)
                            RefreshCmd.Execute(null);
                        else
                            NotifyAllPropertiesChanged();
                        break;

                    case ShownObjectEnum.Draft:
                        if (_draft.Value != null) {
                            _shownObject = ShownObjectEnum.Draft;
                            NotifyAllPropertiesChanged();
                        }
                        break;

                    case ShownObjectEnum.Archive:
                        //if (_selectedArchiveVersion != null) {
                            _shownObject = ShownObjectEnum.Archive;
                            if (_archive == null)
                                RefreshCmd.Execute(null);
                            else
                                NotifyAllPropertiesChanged();
                        //} else {
                        //    ShownObject = ShownObjectEnum.Main;
                        //}
                        break;
                }
            }
        }

        public bool IsExistsMain => /*_main != null &&*/ IsNew == false;
        public bool IsExistsDraft => _draft.Value != null;

        public bool IsExistsArchive => IsNew == false;
        /// <summary>
        /// Shown object is readonly 
        /// </summary>
        public bool IsReadOnly => _shownObject == ShownObjectEnum.Archive
            || (_shownObject == ShownObjectEnum.Main && _draft.Value != null)
            || _shownObject == null
            || IsBusy;

        /// <summary>
        /// View model for archive versions
        /// </summary>
        public ArchiveVersionsVM ArchiveVersionsVM => new ArchiveVersionsVM(Guid, _source, _logger);                

        /// <summary>
        /// Current selected archive version
        /// </summary>
        public int? SelectedArchiveVersion { 
            get => _selectedArchiveVersion; 
            set { 
                if (_selectedArchiveVersion != value) {
                    _selectedArchiveVersion = value;
                    _archive = null;
                    ShownObject = ShownObjectEnum.Archive;
                } else {
                    if (ShownObject != ShownObjectEnum.Archive)
                        ShownObject = ShownObjectEnum.Archive;
                }               
            }
        }

        /// <summary>
        /// Remove draft
        /// </summary>
        public ICommand ClearDraftCmd { get; }

        /// <summary>
        /// Restore archive version
        /// </summary>
        public ICommand RestoreCmd { get; }

        #region ISPRItem implementation 
        public abstract long ID { get; }
        public abstract int Ver { get; }
        public abstract string Name { get; set; }
        #endregion

        protected override async Task Refresh() {
            if (IsNew) {
                if (_draft.Value == null) {
                    _draft.Value = CreateNewItem();
                    _shownObject = ShownObjectEnum.Draft;
                }
                return;
            }

            ObjectContainer<TItem> resp;
            switch (_shownObject) {
                case null:  //initialize refresh
                    resp = await _source.GetMain(Guid, ct);
                    _main = resp.Item;
                    _shownObject = ShownObjectEnum.Main;
                    break;

                case ShownObjectEnum.Archive:                    
                    if (_selectedArchiveVersion == null) {
                        var list = await _source.GetArchiveVersionsListAsync(Guid, default);
                        _selectedArchiveVersion = list.Items.Select(i => (int?)i.Version).FirstOrDefault();
                    }
                    if (_selectedArchiveVersion != null) {
                        _archive = await _source.GetArchiveVersionAsync(Guid, _selectedArchiveVersion.Value, ct);
                        if (_archive == null) {
                            _selectedArchiveVersion = null;
                            ShownObject = ShownObjectEnum.Main;                            
                        }
                    } else {
                        ShownObject = ShownObjectEnum.Main;
                    }
                    break;

                //case ShownObjectEnum.Main:  //main object refresh
                default: {
                        resp = await _source.GetMain(Guid, ct);
                        _main = resp.Item;
                    }
                    break;
            }              
        }

        protected override async Task Save() {
            await base.Save();

            long? code = GetItemCode();            
            if (!_commentProvider.GetComment(JournalAction.Release, ObjectType, code, null, out string comment))
                return;

            if (_draft.Value != null) {
                ObjectContainer<TItem> resp = await _source.Save(_draft, comment, ct);
                _draft.Value = null;
                IsNew = false;
                _main = resp.Item;
                _shownObject = ShownObjectEnum.Main; 
            }
        }        

        /// <summary>
        /// Delete draft
        /// </summary>
        private void ClearDraft()
        {
            if (IsNew) {
                _draft.Value = CreateNewItem();
                IsChanged = false;
                NotifyAllPropertiesChanged();
            } else {
                if (_draft.Value != null) {
                    IsChanged = false;
                    _draft.Value = null;
                }
                if (ShownObject == ShownObjectEnum.Draft)
                    ShownObject = ShownObjectEnum.Main;
            }
        }

        /// <summary>
        /// Restore archive version
        /// </summary>
        /// <returns></returns>
        private async Task RestoreRoutineAsync()
        {
            if (_selectedArchiveVersion == null)
                return;

            long? code = GetItemCode();
            if (!_commentProvider.GetComment(JournalAction.Release, ObjectType, code, null, out string comment))
                return;

            _main = await _source.RestoreAsync(Guid, _selectedArchiveVersion.Value, comment, ct);
            _selectedArchiveVersion = null;
            _shownObject = ShownObjectEnum.Main;
        }

        /// <summary>
        /// Create new item
        /// </summary>
        /// <returns></returns>
        protected abstract TItem CreateNewItem();

        /// <summary>
        /// Catch IsChanged property update notification and make draft
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataObjectVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if (_draft.Value == null && e.PropertyName == nameof(IsChanged) && IsChanged) {
                _draft.Value = _main;
                _main = null;
                _shownObject = ShownObjectEnum.Draft;
                NotifyPropertyChanged(nameof(IsExistsDraft));
                NotifyPropertyChanged(nameof(ShownObject));
            }

            if (e.PropertyName == nameof(IsBusy))
                NotifyPropertyChanged(nameof(IsReadOnly));
        }

        private long? GetItemCode() {
            if (_piCode == null)
                return null;

            if (_main != null)
                return (long?)_piCode.GetValue(_main);

            if (_draft.Value != null)
                return (long?)_piCode.GetValue(_draft.Value);

            return null;
        }
    }
}
