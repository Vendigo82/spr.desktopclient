﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.VM;
using SPR.UI.App.VM.Common;
using SPR.UI.App.Abstractions.DataModel;
using System.Collections.Specialized;
using System.Linq;
using System.Collections.ObjectModel;

namespace SPR.UI.App.VM.Base
{

    /// <summary>
    /// Default view model with IsBusy functionality and standard operations
    /// </summary>
    public abstract class BusyVMBase : NotifyPropertyChangedBase, ICommonVM, IDataInitializer
    {
        protected readonly ILogger _logger;

        volatile private bool _isBusy = false;
        volatile private bool _isChanged = false;
        private Exception _error = null;
        private readonly bool _changeable;
        private readonly bool _refreshAfterSave;

        public BusyVMBase(
            ILogger logger,
            bool changeable = true, 
            bool refreshable = true, 
            bool resetIsChangedOnRefresh = true,
            bool refreshAfterSave = false) 
        {
            _logger = logger;
            _changeable = changeable;   
            _refreshAfterSave = refreshAfterSave;

            if (_changeable)
                SaveCmd = new AsyncCommand(
                    async () => await PerformBusyOperation(HandleSaveAsync, notifyAllChanged: true, resetIsChanged: true), 
                    () => IsBusy == false && IsChanged,
                    ErrorHandler);

            if (refreshable)
                RefreshCmd = new AsyncCommand(
                    async () => await PerformBusyOperation(Refresh, notifyAllChanged: true, resetIsChanged: resetIsChangedOnRefresh),
                    () => IsBusy == false,
                    ErrorHandler);
        }

        protected CancellationToken ct => CancellationToken.None;


        public virtual async Task Initialize(bool fresh) => await PerformRefreshBusyOperation(Refresh);

        /// <summary>
        /// Busy flag
        /// </summary>
        public bool IsBusy { get => _isBusy; protected set { _isBusy = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// Data has been changed flag
        /// </summary>
        public bool IsChanged { 
            get => _isChanged; 
            protected set { 
                _isChanged = value; 
                NotifyPropertyChanged();
                if (SaveCmd != null)
                    (SaveCmd as AsyncCommand).RaiseCanExecuteChanged();
            } 
        }

        /// <summary>
        /// Perform save operation. 
        /// </summary>
        public ICommand SaveCmd { get; }

        /// <summary>
        /// Perform refresh operation.        
        /// </summary>
        public ICommand RefreshCmd { get; }

        #region Errors
        /// <summary>
        /// There was been error
        /// </summary>
        public bool HasError => _error != null;

        /// <summary>
        /// Last error
        /// </summary>
        public Exception Error { 
            get => _error; 
            protected set { 
                _error = value; 
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(HasError));
                NotifyPropertyChanged(nameof(ErrorMessage));
            } 
        }

        /// <summary>
        /// Last error message
        /// </summary>
        public string ErrorMessage => _error?.Message;

        protected void ErrorHandler(Exception e) {
            _logger?.LogError($"{GetType().Name}.ErrorHandler", e);
            Error = e;
        }
        #endregion        

        /// <summary>
        /// Provide refresh operation.
        /// Called code update all properties and reset IsChanged flag after successfull operation
        /// </summary>
        /// <returns></returns>
        protected abstract Task Refresh();

        /// <summary>
        /// Provide save operation.
        /// Called code update all properties and reset IsChanged flag after successfull operation
        /// </summary>
        /// <returns></returns>
        protected virtual async Task Save() => await Task.CompletedTask;

        private async Task HandleSaveAsync()
        {
            await Save();
            if (_refreshAfterSave)
                await Refresh();
        }

        /// <summary>
        /// Perform Busy operation. Before start operation <see cref="IsBusy"/> set to true, then perform operation.
        /// After operation <see cref="IsBusy"/> set to false and raise <see cref="PropertyChanged"/> event for all properties. 
        /// Operation wrapped in try/catch block
        /// </summary>
        /// <param name="operation">performing operation</param>
        /// <param name="notifyAllChanged">Raise PropertyChanged event for all properties after operation</param>
        /// <param name="resetIsChanged">Reset IsChanged flag after succussfully operation</param>
        /// <returns></returns>
        protected Task PerformBusyOperation(
            Func<Task> operation,
            bool notifyAllChanged = false,
            bool resetIsChanged = false)
            => PerformBackgroudOperation(operation, notifyAllChanged: notifyAllChanged, resetIsChanged: resetIsChanged, setIsBusy: true);

        /// <summary>
        /// Perform background operation with optional set <see cref="IsBusy flag"/>
        /// Operation wrapped in try/catch block and set context error if throws exception
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="notifyAllChanged"></param>
        /// <param name="resetIsChanged"></param>
        /// <param name="setIsBusy"></param>
        /// <returns></returns>
        protected async Task PerformBackgroudOperation(
            Func<Task> operation,
            bool notifyAllChanged = false,
            bool resetIsChanged = false,
            bool setIsBusy = false)
        {
            if (setIsBusy)
                IsBusy = true;
            Error = null;
            try {
                await operation();

                if (resetIsChanged)
                    IsChanged = false;

            } catch (Exception e) {
                Error = e;
                _logger?.LogError($"{GetType().Name}.PerformBusyOperation", e);
            } finally {
                try {
                    if (setIsBusy)
                        _isBusy = false;

                    if (notifyAllChanged)
                        NotifyAllPropertiesChanged();
                    else
                        NotifyPropertyChanged(nameof(IsBusy));

                    if (RefreshCmd != null)
                        (RefreshCmd as AsyncCommand).RaiseCanExecuteChanged();
                } catch (Exception e) {
                    Error = e;
                    _logger?.LogError($"{GetType().Name}.PerformBusyOperation finally block", e);
                }
            }
        }

        protected async Task PerformRefreshBusyOperation(Func<Task> operation)
            => await PerformBusyOperation(operation, true, true);

        /// <summary>
        /// Call <see cref="NotifyPropertyChangedBase.NotifyPropertyChanged(string)" and set IsChanged flag/>
        /// </summary>
        /// <param name="propertyName"></param>
        protected void SetIsChangedAndNotify([CallerMemberName] string propertyName = "") {
            NotifyPropertyChanged(propertyName);
            IsChanged = true;
        }

        #region Items collection helpers
        /// <summary>
        /// Create observable collection manager
        /// </summary>
        protected ObservableCollectionManager<TItem> CreateCollectionManager<TItem, TItemSource>(
            Func<ICollection<TItemSource>> sourceAccessor,
            Func<TItemSource, TItem> wrapFunc) 
            where TItem : INotifyPropertyChanged, IItemContainer<TItemSource>
        {
            return ObservableCollectionManager.Create(
                sourceAccessor,
                wrapFunc,
                () => IsChanged = true);
        }

        /// <summary>
        /// List item changed callback. Set <see cref="IsChanged"/>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void CollectionItemChanged(object sender, PropertyChangedEventArgs args) => IsChanged = true;

        /// <summary>
        /// Observable collection changed callback. Set <see cref="IsChanged"/> and subscrube new items to <see cref="CollectionItemChanged(object, PropertyChangedEventArgs)"/>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<INotifyPropertyChanged>()) {
                    i.PropertyChanged += CollectionItemChanged;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<INotifyPropertyChanged>()) {
                    i.PropertyChanged -= CollectionItemChanged;
                }
            }

            if (e.NewItems != null || e.OldItems != null)
                IsChanged = true;
        }

        protected void SubscribeCollectionSetIsChanged<T>(ObservableCollection<T> collection) where T : INotifyPropertyChanged
        {
            collection.CollectionChanged += CollectionChanged;
            collection.Cast<INotifyPropertyChanged>().Subscribe(CollectionItemChanged);
        }
        #endregion
    }
}
