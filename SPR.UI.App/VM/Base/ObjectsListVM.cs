﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using VenSoft.WPFLibrary.Commands;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.App.Abstractions.UI;
using System.Threading;
using System.Windows.Threading;

namespace SPR.UI.App.VM.Base
{
    public class ObjectsListVM<TSrc, TVm> : BusyVMBase
        where TVm : class
    {
        /// <summary>
        /// Provide strings for delete operation
        /// </summary>
        public interface IDeleteStringsProvider
        {
            string Confirm(TSrc item);

            string Reject(TSrc item);
        }

        /// <summary>
        /// Item's source
        /// </summary>
        protected readonly IDataListSource<TSrc> _source;

        /// <summary>
        /// Functor for wrapping datasource objects
        /// </summary>
        private readonly Func<TSrc, TVm> _wrap;        

        private TVm _selected;

        public ObjectsListVM(
            IDataListSource<TSrc> source, 
            Func<TSrc, TVm> wrap, 
            ILogger logger)
            : base(changeable: false, logger: logger) 
        {
            _source = source;
            _wrap = wrap;

            DelayUpdateTimer = new DispatcherTimer {
                IsEnabled = false,
                Interval = TimeSpan.FromMilliseconds(100)
            };
            DelayUpdateTimer.Tick += DelayUpdateTimer_Tick;
        }

        /// <summary>
        /// Delete selected item command
        /// Argument should be null
        /// </summary>
        public ICommand DeleteCmd { get; private set; }

        public override async Task Initialize(bool fresh) 
            => await PerformRefreshBusyOperation(async () => await LoadData(fresh));

        /// <summary>
        /// List of items
        /// </summary>
        public ObservableCollection<TVm> Items { get; private set; }

        /// <summary>
        /// Current selected item
        /// </summary>
        public TVm SelectedItem {
            get => _selected;
            set { 
                _selected = value; 
                NotifyPropertyChanged();
                if (DeleteCmd != null)
                    (DeleteCmd as AsyncCommand).RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Called on initialize and refresh
        /// </summary>
        protected override async Task Refresh() => await LoadData(true);

        /// <summary>
        /// Perform load data operation
        /// </summary>
        /// <param name="fresh"></param>
        /// <returns></returns>
        protected virtual async Task LoadData(bool fresh) 
        {
            if (_logger?.IsEnabled(LogLevel.Debug) == true)
                _logger.LogDebug($"{GetType().Name}.LoadData: fresh={fresh}");
            IEnumerable<TSrc> list = await LoadItemsRoutineAsync(fresh, ct);
            UpdateItems(list);

            if (DeleteCmd != null)
                (DeleteCmd as AsyncCommand).RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Load items
        /// </summary>
        /// <param name="fresh"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        protected virtual Task<IEnumerable<TSrc>> LoadItemsRoutineAsync(bool fresh, CancellationToken ct) 
            => fresh ? _source.LoadItemsAsync(ct) : _source.GetItemsAsync(ct);

        private void UpdateItems(IEnumerable<TSrc> list) {
            Items = new ObservableCollection<TVm>(list.Select(i => _wrap(i)));            
        }

        /// <summary>
        /// Enable <see cref="DeleteCmd"/> and configure command
        /// </summary>
        /// <param name="oper">Delete operation</param>
        /// <param name="unwrap">Unwrap view model's class to source item class</param>
        /// <param name="canExecute">CanExecute operation additional condition</param>
        /// <param name="wndFactory">Windows factory</param>
        /// <param name="strings">user's interface strings provider</param>
        protected void ConfigureDelete(
            IDeleteOperation<TSrc> oper,
            Func<TVm, TSrc> unwrap,
            Func<TSrc, bool> canExecute,
            IWindowFactory wndFactory,
            IDeleteStringsProvider strings)
        {
            Func<bool> func;
            if (canExecute != null)
                func = () => SelectedItem != null && canExecute(unwrap(SelectedItem));
            else
                func = () => SelectedItem != null;

            DeleteCmd = new AsyncCommand(
                execute: () => PerformBusyOperation(() => DeleteRoutineAsync(oper, SelectedItem, unwrap, wndFactory, strings)),
                canExecute: func,
                errorHandler: ErrorHandler);
        }

        /// <summary>
        /// Perform delete item operation
        /// </summary>
        /// <param name="item">item to delete</param>
        /// <param name="oper">delete operation</param>
        /// <param name="wndFactory">window factory</param>
        /// <param name="strings">user's strings provider</param>
        /// <returns></returns>
        private async Task DeleteRoutineAsync(
            IDeleteOperation<TSrc> oper,
            TVm item,
            Func<TVm, TSrc> unwrap,
            IWindowFactory wndFactory,
            IDeleteStringsProvider strings)
        {
            if (item == null)
                return;

            var srcItem = unwrap(item);

            bool dlgResult = wndFactory.ShowConfirmDialog(strings.Confirm(srcItem));
            if (dlgResult) {
                if (await oper.DeleteAsync(srcItem, ct))
                    Items.Remove(item);
                else
                    wndFactory.ShowInformationDialog(strings.Reject(srcItem));
            }
        }

        #region Delay update timer
        /// <summary>
        /// Timer for delay update. Useful for filter data on continues inputing
        /// </summary>
        protected DispatcherTimer DelayUpdateTimer { get; }

        /// <summary>
        /// Reset timer's elapsed time
        /// </summary>
        protected void ResetDelayUpdateTimer()
        {
            DelayUpdateTimer.Stop();
            DelayUpdateTimer.Start();
        }

        protected Task DelayFilterTimerAction()
        {
            DelayUpdateTimer.Stop();
            return PerformRefreshBusyOperation(async () => await Refresh());
        }

        private void DelayUpdateTimer_Tick(object sender, EventArgs e) => DelayFilterTimerAction();
        #endregion

        /// <summary>
        /// Default strings provider.
        /// Confirmation dialog string is {prefix}.DeleteConfirmation.Text.
        /// Reject deletion dialog strings is {prefix}.DeleteUsedInformation.Text
        /// Strings can contain one placeholder for item description
        /// </summary>
        public class DefaultDeleteStringsProvider : IDeleteStringsProvider
        {
            private readonly IResourceStringProvider provider;
            private readonly Func<TSrc, string> description;
            private readonly string prefix;

            /// <summary>
            /// Create new deleting strings provider
            /// </summary>
            /// <param name="resources">Resource strings provider</param>
            /// <param name="description">Make description from source item</param>
            /// <param name="prefix">Resource strins prefix</param>
            public DefaultDeleteStringsProvider(IResourceStringProvider resources, Func<TSrc, string> description, string prefix)
            {
                this.provider = resources ?? throw new ArgumentNullException(nameof(resources));
                this.description = description ?? throw new ArgumentNullException(nameof(description));
                this.prefix = prefix ?? throw new ArgumentNullException(nameof(prefix));
            }

            public string Confirm(TSrc item)
                => string.Format(
                    provider.GetString(string.Concat(prefix, ".DeleteConfirmation.Text")),
                    description?.Invoke(item) ?? "");

            public string Reject(TSrc item)
                => string.Format(
                    provider.GetString(string.Concat(prefix, ".DeleteUsedInformation.Text")),
                    description?.Invoke(item) ?? "");
        }
    }
}
