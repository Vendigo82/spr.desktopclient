﻿using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.DataContract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Base
{
    public class DataObjectTabsVM<T> : NotifyPropertyChangedBase, ISPRItem, ICommonVM where T : class
    {
        public enum TabKind
        {
            Main = 0,
            Test = 1,
            Journal = 2,
            Usage = 3
        }

        /// <summary>
        /// Current selected tab
        /// </summary>
        private TabKind _selectedTab = TabKind.Main;

        /// <summary>
        /// Tab's dictionary
        /// </summary>
        private readonly Dictionary<TabKind, ICommonVM> _tabs;

        /// <summary>
        /// list of tabs which await to refresh on select
        /// </summary>
        private readonly HashSet<TabKind> _awaitRefreshTabs;

        private static readonly HashSet<string> _properties;

        static DataObjectTabsVM()
        {
            IEnumerable<string> list = typeof(DataObjectTabsVM<T>)
                .GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance)
                .Select(i => i.Name)
                .Except(new string[] { nameof(HasError), nameof(Error), nameof(ErrorMessage) });

            _properties = new HashSet<string>(list);
        }

        public DataObjectTabsVM(IDataObjectVM<T> vm, IDataObjectVMFactory<T> factory)
        {
            MainVM = vm ?? throw new ArgumentNullException(nameof(vm));
            TestVM = factory.CreateTestVM(vm);
            JournalVM = factory.CreateJournalVM(MainVM.Guid);
            UsageVM = factory.CreateUsageVM(MainVM.Guid);

            _tabs = new Dictionary<TabKind, ICommonVM>() {
                { TabKind.Main, MainVM },
            };
            if (TestVM != null)
                _tabs.Add(TabKind.Test, TestVM);
            if (JournalVM != null)
                _tabs.Add(TabKind.Journal, JournalVM);
            if (UsageVM != null)
                _tabs.Add(TabKind.Usage, UsageVM);

            foreach (var t in _tabs.Values)
                t.PropertyChanged += VMPropertyChanged;

            RefreshCmd = new SwitchableCommand(GetRefreshCommand());

            _awaitRefreshTabs = new HashSet<TabKind>() { TabKind.Journal, TabKind.Usage };
        }

        /// <summary>
        /// Current selected tab
        /// </summary>
        public TabKind SelectedTab {
            get => _selectedTab;
            set {
                _selectedTab = value;
                ((SwitchableCommand)RefreshCmd).Command = GetRefreshCommand();
                NotifyAllPropertiesChanged();
                if (_awaitRefreshTabs.Contains(_selectedTab)) {
                    _tabs[_selectedTab].RefreshCmd.Execute(null);
                    _awaitRefreshTabs.Remove(_selectedTab);
                }
            }
        }

        /// <summary>
        /// Formula main data VM
        /// </summary>
        public IDataObjectVM<T> MainVM { get; }

        /// <summary>
        /// Formula test VM
        /// </summary>
        public ITestVM TestVM { get; }

        /// <summary>
        /// Journal records view model
        /// </summary>
        public ICommonVM JournalVM { get; }

        /// <summary>
        /// Formula usage view model
        /// </summary>
        public ICommonVM UsageVM { get; }

        #region Common properties and commands
        public bool IsBusy => _tabs[_selectedTab].IsBusy;

        public bool IsChanged => MainVM.IsChanged;

        public ICommand SaveCmd => MainVM.SaveCmd;

        public ICommand RefreshCmd { get; } //_tabs[_selectedTab].RefreshCmd;

        public ICommand TestCmd => TestVM?.TestCmd;

        private ICommand GetRefreshCommand()
        {
            switch (_selectedTab) {
                case TabKind.Test: return MainVM.RefreshCmd;
                default: return _tabs[_selectedTab].RefreshCmd;
            }
        }
        #endregion

        #region Error
        private Exception _exception = null;

        public bool HasError => _exception != null;

        public Exception Error => _exception;

        public string ErrorMessage => _exception?.Message;
        #endregion

        #region ISPRItem interface implementation
        public long ID => MainVM.ID;

        public int Ver => MainVM.Ver;

        public Guid Guid => MainVM.Guid;

        public string Name => MainVM.Name;
        #endregion

        private void VMPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Error) && sender is ICommonVM vm) {
                _exception = vm.Error;
                NotifyPropertyChanged(nameof(HasError));
                NotifyPropertyChanged(nameof(Error));
                NotifyPropertyChanged(nameof(ErrorMessage));
            }

            if (_properties.Contains(e.PropertyName))
                NotifyPropertyChanged(e.PropertyName);
        }
    }
}
