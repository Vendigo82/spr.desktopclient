﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.DataModel;
using SPR.UI.App.Services.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Base
{
    public abstract class PartialContentVM<T> : BusyVMBase where T : class
    {
        /// <summary>
        /// loading data was exhausted
        /// </summary>
        private bool isLoadCompleted = false;

        /// <summary>
        /// loading records
        /// </summary>
        volatile private bool isLoading = false;

        /// <summary>
        /// Loading content tasks' manager
        /// </summary>
        protected readonly TaskManager<PartialListContainer<T>> taskManager = new TaskManager<PartialListContainer<T>>();

        /// <summary>
        /// First loading flag
        /// </summary>
        protected bool firstLoading = true;

        private CancellationTokenSource cts = null;

        public PartialContentVM(ILogger logger) : base(logger, changeable: false)
        {
            LoadNextCmd = new AsyncCommand(
                execute: () => PerformBackgroudOperation(() => LoadWrapper(() => taskManager.RunTask(LoadPart, (r) => ApplyPartialLoad(r, false)))),
                canExecute: () => IsLoadCompleted == false && IsBusy == false && IsLoading == false,
                ErrorHandler);

            LoadAllCmd = new AsyncCommand(
                execute: () => PerformBusyOperation(() => LoadWrapper(LoadAll)),
                canExecute: () => IsLoadCompleted == false && IsBusy == false && IsLoading == false,
                ErrorHandler);

            CancelLoadCmd = new RelayCommand(
                execute: () => cts?.Cancel(),
                canExecute: () => isLoading);
        }

        /// <summary>
        /// Load next part or records
        /// </summary>
        public ICommand LoadNextCmd { get; }

        /// <summary>
        /// Load all records
        /// </summary>
        public ICommand LoadAllCmd { get; }

        /// <summary>
        /// Cancel loading records
        /// </summary>
        public ICommand CancelLoadCmd { get; }

        /// <summary>
        /// List of records
        /// </summary>
        public ObservableCollection<T> Items { get; } = new ObservableCollection<T>();

        /// <summary>
        /// Lock object for access to <see cref="Items"/> collections
        /// </summary>
        protected object ItemsLock => taskManager.Lock;

        protected CancellationToken CancellationToken => cts?.Token ?? CancellationToken.None;

        /// <summary>
        /// Loading data was complete
        /// </summary>
        public bool IsLoadCompleted {
            get => isLoadCompleted;
            private set {
                isLoadCompleted = value;
                NotifyPropertyChanged();
                RefreshCommands();
            }
        }

        /// <summary>
        /// Is currently loading records
        /// </summary>
        public bool IsLoading {
            get => isLoading;
            private set {
                isLoading = value;
                NotifyPropertyChanged();
                RefreshCommands();
            }
        }

        /// <summary>
        /// Reload records within selected period
        /// </summary>
        public Task Reload()
        {
            return PerformBackgroudOperation(() => LoadWrapper(() => taskManager.RunTask(ReloadPart, (r) => ApplyPartialLoad(r, true))));
        }

        protected override async Task Refresh()
        {
            taskManager.Cancel();
            if (firstLoading) {
                await taskManager.RunTask(LoadPart, (r) => ApplyPartialLoad(r, false));
                firstLoading = false;
            } else {
                //loading new records
                bool exhausted;
                do {

                    T record = Items.FirstOrDefault();
                    var resp = await LoadRoutine(record, true, ct);

                    lock (taskManager.Lock) {
                        InsertRange(resp.Items);
                    }

                    exhausted = resp.Exhausted || (resp.Items.Any() == false);
                    //exhausted = await RefreshRoutine();
                } while (exhausted == false);
            }
        }

        /// <summary>
        /// Perform loading operation
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        private async Task LoadWrapper(Func<Task> func)
        {
            try {
                IsLoading = true;

                using (cts = new CancellationTokenSource()) {
                    await func();
                }
            } catch (OperationCanceledException) {
                if (cts.IsCancellationRequested)
                    return;
                throw;
            } finally {
                IsLoading = false;
                cts = null;
            }
        }

        /// <summary>
        /// Load all records from source
        /// </summary>
        /// <returns></returns>
        private async Task LoadAll()
        {
            while (!IsLoadCompleted && !cts.Token.IsCancellationRequested) {
                await taskManager.RunTask(LoadPart, (r) => ApplyPartialLoad(r, false));
            }
        }

        /// <summary>
        /// Load one part of records from source
        /// </summary>
        /// <returns></returns>
        protected Task<PartialListContainer<T>> LoadPart()
        {
            //long? lastId = null;
            T record = null;
            lock (taskManager.Lock) {
                if (Items.Count > 0)
                    record = Items[Items.Count - 1];
                    //lastId = Items[Items.Count - 1].Id;
            }

            return LoadRoutine(record, false, CancellationToken);
        }

        /// <summary>
        /// Load one part of records from source
        /// </summary>
        /// <returns></returns>
        private Task<PartialListContainer<T>> ReloadPart() => LoadRoutine(null, true, CancellationToken);

        /// <summary>
        /// Load records routine
        /// </summary>
        /// <param name="lastRecord">First or last record, from which next partion should be started. Can be null</param>
        /// <param name="freshRecords">Loading new records (refresh command) or old records (continue load command)</param>
        /// <param name="ct"></param>
        /// <returns>Items with exhausted information</returns>
        protected abstract Task<PartialListContainer<T>> LoadRoutine(T record, bool freshRecords, CancellationToken ct);

        /// <summary>
        /// Add loaded records to collection. Clear collection if need
        /// </summary>
        /// <param name="resp"></param>
        /// <param name="clear"></param>
        protected void ApplyPartialLoad(PartialListContainer<T> resp, bool clear)
        {
            if (clear)
                Items.Clear();

            IsLoadCompleted = resp.Exhausted;
            foreach (var item in resp.Items)
                Items.Add(item);
            //Items.AddRange(resp.Items);
        }

        /// <summary>
        /// Insert records to begin of collection
        /// </summary>
        /// <param name="items"></param>
        protected void InsertRange(IEnumerable<T> items)
        {
            int index = 0;
            foreach (var item in items)
                Items.Insert(index++, item);
        }

        private void RefreshCommands()
        {
            ((AsyncCommand)LoadNextCmd).RaiseCanExecuteChanged();
            ((AsyncCommand)LoadAllCmd).RaiseCanExecuteChanged();
            ((RelayCommand)CancelLoadCmd).RaiseCanExecuteChanged();
        }
    }
}
