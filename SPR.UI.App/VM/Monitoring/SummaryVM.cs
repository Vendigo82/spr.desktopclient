﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataModel.Common;
using SPR.UI.App.Services.Utils;
using SPR.UI.App.VM.Base;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring
{
    public class SummaryVM : BusyVMBase
    {
        /// <summary>
        /// Transactions data source
        /// </summary>
        private readonly IMonitoringSource source;

        private readonly DateTimePeriod period = new DateTimePeriod();

        /// <summary>
        /// Loading content tasks' manager
        /// </summary>
        protected readonly TaskManager<Tuple<IEnumerable<SummaryRecordModel>, IEnumerable<SummaryErrorModel>>> taskManager 
            = new TaskManager<Tuple<IEnumerable<SummaryRecordModel>, IEnumerable<SummaryErrorModel>>>();

        public SummaryVM(IMonitoringSource source, ILogger logger) : base(logger)
        {
            this.source = source ?? throw new ArgumentNullException(nameof(source));
            period.Reset();

            taskManager.LoadingCountChanged += (s, e) => 
            NotifyPropertyChanged(nameof(IsLoading));
        }

        /// <summary>
        /// Transactions filter's period
        /// </summary>
        public Tuple<DateTime, DateTime> Period {
            get => period.Period;
            set {
                if (period.Set(value)) {
                    Task _ = PerformBackgroudOperation(ReloadAsync);
                }
            }
        }

        public bool IsLoading => taskManager.IsLoading;

        public IEnumerable<SummaryRecordModel> Items { get; private set; }

        public IEnumerable<SummaryErrorModel> Errors { get; private set; }

        protected override async Task Refresh()
        {
            taskManager.Cancel();
            await ReloadAsync();            
        }

        private async Task ReloadAsync()
        {            
            await taskManager.RunTask(LoadRoutineAsync, AssignItems);
        }

        private async Task<Tuple<IEnumerable<SummaryRecordModel>, IEnumerable<SummaryErrorModel>>> LoadRoutineAsync()
        {
            var filter = new PeriodFilter();
            period.FillFilter(filter);
            var t1 = source.LoadSummaryAsync(filter, ct);
            var t2 = source.LoadSummaryErrorsAsync(filter, ct);
            await Task.WhenAll(t1, t2);
            return new Tuple<IEnumerable<SummaryRecordModel>, IEnumerable<SummaryErrorModel>>(t1.Result, t2.Result);
        }

        private void AssignItems(Tuple<IEnumerable<SummaryRecordModel>, IEnumerable<SummaryErrorModel>> items)
        {
            Items = items.Item1;
            Errors = items.Item2;
            NotifyPropertyChanged(nameof(Items));
            NotifyPropertyChanged(nameof(Errors));
        }

    }
}
