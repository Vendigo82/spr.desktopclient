﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring
{
    public class CalculationResultsVM : BusyVMBase
    {
        /// <summary>
        /// Transactions data source
        /// </summary>
        private readonly IMonitoringSource source;

        /// <summary>
        /// Calculations results for that transaction
        /// </summary>
        private readonly long transactionId;

        public CalculationResultsVM(
            long transactionId,
            IMonitoringSource source,
            ILogger logger) 
            : base(logger, changeable: false, refreshable: true, resetIsChangedOnRefresh: false)
        {
            this.transactionId = transactionId;
            this.source = source;
        }

        public ObservableCollection<CalculationResultModel> Items { get; private set; }

        protected override async Task Refresh()
        {
            var result = await source.LoadCalculationResulsAsync(transactionId, ct);
            Items = new ObservableCollection<CalculationResultModel>(result);
            NotifyPropertyChanged(nameof(Items));
        }
    }
}
