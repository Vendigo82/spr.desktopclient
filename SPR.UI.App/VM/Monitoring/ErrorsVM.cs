﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataModel;
using SPR.UI.App.DataModel.Common;
using SPR.UI.App.VM.Base;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring
{
    public class ErrorsVM : PartialContentVM<TransactionErrorModel>
    {
        /// <summary>
        /// Transactions data source
        /// </summary>
        private readonly IMonitoringSource source;

        private readonly DateTimePeriod period = new DateTimePeriod();
        private TransactionErrorModel selectedItem;
        private TransactionModel selectedTransaction;
        private TransactionDataModel selectedTransactionData;
        private int selectedTransactionDataLoadingCount = 0;

        public ErrorsVM(IMonitoringSource source, ILogger logger) : base(logger)
        {
            this.source = source ?? throw new ArgumentNullException(nameof(source));
            period.Reset();
        }

        /// <summary>
        /// Errors filter's period
        /// </summary>
        public Tuple<DateTime, DateTime> Period {
            get => period.Period;
            set {
                if (period.Set(value))
                    Reload();
            }
        }

        /// <summary>
        /// Selected transaction
        /// </summary>
        public TransactionErrorModel SelectedItem {
            get => selectedItem;
            set {
                if (selectedItem?.Id == value?.Id)
                    return;

                selectedItem = value;
                NotifyPropertyChanged();

                if (selectedItem != null && value.TransId != null) {
                    Task t = PerformBackgroudOperation(() => LoadTransactionDataAsync(value.TransId.Value));
                }
            }
        }

        /// <summary>
        /// Information about selected transaction data
        /// </summary>
        public TransactionDataModel SelectedTransactionData {
            get => selectedTransactionData;
            private set {
                selectedTransactionData = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Inforamtion about transaction which contains selected error
        /// </summary>
        public TransactionModel SelectedTransaction {
            get => selectedTransaction;
            private set {
                selectedTransaction = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Is selected transaction data loading now
        /// </summary>
        public bool IsSelectedTransactionDataLoading => selectedTransactionDataLoadingCount != 0;

        protected override Task<PartialListContainer<TransactionErrorModel>> LoadRoutine(TransactionErrorModel record, bool freshRecords, CancellationToken ct)
        {
            var filter = new PeriodFilter {
                DateFrom = period.DateFrom,
                DateTo = period.DateTo
            };

            return source.LoadErrorsAsync(filter, record?.Id, freshRecords, ct);
        }

        /// <summary>
        /// Load selected transaction's data
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        private async Task LoadTransactionDataAsync(long transactionId)
        {
            Interlocked.Increment(ref selectedTransactionDataLoadingCount);
            NotifyPropertyChanged(nameof(IsSelectedTransactionDataLoading));

            try {
                var data = await source.LoadDataAsync(transactionId, ct);
                if (selectedItem?.TransId == transactionId) {
                    SelectedTransactionData = data.Data;
                    SelectedTransaction = data.Transaction;
                }
            } catch {
                if (selectedItem?.TransId == transactionId) {
                    SelectedTransactionData = null;
                    SelectedTransaction = null;
                }
                throw;
            } finally {
                Interlocked.Decrement(ref selectedTransactionDataLoadingCount);
                NotifyPropertyChanged(nameof(IsSelectedTransactionDataLoading));
            }
        }
    }
}
