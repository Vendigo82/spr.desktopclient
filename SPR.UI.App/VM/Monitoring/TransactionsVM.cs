﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataModel;
using SPR.UI.App.DataModel.Common;
using SPR.UI.App.Services.Utils;
using SPR.UI.App.VM.Base;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Configuration;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Monitoring
{
    public class TransactionsVM : PartialContentVM<TransactionModel>
    {
        /// <summary>
        /// Transactions data source
        /// </summary>
        private readonly IMonitoringSource source;

        private readonly DateTimePeriod period = new DateTimePeriod();
        private TransactionModel selectedItem;
        private TransactionDataModel selectedTransactionData;
        private IEnumerable<TransactionErrorModel> selectedTransactionErrors;
        private int selectedTransactionDataLoadingCount = 0;

        private FilterValues draftFilter = new FilterValues();
        private FilterValues filter = new FilterValues();

        public TransactionsVM(
            IMonitoringSource source,
            ILogger logger) 
            : base(logger)
        {
            this.source = source ?? throw new ArgumentNullException(nameof(source));
            period.Reset();

            ResetFilterCmd = new AsyncCommand(
                () => ResetFilter(), 
                () => IsLoading == false && IsBusy == false, 
                ErrorHandler);
            ApplyFilterCmd = new AsyncCommand<IEnumerable<KeyValuePair<string, string>>>(
                (values) => PerformBackgroudOperation(() => ApplyFilterRoutineAsync(values ?? Enumerable.Empty<KeyValuePair<string, string>>())),
                (values) => IsLoading == false && IsBusy == false, 
                ErrorHandler);
        }

        /// <summary>
        /// Transactions filter's period
        /// </summary>
        public Tuple<DateTime, DateTime> Period {
            get => period.Period;
            set {
                if (period.Set(value))
                    Reload();
            }
        }

        /// <summary>
        /// Information about transaction additional columns
        /// </summary>
        public IEnumerable<TransactionColumnModel> AdditionalColums { get; private set; }

        /// <summary>
        /// Selected transaction
        /// </summary>
        public TransactionModel SelectedItem { 
            get => selectedItem; 
            set {
                if (selectedItem?.Id == value?.Id)
                    return;

                selectedItem = value; 
                NotifyPropertyChanged();

                if (selectedItem != null) {                    
                    Task t = PerformBackgroudOperation(() => LoadTransactionDataAsync(value.Id));
                }
            } 
        }

        /// <summary>
        /// Information about selected transaction data
        /// </summary>
        public TransactionDataModel SelectedTransactionData { 
            get => selectedTransactionData; 
            private set {
                selectedTransactionData = value; 
                NotifyPropertyChanged();
            }
        }
        
        /// <summary>
        /// Selected transaction's errors
        /// </summary>
        public IEnumerable<TransactionErrorModel> SelectedTransactionErrors {
            get => selectedTransactionErrors;
            private set {
                selectedTransactionErrors = value;
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Is selected transaction data loading now
        /// </summary>
        public bool IsSelectedTransactionDataLoading  => selectedTransactionDataLoadingCount != 0; 

        public bool FilterErrors { get => draftFilter.Errors; set { draftFilter.Errors = value; NotifyPropertyChanged(); } }
        public string FilterStep { get => draftFilter.Step; set { draftFilter.Step = string.IsNullOrEmpty(value) ? null : value; NotifyPropertyChanged(); } }
        public string FilterIpAddress { get => draftFilter.IpAddress; set { draftFilter.IpAddress = string.IsNullOrEmpty(value) ? null : value; NotifyPropertyChanged(); } }
        public long? FilterScenarioId { get => draftFilter.ScenarioId; set { draftFilter.ScenarioId = value == 0 ? null : value; NotifyPropertyChanged(); } }
        public string FilterNextStep { get => draftFilter.NextStep; set { draftFilter.NextStep = string.IsNullOrEmpty(value) ? null : value; NotifyPropertyChanged(); } }

        public ICommand ApplyFilterCmd { get; }
        public ICommand ResetFilterCmd { get; }

        private Task ApplyFilterRoutineAsync(IEnumerable<KeyValuePair<string, string>> values)
        {
            draftFilter.Values.Clear();
            foreach (var pair in values)
                draftFilter.Set(pair.Key, pair.Value);

            filter.Assign(draftFilter);
            return Reload();
        }

        private Task ResetFilter()
        {
            draftFilter.Reset();
            RaiseFilterProperties();
            if (!filter.Equals(draftFilter)) {
                filter.Reset();
                return Reload();
            }

            return Task.CompletedTask;
        }

        private void RaiseFilterProperties()
        {
            NotifyPropertyChanged(nameof(FilterErrors));
            NotifyPropertyChanged(nameof(FilterStep));
            NotifyPropertyChanged(nameof(FilterIpAddress));
            NotifyPropertyChanged(nameof(FilterScenarioId));
            NotifyPropertyChanged(nameof(FilterNextStep));
        }

        protected override async Task Refresh()
        {
            taskManager.Cancel();
            if (firstLoading) {
                AdditionalColums = await source.LoadAdditionalColumnsInfoAsync(ct);
                NotifyPropertyChanged(nameof(AdditionalColums));

                await taskManager.RunTask(LoadPart, (r) => ApplyPartialLoad(r, false));
                firstLoading = false;
            } else {
                //loading new records
                bool exhausted;
                do {                    
                    exhausted = await RefreshRoutine();
                } while (exhausted == false);
            }
        }

        private async Task<bool> RefreshRoutine()
        {
            TransactionModel record = null;
            IEnumerable<long> notFinishedIds = Enumerable.Empty<long>();
            lock (ItemsLock) {
                record = Items.FirstOrDefault();
                notFinishedIds = Items.Where(i => i.Finished != true).Select(i => i.Id).ToArray();
            }

            var loadNewTask = LoadRoutine(record, true, ct);
            Task<IEnumerable<TransactionModel>> reloadTask = null;
            if (notFinishedIds.Any()) {
                reloadTask = source.ReloadAsync(notFinishedIds, ct);
                await Task.WhenAll(loadNewTask, reloadTask);
            } else
                await loadNewTask;

            lock (ItemsLock) {
                if (reloadTask != null)
                    UpdateItems(reloadTask.Result);
                InsertRange(loadNewTask.Result.Items);
            }

            return loadNewTask.Result.Exhausted || (loadNewTask.Result.Items.Any() == false);
        }

        protected override Task<PartialListContainer<TransactionModel>> LoadRoutine(TransactionModel record, bool freshRecords, CancellationToken ct)
        {
            return source.LoadAsync(CreateFilter(), record?.Id, freshRecords, ct);
        }

        /// <summary>
        /// Replace items in <see cref="Items"/> to items from <paramref name="newItems"/>
        /// </summary>
        /// <param name="items"></param>
        private void UpdateItems(IEnumerable<TransactionModel> newItems)
        {
            foreach (var item in newItems) {
                bool itemWasReplaced = false;
                for (int i = 0; i < Items.Count && !itemWasReplaced; ++i) {
                    if (Items[i].Id == item.Id) {
                        itemWasReplaced = true;
                        Items.RemoveAt(i);
                        Items.Insert(i, item);
                    }
                }
            }
        }

        /// <summary>
        /// Load selected transaction's data
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        private async Task LoadTransactionDataAsync(long transactionId)
        {
            Interlocked.Increment(ref selectedTransactionDataLoadingCount);
            NotifyPropertyChanged(nameof(IsSelectedTransactionDataLoading));

            try {
                var data = await source.LoadDataAsync(transactionId, ct);
                if (selectedItem?.Id == transactionId) {
                    SelectedTransactionData = data.Data;
                    SelectedTransactionErrors = data.Errors;
                }
            } catch {
                if (selectedItem?.Id == transactionId) {
                    SelectedTransactionData = null;
                    SelectedTransactionErrors = null;
                }
                throw;
            } finally {
                Interlocked.Decrement(ref selectedTransactionDataLoadingCount);
                NotifyPropertyChanged(nameof(IsSelectedTransactionDataLoading));
            }
        }

        private TransactionsFilter CreateFilter()
        {
            if (!draftFilter.Equals(filter)) {
                draftFilter.Assign(filter);
                RaiseFilterProperties();
            }

            var values = new List<KeyValuePair<string, string>>();
            if (!string.IsNullOrEmpty(FilterNextStep))
                values.Add(new KeyValuePair<string, string>(nameof(TransactionModel.StepOut), FilterNextStep));

            values.AddRange(filter.Values);

            return new TransactionsFilter {
                DateFrom = period.DateFrom,
                DateTo = period.DateTo,    
                IpAddress = filter.IpAddress,
                ScenarioCode = filter.ScenarioId,
                Step = filter.Step,
                WithError = filter.Errors == true ? (bool?)true : null,
                Values = values
            };            
        }

        private class FilterValues
        {
            public bool Errors { get; set; }
            public string Step { get; set; }
            public string IpAddress { get; set; }
            public long? ScenarioId { get; set; }
            public string NextStep { get; set; }

            public Dictionary<string, string> Values { get; set; } = new Dictionary<string, string>();

            public void Set(string name, string value)
            {
                if (string.IsNullOrEmpty(value))
                    Values.Remove(name);

                Values[name] = value;
            }

            public override bool Equals(object obj)
            {
                if (obj is FilterValues f)
                    return Errors == f.Errors && Step == f.Step && IpAddress == f.IpAddress && ScenarioId == f.ScenarioId && NextStep == f.NextStep
                        && SequenceEquivalent(Values, f.Values);
                else
                    return base.Equals(obj);
            }

            private static bool SequenceEquivalent(Dictionary<string, string> a, Dictionary<string, string> b)
            {
                if (a.Count() != b.Count())
                    return false;

                foreach (var pair in a) {
                    if (!b.TryGetValue(pair.Key, out var value))
                        return false;

                    if (pair.Value != value)
                        return false;
                }

                return true;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public void Reset()
            {
                Errors = false;
                Step = null;
                IpAddress = null;
                ScenarioId = null;
                NextStep = null;
                Values.Clear();
            }

            public void Assign(FilterValues filter)
            {
                Errors = filter.Errors;
                Step = filter.Step;
                IpAddress = filter.IpAddress;
                ScenarioId = filter.ScenarioId;
                NextStep = filter.NextStep;
                Values.Clear();
                foreach (var pair in filter.Values)
                    Values[pair.Key] = pair.Value;
            }
        }
    }
}
