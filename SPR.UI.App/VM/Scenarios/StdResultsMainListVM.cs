﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Services;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Scenario;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Scenarios
{
    public class StdResultsMainListVM : ObjectsListVM<StdResultSlimModel, StdResultSlimModel> 
    {
        protected new readonly IStdResultsListSource _source;

        public StdResultsMainListVM(
            IStdResultsListSource source,
            IWindowFactory wndFactory,
            IResourceStringProvider resources,
            ILogger logger = null) 
            : base(source, i => i, logger)
        {            
            _source = source;

            AddCmd = new RelayCommand(execute: () => Commands.CommandsLibrary.NewStdResultsCmd.Execute(null, null));

            ConfigureDelete(source, (i) => i, i => i.Usage == false, wndFactory, 
                new DefaultDeleteStringsProvider(resources, i => i.Name, "StdResultsList"));

            SetItemToDefaultCmd = new AsyncCommand<StdResultSlimModel>(
                (m) => PerformBusyOperation(() => SetItemToDefaultRoutine(m), true, true),
                (m) => m != null && m.Default == false,
                ErrorHandler);
        }

        /// <summary>
        /// Add new standard result
        /// </summary>
        public ICommand AddCmd { get; }

        /// <summary>
        /// Set item form argument to default std result command
        /// </summary>
        public ICommand SetItemToDefaultCmd { get; }

        /// <summary>
        /// Set item to default and refresh all list
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task SetItemToDefaultRoutine(StdResultSlimModel model)
        {
            await _source.SetDefaultAsync(model, ct);
            await Refresh();
        }
    }
}
