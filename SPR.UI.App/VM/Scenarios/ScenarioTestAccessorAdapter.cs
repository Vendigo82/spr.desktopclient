﻿using SPR.UI.App.Abstractions.Tests;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.DataContract;
using SPR.UI.WebService.DataContract.Scenario;
using System;

namespace SPR.UI.App.VM.Scenarios
{
    public class ScenarioTestAccessorAdapter : ITestItemAccessor<ScenarioModel>
    {
        private readonly IDataObjectVM<ScenarioModel> _vm;

        public ScenarioTestAccessorAdapter(IDataObjectVM<ScenarioModel> vm)
        {
            _vm = vm ?? throw new ArgumentNullException(nameof(vm));
        }

        public long Id => _vm.ID;

        public Guid Guid => _vm.Guid;

        public ScenarioModel Item {
            get {
                if (_vm.ShownObject == ShownObjectEnum.Main)
                    return null;
                else
                    return _vm.Item;
            }
        }

        public ShownObjectEnum ShownObject => _vm.ShownObject;
    }

    public static class ScenarioVMTestAccessorExtentions
    {
        public static ITestItemAccessor<ScenarioModel> ToTestItemAccessor(this IDataObjectVM<ScenarioModel> vm)
            => new ScenarioTestAccessorAdapter(vm);
    }
}
