﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.DAL.Enums;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;
using SPR.UI.App.VM.Base;

namespace SPR.UI.App.VM.Scenarios
{
    public class StdResultsVM : BusyVMBase
    {
        /// <summary>
        /// Data source
        /// </summary>
        private readonly IStdResultsSource _source;

        /// <summary>
        /// Item
        /// </summary>
        private StdResultModel Item { get; set; }

        /// <summary>
        /// Is new created item
        /// </summary>
        private bool _isNew;

        public StdResultsVM(Guid? id, IStdResultsSource source, ILogger logger)
            : base(logger: logger)
        {
            _source = source ?? throw new ArgumentNullException(nameof(source));
            _isNew = !id.HasValue;
            Id = id ?? Guid.NewGuid();

            SetDefaultCmd = new AsyncCommand(
                () => PerformBusyOperation(SetDefaultRoutine),
                () => !_isNew && !Default,
                ErrorHandler);
        }

        /// <summary>
        /// Id of std result
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Name of result
        /// </summary>
        public string Name { get => Item?.Name; set { Item.Name = value; SetIsChangedAndNotify(); } }
        
        /// <summary>
        /// Default flag
        /// </summary>
        public bool Default { get => Item?.Default ?? false; }

        /// <summary>
        /// Set current std results as default
        /// </summary>
        public ICommand SetDefaultCmd { get; }

        #region Values
        /// <summary>
        /// Add INotifyPropertyChanged functionality to standard results row
        /// </summary>
        public class ValueWrapper : NotifyPropertyChangedBase
        {
            public ValueWrapper(StdResultValueModel item)
            {
                Item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public ValueWrapper()
            {
                Item = new StdResultValueModel() {
                    CalcTime = CalculateTime.After,
                    Expression = "",
                    Name = ""
                };
            }

            public StdResultValueModel Item { get; }

            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public string Expression { get => Item.Expression; set { Item.Expression = value; NotifyPropertyChanged(); } }

            public CalculateTime CalcTime { get => Item.CalcTime; set { Item.CalcTime = value; NotifyPropertyChanged(); } }
        }

        private ObservableCollection<ValueWrapper> _values;

        public ObservableCollection<ValueWrapper> Values {
            get {
                if (_values != null) {
                    _values.UnSubscribe(ValuesItem_Changed);
                    _values.CollectionChanged -= Values_CollectionChanged;
                }

                if (Item == null)
                    return null;

                _values = new ObservableCollection<ValueWrapper>(Item.Values.Select(i => new ValueWrapper(i)));
                _values.Subscribe(ValuesItem_Changed);
                _values.CollectionChanged += Values_CollectionChanged;
                return _values;
            }
        }

        /// <summary>
        /// Called when items was added to or removed from <see cref="Values"/> collection.
        /// Set <see cref="BusyVMBase.IsChanged"/> flag and subscribe/unsubscribe to items PropertyChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Values_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<ValueWrapper>()) {
                    IsChanged = true;
                    Item.Values.Add(i.Item);
                    i.PropertyChanged += ValuesItem_Changed;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<ValueWrapper>()) {
                    IsChanged = true;
                    Item.Values.Remove(i.Item);
                    i.PropertyChanged -= ValuesItem_Changed;
                }
            }
        }

        /// <summary>
        /// Called when item of <see cref="Values"/> changed. Set <see cref="BusyVMBase.IsChanged"/> flag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValuesItem_Changed(object sender, PropertyChangedEventArgs e)
        {
            IsChanged = true;
        }
        #endregion

        protected override async Task Refresh()
        {
            if (!_isNew)
                Item = await _source.GetItemAsync(Id, ct);
            else
                Item = new StdResultModel {
                    Id = Id,
                    Name = "",
                    Values = new List<StdResultValueModel>()
                };
        }

        protected override async Task Save()
        {
            Item = await _source.SaveAsync(Item, ct);
            _isNew = false;
            (SetDefaultCmd as AsyncCommand).RaiseCanExecuteChanged();
        }

        private async Task SetDefaultRoutine()
        {
            if (_isNew)
                return;
            await _source.SetDefault(Id);
            Item.Default = true;

            NotifyPropertyChanged(nameof(Default));
            (SetDefaultCmd as AsyncCommand).RaiseCanExecuteChanged();
        }
    }
}
