﻿using System;
using System.Linq;
using System.Windows.Input;

using VenSoft.WPFLibrary.Commands;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataContract;
using SPR.UI.App.Services;
using SPR.UI.App.Services.Events;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.App.VM.Common;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Dir;
using System.Collections.ObjectModel;

namespace SPR.UI.App.VM.Scenarios
{
    public class ScenarioMainListVM : ObjectsListVM<ScenarioBriefModel, ScenarioMainListVM.ScenarioWrapper>
    {
        private readonly INotificationEventsService events;
        private readonly IDataListFilterSource<ScenarioBriefModel, ScenarioFilter> source;
        private readonly IDataListSource<StepModel> stepSource;
        private readonly CopyExportCommandsHelper helper;
        private readonly ScenarioFilter filter = new ScenarioFilter();

        public ScenarioMainListVM(
            IDataListFilterSource<ScenarioBriefModel, ScenarioFilter> source,
            IDataListSource<StepModel> stepSource,
            IDeleteOperation<ScenarioBriefModel> delete,
            IWindowFactory windowFactory,
            IResourceStringProvider resources,
            INotificationEventsService events = null,
            ILogger logger = null) 
            : base(source, Wrap, logger) 
        {
            this.events = events;
            this.source = source;
            this.stepSource = stepSource;
            helper = new CopyExportCommandsHelper(() => SelectedItem, this, nameof(SelectedItem));

            AddCmd = new RelayCommand(execute: () => Commands.CommandsLibrary.NewScenarioCmd.Execute(null, null));
            ExportCmd = helper.ExportCmd;
            CopyCmd = helper.CopyCmd;
            ResetFilterCmd = new RelayCommand(() => ResetFilter());

            ConfigureDelete(delete, (w) => w.Item, null, windowFactory, 
                new DefaultDeleteStringsProvider(resources, (i) => $"{i.Code} - {i.Name}", "ScenariosListTab"));

            //if (this.events != null) {
            //    this.events.ScenarioDeleted += Events_ScenarioDeleted;
            //}
        }

        /// <summary>
        /// Add new scenario command
        /// </summary>
        public ICommand AddCmd { get; }

        public ICommand ExportCmd { get; private set; }

        public ICommand CopyCmd { get; private set; }

        public ICommand ResetFilterCmd { get; }

        public string FilterName {
            get => filter.NamePart;
            set {
                filter.NamePart = value;
                NotifyPropertyChanged();
                ResetDelayUpdateTimer();
            }
        }

        public long? FilterCode {
            get => filter.Code;
            set {
                filter.Code = value;
                NotifyPropertyChanged();
                ResetDelayUpdateTimer();
            }
        }

        public string FilterStep {
            get => filter.Step;
            set {
                filter.Step = value;
                NotifyPropertyChanged();
                ResetDelayUpdateTimer();
            }
        }

        /// <summary>
        /// List of steps with empty value
        /// </summary>
        public ObservableCollection<string> Steps { get; private set; } = new ObservableCollection<string>(Enumerable.Repeat(string.Empty, 1));

        protected override Task LoadData(bool fresh)
        {
            var stepsTask = UpdateStepsAsync();
            var refreshTask = base.LoadData(fresh);
            return Task.WhenAll(stepsTask, refreshTask);
        }

        protected override Task<IEnumerable<ScenarioBriefModel>> LoadItemsRoutineAsync(bool fresh, CancellationToken ct)
        {
            var requestFilter = new ScenarioFilter {
                NamePart = filter.NamePart,
                Code = filter.Code,
                Step = filter.Step
            };

            return source.LoadItemsAsync(requestFilter, ct);
        }

        /// <summary>
        /// Update list of steps
        /// </summary>
        /// <returns></returns>
        private async Task UpdateStepsAsync()
        {
            var loadedSteps = await stepSource.GetItemsAsync(ct);
            var newSteps = loadedSteps.Select(i => i.Name).Except(Steps).ToArray();
            foreach (var s in newSteps)
                Steps.Add(s);
        }

        public class ScenarioWrapper : IScenarioItem
        {
            public ScenarioBriefModel Item { get; }

            public ScenarioWrapper(ScenarioBriefModel item) {
                Item = item;
            }

            /// <summary>
            /// Formula or draft code.
            /// Implements IScenarioItem
            /// </summary>
            public long Id => Item.Code;

            public Guid Guid => Item.Guid;

            /// <summary>
            /// Scenario id for display in view
            /// </summary>
            public long? Code => Item.Code;

            /// <summary>
            /// Scenario name
            /// </summary>
            public string Name => Item.Name;

            /// <summary>
            /// Is scenario enabled or not
            /// </summary>
            public bool Enabled => Item.Enabled;

            /// <summary>
            /// Income step
            /// </summary>
            public string Step => Item.Step;

            /// <summary>
            /// Outcome step for success
            /// </summary>
            public string StepSuccess => Item.StepSuccess;

            /// <summary>
            /// Outcome step for reject
            /// </summary>
            public string StepReject => Item.StepReject;

            /// <summary>
            /// Scenario quota
            /// </summary>
            public byte? Quota => Item.Quota;

            /// <summary>
            /// Scenario has filter or not
            /// </summary>
            public bool? HasFilter => Item.HasFilter;

            /// <summary>
            /// Scenario priority
            /// </summary>
            public short? Priority => Item.Prior;
        }

        private static ScenarioWrapper Wrap(ScenarioBriefModel brief) => new ScenarioWrapper(brief);

        private void Events_ScenarioDeleted(object sender, Services.Events.Args.GuidArgs e) {
            ScenarioWrapper item = Items.Cast<ScenarioWrapper>().Where(i => i.Guid == e.Guid).FirstOrDefault();
            if (item != null)
                Items.Remove(item);
        }

        private void ResetFilter()
        {
            filter.Step = null;
            filter.Code = null;
            filter.NamePart = null;
            NotifyPropertyChanged(nameof(FilterStep));
            NotifyPropertyChanged(nameof(FilterCode));
            NotifyPropertyChanged(nameof(FilterName));
            var _ = DelayFilterTimerAction();
        }
    }
}
