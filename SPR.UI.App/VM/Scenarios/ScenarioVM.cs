﻿using SPR.DAL.Enums;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataModel;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Services;
using SPR.UI.App.VM.Base;
using SPR.UI.App.VM.Common;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Scenarios
{
    public class ScenarioVM : DataObjectVM<ScenarioModel>
    {
        public interface IStringsProvider
        {
            string ActivateConfirmation { get; }
            string DeactivateConfirmation { get; }
        }

        private readonly IDataListSource<StdResultSlimModel> _stdResultsSource;
        private readonly ObservableCollectionManager<CalcResultWrapper> _calcResultsManager;
        private readonly ObservableCollectionManager<CheckupWrapper> _checkupsManager;
        private readonly IScenarioSource _source;
        private readonly IWindowFactory _windowFactory;
        private readonly IStringsProvider _stringsProvider;

        public ScenarioVM(
            Guid? guid,
            IScenarioSource source,
            IDataListSource<StdResultSlimModel> stdResultsSource,
            ILogger logger, 
            IConfirmActionCommentProvider commentProvider,
            IWindowFactory windowFactory,
            IStringsProvider stringsProvider) 
            : base(guid, SerializedObjectType.Scenario, source, logger, commentProvider)
        {
            _stdResultsSource = stdResultsSource;
            _source = source;
            _windowFactory = windowFactory;
            _stringsProvider = stringsProvider;

            _calcResultsManager = CreateCollectionManager(() => Item?.CalcResults, i => new CalcResultWrapper(i));
            _checkupsManager = CreateCollectionManager(() => Item?.Checkups, i => new CheckupWrapper(i));
        }

        #region Main properties
        /// <summary>
        /// Is scenario enabled or not
        /// </summary>
        public bool Enabled {
            get => MainItem?.Enabled ?? false;
            set { 
                if (MainItem != null && IsBusy == false && MainItem.Enabled != value) {
                    var _ = PerformBusyOperation(() => ActivationScenarioRoutine(value));
                }
            }
        }

        /// <summary>
        /// Scenario version
        /// </summary>
        public int Version => Item?.Version ?? 0;

        /// <summary>
        /// Scenario minor version
        /// </summary>
        public int MinorVersion => Item?.MinorVersion ?? 0;

        /// <summary>
        /// Name of the scenario
        /// </summary>
        public override string Name { get => Item?.Name; set { Item.Name = value; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Description of the scenario
        /// </summary>
        public string Descr { get => Item?.Descr; set { Item.Descr = value; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Incoming step
        /// </summary>
        public string Step { get => Item?.Step; set { Item.Step = value; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Step on success
        /// </summary>
        public string StepSuccess { get => Item?.StepSuccess; set { Item.StepSuccess = value; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Step on reject
        /// </summary>
        public string StepReject { get => Item?.StepReject; set { Item.StepReject = value; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Is primary scenario or not
        /// </summary>
        public bool IsPrimary { get => Quota >= 100; set { Quota = value ? Quota = 100 : Quota = 50; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Scenario quota
        /// </summary>
        public byte Quota { 
            get => Item?.Quota ?? 0; 
            set { 
                Item.Quota = Math.Min((byte)100, Math.Max((byte)1, value)); 
                SetIsChangedAndNotify(); 
                NotifyPropertyChanged(nameof(IsPrimary)); 
            } 
        }

        /// <summary>
        /// Scenario priority
        /// </summary>
        public short Prior { get => Item?.Prior ?? 0; set { Item.Prior = value; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Scenario filter
        /// </summary>
        public FormulaBriefModel FilterFormula { get => Item?.FilterFormula; set { Item.FilterFormula = value; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Should scenario perform all checkups or stop on first reject
        /// </summary>
        public bool PerformAllCheckups { get => Item?.PerformAllCheckups ?? false; set { Item.PerformAllCheckups = value; SetIsChangedAndNotify(); } }
        #endregion

        #region Std results
        /// <summary>
        /// Standard result for scenario
        /// </summary>
        public StdResultBriefModel StdResult { 
            get => Item?.StdResult; 
            set { 
                Item.StdResult = value; 
                SetIsChangedAndNotify(); 
                NotifyPropertyChanged(nameof(StdResultId)); 
            } 
        }

        /// <summary>
        /// standard result's id
        /// </summary>
        public Guid? StdResultId { 
            get => Item?.StdResult?.Id;
            set {
                if (value.HasValue)
                    StdResult = StdResultsList.FirstOrDefault(i => i.Id == value.Value);
                else
                    StdResult = null;
            }
        }

        /// <summary>
        /// List of all standard results
        /// </summary>
        public ObservableCollection<StdResultBriefModel> StdResultsList { get; private set; }
        #endregion

        #region Calc results
        /// <summary>
        /// Calc result model wrapping class
        /// </summary>
        public class CalcResultWrapper : NotifyPropertyChangedBase, IItemContainer<CalcResultModel>
        {
            public CalcResultWrapper(CalcResultModel item)
            {
                Item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public CalcResultWrapper()
            {
                Item = new CalcResultModel() { CalcTime = CalculateTime.Before, Enabled = true, Out = true };
            }

            public CalcResultModel Item { get; }

            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public FormulaBriefModel Formula { get => Item.Formula; set { Item.Formula = value; NotifyPropertyChanged(); } }

            public bool Enabled { get => Item.Enabled; set { Item.Enabled = value; NotifyPropertyChanged(); } }

            public int Order { get => Item.Order; set { Item.Order = value; NotifyPropertyChanged(); } }

            public bool Out { get => Item.Out; set { Item.Out = value; NotifyPropertyChanged(); } }

            public CalculateTime CalcTime { get => Item.CalcTime; set { Item.CalcTime = value; NotifyPropertyChanged(); } }
        }

        public ObservableCollection<CalcResultWrapper> CalcResults => _calcResultsManager.Create();
        #endregion

        #region Checkups
        public class CheckupWrapper : NotifyPropertyChangedBase, IItemContainer<CheckupModel>
        {
            public CheckupWrapper(CheckupModel item)
            {
                Item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public CheckupWrapper()
            {
                Item = new CheckupModel { Enabled = true };
            }

            public CheckupModel Item { get; }

            public FormulaScenarioCheckupModel Formula { get => Item.Formula; set { Item.Formula = value; NotifyPropertyChanged(); } }

            public bool Enabled { get => Item.Enabled; set { Item.Enabled = value; NotifyPropertyChanged(); } }

            public int Order { get => Item.Order; set { Item.Order = value; NotifyPropertyChanged(); } }

            public int? RejectCode => Item.Formula?.RejectCode;

            public string RejectStep => Item.Formula?.RejectStep;

            public IEnumerable<int> RejectCodes { 
                get {
                    if (Item.Formula == null)
                        return Enumerable.Empty<int>();

                    IEnumerable<int> list;
                    if (Item.Formula.RejectCode != null)
                        list = Enumerable.Repeat(Item.Formula.RejectCode.Value, 1);
                    else
                        list = Enumerable.Empty<int>();

                    if (Item.Formula.NestedRejectCodes != null)
                        list = list.Concat(Item.Formula.NestedRejectCodes);

                    return list;
                } 
            }

            public IEnumerable<string> RejectSteps {
                get {
                    if (Item.Formula == null)
                        return Enumerable.Empty<string>();

                    IEnumerable<string> list;
                    if (Item.Formula.RejectStep != null)
                        list = Enumerable.Repeat(Item.Formula.RejectStep, 1);
                    else
                        list = Enumerable.Empty<string>();

                    if (Item.Formula.NestedSteps != null)
                        list = list.Concat(Item.Formula.NestedSteps);

                    return list;
                }
            }

            public bool AbortScenario => (Item.Formula?.AbortScenario ?? false) || (Item.Formula?.NestedAbortScenario ?? false);
        }

        public ObservableCollection<CheckupWrapper> Checkups => _checkupsManager.Create();
        
        public IEnumerable<FormulaScenarioCheckupModel> CheckupsFormulaList {
            get => Item.Checkups.Select(i => i.Formula);
            set {
                var toDelete = new List<CheckupModel>();
                foreach (var item in Item.Checkups) {
                    if (!value.Any(i => i.Guid == item.Formula.Guid))
                        toDelete.Add(item);
                }

                var toAdd = new List<FormulaScenarioCheckupModel>();
                foreach (var item in value) {
                    if (!Item.Checkups.Any(i => i.Formula.Guid == item.Guid))
                        toAdd.Add(item);
                }

                foreach (var item in toDelete)
                    Item.Checkups.Remove(item);
                foreach (var item in toAdd) {
                    Item.Checkups.Add(new CheckupModel { 
                        Enabled = true,
                        Formula = item
                    });
                }

                SetIsChangedAndNotify(nameof(Checkups));
            }
        }
        #endregion


        #region ISPRItem implementation
        public override long ID => Code ?? 0;

        public override int Ver => Version;
        #endregion

        protected override async Task Refresh()
        {
            //TODO: assign default result for new created object
            if (StdResultsList == null) {
                Task baseRefresh = base.Refresh();
                Task<IEnumerable<StdResultSlimModel>> stdResultsTask = _stdResultsSource.GetItemsAsync(ct);

                await Task.WhenAll(baseRefresh, stdResultsTask);

                StdResultsList = new ObservableCollection<StdResultBriefModel>(stdResultsTask
                    .Result
                    .Select(i => new StdResultBriefModel { 
                        Id = i.Id,
                        Name = i.Name
                    }));

                if (IsNew) {
                    var def = stdResultsTask.Result.FirstOrDefault(i => i.Default);
                    if (def != null)
                        StdResult = new StdResultBriefModel { Id = def.Id, Name = def.Name };                    
                }            
            } else {
                await base.Refresh();
            }                        
        }

        private async Task ActivationScenarioRoutine(bool enabled)
        {
            if (!_windowFactory.ShowConfirmDialog(enabled
                ? _stringsProvider.ActivateConfirmation
                : _stringsProvider.DeactivateConfirmation))
                return;
            await _source.ActivateAsync(Guid, enabled);
            MainItem.Enabled = enabled;
            NotifyPropertyChanged(nameof(Enabled));
        }

        protected override ScenarioModel CreateNewItem()
        {
            return new ScenarioModel {
                Quota = 100,
                Guid = Guid,
                Enabled = false,
                CalcResults = new List<CalcResultModel>(),
                Checkups = new List<CheckupModel>()
            };
        }
    }
}
