﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SPR.DAL.Enums;
using SPR.UI.App.Abstractions.Tests;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.UI.App.VM.Scenarios
{
    public class ScenarioTabsVMFactory : IDataObjectVMFactory<ScenarioModel>
    {
        private readonly ITypeResolver _typeResolver;

        public ScenarioTabsVMFactory(ITypeResolver typeResolver)
        {
            _typeResolver = typeResolver ?? throw new ArgumentNullException(nameof(typeResolver));
        }

        public ICommonVM CreateJournalVM(Guid guid)
            => _typeResolver.Resolve<ICommonVM>(Modules.TAB_OBJECT_JOURNAL, new[] {
                new KeyValuePair<Type, object>(typeof(SerializedObjectType), SerializedObjectType.Scenario),
                new KeyValuePair<Type, object>(typeof(Guid), guid)
            });

        public ITestVM CreateTestVM(IDataObjectVM<ScenarioModel> vm) 
            => _typeResolver.Resolve<ScenarioTestVM>(null,
               new KeyValuePair<Type, object>(typeof(ITestItemAccessor<ScenarioModel>), vm.ToTestItemAccessor()));

        public ICommonVM CreateUsageVM(Guid guid) => null;
    }
}
