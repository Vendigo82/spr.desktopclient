﻿using Newtonsoft.Json.Linq;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Tests;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.WebService.TestModule.Client;
using SPR.WebService.TestModule.DataContract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Scenarios
{
    public class ScenarioTestVM : BusyVMBase, ITestVM
    {
        private string requestData;

        private readonly ITestItemAccessor<ScenarioModel> itemAccessor;
        private readonly ITestModuleClient testClient;
        private readonly ITestResultsAcceptor testResultsAcceptor;
        private readonly ITestInputDataProvider testDataProvider;

        public ScenarioTestVM(
            ITestItemAccessor<ScenarioModel> itemAccessor,
            ITestModuleClient testClient,
            ITestResultsAcceptor testResultsAcceptor,
            ITestInputDataProvider testDataProvider,
            ILogger logger) 
            : base(logger, changeable: false, refreshable: false)
        {
            this.itemAccessor = itemAccessor ?? throw new ArgumentNullException(nameof(itemAccessor));
            this.testClient = testClient ?? throw new ArgumentNullException(nameof(testClient));
            this.testResultsAcceptor = testResultsAcceptor ?? throw new ArgumentNullException(nameof(testResultsAcceptor));
            this.testDataProvider = testDataProvider;

            TestCmd = new AsyncCommand(
                async () => await PerformBusyOperation(TestRoutine),
                canExecute: () => IsBusy == false,
                errorHandler: ErrorHandler);

            PropertyChanged += ScenarioTestVM_PropertyChanged;

            RequestData = this.testDataProvider.GetInputData();
        }

        /// <summary>
        /// Run test command
        /// </summary>
        public ICommand TestCmd { get; }

        private void ScenarioTestVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(IsBusy))
                (TestCmd as AsyncCommand).RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Http request body
        /// </summary>
        public string RequestData { get => this.requestData; set { this.requestData = value; NotifyPropertyChanged(); } }

        protected override Task Refresh() => Task.CompletedTask;

        /// <summary>
        /// Test scenario routine
        /// </summary>
        /// <returns></returns>
        private async Task TestRoutine()
        {
            TestResponse<ScenarioResultDto> response;

            JObject data = string.IsNullOrEmpty(RequestData) ? new JObject() : JObject.Parse(RequestData);
            var shownObject = this.itemAccessor.ShownObject;

            this.testResultsAcceptor.StartTesting(DataContract.ObjectType.Scenario, this.itemAccessor.Id, shownObject);

            try {
                if (this.itemAccessor.Item == null)
                    response = await this.testClient.TestScenario(new TestMainScenarioRequest() {
                        ScenarioId = this.itemAccessor.Id,
                        Data = data,
                    }, CancellationToken.None);
                else {
                    response = await this.testClient.TestScenario(new TestDraftScenarioRequest() {
                        Scenario = this.itemAccessor.Item,
                        Data = data,
                    }, CancellationToken.None);
                }
            } catch {
                this.testResultsAcceptor.Cancel(DataContract.ObjectType.Scenario, this.itemAccessor.Id, shownObject);
                throw;
            }

            this.testResultsAcceptor.PushResult(this.itemAccessor.Id, shownObject, response);
        }
    }
}
