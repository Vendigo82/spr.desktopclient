﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Scenarios
{
    public class StdResultsUsageVM : BusyVMBase
    {
        /// <summary>
        /// Data source
        /// </summary>
        private readonly IStdResultsSource source;

        /// <summary>
        /// Std results item's id
        /// </summary>
        private readonly Guid itemId;

        public StdResultsUsageVM(Guid itemId, IStdResultsSource source, ILogger logger) 
            : base(logger, changeable: false, refreshable: true)
        {
            this.itemId = itemId;
            this.source = source ?? throw new ArgumentNullException(nameof(source));
        }

        public IEnumerable<FormulaBriefModel> Formulas => Enumerable.Empty<FormulaBriefModel>();

        public IEnumerable<ScenarioSlimModel> Scenarios { get; private set; } = Enumerable.Empty<ScenarioSlimModel>();

        protected override async Task Refresh()
        {
            var usage = await source.GetUsageAsync(itemId, ct);
            Scenarios = usage.Scenarios;
        }
    }
}
