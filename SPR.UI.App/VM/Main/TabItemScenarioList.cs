﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SPR.UI.App.VM.Main
{
    public class TabItemObjectsList : TabItemVM
    {
        public TabItemObjectsList(Control content, string caption) : base(content, caption, nameof(TabItemObjectsList), false) {
            (content as INotifyPropertyChanged).PropertyChanged += TabItemObjectsList_PropertyChanged;
        }

        private void TabItemObjectsList_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "SelectedItem") {
                NotifyPropertyChanged(nameof(ExportedObject));
                NotifyPropertyChanged(nameof(IsExportEnabled));
            }
        }

        /// <summary>
        /// Export button is enabled
        /// </summary>
        public bool IsExportEnabled => ExportedObject != null;

        /// <summary>
        /// Object for export
        /// </summary>
        public object ExportedObject {
            get {
                Type t = Content.GetType();
                PropertyInfo pi = t.GetProperty("SelectedItem");
                if (pi == null || pi.GetMethod == null)
                    return null;
                return pi.GetValue(Content);
            }
        }
    }
}
