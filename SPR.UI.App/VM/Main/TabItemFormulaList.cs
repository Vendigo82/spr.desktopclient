﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SPR.UI.App.VM.Main
{
    /// <summary>
    /// TabItem for formula list
    /// </summary>
    public class TabItemFormulaList : TabItemVM
    {
        public TabItemFormulaList(Control content) : base(content, "Формулы", nameof(TabItemFormulaList), false) {
            (content as INotifyPropertyChanged).PropertyChanged += TabItemFormulaList_PropertyChanged;
        }

        private void TabItemFormulaList_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "SelectedItem") {
                NotifyPropertyChanged(nameof(ExportedObject));
                NotifyPropertyChanged(nameof(IsExportEnabled));
            }
        }

        /// <summary>
        /// Export button is enabled
        /// </summary>
        public bool IsExportEnabled => ExportedObject != null;

        /// <summary>
        /// Object for export
        /// </summary>
        public object ExportedObject {
            get {
                Type t = Content.GetType();
                PropertyInfo pi = t.GetProperty("SelectedItem");
                if (pi == null || pi.GetMethod == null)
                    return null;
                return pi.GetValue(Content);
            }
        }
    }
}
