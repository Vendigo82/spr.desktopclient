﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Threading;

using SPR.DAL.Enums;
using SPR.UI.WebService.DataContract.Profile;
using SPR.UI.WebClient;
using VenSoft.WPFLibrary;
using VenSoft.WPFLibrary.Commands;
using SPR.UI.App.VM.Base;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.WebService;

namespace SPR.UI.App.VM.Main
{
    /// <summary>
    /// View model for user's profile
    /// </summary>
    public class ProfileVM : BusyVMBase
    {
        private string _login;
        private string _name;
        private bool _changePasswordMode = false;
        private bool _passwordsNotEqual = false;
        private bool _invalidOldPassword = false;
        private bool _invalidNewPassword = false;
        private string _invalidNewPasswordReason = null;
        private bool _isChanging = false;

        private readonly IBadPasswordReasonInterpretator _interpretator;
        private readonly IProfileService _source;

        public interface IBadPasswordReasonInterpretator
        {
            string GetText(string reason);
        }

        public ProfileVM(IProfileService source, ILogger logger, IBadPasswordReasonInterpretator reasonInterpretator = null)
            : base(logger)
        {
            _source = source;
            _interpretator = reasonInterpretator;

            SwitchToChangePasswordCmd = new RelayCommand(
                () => { ChangePasswordMode = true; },
                () => ChangePasswordMode == false);

            CancelChangePasswordCmd = new RelayCommand(
                () => { ChangePasswordMode = false; },
                () => _isChanging == false);

            ChangePasswordCmd = new AsyncCommand<Tuple<object, object, object>>(
                async (p) => await ChangePassword(Extract(p.Item1), Extract(p.Item2), Extract(p.Item3)),
                errorHandler: ErrorHandler
            );
        }

        public string Login { get => _login; private set { _login = value; NotifyPropertyChanged(); } }

        public string Name { get => _name; set { _name = value; NotifyPropertyChanged(); IsChanged = true; } }

        //public UserLoginTypeEnum LoginType { get; private set; }

        //public IEnumerable<UserRoleEnum> Roles { get; private set; }

        /// <summary>
        /// Show is form in change password mode
        /// </summary>
        public bool ChangePasswordMode { 
            get => _changePasswordMode; 
            private set { _changePasswordMode = value; NotifyPropertyChanged(); } 
        }

        /// <summary>
        /// New and repeat passwords are not equal
        /// </summary>
        public bool PasswordsNotEqual { 
            get => _passwordsNotEqual; 
            private set { _passwordsNotEqual = value; NotifyPropertyChanged(); } 
        }

        /// <summary>
        /// Old password was invalid
        /// </summary>
        public bool InvalidOldPassword { 
            get => _invalidOldPassword; 
            private set { _invalidOldPassword = value; NotifyPropertyChanged(); } 
        }

        /// <summary>
        /// New password was invalid
        /// </summary>
        public bool InvalidNewPassword {
            get => _invalidNewPassword;
            private set { _invalidNewPassword = value; NotifyPropertyChanged(); } 
        }

        /// <summary>
        /// Reason of rejecting new password
        /// </summary>
        public string InvalidNewPasswordReason { 
            get => _invalidNewPasswordReason;
            private set { _invalidNewPasswordReason = value; NotifyPropertyChanged(); } 
        }

        /// <summary>
        /// Switch form to change password mode
        /// </summary>
        public ICommand SwitchToChangePasswordCmd { get; }

        /// <summary>
        /// Cancel change password mode
        /// </summary>
        public ICommand CancelChangePasswordCmd { get; }

        /// <summary>
        /// Change password command
        /// Required parameted Tuple with three wrapped string parameters (old, new and repeat new password)
        /// <see cref="IWrappedParameter{T}"/>
        /// </summary>
        public ICommand ChangePasswordCmd { get; }

        /// <summary>
        /// Refresh operation
        /// </summary>
        protected override async Task Refresh() {
            Profile profile = await _source.Profile(CancellationToken.None);
            UpdateProperties(profile);
        }

        /// <summary>
        /// Save operation
        /// </summary>
        protected override async Task Save() {
            Profile profile = await _source.Edit(Name, CancellationToken.None);
            UpdateProperties(profile);
        }

        /// <summary>
        /// Perform change password
        /// </summary>
        /// <param name="oldpsw"></param>
        /// <param name="newpsw"></param>
        /// <param name="repeatpsw"></param>
        /// <returns></returns>
        private async Task ChangePassword(string oldpsw, string newpsw, string repeatpsw) {
            ClearChangePasswordResults();

            Error = null;
            if (string.IsNullOrEmpty(oldpsw))
                oldpsw = null;
            if (string.IsNullOrEmpty(newpsw))
                newpsw = null;
            if (string.IsNullOrEmpty(repeatpsw))
                repeatpsw = null;

            if (newpsw != repeatpsw) {
                PasswordsNotEqual = true;
                return;
            }

            try {
                _isChanging = true;
                (CancelChangePasswordCmd as RelayCommand).RaiseCanExecuteChanged();
                var result = await _source.ChangePassword(oldpsw, newpsw, CancellationToken.None);
                switch (result.Result)
                {
                    case ChangePasswordResultType.Success: 
                        ChangePasswordMode = false;
                        break;

                    case ChangePasswordResultType.InvalidOldPassword:
                        InvalidOldPassword = true;
                        break;

                    case ChangePasswordResultType.InvalidNewPassword:
                        InvalidNewPassword = true;
                        InvalidNewPasswordReason = _interpretator?.GetText(result.ReasonCode) ?? result.ReasonCode;
                        break;
                }
            } 
            finally {
                _isChanging = false;
                (CancelChangePasswordCmd as RelayCommand).RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// Converts <paramref name="o"/> to <see cref="IWrappedParameter{T}" and get string/>
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private string Extract(object o) => ((IWrappedParameter<string>)o).Value;

        /// <summary>
        /// Clear change password error statuses
        /// </summary>
        private void ClearChangePasswordResults() {
            PasswordsNotEqual = false;
            InvalidOldPassword = false;
            InvalidNewPassword = false;
            InvalidNewPasswordReason = null;
        }

        private void UpdateProperties(Profile profile) {
            _login = profile.Login;
            _name = profile.Name;
            //LoginType = profile.LoginType;
            //Roles = profile.Roles != null ? profile.Roles.Select(p => p.UserRole).ToArray() : new UserRoleEnum[] { };
            (SwitchToChangePasswordCmd as RelayCommand).RaiseCanExecuteChanged();
        }


    }
}
