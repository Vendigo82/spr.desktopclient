﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SPR.UI.App.VM.Main
{
    public static class TabItemVMExtentions
    {
        /// <summary>
        /// Get command with name <paramref name="name"/> from data context.
        /// Search property. If property not found, then returns null. 
        /// If property has found, check property type. If property type is not <see cref=" ICommand"/> 
        /// or type derived from <see cref="ICommand"/> then throw exception.
        /// </summary>
        /// <param name="tab"></param>
        /// <param name="name">name of property</param>
        /// <returns>Value of property or null</returns>
        /// <exception cref="InvalidCastException">Property type is not <see cref="ICommand"/> or derived from it</exception>
        public static ICommand TryGetCommand(this ITabItemVM tab, string name) {
            if (tab != null && tab.DataContext != null) {
                PropertyInfo pi = tab.DataContext.GetType().GetProperty(name);
                if (pi == null)
                    return null;

                Type ptype = pi.PropertyType;
                if (ptype == typeof(ICommand) || ptype.FindInterfaces(Module.FilterTypeName, nameof(ICommand)).Any()) {
                    object value = pi.GetValue(tab.DataContext);
                    if (value == null)
                        return null;

                    return (ICommand)value;
                } else
                    throw new InvalidCastException(
                        $"Type of property [{name}] of DataContext of tab with name {tab.Title} is {pi.PropertyType.Name},"
                        + " but expected ICommand");
            }
            return null;
        }

        public static bool IsChanged(this ITabItemVM item) {
            if (item.DataContext == null)
                return false;
            Type type = item.DataContext.GetType();
            PropertyInfo info = type.GetProperty("IsChanged");
            if (info == null)
                return false;
            return (bool)info.GetValue(item.DataContext);
        }
    }
}
