﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SPR.UI.App.VM.Main
{
    /// <summary>
    /// Standard results tab item VM
    /// </summary>
    public class StdResultsTabItemVM : TabItemBase, ITabItemVM
    {
        public StdResultsTabItemVM(Control content) : base(content)
        {
            if (content.DataContext is INotifyPropertyChanged p)
                p.PropertyChanged += DataContext_PropertyChanged;
            if (content.DataContext.TryGetPropertyValue("Name", out string val))
                _vmName = val;
        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name" && (Content as Control).DataContext.TryGetPropertyValue(e.PropertyName, out string val)) {
                _vmName = val;
                NotifyPropertyChanged(nameof(Title));
            }                
        }

        private string _vmName;
        private Guid Id => (Content as Control).DataContext.GetPropertyValue<Guid>("Id");

        public override string Title => string.Concat("SR-", _vmName ?? string.Empty);

        public override object Key => new StdResultsKey(Id);

        public static object CreateKey(Guid id) => new StdResultsKey(id);

        private class StdResultsKey
        {
            private Guid id;

            public StdResultsKey(Guid id)
            {
                this.id = id;
            }

            public override bool Equals(object obj)
            {
                if (obj is StdResultsKey k)
                    return id == k.id;
                else
                    return base.Equals(obj);
            }

            public override int GetHashCode() => id.GetHashCode();
        }
    }
}
