﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.UI.App.Services;

namespace SPR.UI.App.VM.Main
{
    public class ProfileVMReasonInterpretator : ProfileVM.IBadPasswordReasonInterpretator
    {
        private readonly IResourceStringProvider _provider;
        private const string PREFIX = "ProfileTab.BadNewPassword.";

        public ProfileVMReasonInterpretator(IResourceStringProvider provider) {
            _provider = provider;
        }

        public string GetText(string reason) => (_provider?.GetString(PREFIX + reason)) ?? reason;
    }
}
