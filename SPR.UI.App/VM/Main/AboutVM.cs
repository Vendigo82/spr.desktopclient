﻿using SPR.UI.App.Model;
using SPR.UI.App.Services;

namespace SPR.UI.App.VM.Main
{
    public class AboutVM
    {
        private readonly IAppVersionProvider _version;
        private readonly ILogoProvider _logo;

        public AboutVM(IAppVersionProvider version, ILogoProvider logo) {
            _version = version;
            _logo = logo;
        }

        public string AppVersion => _version.AppVersion;

        /// <summary>
        /// Path to the logo
        /// </summary>
        public string Logo => _logo.PathToLogo;
    }
}
