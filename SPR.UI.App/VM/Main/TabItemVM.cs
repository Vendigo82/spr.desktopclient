﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using SPR.DAL.Enums;
using SPR.UI.App.DataContract;

namespace SPR.UI.App.VM.Main
{
    public interface ITabItemVM
    {
        UIElement Content { get; }
        object DataContext { get; }
        string Title { get; }
        bool CanClose { get; }
        bool IsChanged { get; }

        object Key { get; }
    }    

    public abstract class TabItemBase : NotifyPropertyChangedBase, ITabItemVM
    {
        public TabItemBase(Control content) {
            Content = content;
            DataContext = content.DataContext;
            (DataContext as INotifyPropertyChanged).PropertyChanged += TabItemBase_PropertyChanged;
        }

        private void TabItemBase_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(IsChanged))
                NotifyPropertyChanged(nameof(IsChanged));
        }

        public UIElement Content { get; }
        public object DataContext { get; }
        public bool CanClose => true;

        public abstract string Title { get; }

        public bool IsChanged => this.IsChanged();

        public abstract object Key { get; }

        //public abstract string Key { get; }
    }

    public class TabItemVM : NotifyPropertyChangedBase, ITabItemVM
    {
        public UIElement Content { get; }
        public object DataContext { get; }
        public string Title { get; }
        public bool CanClose { get; }
        public bool IsChanged => this.IsChanged();

        public object Key { get; }

        public TabItemVM(Control content, string title, string key, bool canClose = true) {
            Content = content;
            DataContext = content.DataContext;
            Title = title;
            CanClose = canClose;
            Key = key ?? title;
            if (DataContext is INotifyPropertyChanged propertyChanged)
                propertyChanged.PropertyChanged += TabItemVM_PropertyChanged;
        }

        private void TabItemVM_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(IsChanged))
                NotifyPropertyChanged(nameof(IsChanged));
        }
    }

    public abstract class SPRTabItemVM : TabItemBase, ITabItemVM
    {
        private string _prefix;
        private readonly SerializedObjectType type;

        public SPRTabItemVM(Control content, string prefix, SerializedObjectType type) : base(content) {
            _prefix = prefix;
            this.type = type;
            Item = content.DataContext as ISPRItem;

            Item.PropertyChanged += SPRTabItemVM_PropertyChanged;            
        }

        private void SPRTabItemVM_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(Item.ID) || e.PropertyName == nameof(Item.Name))
                NotifyPropertyChanged(nameof(Title));
        }

        public ISPRItem Item { get; }

        public override string Title {
            get {
                string name = Item.Name ?? "";
                if (name.Length > 10)
                    name = name.Substring(0, 10);
                if (Item.ID != 0)
                    return string.Concat(_prefix, Item.ID.ToString(), " - ", name);
                else
                    return string.Concat(_prefix, "* - ", name);
            }
        }

        public override object Key => new ObjectTabKey(type, Item.Guid);
    }

    public class FormulaTabItem : SPRTabItemVM
    {
        public FormulaTabItem(Control content) : base(content, "F", SerializedObjectType.Formula) { }

        public static ObjectTabKey CreateKey(Guid guid) => new ObjectTabKey(SerializedObjectType.Formula, guid);
    }

    public class ScenarioTabItem : SPRTabItemVM
    {
        public ScenarioTabItem(Control content) : base(content, "S", SerializedObjectType.Scenario) { }

        public static ObjectTabKey CreateKey(Guid guid) => new ObjectTabKey(SerializedObjectType.Scenario, guid);
    }
    
    public class ObjectTabKey
    {
        public ObjectTabKey(SerializedObjectType type, Guid guid)
        {
            Type = type;
            Guid = guid;
        }

        public SerializedObjectType Type { get; }

        public Guid Guid { get; }

        public override bool Equals(object obj)
        {
            if (obj is ObjectTabKey k)
                return Guid == k.Guid && Type == k.Type;
            else
                return base.Equals(obj);
        }

        public override int GetHashCode() => Guid.GetHashCode();
    }

}
