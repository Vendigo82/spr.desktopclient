﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Commands;
using SPR.UI.App.DataContract;
using SPR.UI.App.Model;
using SPR.UI.App.Model.Objects;
using SPR.UI.App.Services;
using SPR.UI.App.Services.Events;
using SPR.UI.App.View;
using SPR.UI.App.Windows.StepConstraints;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Main
{

    /// <summary>
    /// ViewModel for main window
    /// </summary>
    public class MainVM : NotifyPropertyChangedBase, IDisposable
    {
        private readonly LinkedList<ITabItemVM> _selectedTabs = new LinkedList<ITabItemVM>();
        private const int _selectedTabsOrderMaxLength = 50;

        private readonly IMainWndModel _model;
        private readonly IWindowFactory _wndFactory;
        private readonly IFactory<ICommonVM> _vmFactory;
        private readonly ITabTitleProvider _titleProvider;
        private readonly ILogger _logger;
        private readonly INotificationEventsService _events;

        /// <summary>
        /// performing background operation
        /// </summary>
        private bool _isBusy = true;

        /// <summary>
        /// user interface enabled or disabled
        /// </summary>
        private bool _isEnabled = false;

        /// <summary>
        /// user was perform authorization
        /// </summary>
        private bool _isAuthorized = false;

        /// <summary>
        /// App initializing was complete and app version is OK
        /// </summary>
        private bool _isInitializedComplete = false;

        /// <summary>
        /// Tabs Scenario and Formula was created and added
        /// </summary>
        private bool _mainTabsWasCreated = false;

        /// <summary>
        /// List of commands for tabs
        /// </summary>
        private readonly Dictionary<string, SwitchableCommand> _tabCommands = new Dictionary<string, SwitchableCommand>();

        public MainVM(
            IMainWndModel model,
            IWindowFactory wndFactory,
            IFactory<ICommonVM> vmFactory,
            ITabTitleProvider titleProvider,
            ILogger logger = null,
            INotificationEventsService events = null)
        {
            _model = model;
            _wndFactory = wndFactory;
            _vmFactory = vmFactory;
            _titleProvider = titleProvider;
            _logger = logger;
            _events = events;

            if (_events != null)
                _events.NeedAuthentification += Events_NeedAuthentification;

            Tabs = new ObservableCollection<ITabItemVM>();
            Tabs.CollectionChanged += Tabs_CollectionChanged;

            RegisterBinding(CommandsLibrary.OpenCmd, (s, e) => ExecOpenCommand((string)e.Parameter));
            RegisterBinding(CommandsLibrary.OpenFormulaCmd, (s, e) => OpenFormulaEditView(ArgToFormulaData(e.Parameter)));
            RegisterBinding(CommandsLibrary.NewFormulaCmd, (s, e) => CreateNewFormulaTab());            
            //RegisterBinding(CommandsLibrary.DeleteFormulaCmd, (s, e) => DeleteFormula(ArgToFormulaData(e.Parameter)));// (IIDVerExt)e.Parameter));
            RegisterBinding(CommandsLibrary.OpenScenarioCmd, (s, e) => OpenScenarioEditView(ArgToScenarioData(e.Parameter)));
            RegisterBinding(CommandsLibrary.NewScenarioCmd, (s, e) => CreateNewScenario());
            //RegisterBinding(CommandsLibrary.DeleteScenarioCmd, (s, e) => DeleteScenario(ArgToScenarioData(e.Parameter)));
            RegisterBinding(CommandsLibrary.OpenStdResultsCmd, (s, e) => OpenStdResultsEditTab((StdResultModelBase)e.Parameter));
            RegisterBinding(CommandsLibrary.NewStdResultsCmd, (s, e) => OpenStdResultsEditTab(null));
            RegisterBinding(CommandsLibrary.OpenStepConstraintsCmd, (s, e) => OpenStepConstraintsTab((StepConstraintsOpenArgs)e.Parameter));

            CreateAllCommands();
        }

        public void Initialize()
        {
            if (!SelectInstance())
            {
                Application.Current.MainWindow.Close();
                return;
            }

            CheckAppVersion();
        }

        public string WndTitle => $"ADM {_model.InstanceName ?? ""} ({_model.ServerUrl})";

        public bool IsBusy { get => _isBusy; private set { _isBusy = value; NotifyPropertyChanged(); } }

        public bool IsEnabled { get => _isEnabled; private set { _isEnabled = value; NotifyPropertyChanged(); } }

        public bool IsAuthorized { get => _isAuthorized; private set { _isAuthorized = value; NotifyPropertyChanged(); } }

        public bool IsDocumentationEnabled => _model.IsDocumentationEnabled;

        public bool IsInitializedComplete { 
            get => _isInitializedComplete; 
            private set { _isInitializedComplete = value; NotifyPropertyChanged(); } 
        }

        public CommandBindingCollection CommandBindings { get; } = new CommandBindingCollection();

        /// <summary>
        /// List of tabs
        /// </summary>
        public ObservableCollection<ITabItemVM> Tabs { get; }

        /// <summary>
        /// Current selected tab
        /// </summary>
        public ITabItemVM SelectedTab {
            get => _selectedTabs.Last?.Value;
            set {
                if (SelectedTab != value) {
                    _selectedTabs.AddLast(value);
                    while (_selectedTabs.Count > _selectedTabsOrderMaxLength)
                        _selectedTabs.RemoveFirst();
                    NotifyPropertyChanged();
                    UpdateTabCommands();
                }
            }
        }

        #region Tab commands
        public ICommand AddCmd { get; private set; }
        public ICommand DeleteCmd { get; private set; }
        public ICommand SaveCmd { get; private set; }
        public ICommand RefreshCmd { get; private set; }
        public ICommand ApplyCmd { get; private set; }

        public ICommand ExportCmd { get; private set; }
        public ICommand CopyCmd { get; private set; }

        public ICommand TestCmd { get; private set; }
        #endregion

        public void ExecOpenCommand(string parameter) {
            if (_logger?.IsEnabled(LogLevel.Debug) == true)
                _logger.LogDebug($"ExecOpenCommand: parameter {parameter}");

            string section = ExtractParameters(parameter, out IEnumerable<KeyValuePair<string, string>> pairs);

            switch (section) {

                //login window
                case CommandsLibrary.Login:
                    Login();
                    break;

                //factory command for dialog window
                case CommandsLibrary.DIALOG:
                    if (pairs.Any())
                        ShowDialogWnd(pairs.First().Key);
                    else
                        throw new InvalidOperationException("Open dialog command does not provide dialog name");
                    break;

                //factory command for tabs
                case CommandsLibrary.TAB:
                    if (pairs.Any())
                        CreateTab(pairs.First().Key);
                    else
                        throw new InvalidOperationException("Open tab command does not provide tab name");
                    break;
            }
        }

        private void OpenFormulaEditView(IFormulaItem formula)
        {
            if (SelectTabIfExists(FormulaTabItem.CreateKey(formula.Guid)))
                return;

            Control control = CreateFormulaViewControl(formula.Type, formula.Guid);
            //control = _wndFactory.CreateFormulaView(DAL.Enums.FormulaType.Expression, formula.Guid);
            //Control tab = _wndFactory.CreateTabContainer(control);
            AddFormulaTab(control);
        }

        /// <summary>
        /// Add edit existed or create new standard results tab
        /// </summary>
        /// <param name="model">Model to edit existed or null to create new</param>
        private void OpenStdResultsEditTab(StdResultModelBase model)
        {
            if (model != null && SelectTabIfExists(StdResultsTabItemVM.CreateKey(model.Id)))
                return;
            Control control = _wndFactory.CreateStdResultsView(model?.Id);
            AddStdResultsTab(control, true);
        }

        private void OpenStepConstraintsTab(StepConstraintsOpenArgs args)
        {
            var tabKey = $"{args.StepName}_postconditions";
            if (SelectTabIfExists(tabKey))
                return;

            var vm = _vmFactory.Create(Modules.TAB_STEP_CONSTRAINTS, args);
            var view = _wndFactory.CreateView(Modules.TAB_STEP_CONSTRAINTS);
            view.DataContext = vm;

            AddStaticTab(view, $"{args.StepName}: PC", tabKey);
        }

        private Control CreateFormulaViewControl(SPR.DAL.Enums.FormulaType type, Guid? guid) {
            return _wndFactory.CreateFormulaView(type, guid);          
        }

        /// <summary>
        /// Create tab with new formula item
        /// </summary>
        private void CreateNewFormulaTab() {
            if (_wndFactory.ShowSelectItemDialog<SPR.DAL.Enums.FormulaType>(Modules.FORMULA_TYPE_SELECT_WND, true, out var type)) {
                Control control = _wndFactory.CreateFormulaView(type, null);
                AddFormulaTab(control);
            }
        }

        private void OpenScenarioEditView(IScenarioItem origin)
        {
            //Control view = _uiModel.CreateScenarioEditView(origin);
            //if (view == null)
            //    return; //TODO: show message
            //AddScenarioTab(view);
            if (SelectTabIfExists(ScenarioTabItem.CreateKey(origin.Guid)))
                return;

            Control control = _wndFactory.CreateScenarioView(origin.Guid);
            AddScenarioTab(control);
        }

        private void CreateNewScenario() {
            Control view = _wndFactory.CreateScenarioView(null);
            //Control view = _uiModel.CreateNewScenarioView();
            //if (view == null)
            //    return;
            AddScenarioTab(view);
        }

        private void AddFormulaTab(Control content, bool select = true) 
        {            
            var tab = new FormulaTabItem(content);
            ICommand cmd = tab.TryGetCommand(nameof(RefreshCmd));
            if (cmd != null)
                cmd.Execute(null);

            AddTab(tab, select);
        }

        private void AddScenarioTab(Control content, bool select = true) {
            //AddTab(new ScenarioTabItem(content), select);            
            var tab = new ScenarioTabItem(content);
            ICommand cmd = tab.TryGetCommand(nameof(RefreshCmd));
            if (cmd != null)
                cmd.Execute(null);

            AddTab(tab, select);
        }

        /// <summary>
        /// Add tab with standard results
        /// </summary>
        /// <param name="content"></param>
        /// <param name="select"></param>
        private void AddStdResultsTab(Control content, bool select = true)
        { 
            AddTab(new StdResultsTabItemVM(content), select);
            if (content.DataContext is IDataInitializer i)
                i.Initialize(true);
        }

        private void AddTab(ITabItemVM tab, bool select = true) {
            //tab.Content = _wndFactory.CreateTabContainer(tab. control);
            Tabs.Add(tab);
            if (select)
                SelectedTab = tab;
        }

        /// <summary>
        /// Check if exist scenario or formula tab with that ID and select it
        /// </summary>
        /// <typeparam name="T">Formula or Scenario type</typeparam>
        /// <param name="id">Formula or Scenario ID</param>
        /// <returns>true if tab exists, and false otherwise</returns>
        private bool SelectTabIfExists(object key) 
        {
            ITabItemVM tab = Tabs.Where(f => f.Key != null && f.Key.Equals(key)).FirstOrDefault();
            if (tab != null) {
                SelectedTab = tab;
                return true;
            } else
                return false;
        }

        private void Tabs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            if (e.OldItems != null) {
                bool isSelectedClosed = false;
                foreach (var tab in e.OldItems.Cast<ITabItemVM>()) {
                    if (SelectedTab == tab)
                        isSelectedClosed = true;                    
                    while (_selectedTabs.Remove(tab)) { }
                    DisposeTab(tab);
                }

                if (isSelectedClosed) {
                    NotifyPropertyChanged(nameof(SelectedTab));
                    UpdateTabCommands();
                }
            }
        }

        private void RegisterBinding(ICommand cmd, ExecutedRoutedEventHandler handler) {
            var binding = new CommandBinding(cmd, handler);
            CommandManager.RegisterClassCommandBinding(typeof(MainVM), binding);
            CommandBindings.Add(binding);
        }

        private string ExtractParameters(string str, out IEnumerable<KeyValuePair<string, string>> pairs) {
            pairs = null;
            string[] first = str.Split(':');
            if (first.Length == 0)
                return null;
            if (first.Length > 1) {
                string[] second = first[1].Split(';');
                pairs = second.Select(s => {
                    string[] p = s.Split('=');
                    return new KeyValuePair<string, string>(p[0], p.Length > 1 ? p[1] : null);
                });
            }
            return first[0];
        }

        private bool? ShowDialogWnd(string name) {
            Window wnd = _wndFactory.CreateWindowOwned(name);
            return wnd.ShowDialog();
        }

        /// <summary>
        /// Create new tab by name
        /// </summary>
        /// <param name="name">tab name</param>
        private void CreateTab(string name, bool canClose = true, bool selectTab = true, Func<TabItemVM> tabItemFactory = null) 
        {
            if (SelectTabIfExists(name))
                return;

            Control control = _wndFactory.CreateView(name);
            ICommonVM vm = null;
            if (control.DataContext == null)
                vm = _vmFactory.Create(name);

            Control tab;
            if (control.GetType().GetCustomAttribute<ViewBusyIndicatorAttribute>() == null)
                tab = _wndFactory.CreateTabContainer(control);
            else
                tab = control;

            if (vm != null)
                tab.DataContext = vm;

            AddStaticTab(tab, _titleProvider.Title(name), name, refresh: true, canClose: canClose, select: selectTab, 
                tabItemFactory: tabItemFactory);
        }

        private void AddStaticTab(Control content, string title, string keyName, bool select = true, bool refresh = false, bool canClose = true,
            Func<TabItemVM> tabItemFactory = null) 
        {
            TabItemVM tab = tabItemFactory?.Invoke() ?? new TabItemVM(content, title, keyName, canClose: canClose);
            if (tab.DataContext != null && tab.DataContext is IDataInitializer di)
                di.Initialize(true);
            AddTab(tab, select);
        }

        private void CheckAppVersion() {
            if (_logger?.IsEnabled(LogLevel.Debug) == true)
                _logger.LogDebug("Check app version");

            string error = null;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (s, e) => {
                try {
                    InitializeService();
                    NotifyPropertyChanged(nameof(IsDocumentationEnabled));
                } catch (AggregateException exc) {
                    _logger?.LogError("Check version error", exc);
                    error = exc.InnerException.Message;
                } catch (Exception exc) {
                    _logger?.LogError("Check version error", exc);
                    error = exc.Message;
                }
            };
            worker.RunWorkerCompleted += (e, a) => {
                IsBusy = false;
                if (error != null) {
                    MessageBox.Show(error, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    Application.Current.Shutdown();                
                } else {
                    if (_logger?.IsEnabled(LogLevel.Debug) == true)
                        _logger?.LogDebug("Version is OK");
                    Login();
                }
            };
            worker.RunWorkerAsync();
        }

        private void Login() {
            if (_logger?.IsEnabled(LogLevel.Debug) == true)
                _logger?.LogDebug("Login...");
            if (ShowDialogWnd(Modules.WND_LOGIN) == true) {
                if (_logger?.IsEnabled(LogLevel.Debug) == true)
                    _logger?.LogDebug("Loginned");
                IsAuthorized = true;
                IsEnabled = true;
                NotifyPropertyChanged(nameof(WndTitle));
                NotifyPropertyChanged(nameof(IsDocumentationEnabled));
                if (_mainTabsWasCreated == false) {                  
                    CreateTab(Modules.TAB_FORMULAS_LIST, canClose: false);
                    CreateTab(Modules.TAB_SCENARIOS_LIST, canClose: false, selectTab: false);
                    _mainTabsWasCreated = true;
                }
            } else {
                IsAuthorized = false;
                IsEnabled = false;
            }
            IsInitializedComplete = true;
        }

        private bool SelectInstance()
        {
            var instances = _model.GetInstances();
            if (!instances.Any())
            {
                _wndFactory.ShowErrorDialog("ADM intances does not found in configuration file");
                return false;
            }

            InstanceSettingsModel instance;
            if (instances.Count() == 1)
                instance = instances.Single();
            else
            {
                var wnd = _wndFactory.CreateWindowVM(Modules.SELECT_INSTANCE_DLG);
                if (wnd.ShowDialog() != true)
                    return false;

                instance = (wnd.DataContext as ISelectedItemAccessor<InstanceSettingsModel>).SelectedItem;
            }

            _model.SetInstance(instance);
            NotifyPropertyChanged(nameof(WndTitle));
            return true;
        }

        private void InitializeService()
        {
            var task = _model.InitializeAsync(CancellationToken.None);
            task.Wait();
        }

        private void DisposeTab(ITabItemVM tab) {
            ((tab.Content as Control)?.DataContext as IDisposable)?.Dispose();
        }

        public void Dispose()
        {
            foreach (var tab in Tabs)
                DisposeTab(tab);
            Tabs.Clear();
        }

        /// <summary>
        /// Create SwitchableCommand for all unassigned properties of ICommand type
        /// </summary>
        private void CreateAllCommands() {
            foreach (var pi in GetType().GetProperties()) {
                if (pi.GetValue(this) == null) {
                    var ptype = pi.PropertyType;
                    if (ptype == typeof(ICommand) || ptype == typeof(SwitchableCommand))
                        CreateTabCommand(pi);
                }
            }
        }

        /// <summary>
        /// Create SwitchableCommand and assign it to property value
        /// </summary>
        /// <param name="pi"></param>
        private void CreateTabCommand(PropertyInfo pi) {
            SwitchableCommand cmd = new SwitchableCommand();
            _tabCommands.Add(pi.Name, cmd);

            pi.SetValue(this, cmd);
        }

        /// <summary>
        /// Update command for all tab commands
        /// </summary>
        private void UpdateTabCommands() {
            foreach (var pair in _tabCommands) {
                ICommand cmd = SelectedTab?.TryGetCommand(pair.Key);
                pair.Value.Command = cmd;
            }
        }

        //Method will be deleted
        private IFormulaItem ArgToFormulaData(object arg) 
        {
            if (arg is IFormulaItem fi)
                return fi;
            else if (arg is FormulaBriefModel fbd)
                return new FormulaItem { Id = fbd.Code, Type = fbd.Type, Guid = fbd.Guid, Name = fbd.Name };
            else
                throw new ArgumentException($"Unknown argument type {arg.GetType().Name}");
        }

        private IScenarioItem ArgToScenarioData(object arg) 
        {
            if (arg is IScenarioItem si)
                return si;
            else if (arg is ScenarioSlimModel m)
                return new ScenarioItem { Id = m.Code, Guid = m.Guid, Name = m.Name };            
            else
                throw new ArgumentException($"Unknown argument type {arg.GetType().Name}");
        }

        private void Events_NeedAuthentification(object sender, EventArgs e) {
            if (IsAuthorized) {
                IsAuthorized = false;
                Login();
            }
        }
    }
}
