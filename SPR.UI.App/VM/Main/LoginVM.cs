﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using SPR.UI.App.Services;
using SPR.UI.App.Services.LocalData;
using VenSoft.WPFLibrary;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Main
{
    /// <summary>
    /// Login window view model
    /// </summary>
    public class LoginVM : NotifyPropertyChangedBase
    {
        private readonly ILogoProvider _logo;
        private readonly IAuthService _authService;
        private readonly ILogginnedHistory _loginHistory;

        private bool _isBusy = false;
        private LoginResult _result = LoginResult.Unknown;
        private string _errorMsg = null;
        private string _user = null;

        public enum LoginResult
        {
            Unknown,
            Success,
            Failed,
            Error
        }

        public LoginVM(ILogoProvider logo, IAuthService authService, ILogginnedHistory loginHistory = null) {
            _logo = logo;
            _authService = authService;
            _loginHistory = loginHistory;

            if (loginHistory != null)
                Username = loginHistory.LogginnedUsers?.FirstOrDefault();

            LoginCmd = new AsyncCommand<IWrappedParameter<string>>(
                async p => await Login(p.Value), 
                (p) => string.IsNullOrEmpty(Username) == false);            
        }

        public bool IsBusy { get => _isBusy; private set { _isBusy = value; NotifyPropertyChanged(); } 
        }

        /// <summary>
        /// Path to the logo
        /// </summary>
        public string Logo => _logo.PathToLogo;

        /// <summary>
        /// User name
        /// </summary>
        public string Username { get => _user; set { _user = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// List of early logginned users
        /// </summary>
        public IEnumerable<string> UsernameHistory => _loginHistory?.LogginnedUsers;

        /// <summary>
        /// Perform login
        /// Command argument must be of <see cref="IWrappedParameter<string>" type/> and contains password
        /// </summary>
        public ICommand LoginCmd { get; }

        /// <summary>
        /// Login result after performing login cmd
        /// </summary>
        public LoginResult Result { get => _result; private set { _result = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// Error message for <see cref="LoginResult.Error"/> state
        /// </summary>
        public string ErrorMessage { get => _errorMsg; set { _errorMsg = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// Method for <see cref="LoginCmd"/> command
        /// </summary>
        /// <param name="psw"></param>
        private async Task Login(string psw) {
            psw = string.IsNullOrEmpty(psw) ? null : psw;

            try {
                IsBusy = true;
                Result = LoginResult.Unknown;
                ErrorMessage = null;

                bool success = await _authService.LoginAsync(Username, psw, CancellationToken.None);

                if (success)
                    _loginHistory?.AddLogin(Username);

                Result = success ? LoginResult.Success : LoginResult.Failed;
            } catch (Exception e) {
                ErrorMessage = e.Message;
                Result = LoginResult.Error;
            } finally {
                IsBusy = false;
            }
        }               
    }
}
