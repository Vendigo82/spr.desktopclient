﻿using SPR.DAL.Enums;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataContract;
using SPR.UI.App.VM.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Serialization
{
    public class CopyVM : BusyVMBase
    {
        private readonly ISerialization serialization;
        private readonly IFormulaItem formula;
        private readonly IScenarioItem scenario;       

        public CopyVM(object contextItem, ISerialization serialization, ILogger logger) 
            : base(changeable: false, refreshable: false, logger: logger)
        {
            this.serialization = serialization ?? throw new ArgumentNullException(nameof(serialization));
            if (contextItem is IFormulaItem f)
                formula = f;
            else if (contextItem is IScenarioItem s)
                scenario = s;
            else
                throw new ArgumentException($"Expected argument of type {nameof(IFormulaItem)} or {nameof(IScenarioItem)}, but received {contextItem.GetType().Name}", nameof(contextItem));

            ShallowCopyCmd = new AsyncCommand(
                () => PerformBusyOperation(ShallowCopyRoutineAsync), 
                () => IsBusy == false && ImportComplete == false, 
                ErrorHandler);

            DeepCopyCmd = new AsyncCommand(
                () => PerformBusyOperation(DeepCopyRoutineAsync),
                () => IsBusy == false && ImportComplete == false,
                ErrorHandler);
        }

        public ICommand ShallowCopyCmd { get; }

        public ICommand DeepCopyCmd { get; }

        public bool ImportComplete { get; private set; } = false;

        #region Properties
        public ObjectType ObjectType => formula != null ? ObjectType.Formula : ObjectType.Scenario;

        public Guid Guid => formula?.Guid ?? scenario.Guid;

        public long Code => formula?.Id ?? scenario.Id;

        public string Name => formula?.Name ?? scenario?.Name ?? string.Empty;
        #endregion

        protected override Task Refresh() => Task.CompletedTask;

        private async Task ShallowCopyRoutineAsync()
        {
            _logger?.LogDebug($"Starting shallow copy of {ObjectType} ({Guid}) - '{Name}'");
            var result = await this.serialization.ShallowCopyAsync(ObjectType, Guid, ct);
            _logger?.LogInfo($"Shallow copy complete");
            ImportComplete = true;
            NotifyPropertyChanged(nameof(ImportComplete));
        }

        private async Task DeepCopyRoutineAsync()
        {
            _logger?.LogDebug($"Starting deep copy of {ObjectType} ({Guid}) - '{Name}'");
            var result = await this.serialization.DeepCopyAsync(ObjectType, Guid, ct);
            _logger?.LogInfo($"Deep copy complete");
            ImportComplete = true;
            NotifyPropertyChanged(nameof(ImportComplete));
        }
    }
}
