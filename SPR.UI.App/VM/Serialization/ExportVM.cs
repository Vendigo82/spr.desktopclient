﻿using SPR.DAL.Enums;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.DataContract;
using SPR.UI.App.VM.Base;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SPR.UI.App.VM.Serialization
{
    public class ExportVM : BusyVMBase, IDataInitializer
    {
        private readonly ISerialization serialization;
        private readonly IFormulaItem formula;
        private readonly IScenarioItem scenario;

        public ExportVM(object contextItem, ISerialization serialization, ILogger logger) : base(changeable: false, refreshable: false, logger: logger)
        {
            this.serialization = serialization ?? throw new ArgumentNullException(nameof(serialization));
            if (contextItem is IFormulaItem f)
                formula = f;
            else if (contextItem is IScenarioItem s)
                scenario = s;
            else
                throw new ArgumentException($"Expected argument of type {nameof(IFormulaItem)} or {nameof(IScenarioItem)}, but received {contextItem.GetType().Name}", nameof(contextItem));            
        }

        public override Task Initialize(bool fresh)
        {
            return PerformBusyOperation(Serialize);
        }

        private async Task Serialize()
        {
            string data;
            if (formula != null)
                data = await serialization.SerializeFormulaAsync(formula.Guid);
            else
                data = await serialization.SerializeScenarioAsync(scenario.Guid);

            DataContent = data;
            NotifyPropertyChanged(nameof(DataContent));
        }

        public ICommand CancelCmd { get; }

        public SerializedObjectType ObjectType => formula != null ? SerializedObjectType.Formula : SerializedObjectType.Scenario;

        public Guid Guid => formula?.Guid ?? scenario.Guid;

        public long Code => formula?.Id ?? scenario.Id;

        public string Name => formula?.Name ?? scenario?.Name ?? string.Empty;

        public string FileName => $"{ObjectType}({Code}) {Name}";

        public string DataContent { get; private set; }

        protected override Task Refresh()
        {
            //throw new NotImplementedException();
            return Task.CompletedTask;
        }
    }
}
