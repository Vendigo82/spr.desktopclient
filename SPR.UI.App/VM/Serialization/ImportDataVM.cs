﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.VM.Base;
using SPR.UI.WebClient;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.WebService.DataContract.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Serialization
{
    public class ImportDataVM : BusyVMBase
    {
        private readonly string data;
        private readonly ISerialization client;
        private readonly ImportSettingsModel settings = new ImportSettingsModel();

        public ImportDataVM(
            string data,
            ISerialization client,
            ILogger logger)
            : base(logger: logger, changeable: false)
        {
            this.data = data ?? throw new ArgumentNullException(nameof(data));
            this.client = client ?? throw new ArgumentNullException(nameof(client));

            ImportCmd = new AsyncCommand(
                execute: () => PerformBusyOperation(ImportRoutine),
                canExecute: () => !IsBusy && !ImportComplete,
                ErrorHandler);
        }

        public ICommand ImportCmd { get; }

        public bool ImportComplete { get; private set; }

        #region Imported data info
        public IEnumerable<ItemExistanceModel<string>> Steps { get; set; }

        public IEnumerable<ItemExistanceModel<int>> RejectReasons { get; set; }

        public IEnumerable<ItemExistanceModel<string>> Servers { get; set; }

        public IEnumerable<ItemExistanceModel<string>> SqlServers { get; set; }

        public IEnumerable<ItemExistanceModel<string>> GoogleBqConnections { get; set; }

        public IEnumerable<ObjectExistanceModel<StdResultBriefModel>> StdResults { get; set; }

        public IEnumerable<ObjectExistanceModel<FormulaBriefModel>> DependencesFormulas { get; set; }

        public IEnumerable<ObjectExistanceModel<FormulaBriefModel>> Formulas { get; set; }

        public IEnumerable<ObjectExistanceModel<ScenarioSlimModel>> Scenarios { get; set; }
        #endregion

        #region settings
        /// <summary>
        /// Create dublicates for main objects
        /// </summary>
        public bool CreateDublicate { 
            get => settings.CreateDublicateForMainObjects; 
            set { settings.CreateDublicateForMainObjects = value; NotifyPropertyChanged(); } 
        }

        /// <summary>
        /// Exists imported main object so dublicate can be created
        /// </summary>
        public bool CanCreateDublicate { get; set; }

        /// <summary>
        /// Update dependences formulas method
        /// </summary>
        public ImportUpdateKind UpdateDependencyKind { 
            get => settings.UpdateNestedFormulasKind;
            set { settings.UpdateNestedFormulasKind = value; NotifyPropertyChanged(); }
        }

        public bool RewriteStdResults { 
            get => settings.RewriteStdResults;
            set { settings.RewriteStdResults = value; NotifyPropertyChanged(); } 
        }

        public bool RewriteSteps {
            get => settings.RewriteSteps;
            set { settings.RewriteSteps = value; NotifyPropertyChanged(); } 
        }

        public bool RewriteRejectReasons { 
            get => settings.RewriteRejectReasons;
            set { settings.RewriteRejectReasons = value; NotifyPropertyChanged(); } 
        }

        public bool RewriteServers {
            get => settings.RewriteServers;
            set { settings.RewriteServers = value; NotifyPropertyChanged(); }
        }

        public bool RewriteSqlServers {
            get => settings.RewriteSqlServers;
            set { settings.RewriteSqlServers = value; NotifyPropertyChanged(); } 
        }

        public bool RewriteGoogleBQConnections {
            get => settings.RewriteGoogleBqConnections;
            set { settings.RewriteGoogleBqConnections = value; NotifyPropertyChanged(); }
        }
        #endregion

        private async Task ImportRoutine()
        {
            if (_logger?.IsEnabled(LogLevel.Debug) == true)
                _logger.LogDebug($"Importing data {data}");
            else
                _logger?.LogInfo($"Importing data");

            var _ = await client.ImportAsync(data, settings, null, ct);

            _logger?.LogInfo($"Importing data success");

            ImportComplete = true;
            NotifyPropertyChanged(nameof(ImportComplete));
        }

        protected override async Task Refresh()
        {
            var existance = await client.CheckExistanceAsync(data, ct);

            Steps = existance.Steps;
            RejectReasons = existance.RejectReasons;
            Servers = existance.Servers;
            SqlServers = existance.SqlServers;
            GoogleBqConnections = existance.GoogleBQConnections;
            StdResults = existance.StdResults;
            Formulas = existance.Formulas.Where(i => existance.FormulasIds.Contains(i.Item.Guid)).ToArray();
            Scenarios = existance.Scenarios;
            DependencesFormulas = existance.Formulas.Where(i => ! existance.FormulasIds.Contains(i.Item.Guid)).ToArray();

            CanCreateDublicate = Formulas.Any(i => i.Exists) || Scenarios.Any(i => i.Exists);
        }
    }
}
