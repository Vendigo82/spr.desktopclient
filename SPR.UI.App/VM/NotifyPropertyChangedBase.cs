﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM
{
    public class NotifyPropertyChangedBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "") 
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>
        /// Raise PropertyChanged event for all properties
        /// </summary>
        public void NotifyAllPropertiesChanged() {
            foreach (var prop in GetType().GetProperties())
                NotifyPropertyChanged(prop.Name);
        }
    }
}
