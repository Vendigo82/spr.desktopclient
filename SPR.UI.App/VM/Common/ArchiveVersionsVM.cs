﻿using SPR.UI.WebService.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.UI.App.Abstractions;
using SPR.UI.App.VM.Base;
using SPR.UI.Core.Shared;

namespace SPR.UI.App.VM.Common
{
    public class ArchiveVersionsVM : BusyVMBase
    {
        private readonly IDataVersionsSource _source;
        private readonly Guid _guid;
        private Wrapper _selectedItem;

        public ArchiveVersionsVM(Guid guid, IDataVersionsSource source, ILogger logger) 
            : base(changeable: false, logger: logger) {
            _source = source;
            _guid = guid;
        }

        public ObservableCollection<Wrapper> ArchivedVersions { get; private set; }

        /// <summary>
        /// Selected item from <see cref="Items"/>
        /// </summary>
        public Wrapper SelectedItem { 
            get => _selectedItem; 
            set { _selectedItem = value; NotifyPropertyChanged(); } 
        }

        /// <summary>
        /// Selected item's key value
        /// </summary>
        public int? SelectedValue {
            get => _selectedItem != null ? _selectedItem.Ver : (int?)null;
            set {
                SelectedItem = ArchivedVersions.Where(i => i.Ver == value).FirstOrDefault();
                NotifyPropertyChanged();
            }
        }

        public class Wrapper
        {
            private readonly VersionInfo _item;

            public Wrapper(VersionInfo item) {
                _item = item;
            }

            public int Ver => _item.Version;

            public DateTime InsertedDate => _item.DateTime;
        }

        protected override async Task Refresh() {
            var response = await _source.GetArchiveVersionsListAsync(_guid, ct);
            ArchivedVersions = new ObservableCollection<Wrapper>(response.Items.Select(i => new Wrapper(i)));
        }
    }
}
