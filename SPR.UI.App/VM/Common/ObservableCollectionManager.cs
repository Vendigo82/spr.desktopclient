﻿using SPR.UI.App.Abstractions.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace SPR.UI.App.VM.Common
{
    public static class ObservableCollectionManager
    {
        /// <summary>
        /// Create observable collection manager in association with source collection
        /// </summary>
        /// <typeparam name="TItem">Type of observable collection item</typeparam>
        /// <typeparam name="TSourceItem">Type of source item</typeparam>
        /// <param name="collectionAccessor">source collection accessor</param>
        /// <param name="wrapFunc">wrapping function</param>
        /// <param name="somethingChanged">called when something changed (collection or collection items)</param>
        /// <returns></returns>
        public static ObservableCollectionManager<TItem> Create<TItem, TSourceItem>(
            Func<ICollection<TSourceItem>> collectionAccessor,
            Func<TSourceItem, TItem> wrapFunc,
            Action somethingChanged)
            where TItem : INotifyPropertyChanged, IItemContainer<TSourceItem>
        {
            return new ObservableCollectionManager<TItem>(
                () => collectionAccessor()?.Select(s => wrapFunc(s)),
                (i) => collectionAccessor().Add(i.Item),
                (i) => collectionAccessor().Remove(i.Item),
                somethingChanged,
                (s, e) => somethingChanged());
        }            
    }

    /// <summary>
    /// Manage observable collection and collection items. Provide some events when collection or items changed
    /// </summary>
    /// <typeparam name="TItem">Type of collection item</typeparam>
    public class ObservableCollectionManager<TItem> where TItem : INotifyPropertyChanged
    {
        private readonly Action<TItem> _itemAdded;
        private readonly Action<TItem> _itemRemoved;
        private readonly Action _collectionChanged;
        private readonly PropertyChangedEventHandler _itemChangedItemHandler;
        private readonly Func<IEnumerable<TItem>> _itemsSource;

        /// <summary>
        /// managed collection
        /// </summary>
        private ObservableCollection<TItem> _collection;

        /// <summary>
        /// Construct new <see cref="ObservableCollection{T}"/> manager
        /// </summary>
        /// <param name="itemsSource">items list factory</param>
        /// <param name="itemAdded">Called when new item added to collection</param>
        /// <param name="itemRemoved">Called wehn item removed from collection</param>
        /// <param name="collectionChanged">Called when collection changed (item added or removed)</param>
        /// <param name="itemChangedItemHandler">Called when any of items was raised <see cref="INotifyPropertyChanged.PropertyChanged"/> event</param>
        public ObservableCollectionManager(
            Func<IEnumerable<TItem>> itemsSource,
            Action<TItem> itemAdded, 
            Action<TItem> itemRemoved, 
            Action collectionChanged, 
            PropertyChangedEventHandler itemChangedItemHandler)
        {
            _itemAdded = itemAdded ?? throw new ArgumentNullException(nameof(itemAdded));
            _itemRemoved = itemRemoved ?? throw new ArgumentNullException(nameof(itemRemoved));
            _collectionChanged = collectionChanged ?? throw new ArgumentNullException(nameof(collectionChanged));
            _itemChangedItemHandler = itemChangedItemHandler ?? throw new ArgumentNullException(nameof(itemChangedItemHandler));
            _itemsSource = itemsSource ?? throw new ArgumentNullException(nameof(itemsSource));
        }        

        /// <summary>
        /// Create new list of nested formulas
        /// </summary>
        public ObservableCollection<TItem> Create()
        {
            if (_collection != null) {
                _collection.CollectionChanged -= CollectionChanged;
                _collection.Cast<INotifyPropertyChanged>().UnSubscribe(_itemChangedItemHandler);
            }

            var items = _itemsSource();
            if (items == null)
                return null;

            _collection = new ObservableCollection<TItem>(items);
            
            _collection.CollectionChanged += CollectionChanged;
            _collection.Cast<INotifyPropertyChanged>().Subscribe(_itemChangedItemHandler);

            return _collection;
        }

        /// <summary>
        /// Called when managed collection  was added or removed items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<TItem>()) {
                    _itemAdded?.Invoke(i);
                    i.PropertyChanged += _itemChangedItemHandler;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<TItem>()) {
                    _itemRemoved?.Invoke(i);
                    i.PropertyChanged -= _itemChangedItemHandler;
                }
            }

            if (e.NewItems != null || e.OldItems != null)
                _collectionChanged?.Invoke();
        }
    }
}
