﻿using SPR.UI.App.Commands;
using System;
using System.ComponentModel;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Common
{
    public class CopyExportCommandsHelper
    {
        public CopyExportCommandsHelper(Func<object> SelectedItemAccessor, INotifyPropertyChanged vm, string selectedItemPropertyName = "SelectedItem")
        {
            ExportCmd = new RelayCommand(
                execute: () => GlobalCommands.ExportCmd.Execute(SelectedItemAccessor()),
                canExecute: () => SelectedItemAccessor() != null);

            CopyCmd = new RelayCommand(
                execute: () => GlobalCommands.CopyCmd.Execute(SelectedItemAccessor()),
                canExecute: () => SelectedItemAccessor() != null);

            vm.PropertyChanged += (s, e) => {
                if (e.PropertyName == selectedItemPropertyName)
                    UpdateCommandsCanExecute();
            };
        }

        public ICommand ExportCmd { get; private set; }

        public ICommand CopyCmd { get; private set; }

        private void UpdateCommandsCanExecute()
        {
            ((RelayCommand)ExportCmd).RaiseCanExecuteChanged();
            ((RelayCommand)CopyCmd).RaiseCanExecuteChanged();
        }
    }
}
