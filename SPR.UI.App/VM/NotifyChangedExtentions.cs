﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM
{
    public static class NotifyChangedExtentions
    {
        /// <summary>
        /// Subscribes on all elements PropertyChanged event
        /// </summary>
        /// <param name="list"></param>
        /// <param name="hundler"></param>
        public static void Subscribe(this IEnumerable<INotifyPropertyChanged> list, PropertyChangedEventHandler hundler) {
            foreach (var item in list)
                item.PropertyChanged += hundler;
        }

        /// <summary>
        /// Unsubscribe from all elements PropertyChanged event
        /// </summary>
        /// <param name="list"></param>
        /// <param name="hundler"></param>
        public static void UnSubscribe(this IEnumerable<INotifyPropertyChanged> list, PropertyChangedEventHandler hundler) {
            foreach (var item in list)
                item.PropertyChanged -= hundler;
        }
    }
}
