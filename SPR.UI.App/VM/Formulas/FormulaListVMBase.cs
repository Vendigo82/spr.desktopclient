﻿using SPR.DAL.Enums;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.VM.Base;
using SPR.UI.WebClient.DataTypes;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Formulas
{
    public abstract class FormulaListVMBase<TSrc, TVm> : ObjectsListVM<TSrc, TVm> where TVm : class
    {
        protected readonly IDataListFilterSource<TSrc, FormulaFilter> source;
        private readonly FormulaFilter filter = new FormulaFilter();

        protected FormulaListVMBase(IDataListFilterSource<TSrc, FormulaFilter> source, Func<TSrc, TVm> wrap, ILogger logger) 
            : base(source, wrap, logger)
        {
            this.source = source;

            ResetFilterCmd = new RelayCommand(() => ResetFilter());
        }

        public string FilterName { 
            get => filter.NamePart; 
            set { 
                filter.NamePart = value; 
                NotifyPropertyChanged();
                ResetDelayUpdateTimer();
            } 
        }

        public long? FilterCode {
            get => filter.Code;
            set {
                filter.Code = value;
                NotifyPropertyChanged();
                ResetDelayUpdateTimer();
            }
        }

        public bool FilterCheckup {
            get => filter.CanBeCheckup ?? false;
            set {
                filter.CanBeCheckup = value;
                NotifyPropertyChanged();
                ResetDelayUpdateTimer();
            }
        }

        public FormulaType? FilterType {
            get => filter.FormulaType;
            set {
                filter.FormulaType = value;
                NotifyPropertyChanged();
                ResetDelayUpdateTimer();
            }
        }

        public bool FilterNotUsed {
            get => filter.NotUsed ?? false;
            set {
                filter.NotUsed = value;
                NotifyPropertyChanged();
                ResetDelayUpdateTimer();
            }
        }

        public ICommand ResetFilterCmd { get; }

        protected override Task<IEnumerable<TSrc>> LoadItemsRoutineAsync(bool fresh, CancellationToken ct)
        {
            var requestFilter = new FormulaFilter {
                NamePart = filter.NamePart,
                Code = filter.Code,
                CanBeCheckup = (filter.CanBeCheckup ?? false) ? true : (bool?)null,
                FormulaType = filter.FormulaType,
                NotUsed = (filter.NotUsed ?? false) ? true : (bool?)null
            };

            return source.LoadItemsAsync(requestFilter, ct);
        }  
               
        private void ResetFilter()
        {
            filter.CanBeCheckup = null;
            filter.Code = null;
            filter.FormulaType = null;
            filter.NamePart = null;
            NotifyPropertyChanged(nameof(FilterCheckup));
            NotifyPropertyChanged(nameof(FilterCode));
            NotifyPropertyChanged(nameof(FilterName));
            NotifyPropertyChanged(nameof(FilterType));
            var _ = DelayFilterTimerAction();
        }
    }
}
