﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Formula;
using System.Linq;

namespace SPR.UI.App.VM.Formulas
{

    /// <summary>
    /// List of working versions of formulas
    /// </summary>
    public class FormulaListVM : FormulaListVMBase<FormulaBriefModel, FormulaBriefModel>
    {
        public FormulaListVM(IDataListFilterSource<FormulaBriefModel, FormulaFilter> source, ILogger logger = null) 
            : base(source, (i) => i, logger) {
        }

        public long? SelectedValue { 
            get => SelectedItem?.Code;
            set {
                SelectedItem = Items.Where(i => i.Code == value).FirstOrDefault();
                NotifyPropertyChanged();
            } 
        }
    }
}
