﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace SPR.UI.App.VM.Formulas
{
    public class CheckupBunchFormulaVM : FormulaVM
    {
        public CheckupBunchFormulaVM(
            Guid? guid, 
            IFormulaRepository source, 
            IDictionaryService<int, RejectReasonModel> rejectReasonSource = null, 
            IDictionaryService<string, StepModel> stepSource = null, 
            ILogger logger = null, 
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider)
        {
        }

        public bool PerformAll {
            get => Item?.Bunch.PerformAll ?? false;
            set { Item.Bunch.PerformAll = value; SetIsChangedAndNotify(); }
        }

        /// <summary>
        /// Wrapper class for bunch rows
        /// </summary>
        public class RowWrapper : NotifyPropertyChangedBase
        {
            public RowWrapper(BunchRowModel item)
            {                
                Item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public RowWrapper() : this(new BunchRowModel() { Body = string.Empty })
            {

            }

            public BunchRowModel Item { get; }

            public FormulaVM Owner { get; set; }

            public bool IsReadOnly { get => Owner?.IsReadOnly ?? false; }

            public int Order { get => Item.Order; set { Item.Order = value; NotifyPropertyChanged(); } }

            public string Body { get => Item.Body; set { Item.Body = value ?? string.Empty; NotifyPropertyChanged(); } }

            public int? RejectCode { 
                get => Item.RejectCode;
                set {
                    Item.RejectCode = value;
                    if (Item.RejectCode == null) {
                        BlackList = null;
                        RejectStep = null;
                    }
                    NotifyPropertyChanged();
                } 
            } 

            public int? BlackList { get => Item.BlackList; set { Item.BlackList = value; NotifyPropertyChanged(); } }

            public string RejectStep { get => Item.RejectStep; set { Item.RejectStep = value; NotifyPropertyChanged(); } }

            public bool AbortScenario { get => Item.AbortScenario; set { Item.AbortScenario = value; NotifyPropertyChanged(); } }
        }

        /// <summary>
        /// List of rows
        /// </summary>
        private ObservableCollection<RowWrapper> _rows;

        /// <summary>
        /// List of rows
        /// </summary>
        public ObservableCollection<RowWrapper> Rows {
            get {
                if (_rows != null) {
                    _rows.UnSubscribe(RowsItem_Changed);
                    _rows.CollectionChanged -= Rows_CollectionChanged;
                }

                if (Item == null)
                    return null;

                _rows = new ObservableCollection<RowWrapper>(Item.Bunch.Rows.Select(i => new RowWrapper(i) { Owner = this }));
                _rows.Subscribe(RowsItem_Changed);
                _rows.CollectionChanged += Rows_CollectionChanged;
                return _rows;
            }
        }

        /// <summary>
        /// Called when items was added to or removed from <see cref="Rows"/> collection.
        /// Set <see cref="BusyVMBase.IsChanged"/> flag and subscribe/unsubscribe to items PropertyChanged event
        /// </summary>
        private void Rows_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<RowWrapper>()) {
                    IsChanged = true;
                    Item.Bunch.Rows.Add(i.Item);
                    i.PropertyChanged += RowsItem_Changed;
                    i.Owner = this;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<RowWrapper>()) {
                    IsChanged = true;
                    Item.Bunch.Rows.Remove(i.Item);
                    i.PropertyChanged -= RowsItem_Changed;
                }
            }
        }

        /// <summary>
        /// Called when item of <see cref="Rows"/> changed. Set <see cref="BusyVMBase.IsChanged"/> flag
        /// </summary>
        private void RowsItem_Changed(object sender, PropertyChangedEventArgs e)
        {
            IsChanged = true;
        }

        protected override FormulaModel CreateNewItem()
        {
            var item = base.CreateNewItem();
            item.Type = DAL.Enums.FormulaType.BunchFormula;
            item.Bunch = new BunchModel() {
                Rows = new List<BunchRowModel>()
            };
            return item;
        }
    }
}
