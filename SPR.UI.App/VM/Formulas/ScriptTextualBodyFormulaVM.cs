﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;

namespace SPR.UI.App.VM.Formulas
{
    public class ScriptTextualBodyFormulaVM : TextualBodyFormulaVM
    {
        public ScriptTextualBodyFormulaVM(
            Guid? guid,
            IFormulaRepository source,
            IDictionaryService<int, RejectReasonModel> rejectReasonSource = null,
            IDictionaryService<string, StepModel> stepSource = null, 
            ILogger logger = null, 
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider) {
        }

        protected override FormulaModel CreateNewItem() {
            var data = base.CreateNewItem();
            data.Type = DAL.Enums.FormulaType.Script;
            return data;
        }
    }
}
