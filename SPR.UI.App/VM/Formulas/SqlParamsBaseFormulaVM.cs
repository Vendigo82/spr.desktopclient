﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace SPR.UI.App.VM.Formulas
{
    /// <summary>
    /// View model with list of sql params
    /// </summary>
    public abstract class SqlParamsBaseFormulaVM : FormulaVM
    {
        public SqlParamsBaseFormulaVM(
            Guid? guid,
            IFormulaRepository source, 
            IDictionaryService<int, RejectReasonModel> rejectReasonSource = null, 
            IDictionaryService<string, StepModel> stepSource = null, 
            ILogger logger = null,
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider)
        {
        }

        /// <summary>
        /// List of model's sql parameters
        /// </summary>
        protected abstract List<SqlQueryParamModel> SqlParamsModel { get; }

        #region Parameters
        /// <summary>
        /// Wrap <see cref="SqlQueryParamData"/> with INotifyPropertyChanged interface
        /// </summary>
        public class SqlParamWrapper : NotifyPropertyChangedBase
        {
            public SqlParamWrapper(SqlQueryParamModel p)
            {
                Item = p;
            }

            public SqlParamWrapper() : this(new SqlQueryParamModel() { Name = string.Empty, Value = string.Empty })
            {
            }

            public SqlQueryParamModel Item { get; }

            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public string Value { get => Item.Value; set { Item.Value = value; NotifyPropertyChanged(); } }
        }

        private ObservableCollection<SqlParamWrapper> _sqlParams;

        /// <summary>
        /// List of query parameters
        /// </summary>
        public ObservableCollection<SqlParamWrapper> SqlParams {
            get {
                if (_sqlParams != null) {
                    _sqlParams.UnSubscribe(SqlParamsItem_Changed);
                    _sqlParams.CollectionChanged -= SqlParams_CollectionChanged;
                }

                if (Item == null)
                    return null;

                _sqlParams = new ObservableCollection<SqlParamWrapper>(SqlParamsModel.Select(i => new SqlParamWrapper(i)));
                _sqlParams.Subscribe(SqlParamsItem_Changed);
                _sqlParams.CollectionChanged += SqlParams_CollectionChanged;
                return _sqlParams;
            }
        }

        /// <summary>
        /// Called when items was added to or removed from <see cref="SqlParams"/> collection.
        /// Set <see cref="BusyVMBase.IsChanged"/> flag and subscribe/unsubscribe to items PropertyChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SqlParams_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<SqlParamWrapper>()) {
                    IsChanged = true;
                    SqlParamsModel.Add(i.Item);
                    i.PropertyChanged += SqlParamsItem_Changed;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<SqlParamWrapper>()) {
                    IsChanged = true;
                    SqlParamsModel.Remove(i.Item);
                    i.PropertyChanged -= SqlParamsItem_Changed;
                }
            }
        }

        /// <summary>
        /// Called when item of <see cref="SqlParams"/> changed. Set <see cref="BusyVMBase.IsChanged"/> flag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SqlParamsItem_Changed(object sender, PropertyChangedEventArgs e)
        {
            IsChanged = true;
        }
        #endregion
    }
}
