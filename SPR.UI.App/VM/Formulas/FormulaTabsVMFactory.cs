﻿using System;
using SPR.DAL.Enums;
using SPR.UI.WebService.DataContract.Formula;

namespace SPR.UI.App.VM.Formulas
{
    using Abstractions.VM;
    using Abstractions.Tests;
    using Services;
    using System.Net.Http;

    public class FormulaTabsVMFactory : IDataObjectVMFactory<FormulaModel>
    {
        private readonly ITypeResolver _typeResolver;
        private readonly FormulaType formulaType;

        public FormulaTabsVMFactory(FormulaType formulaType, ITypeResolver typeResolver) 
        {
            _typeResolver = typeResolver ?? throw new ArgumentNullException(nameof(typeResolver));
            this.formulaType = formulaType;
        }

        public ITestVM CreateTestVM(IDataObjectVM<FormulaModel> vm)
            => _typeResolver.Resolve<FormulaTestVM, ITestItemAccessor<FormulaModel>, FormulaType, Func<HttpClient>>(
                null, vm.ToTestItemAccessor(), formulaType, () => new HttpClient());
               //new KeyValuePair<Type, object>(typeof(ITestItemAccessor<FormulaModel>), vm.ToTestItemAccessor()));

        public ICommonVM CreateJournalVM(Guid guid)
            => _typeResolver.Resolve<ICommonVM, SerializedObjectType, Guid>(
                Modules.TAB_OBJECT_JOURNAL, SerializedObjectType.Formula, guid);
        //new[] {
        //new KeyValuePair<Type, object>(typeof(SerializedObjectType), SerializedObjectType.Formula),
        //new KeyValuePair<Type, object>(typeof(Guid), guid)
        //});

        public ICommonVM CreateUsageVM(Guid guid)
            => _typeResolver.Resolve<ICommonVM, Guid>(Modules.TAB_FORMULA_USAGE, guid);//new KeyValuePair<Type, object>(typeof(Guid), guid));
    }
}
