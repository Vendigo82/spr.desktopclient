﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace SPR.UI.App.VM.Formulas
{
    public class ScoreCardFormulaVM : FormulaVM
    {
        public ScoreCardFormulaVM(
            Guid? guid, 
            IFormulaRepository source, 
            IDictionaryService<int, RejectReasonModel> rejectReasonSource = null, 
            IDictionaryService<string, StepModel> stepSource = null, 
            ILogger logger = null, 
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider)
        {
        }

        #region Score card header properties
        public double Initial { 
            get => Item?.ScoreCard.Initial ?? 0; 
            set { Item.ScoreCard.Initial = value; SetIsChangedAndNotify(); }
        }

        public string FinalExpression { 
            get => Item?.ScoreCard.FinalExpression; 
            set { Item.ScoreCard.FinalExpression = value; SetIsChangedAndNotify(); } 
        }
        #endregion

        #region Variables
        /// <summary>
        /// Variables data model wrapper. Adapt web-service model to user interface
        /// </summary>
        public class VariableWrapper : NotifyPropertyChangedBase
        {
            public VariableWrapper(ScoreCardFormulaVM vm, ScoreCardVarModel item)
            {
                VM = vm ?? throw new ArgumentNullException(nameof(vm));
                Item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public VariableWrapper()
            {
                //VM = vm ?? throw new ArgumentNullException(nameof(vm));
                Item = new ScoreCardVarModel() { 
                    Conditions = new List<ScoreCardConditionModel>(),
                    Enable = true,
                    Otherwise = 0
                };
            }

            public ScoreCardVarModel Item { get; }

            public ScoreCardFormulaVM VM { get; set; }

            public bool Enable { get => Item.Enable; set { Item.Enable = value; NotifyPropertyChanged(); } }

            public string Expression { get => Item.Expression; set { Item.Expression = value; NotifyPropertyChanged(); } }

            public string Descr { get => Item.Descr; set { Item.Descr = value; NotifyPropertyChanged(); } }

            public double Otherwise { get => Item.Otherwise; set { Item.Otherwise = value; NotifyPropertyChanged(); } }

            private ObservableCollection<ConditionWrapper> _conditions = null;

            /// <summary>
            /// List of conditions
            /// </summary>
            public ObservableCollection<ConditionWrapper> Conditions {
                get {
                    if (_conditions != null) {
                        _conditions.UnSubscribe(VM.ListItem_PropertyChanged);
                        _conditions.CollectionChanged -= Conditions_CollectionChanged;
                    }

                    if (Item == null)
                        return null;

                    if (_conditions == null) {
                        _conditions = new ObservableCollection<ConditionWrapper>(
                            Item.Conditions.Select(i => new ConditionWrapper(Item, i)));
                        _conditions.Subscribe(VM.ListItem_PropertyChanged);
                        _conditions.CollectionChanged += Conditions_CollectionChanged;
                    }

                    return _conditions;
                }
            }

            /// <summary>
            /// Called when list of conditions changed
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void Conditions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                if (e.NewItems != null) {
                    foreach (var i in e.NewItems.Cast<ConditionWrapper>()) {
                        VM.IsChanged = true;
                        i.Variable = Item;
                        Item.Conditions.Add(i.Item);
                        i.PropertyChanged += VM.ListItem_PropertyChanged;
                    }
                }

                if (e.OldItems != null) {
                    foreach (var i in e.OldItems.Cast<ConditionWrapper>()) {
                        VM.IsChanged = true;
                        i.Variable.Conditions.Remove(i.Item);
                        i.PropertyChanged -= VM.ListItem_PropertyChanged;
                    }
                }
            }
        }

        /// <summary>
        /// List of score card variables
        /// </summary>
        private ObservableCollection<VariableWrapper> _variables = null;

        /// <summary>
        /// List of score card variables
        /// </summary>
        public ObservableCollection<VariableWrapper> Variables {
            get { 
                if (_variables != null) {
                    _variables.UnSubscribe(ListItem_PropertyChanged);
                    _variables.CollectionChanged -= Variables_CollectionChanged;
                }

                if (Item == null)
                    return null;

                if (_variables == null) {
                    _variables = new ObservableCollection<VariableWrapper>(
                        Item.ScoreCard.Variables.Select(i => new VariableWrapper(this, i)));
                    _variables.Subscribe(ListItem_PropertyChanged);
                    _variables.CollectionChanged += Variables_CollectionChanged;
                }

                return _variables;
            }
        }

        /// <summary>
        /// Called when list of score card variables was changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Variables_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<VariableWrapper>()) {
                    IsChanged = true;
                    i.VM = this;
                    Item.ScoreCard.Variables.Add(i.Item);
                    i.PropertyChanged += ListItem_PropertyChanged;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<VariableWrapper>()) {
                    IsChanged = true;
                    Item.ScoreCard.Variables.Remove(i.Item);
                    i.PropertyChanged -= ListItem_PropertyChanged;
                }
            }
        }


        #region Conditions
        /// <summary>
        /// Condition data model wrapper. Adapt web-service data model<see cref="ScoreCardConditionModel"/> to ui data model
        /// </summary>
        public class ConditionWrapper : NotifyPropertyChangedBase
        {
            public ConditionWrapper(ScoreCardVarModel variable, ScoreCardConditionModel item)
            {
                Variable = variable ?? throw new ArgumentNullException(nameof(variable));
                Item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public ConditionWrapper()
            {
                Item = new ScoreCardConditionModel {
                    Enable = true,
                    Expression = string.Empty,
                    Order = 0,
                    Value = 0
                };
            }

            public ScoreCardConditionModel Item { get; }

            public ScoreCardVarModel Variable { get; set; }

            public bool Enable { get => Item?.Enable ?? false; set { Item.Enable = value; NotifyPropertyChanged(); } }

            public string Expression { get => Item?.Expression; set { Item.Expression = value; NotifyPropertyChanged(); } }

            public short Order { get => Item?.Order ?? 0; set { Item.Order = value; NotifyPropertyChanged(); } }

            public double Value { get => Item?.Value ?? 0; set { Item.Value = value; NotifyPropertyChanged(); } }
        }

        
        #endregion

        /// <summary>
        /// Called when score card variable or condition changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void ListItem_PropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            IsChanged = true;
        }
        #endregion

        protected override FormulaModel CreateNewItem()
        {
            var data = base.CreateNewItem();
            data.Type = DAL.Enums.FormulaType.ScoreCard;
            data.ScoreCard = new ScoreCardModel() {
                Variables = new List<ScoreCardVarModel>()
            };
            return data;
        }
    }
}
