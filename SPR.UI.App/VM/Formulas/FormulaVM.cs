﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

using SPR.DAL.Enums;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Services;
using SPR.UI.App.VM.Base;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using log4net.Repository.Hierarchy;

namespace SPR.UI.App.VM.Formulas
{

    public abstract class FormulaVM : DataObjectVM<FormulaModel>
    {
        private readonly IFormulaRepository _formulaRepository;
        private readonly IDictionaryService<int, RejectReasonModel> _rejectReasonSource;
        private readonly IDictionaryService<string, StepModel> _stepSource;

        public FormulaVM(
            Guid? guid,
            IFormulaRepository formulaRepository,
            IDictionaryService<int, RejectReasonModel> rejectReasonSource = null,
            IDictionaryService<string, StepModel> stepSource = null,
            ILogger logger = null,
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, SerializedObjectType.Formula, formulaRepository, logger, commentProvider) 
        {
            _formulaRepository = formulaRepository;
            _rejectReasonSource = rejectReasonSource;
            _stepSource = stepSource;

            PropertyChanged += FormulaVM_PropertyChanged;
        }

        #region Description properties
        /// <summary>
        /// Formula code
        /// </summary>
        //public long? Code => Main?.Code; //Item?.Code;

        ///// <summary>
        ///// Formula id
        ///// </summary>
        //public Guid Guid => Item?.Guid ?? Guid.Empty;

        /// <summary>
        /// Formula version
        /// </summary>
        public int Version => Item?.Version ?? 0;

        /// <summary>
        /// Formula minor version
        /// </summary>
        public int MinorVersion => Item?.MinorVersion ?? 0;

        /// <summary>
        /// Type of formula
        /// </summary>
        public FormulaType FormulaType => Item?.Type ?? FormulaType.Expression;

        /// <summary>
        /// Name of the formula
        /// </summary>
        public override string Name { get => Item?.Name; set { Item.Name = value; SetIsChangedAndNotify(); } }

        /// <summary>
        /// Formula description
        /// </summary>
        public string Descr { get => Item?.Descr; set { Item.Descr = value; SetIsChangedAndNotify(); } }
        #endregion

        #region Formula data properties
        /// <summary>
        /// Formula register
        /// </summary>
        public string RegisterName { get => Item?.RegisterName; set { Item.RegisterName = value; SetIsChangedAndNotify(); } }

        private IEnumerable<FormulaBriefModel> _registerUsage = Enumerable.Empty<FormulaBriefModel>();

        public IEnumerable<FormulaBriefModel> RegisterUsage
        {
            get => _registerUsage;
            set { _registerUsage = value; NotifyPropertyChanged(); NotifyPropertyChanged(nameof(IsRegisterNameUsed)); }
        }

        public bool IsRegisterNameUsed => RegisterUsage.Any();

        private async Task UpdateRegisterUsage(string registerName)
        {
            if (registerName != RegisterName)
                RegisterUsage = Enumerable.Empty<FormulaBriefModel>();

            if (string.IsNullOrEmpty(registerName))
                return;

            try
            {
                var usage = await _formulaRepository.GetRegisterNameUsage(registerName);

                if (registerName == RegisterName)
                    RegisterUsage = usage.Where(i => i.Guid != Guid).ToArray();
            }
            catch (Exception e)
            {
                _logger.LogError("Exception on request register name use", e);
            }
        }
        #endregion

        #region Checkup properties
        public bool CanBeCheckup { get => Item?.IsCheckup ?? false; set { Item.IsCheckup = value; SetIsChangedAndNotify(); } }

        public int? RejectCode { get => Item?.RejectCode; set { Item.RejectCode = value; SetIsChangedAndNotify(); } }

        private RejectReasonModel _rejectCodeItem;
        public RejectReasonModel RejectCodeItem {
            get => _rejectCodeItem;
            private set { _rejectCodeItem = value; NotifyPropertyChanged(); } }

        public string RejectStep { get => Item?.RejectStep; set { Item.RejectStep = value; SetIsChangedAndNotify(); } }

        private StepModel _rejectStepItem;
        public StepModel RejectStepItem { get => _rejectStepItem; private set { _rejectStepItem = value; NotifyPropertyChanged(); } }

        public int? BlackList { get => Item?.BlackList; set { Item.BlackList = value; SetIsChangedAndNotify(); } }

        public bool AbortScenario { get => Item?.AbortScenario ?? false; set { Item.AbortScenario = value; SetIsChangedAndNotify(); } }
        #endregion

        #region Parameters
        /// <summary>
        /// Wrapper for ParamData
        /// </summary>
        public class ParamWrapper : NotifyPropertyChangedBase
        {
            public ParamWrapper(ParamModel item) {
                Item = item;
            }

            public ParamWrapper() {
                Item = new ParamModel();
            }

            public ParamModel Item { get; }

            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public string Value { get => Item.Value; set { Item.Value = value; NotifyPropertyChanged(); } }

            public UI.WebService.DataContract.Enums.SPRDataType Type { get => Item.Type; set { Item.Type = value; NotifyPropertyChanged(); } }
        }

        private ObservableCollection<ParamWrapper> _params;

        /// <summary>
        /// List of parameters
        /// </summary>
        public ObservableCollection<ParamWrapper> Params { 
            get {
                if (_params != null) {
                    _params.UnSubscribe(ParamsItem_Changed);
                    _params.CollectionChanged -= Params_CollectionChanged;
                }

                if (Item == null)
                    return null;

                _params = new ObservableCollection<ParamWrapper>(Item.Params.Select(i => new ParamWrapper(i)));
                _params.Subscribe(ParamsItem_Changed);
                _params.CollectionChanged += Params_CollectionChanged;
                return _params;
            }
        }

        /// <summary>
        /// Called when items was added to or removed from <see cref="Params"/> collection.
        /// Set <see cref="BusyVMBase.IsChanged"/> flag and subscribe/unsubscribe to items PropertyChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Params_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<ParamWrapper>()) {
                    IsChanged = true;
                    Item.Params.Add(i.Item);
                    i.PropertyChanged += ParamsItem_Changed;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<ParamWrapper>()) {
                    IsChanged = true;
                    Item.Params.Remove(i.Item);
                    i.PropertyChanged -= ParamsItem_Changed;
                }
            }
        }

        /// <summary>
        /// Called when item of <see cref="Params"/> changed. Set <see cref="BusyVMBase.IsChanged"/> flag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ParamsItem_Changed(object sender, PropertyChangedEventArgs e) {
            IsChanged = true;
        }
        #endregion

        #region Nested formulas
        public class NestedFormulaWrapper : NotifyPropertyChangedBase
        {
            public NestedFormulaWrapper(NestedFormulaModel item) {
                Item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public NestedFormulaWrapper() {
                Item = new NestedFormulaModel() { Enable = true };
            }

            public NestedFormulaModel Item { get; }


            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public bool Enable { get => Item.Enable; set { Item.Enable = value; NotifyPropertyChanged(); } }

            public FormulaBriefModel Formula { get => Item.Formula; set { Item.Formula = value; NotifyPropertyChanged(); } }

            public bool RunIfEnabled { get => Item.RunIfEnabled; set { Item.RunIfEnabled = value; NotifyPropertyChanged(); } }

            public string RunIfExpression { get => Item.RunIfExpression; set { Item.RunIfExpression = value; NotifyPropertyChanged(); } }

            public int Order { get => Item.Order; set { Item.Order = value; NotifyPropertyChanged(); } }
        }

        private ObservableCollection<NestedFormulaWrapper> _nestedFormulas;

        /// <summary>
        /// List of nested formulas
        /// </summary>
        public ObservableCollection<NestedFormulaWrapper> NestedFormulas {
            get {
                if (_nestedFormulas != null) {
                    _nestedFormulas.CollectionChanged -= NestedFormulas_CollectionChanged;
                    _nestedFormulas.UnSubscribe(NestedFormulasItem_Changed);
                }

                if (Item == null)
                    return null;

                _nestedFormulas = new ObservableCollection<NestedFormulaWrapper>(Item.ChildFormulas
                    .OrderBy(i => i.Order)
                    .Select(i => new NestedFormulaWrapper(i)));
                _nestedFormulas.CollectionChanged += NestedFormulas_CollectionChanged;
                _nestedFormulas.Subscribe(NestedFormulasItem_Changed);
                return _nestedFormulas;
            }
        }

        /// <summary>
        /// Called when collection <see cref="NestedFormulas"/> was changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NestedFormulas_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<NestedFormulaWrapper>()) {
                    IsChanged = true;
                    Item.ChildFormulas.Add(i.Item);
                    i.PropertyChanged += NestedFormulasItem_Changed;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<NestedFormulaWrapper>()) {
                    IsChanged = true;
                    Item.ChildFormulas.Remove(i.Item);
                    i.PropertyChanged -= NestedFormulasItem_Changed;
                }
            }
        }

        /// <summary>
        /// Called when any item of <see cref="NestedFormulas"/> was changed. Set <see cref="BusyVMBase.IsChanged"/> flag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NestedFormulasItem_Changed(object sender, PropertyChangedEventArgs e) {
            IsChanged = true;
        }
        #endregion

        #region ISPRItem implementation
        public override long ID => Code ?? 0;

        public override int Ver => Version;        
        #endregion       

        #region Update dependency properties
        /// <summary>
        /// Update dependent properties
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void FormulaVM_PropertyChanged(object sender, PropertyChangedEventArgs e) {      
            switch (e.PropertyName) {
                case nameof(RejectCode):
                    RejectCodeChanged();
                    break;
                case nameof(RejectStep):
                    RejectStepChanged();
                    break;
                case nameof(RegisterName):
                    var _ = UpdateRegisterUsage(RegisterName);
                    break;
            }
        }

        /// <summary>
        /// Update property <see cref="RejectCodeItem"/> when <see cref="RejectCode"/> changed
        /// </summary>
        private void RejectCodeChanged() {
            int? tmp = RejectCode;
            if (tmp.HasValue) {
                RejectCodeItem = new RejectReasonModel() { Code = tmp.Value };
                _rejectReasonSource?.GetItemAsync(tmp.Value, CancellationToken.None).ContinueWith((t) => {
                    if (t.Status == TaskStatus.RanToCompletion) {
                        if (t.Result.Code == RejectCode) {
                            RejectCodeItem = t.Result;
                        }
                    } else {
                        _logger?.LogError("Error on get reject code item", t.Exception);
                    }
                }, TaskContinuationOptions.ExecuteSynchronously);
            } else
                RejectCodeItem = null;
        }

        /// <summary>
        /// Update property <see cref="RejectStepItem"/> when <see cref="RejectStep"/> changed
        /// </summary>
        private void RejectStepChanged() {
            string tmp = RejectStep;
            if (tmp != null) {
                RejectStepItem = new StepModel() { Name = tmp };
                _stepSource?.GetItemAsync(tmp, CancellationToken.None).ContinueWith((t) => {
                    if (t.Status == TaskStatus.RanToCompletion) {
                        if (t.Result.Name == RejectStep)
                            RejectStepItem = t.Result;
                    } else
                        _logger?.LogError("Error on get reject step item", t.Exception);
                }, TaskContinuationOptions.ExecuteSynchronously);
            } else
                RejectStepItem = null;
        }
        #endregion

        protected override FormulaModel CreateNewItem() => new FormulaModel() {
            Guid = Guid,
            Params = new List<ParamModel>(),
            ChildFormulas = new List<NestedFormulaModel>()
        };        
    }
}
