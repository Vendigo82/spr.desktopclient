﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Formulas
{
    using Abstractions.Tests;
    using Abstractions.VM;
    using Services;
    using Services.UI;
    using SPR.UI.App.DataContract;
    using SPR.UI.WebService.DataContract.Formula;

    /// <summary>
    /// Adapts <see cref="FormulaVM" to <see cref="ITestItemAccessor{T}"/>/>
    /// </summary>
    public class FormulaTestAccessorAdapter : ITestItemAccessor<FormulaModel>
    {
        private readonly IDataObjectVM<FormulaModel> _vm;

        public FormulaTestAccessorAdapter(IDataObjectVM<FormulaModel> vm) {
            _vm = vm ?? throw new ArgumentNullException(nameof(vm));
        }

        public long Id => _vm.ID;

        public Guid Guid => _vm.Guid;

        public FormulaModel Item => _vm.Item;//{
        //    get {
        //        if (_vm.ShownObject == ShownObjectEnum.Main)
        //            return null;
        //        else
        //            return _vm.Item;
        //    }
        //}

        public ShownObjectEnum ShownObject => _vm.ShownObject;
    }

    public static class FormulaVMTestAccessorExtentions
    {
        public static ITestItemAccessor<FormulaModel> ToTestItemAccessor(this IDataObjectVM<FormulaModel> vm)
            => new FormulaTestAccessorAdapter(vm);
    }
}
