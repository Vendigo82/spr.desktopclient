﻿using Newtonsoft.Json;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Formulas
{
    public class RestServiceFormulaVM : SqlParamsBaseFormulaVM
    {
        private readonly IDictionaryService<string, ServerModel> restServiceSource;

        /// <summary>
        /// Extenden information about server property
        /// </summary>
        private ServerModel serverModel;

        /// <summary>
        /// Available http verbs
        /// </summary>
        private readonly IEnumerable<string> _httpVerbs = new[] { "GET", "POST", "PUT", "DELETE" };

        public RestServiceFormulaVM(
            Guid? guid,
            IFormulaRepository source, 
            IDictionaryService<int, RejectReasonModel> rejectReasonSource, 
            IDictionaryService<string, StepModel> stepSource, 
            IDictionaryService<string, ServerModel> restServiceSource,
            ILogger logger = null, 
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider)
        {
            this.restServiceSource = restServiceSource ?? throw new ArgumentNullException(nameof(restServiceSource));
        }

        #region Main properties
        public string Server {
            get => Item?.Rest.Server;
            set { Item.Rest.Server = value; SetIsChangedAndNotify(); }
        }

        public ServerModel ServerItem {
            get => serverModel;
            set { serverModel = value; NotifyPropertyChanged(); }
        }

        public string Result {
            get => Item?.Rest.Result;
            set { Item.Rest.Result = string.IsNullOrEmpty(value) ? null : value; SetIsChangedAndNotify(); }
        }

        public bool AllFields {
            get => Item?.Rest.AllFields ?? false;
            set { Item.Rest.AllFields = value; SetIsChangedAndNotify(); }
        }

        public bool AllSimpleFields {
            get => Item?.Rest.AllSimpleFields ?? false;
            set { Item.Rest.AllSimpleFields = value; SetIsChangedAndNotify(); }
        }

        public string Fields {
            get => Item?.Rest.Fields;
            set { Item.Rest.Fields = string.IsNullOrEmpty(value) ? null : value; SetIsChangedAndNotify(); }
        }

        /// <summary>
        /// request should inlude input data form
        /// </summary>
        public bool IncludeInputData {
            get => Item?.Rest.IncludeInputData ?? false;
            set { Item.Rest.IncludeInputData = value; SetIsChangedAndNotify(); }
        }

        /// <summary>
        /// path to include input data form
        /// </summary>
        public string RequestInputDataPath { 
            get => Item?.Rest.RequestInputDataPath;
            set { Item.Rest.RequestInputDataPath = value.NullIfEmpty(); SetIsChangedAndNotify(); }
        }

        /// <summary>
        /// request should include context values
        /// </summary>
        public bool IncludeAllContextValues { 
            get => Item?.Rest.IncludeAllContextValues ?? false;
            set { Item.Rest.IncludeAllContextValues = value; SetIsChangedAndNotify(); } 
        }

        /// <summary>
        /// raise error on not successfull http status code
        /// </summary>
        public bool EnsureStatusCodeIsSuccess { 
            get => Item?.Rest.EnsureStatusCodeIsSuccess ?? false;
            set { Item.Rest.EnsureStatusCodeIsSuccess = value; SetIsChangedAndNotify(); } 
        }

        /// <summary>
        /// raise error on invalid json
        /// </summary>
        public bool ThrowsExceptionOnInvalidJson {
            get => Item?.Rest.ThrowsExceptionOnInvalidJson ?? false;
            set { Item.Rest.ThrowsExceptionOnInvalidJson = value; SetIsChangedAndNotify(); } 
        }

        /// <summary>
        /// save response body to specific property
        /// </summary>
        public string ResponsePropertyName { 
            get => Item?.Rest.ResponsePropertyName;
            set { Item.Rest.ResponsePropertyName = value.NullIfEmpty(); SetIsChangedAndNotify(); } 
        }

        /// <summary>
        /// save response code to specific property
        /// </summary>
        public string StatusCodePropertyName {
            get => Item?.Rest.StatusCodePropertyName;
            set { Item.Rest.StatusCodePropertyName = value.NullIfEmpty(); SetIsChangedAndNotify(); } 
        }

        /// <summary>
        /// raise error if [Result] path does not found in response body
        /// </summary>
        public bool ThrowExceptionIfResultPathNotFound {
            get => Item?.Rest.ThrowExceptionIfResultPathNotFound ?? false;
            set { Item.Rest.ThrowExceptionIfResultPathNotFound = value; SetIsChangedAndNotify(); } 
        }

        /// <summary>
        /// save raw data property if json was invalid
        /// </summary>
        public string ResponseRawDataProperty {
            get => Item?.Rest.ResponseRawDataProperty;
            set { Item.Rest.ResponseRawDataProperty = value.NullIfEmpty(); SetIsChangedAndNotify(); }
        }

        /// <summary>
        /// Local service url
        /// </summary>
        public string LocalUrl {
            get => Item?.Rest.LocalUrl; 
            set { Item.Rest.LocalUrl = value.NullIfEmpty(); SetIsChangedAndNotify(); } 
        }

        public string HttpVerb {
            get => Item?.Rest.HttpVerb;
            set { Item.Rest.HttpVerb = value; SetIsChangedAndNotify(); }
        }

        public bool WithBody {
            get => Item?.Rest.WithBody ?? true;
            set { Item.Rest.WithBody = value; SetIsChangedAndNotify(); }
        }
        
        public bool IgnoreNetworkErrors
        {
            get => Item?.Rest.IgnoreNetworkErrors ?? false;
            set { Item.Rest.IgnoreNetworkErrors = value; SetIsChangedAndNotify(); }
        }

        public bool AddSuccessProperties
        {
            get => Item?.Rest.AddSuccessProperties ?? false;
            set { Item.Rest.AddSuccessProperties = value; SetIsChangedAndNotify(); }
        }
        #endregion

        protected override List<SqlQueryParamModel> SqlParamsModel => Item?.Rest.Properties;

        public IEnumerable<string> HttpVerbs => _httpVerbs;

        protected override void FormulaVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.FormulaVM_PropertyChanged(sender, e);
            if (e.PropertyName == nameof(Server))
                RestServerChanged();
        }

        /// <summary>
        /// Update property <see cref="ServerItem"/> when <see cref="Server"/> changed
        /// </summary>
        private void RestServerChanged()
        {
            string tmp = Server;
            if (tmp != null) {
                ServerItem = new ServerModel() { Name = tmp };
                restServiceSource?.GetItemAsync(tmp, default).ContinueWith((t) => {
                    if (t.Status == TaskStatus.RanToCompletion) {
                        if (t.Result.Name == Server) {
                            ServerItem = t.Result;
                        }
                    } else {
                        _logger?.LogError("Error on get reject code item", t.Exception);
                    }
                }, TaskContinuationOptions.ExecuteSynchronously);
            } else
                ServerItem = null;
        }

        protected override FormulaModel CreateNewItem()
        {
            var data = base.CreateNewItem();
            data.Type = DAL.Enums.FormulaType.RESTService;
            data.Rest = new RestServiceModel {
                IncludeInputData = true,
                IncludeAllContextValues = true,
                AllFields = true,               
                Properties = new List<SqlQueryParamModel>(),
                ThrowsExceptionOnInvalidJson = true,
                ThrowExceptionIfResultPathNotFound = true,
                EnsureStatusCodeIsSuccess = true,
                HttpVerb = "POST",
                WithBody = true,
            };
            return data;
        }
    }
}
