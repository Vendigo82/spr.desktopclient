﻿using System;
using System.Linq;
using System.Windows.Input;

using VenSoft.WPFLibrary.Commands;
using SPR.DAL.Enums;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.DataContract;
using SPR.UI.App.Services;
using SPR.UI.App.Services.Events;
using SPR.UI.App.Services.Events.Args;
using SPR.UI.App.VM.Common;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebClient.DataTypes;

namespace SPR.UI.App.VM.Formulas
{
    public class FormulaMainListVM : FormulaListVMBase<FormulaBriefModel, FormulaMainListVM.FormulaWrapper>
    {
        private readonly INotificationEventsService _events;
        private readonly CopyExportCommandsHelper helper;

        public FormulaMainListVM(
            IDataListFilterSource<FormulaBriefModel, FormulaFilter> source,
            IDeleteOperation<FormulaBriefModel> deleteOperation,
            IWindowFactory wndFactory,
            IResourceStringProvider resource,
            INotificationEventsService events = null,
            ILogger logger = null) 
            : base(source, Wrap, logger: logger) 
        {
            helper = new CopyExportCommandsHelper(() => SelectedItem, this, nameof(SelectedItem));
            _events = events;

            ConfigureDelete(deleteOperation, i => i.Item, null, wndFactory, 
                new DefaultDeleteStringsProvider(resource, i => $"{i.Code} - {i.Name}", "FormulasListTab"));

            AddCmd = new RelayCommand(execute: () => Commands.CommandsLibrary.NewFormulaCmd.Execute(null, null));
            ExportCmd = helper.ExportCmd;
            CopyCmd = helper.CopyCmd;

            //if (_events != null) {
            //    _events.FormulaDeleted += Events_FormulaDeleted;               
            //}

            PropertyChanged += (s, e) => {
                if (e.PropertyName == nameof(SelectedItem))
                    (DeleteCmd as AsyncCommand).RaiseCanExecuteChanged();
            };
        }

        /// <summary>
        /// Add new formula command
        /// </summary>
        public ICommand AddCmd { get; }

        /// <summary>
        /// Export selected object
        /// </summary>
        public ICommand ExportCmd { get; private set; }

        /// <summary>
        /// Make copy of selected object
        /// </summary>
        public ICommand CopyCmd { get; private set; }

        private static FormulaWrapper Wrap(FormulaBriefModel brief) => new FormulaWrapper(brief);

        /// <summary>
        /// Formula wrapper 
        /// </summary>
        public class FormulaWrapper : IFormulaItem
        {
            public FormulaWrapper(FormulaBriefModel item) {
                Item = item;
            }

            public FormulaBriefModel Item { get; }

            /// <summary>
            /// Formula id.
            /// Implements IFormulaItem
            /// </summary>
            public long Id => Item.Code;

            /// <summary>
            /// Formula or draft code.
            /// Implements IFormulaItem
            /// </summary>
            public Guid Guid => Item.Guid;

            /// <summary>
            /// Formula type
            /// Implements IFormulaItem
            /// </summary>
            public FormulaType Type => Item.Type;

            /// <summary>
            /// Formula id for display in view
            /// </summary>
            public long? Code => Item.Code;

            /// <summary>
            /// Formula name
            /// </summary>
            public string Name => Item.Name;
        }

        private void Events_FormulaDeleted(object sender, GuidArgs e) {
            FormulaWrapper item = Items.Cast<FormulaWrapper>().Where(i => i.Guid == e.Guid).FirstOrDefault();
            if (item != null)
                Items.Remove(item);
        }
    }
}
