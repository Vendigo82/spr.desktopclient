﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Formulas
{
    /// <summary>
    /// VM for multiselect formula checkups window
    /// </summary>
    public class FormulaCheckupsListVM : ObjectsListVM<FormulaScenarioCheckupModel, FormulaCheckupsListVM.ItemWrapper>
    {
        public FormulaCheckupsListVM(            
            IDataListSource<FormulaScenarioCheckupModel> source,
            ILogger logger = null) 
            : base(source, i => new ItemWrapper(i), logger)
        {
        }

        public IEnumerable<FormulaScenarioCheckupModel> SelectedItems {
            get => Items.Where(i => i.Checked).Select(i => i.Item);
            set {
                foreach (var i in Items.Where(i => i.Checked))
                    i.Checked = false;
                foreach (var selected in value) {
                    var item = Items.FirstOrDefault(i => i.Item.Guid == selected.Guid);
                    if (item != null)
                        item.Checked = true;
                }
            }
        }

        /// <summary>
        /// Add Checked flag to FormulaScenarioCheckupModel
        /// </summary>
        public class ItemWrapper : NotifyPropertyChangedBase
        {
            private bool @checked = false;

            public ItemWrapper(FormulaScenarioCheckupModel item)
            {
                Item = item ?? throw new ArgumentNullException(nameof(item));
            }

            public FormulaScenarioCheckupModel Item { get; }            

            public bool Checked { get => @checked; set { @checked = value; NotifyPropertyChanged(); } }

            public FormulaBriefModel Formula => Item;

            public int? RejectCode => Item.RejectCode;

            public string RejectStep => Item.RejectStep;

            public bool AbortScenario => Item.AbortScenario;
        }

        protected override async Task Refresh()
        {
            Guid[] selected = null;
            if (Items != null)
                selected = Items.Where(i => i.Checked).Select(i => i.Item.Guid).ToArray();

            SelectedItem = null;

            await base.Refresh();

            //need to set selected item to enable "Commit" button
            SelectedItem = new ItemWrapper(new FormulaScenarioCheckupModel());

            if (selected != null) {
                foreach (var s in selected) {
                    var item = Items.Where(i => i.Item.Guid == s).FirstOrDefault();
                    if (item != null)
                        item.Checked = true;
                }
            }
        }

        public override async Task Initialize(bool fresh)
        {
            await base.Initialize(fresh);
            ////need to set selected item to enable "Commit" button
            SelectedItem = new ItemWrapper(new FormulaScenarioCheckupModel());
        }
    }
}
