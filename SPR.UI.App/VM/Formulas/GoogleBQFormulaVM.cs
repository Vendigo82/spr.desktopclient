﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SPR.UI.App.VM.Formulas
{
    using DAL.Enums;
    using Abstractions;
    using Services;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using SPR.UI.WebService.DataContract.Formula;
    using SPR.UI.WebService.DataContract.Dir;
    using SPR.UI.App.Abstractions.DataSources;
    using SPR.UI.Core.UseCases.FormulaEditor;

    public class GoogleBQFormulaVM : FormulaVM
    {
        public GoogleBQFormulaVM(
            Guid? guid,
            IFormulaRepository source,
            IDictionaryService<int, RejectReasonModel> rejectReasonSource = null,
            IDictionaryService<string, StepModel> stepSource = null, 
            ILogger logger = null, 
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider) {
        }

        #region GoogleBQ properties
        public string ConnectionName { 
            get => Item?.GoogleBQ.ConnectionName; 
            set { Item.GoogleBQ.ConnectionName = value; SetIsChangedAndNotify(); } 
        }

        public string Query {
            get => Item?.GoogleBQ.Query;
            set { Item.GoogleBQ.Query = value; SetIsChangedAndNotify(); }
        }

        public QueryResultType ResultType {
            get => Item?.GoogleBQ.ResultType ?? QueryResultType.Table;
            set { Item.GoogleBQ.ResultType = value; SetIsChangedAndNotify(); }
        }

        public TimeSpan? Timeout {
            get => Item?.GoogleBQ.Timeout;
            set { Item.GoogleBQ.Timeout = value; SetIsChangedAndNotify(); }
        }
        #endregion

        #region Parameters
        /// <summary>
        /// Wrap <see cref="SqlQueryParamData"/> with INotifyPropertyChanged interface
        /// </summary>
        public class SqlParamWrapper : NotifyPropertyChangedBase
        {
            public SqlParamWrapper(SqlQueryParamModel p) {
                Item = p;
            }

            public SqlParamWrapper() : this(new SqlQueryParamModel()) {
            }

            public SqlQueryParamModel Item { get; }

            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public string Value { get => Item.Value; set { Item.Value = value; NotifyPropertyChanged(); } }
        }

        private ObservableCollection<SqlParamWrapper> _sqlParams;

        /// <summary>
        /// List of query parameters
        /// </summary>
        public ObservableCollection<SqlParamWrapper> SqlParams {
            get {
                if (_sqlParams != null) {
                    _sqlParams.UnSubscribe(SqlParamsItem_Changed);
                    _sqlParams.CollectionChanged -= SqlParams_CollectionChanged;
                }

                if (Item == null)
                    return null;

                _sqlParams = new ObservableCollection<SqlParamWrapper>(Item.GoogleBQ.SqlParams.Select(i => new SqlParamWrapper(i)));
                _sqlParams.Subscribe(SqlParamsItem_Changed);
                _sqlParams.CollectionChanged += SqlParams_CollectionChanged;
                return _sqlParams;
            }
        }

        /// <summary>
        /// Called when items was added to or removed from <see cref="SqlParams"/> collection.
        /// Set <see cref="BusyVMBase.IsChanged"/> flag and subscribe/unsubscribe to items PropertyChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SqlParams_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<SqlParamWrapper>()) {
                    IsChanged = true;
                    Item.GoogleBQ.SqlParams.Add(i.Item);
                    i.PropertyChanged += SqlParamsItem_Changed;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<SqlParamWrapper>()) {
                    IsChanged = true;
                    Item.GoogleBQ.SqlParams.Remove(i.Item);
                    i.PropertyChanged -= SqlParamsItem_Changed;
                }
            }
        }

        /// <summary>
        /// Called when item of <see cref="SqlParams"/> changed. Set <see cref="BusyVMBase.IsChanged"/> flag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SqlParamsItem_Changed(object sender, PropertyChangedEventArgs e) {
            IsChanged = true;
        }
        #endregion

        protected override FormulaModel CreateNewItem() {
            var data = base.CreateNewItem();
            data.Type = DAL.Enums.FormulaType.GoogleBQ;
            data.GoogleBQ = new GoogleBQModel() {
                SqlParams = new List<SqlQueryParamModel>(),
                Query = string.Empty,
                ResultType = QueryResultType.Table
            };
            return data;
        }
    }
}
