﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.VM.Base;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Formulas
{
    public class FormulaUsageVM : BusyVMBase
    {
        private readonly IFormulaRepository _source;
        private readonly Guid _guid;

        public FormulaUsageVM(Guid guid, IFormulaRepository source, ILogger logger) 
            : base(changeable: false, refreshable: true, logger: logger)
        {
            _guid = guid;
            _source = source ?? throw new ArgumentNullException(nameof(source));
        }

        public IEnumerable<FormulaBriefModel> UsedInFormulas { get; private set; }
        public IEnumerable<ScenarioSlimModel> UsedInScenarioAsFilter { get; private set; }
        public IEnumerable<ScenarioSlimModel> UsedInScenarioAsCalcResult { get; private set; }
        public IEnumerable<ScenarioSlimModel> UsedInScenarioAsCheckup { get; private set; }

        protected override async Task Refresh()
        {
            FormulaUsageResponse resp = await _source.GetUsage(_guid, ct);
            UsedInFormulas = resp.Formulas;
            UsedInScenarioAsFilter = GetScenarioList(resp, FormulaUsageType.Filter);
            UsedInScenarioAsCalcResult = GetScenarioList(resp, FormulaUsageType.CalcResult);
            UsedInScenarioAsCheckup = GetScenarioList(resp, FormulaUsageType.Checkup);
        }

        private IEnumerable<ScenarioSlimModel> GetScenarioList(FormulaUsageResponse resp, FormulaUsageType type)
        {
            return resp.Scenarios.Where(i => i.Key == type).Select(i => i.Value).FirstOrDefault()
                ?? new ScenarioSlimModel[] { };
        }
    }
}
