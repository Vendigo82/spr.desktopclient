﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Threading;

using Newtonsoft.Json.Linq;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Formulas
{
    using Abstractions;
    using Abstractions.Tests;
    using Abstractions.VM;
    using SPR.WebService.TestModule.Client;
    using SPR.WebService.TestModule.DataContract;
    using SPR.UI.WebService.DataContract.Formula;
    using SPR.UI.App.VM.Base;
    using SPR.DAL.Enums;
    using System.Net.Http;
    using SPR.UI.App.DataContract.Testing;
    using SPR.WebService.TestModule.DataContract.Rest;
    using System.Diagnostics;
    using Newtonsoft.Json;
    using SPR.UI.App.Extensions;

    public class FormulaTestVM : BusyVMBase, ITestVM
    {
        private string _requestData;
        private RestTestingMethod _restTestingMethod = RestTestingMethod.ServerSide;    //method of testing rest formula
        private readonly FormulaType formulaType;

        private readonly IMapper mapper;
        private readonly ITestItemAccessor<FormulaModel> _dataAccessor;
        private readonly ITestModuleClient _testClient;
        private readonly ITestResultsAcceptor _testResultsAcceptor;
        private readonly ITestInputDataProvider _testDataProvider;
        private readonly Func<HttpClient> httpClientFactory;

        public FormulaTestVM(
            FormulaType formulaType,
            ITestItemAccessor<FormulaModel> dataAccessor, 
            ITestModuleClient testClient, 
            ITestResultsAcceptor testResultsAcceptor,
            ITestInputDataProvider testDataProvider,
            IMapper mapper,
            Func<HttpClient> httpClientFactory,
            ILogger logger) 
            : base(changeable: false, refreshable: false, logger: logger) 
        {
            this.formulaType = formulaType;
            _dataAccessor = dataAccessor ?? throw new ArgumentNullException(nameof(dataAccessor));
            _testClient = testClient ?? throw new ArgumentNullException(nameof(testClient));
            _testResultsAcceptor = testResultsAcceptor ?? throw new ArgumentNullException(nameof(testResultsAcceptor));
            _testDataProvider = testDataProvider;
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(mapper));

            TestCmd = new AsyncCommand(async () => await PerformBusyOperation(Test),
                canExecute: () => IsBusy == false,
                errorHandler: ErrorHandler);

            PropertyChanged += FormulaTestVM_PropertyChanged;

            RequestData = _testDataProvider.GetInputData();

            Registry = new ObservableCollection<ValueItem>();
            ScenarioValues = new ObservableCollection<ValueItem>();
        }

        private void FormulaTestVM_PropertyChanged(object sender, PropertyChangedEventArgs e) 
        {
            if (e.PropertyName == nameof(IsBusy))
                (TestCmd as AsyncCommand).RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Local REST formula testing
        /// </summary>
        public RestTestingMethod RestTestingMethod { get => _restTestingMethod; set { _restTestingMethod = value; NotifyPropertyChanged(); } }

        public bool EnableLocalTesting => this.formulaType == FormulaType.RESTService;
        
        /// <summary>
        /// Http request body
        /// </summary>
        public string RequestData { get => _requestData; set { _requestData = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// List of input registry values
        /// </summary>
        public ObservableCollection<ValueItem> Registry { get; private set; }

        /// <summary>
        /// List of already calculated scenario values
        /// </summary>
        public ObservableCollection<ValueItem> ScenarioValues { get; private set; }        

        /// <summary>
        /// NotifyPropertyChanged wrapper for <see cref="ValueInfo"/>
        /// </summary>
        public class ValueItem : NotifyPropertyChangedBase
        {
            private string _name = "";
            private string _value = "";

            public ValueItem()
            {
            }

            public ValueItem(ValueInfo v)
            {
                _name = v.Name;
                _value = v.Value.ToString();
            }

            public string Name { get => _name; set { _name = value; NotifyPropertyChanged(); } }

            public string Value { get => _value; set { _value = value; NotifyPropertyChanged(); } }

            public ValueInfo ToValue() => new ValueInfo() { 
                Name = !string.IsNullOrEmpty(_name) ? _name : throw new NullReferenceException("Value name can't be empty"), 
                Value = string.IsNullOrEmpty(_value) ? JValue.CreateNull() : JToken.Parse(_value), 
                Type = "" 
            };
        }

        protected override Task Refresh() => Task.CompletedTask;

        /// <summary>
        /// Run test command
        /// </summary>
        public ICommand TestCmd { get; }

        /// <summary>
        /// Test formula routine
        /// </summary>
        /// <returns></returns>
        private async Task Test() 
        {
            TestFormulaResponse response;
            IEnumerable<KeyValuePair<string, object>> additional = null;

            JObject data = string.IsNullOrEmpty(RequestData) ? new JObject() : JObject.Parse(RequestData);
            var shownObject = _dataAccessor.ShownObject;

            _testResultsAcceptor.StartTesting(DataContract.ObjectType.Formula, _dataAccessor.Id, shownObject);

            try {
                if (_dataAccessor.ShownObject == DataContract.ShownObjectEnum.Main && (RestTestingMethod == RestTestingMethod.ServerSide || !EnableLocalTesting))
                    response = await _testClient.TestFormula(new TestMainFormulaRequest() {
                        FormulaId = _dataAccessor.Id,
                        Data = data,
                        Registry = ToValueInfoList(Registry),
                        Results = ToValueInfoList(ScenarioValues)
                    }, CancellationToken.None);
                else if (_dataAccessor.Item != null) {
                    if (RestTestingMethod != RestTestingMethod.ServerSide) {
                        var tuple = await LocalTestingRoutine(data);
                        response = tuple.Item1;
                        additional = tuple.Item2;
                        //return;
                    } else
                        response = await _testClient.TestFormula(new TestDraftFormulaRequest() {
                            Formula = _dataAccessor.Item,
                            Data = data,
                            Registry = ToValueInfoList(Registry),
                            Results = ToValueInfoList(ScenarioValues)
                        }, CancellationToken.None);
                } else {
                    _logger.LogWarn("Test formula failed: DataAccessor.Item is null");
                    throw new InvalidOperationException("Testing item does not found");
                }
            } catch {
                _testResultsAcceptor.Cancel(DataContract.ObjectType.Formula, _dataAccessor.Id, shownObject);
                throw;
            }

            _testResultsAcceptor.PushResult(_dataAccessor.Id, shownObject, response, additional);
        }

        private async Task<Tuple<TestFormulaResponse, IEnumerable<KeyValuePair<string, object>>>> LocalTestingRoutine(JObject data)
        {
            // perform initial rest formula test - obtain http request body and etc
            var initialResponse = await _testClient.InitialTestRestFormulaAsync(new TestDraftFormulaRequest
            {
                Data = data,
                Formula = _dataAccessor.Item,
                Registry = ToValueInfoList(Registry),
                Results = ToValueInfoList(ScenarioValues)
            }, CancellationToken.None);

            var result = mapper.Map<TestFormulaResponse>(initialResponse);

            // quit if not success
            if (!initialResponse.Success)
                return MakeTuple(result);

            // if ViewRequest mode then return result
            if (RestTestingMethod == RestTestingMethod.MakeRequest)
            {
                //var result = mapper.Map<TestFormulaResponse>(initialResponse);
                result.Data = new FormulaResultDto { Result = "** interrupted", Type = string.Empty };
                result.Result = result.Data.Result;
                result.Type = result.Data.Type;
                return MakeTuple(result, new[] {
                    new KeyValuePair<string, object>("HttpRequestMethod", initialResponse.Data.HttpVerb),
                    new KeyValuePair<string, object>("HttpRequestUrl", initialResponse.Data.Url),
                    new KeyValuePair<string, object>("HttpRequestBody", initialResponse.Data.Body)
                });
            }

            // make http request from local machine
            var sw = Stopwatch.StartNew();
            var responseData = await PerformLocalHttpRequestAsync(initialResponse.Data);
            sw.Stop();

            // perform final step of testing rest formula - get formula result from http response data
            var finalResponse = await _testClient.FinalTestRestFormulaAsync(new FinalRestFormulaRequest
            {
                Formula = _dataAccessor.Item,
                Response = CreateRequestResponseDto(responseData)
            });

            result.Success = finalResponse.Success;
            result.Error = finalResponse.Error;

            result.Data = finalResponse.Data;
            result.CalculatedTime = result.CalculatedTime + sw.Elapsed + finalResponse.CalculatedTime;
            result.Result = finalResponse.Data?.Result;
            result.Type = finalResponse.Data?.Type;

            return MakeTuple(result, new[] {
                new KeyValuePair<string, object>("HttpRequestMethod", initialResponse.Data.HttpVerb),
                new KeyValuePair<string, object>("HttpRequestUrl", initialResponse.Data.Url),
                new KeyValuePair<string, object>("HttpRequestBody", initialResponse.Data.Body),
                new KeyValuePair<string, object>("HttpResponseStatusCode", responseData.HttpStatusCode),
                new KeyValuePair<string, object>("HttpResponseBody", responseData.ResponseBody),
                new KeyValuePair<string, object>("HttpRequestTime", sw.Elapsed),
            }.AppendIf(new KeyValuePair<string, object>("HttpRequestError", responseData.RequestException?.MessageWithInnerExceptions()), responseData.RequestException != null)
            .ToArray());
        }

        private static HttpRequestResponseDto CreateRequestResponseDto(ResponseDataModel responseData)
        {
            return new HttpRequestResponseDto
            {
                ResponseReceived = responseData.RequestException == null,
                HttpStatusCode = responseData.HttpStatusCode,
                ResponseData = responseData.ResponseBody,
            };
        }

        private static Tuple<TestFormulaResponse, IEnumerable<KeyValuePair<string, object>>> MakeTuple(TestFormulaResponse response, IEnumerable<KeyValuePair<string, object>> additional = null) 
            => new Tuple<TestFormulaResponse, IEnumerable<KeyValuePair<string, object>>>(response, additional);

        private async Task<ResponseDataModel> PerformLocalHttpRequestAsync(RestFormulaRequestInfoDto requestInfo)
        {
            try
            {
                using (var httpClient = httpClientFactory())
                {
                    httpClient.Timeout = requestInfo.Timeout;
                    var requestMessage = new HttpRequestMessage(new HttpMethod(requestInfo.HttpVerb), requestInfo.Url);
                    if (requestInfo.Body != null)
                        requestMessage.Content = new StringContent(requestInfo.Body, Encoding.UTF8, "application/json");

                    var responseMessage = await httpClient.SendAsync(requestMessage);
                    var responseBody = await responseMessage.Content.ReadAsStringAsync();

                    return new ResponseDataModel
                    {
                        HttpStatusCode = (int)responseMessage.StatusCode,
                        ResponseBody = responseBody
                    };
                }
            }
            catch (HttpRequestException e)
            {
                return new ResponseDataModel { RequestException = e };
            }
            catch (OperationCanceledException e)
            {
                return new ResponseDataModel { RequestException = e };
            }
        }

        private static IEnumerable<ValueInfo> ToValueInfoList(IEnumerable<ValueItem> list) 
        {
            if (list?.Any() == true)
                return list.Select(i => i.ToValue());
            else
                return null;
        }

        private class ResponseDataModel
        {
            public Exception RequestException { get; set; }

            public string ResponseBody { get; set; }

            public int HttpStatusCode { get; set; }
        }
    }
}
