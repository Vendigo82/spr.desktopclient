﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;

namespace SPR.UI.App.VM.Formulas
{
    public class TextualBodyFormulaVM : FormulaVM
    {
        public TextualBodyFormulaVM(
            Guid? guid,
            IFormulaRepository source,
            IDictionaryService<int, RejectReasonModel> rejectReasonSource = null,
            IDictionaryService<string, StepModel> stepSource = null, 
            ILogger logger = null,
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider) {
        }

        public string Body { get => Item?.TextualBody.Body; set { Item.TextualBody.Body = value; SetIsChangedAndNotify(); } }

        protected override FormulaModel CreateNewItem() {
            var data = base.CreateNewItem();
            data.Type = DAL.Enums.FormulaType.Expression;
            data.TextualBody = new TextualBodyModel() { Body = string.Empty };
            return data;
        }
    }
}
