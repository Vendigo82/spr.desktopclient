﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;

namespace SPR.UI.App.VM.Formulas
{
    /// <summary>
    /// View model for SQL formula
    /// </summary>
    public class SqlQueryFormulaVM : SqlParamsBaseFormulaVM
    {
        public SqlQueryFormulaVM(
            Guid? guid,
            IFormulaRepository source, 
            IDictionaryService<int, RejectReasonModel> rejectReasonSource = null, 
            IDictionaryService<string, StepModel> stepSource = null, 
            ILogger logger = null, 
            IConfirmActionCommentProvider commentProvider = null) 
            : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider)
        {
        }

        #region Sql properties
        public string Server {
            get => Item?.Sql.Server;
            set { Item.Sql.Server = value; SetIsChangedAndNotify(); }
        }

        public string Sql {
            get => Item?.Sql.Sql;
            set { Item.Sql.Sql = value; SetIsChangedAndNotify(); }
        }

        public DAL.Enums.QueryResultType? ResultType {
            get => Item?.Sql.ResultType;
            set { Item.Sql.ResultType = value ?? default; SetIsChangedAndNotify(); }
        }

        public System.Data.IsolationLevel? IsolationLevel {
            get => Item?.Sql.IsolationLevel;
            set { Item.Sql.IsolationLevel = value; SetIsChangedAndNotify(); }
        }

        protected override List<SqlQueryParamModel> SqlParamsModel => Item?.Sql.SqlParams;
        #endregion



        protected override FormulaModel CreateNewItem()
        {
            var item = base.CreateNewItem();
            item.Type = DAL.Enums.FormulaType.SqlQuery;
            item.Sql = new SqlQueryModel {
                Sql = string.Empty,
                ResultType = DAL.Enums.QueryResultType.Table,
                SqlParams = new List<SqlQueryParamModel>(),
            };
            return item;
        }
    }
}
