﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.VM.Dirs
{
    public class DirRejectReasonVM : DirBaseVM<int, RejectReasonModel>
    {
        public DirRejectReasonVM(
            IDictionaryCRUDService<RejectReasonModel> service,
            ILogger logger,
            IWindowFactory wndFactory = null, 
            IResourceStringProvider resource = null) 
            : base(service, Wrap, logger, wndFactory, resource) {
        }

        public class ItemWrapper : NotifyPropertyChangedBase, IItemWrapper
        {
            public ItemWrapper() {
                Id = 0;
                Item = new RejectReasonModel();
            }

            public ItemWrapper(RejectReasonModel item) {
                Id = item.Code;
                Item = item;
            }

            public int Id { get; }

            public int Key => Item.Code;

            public BunchOperation? UpdateStatus { get; set; } = null;

            public RejectReasonModel Item { get; }

            public bool IsUsed => Item.Used;            

            public int Code { get => Item.Code; 
                set { 
                    if (UpdateStatus == BunchOperation.Insert)
                        Item.Code = value;
                    NotifyPropertyChanged();
                } 
            }

            public string Descr { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }
        }

        private static IItemWrapper Wrap(RejectReasonModel item) => new ItemWrapper(item);
    }
}
