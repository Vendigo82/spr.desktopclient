﻿using SPR.DAL.Enums;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.VM.Dirs
{
    /// <summary>
    /// VM for dictionary step
    /// </summary>
    public class DirStepVM : DirBaseVM<string, StepModel>
    {
        public DirStepVM(
            IDictionaryCRUDService<StepModel> source,
            ILogger logger,
            IWindowFactory wndFactory = null,
            IResourceStringProvider resource = null) 
            : base(source, Wrap, logger, wndFactory, resource){
        }

        /// <summary>
        /// NotifyPropertyChanged wrapper for StepItem
        /// </summary>
        public class ItemWrapper : NotifyPropertyChangedBase, IItemWrapper
        {
            public ItemWrapper() {
                Item = new StepModel();
            }

            public ItemWrapper(StepModel item) {
                Item = item;
                Id = item.Id;
            }

            #region Properties
            public string Name { get => Item.Name; set { Item.Name = value.NullIfEmpty(); NotifyPropertyChanged(); } }

            public string Descr { get => Item.Descr; set { Item.Descr = value.NullIfEmpty(); NotifyPropertyChanged(); } }

            public int Order { get => Item.Order; set { Item.Order = value; NotifyPropertyChanged(); } }

            public StepScopeEnum Scope { get => Item.Scope; set { Item.Scope = value; NotifyPropertyChanged(); } }
            #endregion

            #region IItemWrapper implementation
            public int Id { get; }

            public string Key => Item.Name;

            public BunchOperation? UpdateStatus { get; set; } = null;

            public StepModel Item { get; }

            public bool IsUsed { get => Item.Used; }
            #endregion
        }

        private static IItemWrapper Wrap(StepModel item) => new ItemWrapper(item);
    }
}
