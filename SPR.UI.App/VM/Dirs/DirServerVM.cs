﻿using System;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.VM.Dirs
{
    public class DirServerVM : DirBaseVM<string, ServerModel>
    {
        public DirServerVM(
            IDictionaryCRUDService<ServerModel> service,
            ILogger logger,
            IWindowFactory wndFactory = null, 
            IResourceStringProvider resource = null) 
            : base(service, Wrap, logger, wndFactory, resource) {
        }

        public class ItemWrapper : NotifyPropertyChangedBase, IItemWrapper
        {
            public ItemWrapper() {
                Item = new ServerModel();
            }

            public ItemWrapper(ServerModel item) {
                Id = item.Id;
                Item = item;
            }

            public int Id { get; }

            public string Key => Item.Name;

            public BunchOperation? UpdateStatus { get; set; }

            public ServerModel Item { get; }

            public bool IsUsed => Item.Used;

            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public string Url { get => Item.Url; set { Item.Url = value; NotifyPropertyChanged(); } }

            public TimeSpan? Timeout { get => Item.Timeout; set { Item.Timeout = value; NotifyPropertyChanged(); } }
        }

        private static IItemWrapper Wrap(ServerModel item) => new ItemWrapper(item);
    }
}
