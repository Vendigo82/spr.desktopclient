﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Dirs
{
    public class DirUsageVM : BusyVMBase
    {
        private readonly IDictionarySource dictionarySource;
        private readonly string dictionaryName;
        private readonly string itemId;

        public DirUsageVM(Tuple<string, string> dictionaryItem, IDictionarySource dictionarySource, ILogger logger) 
            : base(logger, changeable: false, refreshable: true, resetIsChangedOnRefresh: false)
        {
            this.dictionaryName = dictionaryItem.Item1 ?? throw new ArgumentNullException(nameof(dictionaryItem));
            this.itemId = dictionaryItem.Item2 ?? throw new ArgumentNullException(nameof(dictionaryItem));
            this.dictionarySource = dictionarySource ?? throw new ArgumentNullException(nameof(dictionarySource));
        }

        public IEnumerable<FormulaBriefModel> Formulas { get; private set; } = Enumerable.Empty<FormulaBriefModel>();

        public IEnumerable<ScenarioSlimModel> Scenarios { get; private set; } = Enumerable.Empty<ScenarioSlimModel>();

        protected override async Task Refresh()
        {
            var usage = await dictionarySource.DirItemInfo(dictionaryName, itemId, ct);
            Formulas = usage.Formulas;
            Scenarios = usage.Scenarios;
        }
    }
}
