﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Services;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Dir;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Dirs
{
    /// <summary>
    /// Base view model class for dictionaries
    /// Provide base dictionary operations
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="T"></typeparam>
    public abstract class DirBaseVM<TKey, T> : BusyVMBase, IDataInitializer where T : class, new()
    {
        /// <summary>
        /// Dictionary items source
        /// </summary>
        private readonly IDictionaryCRUDService<T> _service;

        /// <summary>
        /// Windows factory
        /// </summary>
        private readonly IWindowFactory _wndFactory;

        /// <summary>
        /// Provide resources strings
        /// </summary>
        private readonly IResourceStringProvider _resource;

        /// <summary>
        /// list of deleted items
        /// </summary>
        private HashSet<int> _deletedItems = new HashSet<int>();

        /// <summary>
        /// Function delegate for wrapping items
        /// </summary>
        private readonly Func<T, IItemWrapper> _wrap;

        private IItemWrapper _selectedItem = null;

        /// <summary>
        /// Item wrapper interface
        /// </summary>
        public interface IItemWrapper : INotifyPropertyChanged
        {
            /// <summary>
            /// Item's id
            /// </summary>
            int Id { get; }

            /// <summary>
            /// Item's key (web-service formula data's unique key)
            /// </summary>
            TKey Key { get; }

            /// <summary>
            /// has value if item has been edited, inserted or updated.
            /// Null if item does not need update
            /// </summary>
            BunchOperation? UpdateStatus { get; set; }

            /// <summary>
            /// Item
            /// </summary>
            T Item { get; }

            /// <summary>
            /// is this item used anywhere
            /// </summary>
            bool IsUsed { get; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">dictionary items source</param>
        public DirBaseVM(IDictionaryCRUDService<T> service,
            Func<T, IItemWrapper> wrap,
            ILogger logger,
            IWindowFactory wndFactory = null, 
            IResourceStringProvider resource = null) : base(logger) 
        {
            _service = service;
            _wndFactory = wndFactory;
            _resource = resource;
            _wrap = wrap;

            AddCmd = new RelayCommand(
                execute: () => { var item = _wrap(new T()); Items.Add(item); SelectedItem = item; },
                canExecute: () => IsBusy == false);

            DeleteCmd = new RelayCommand(
                execute: () => Items.Remove(SelectedItem),
                canExecute: () => IsBusy == false && SelectedItem != null && SelectedItem.IsUsed == false);

            
        }

        /// <inheritdoc/>
        public override async Task Initialize(bool fresh) 
            => await PerformRefreshBusyOperation(async () => await LoadData(fresh));

        /// <summary>
        /// Collection of wrapped items
        /// </summary>
        public ObservableCollection<IItemWrapper> Items { get; private set; } = null;

        /// <summary>
        /// Selected item from <see cref="Items"/>
        /// </summary>
        public IItemWrapper SelectedItem { get => _selectedItem; set { _selectedItem = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// System anme of the dictionary
        /// </summary>
        public string DictionaryName => _service.DictionaryName;

        /// <summary>
        /// Selected item's key value
        /// </summary>
        public TKey SelectedValue { 
            get => _selectedItem != null ? _selectedItem.Key : default;  
            set {
                SelectedItem = Items.Where(i => i.Key.Equals(value)).FirstOrDefault();
                NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Add new item
        /// </summary>
        public ICommand AddCmd { get; }

        /// <summary>
        /// Remove <see cref="SelectedItem"/> from <see cref="Items"/>
        /// </summary>
        public ICommand DeleteCmd { get; }

        /// <summary>
        /// Perform refresh operation
        /// </summary>
        protected override async Task Refresh() => await LoadData(true);

        /// <summary>
        /// Perform load data operation
        /// </summary>
        /// <param name="fresh"></param>
        /// <returns></returns>
        private async Task LoadData(bool fresh) {
            IEnumerable<T> list = fresh ? await _service.LoadItemsAsync(ct) : await _service.GetItemsAsync(ct);
            UpdateItems(list);
        }

        /// <summary>
        /// Perform save operation
        /// </summary>
        protected override async Task Save() {
            //prepare inserted and updated items
            List<BunchUpdateItemModel<T>> updateList = new List<BunchUpdateItemModel<T>>();
            foreach (var i in Items) {
                if (i.UpdateStatus != null) {
                    switch (i.UpdateStatus.Value) {
                        case BunchOperation.Update:
                            updateList.Add(new BunchUpdateItemModel<T>() {
                                Operation = BunchOperation.Update,
                                Item = i.Item,
                                Id = i.Id
                            });
                            break;

                        case BunchOperation.Insert:
                            updateList.Add(new BunchUpdateItemModel<T>() {
                                Operation = BunchOperation.Insert,
                                Item = i.Item
                            });
                            break;

                        case BunchOperation.Delete:
                            throw new InvalidOperationException("Items contains dictionary item with delete state");                            
                    }
                }
            }

            //prepare deleted items
            foreach (var i in _deletedItems)
                updateList.Add(new BunchUpdateItemModel<T>() {
                    Operation = BunchOperation.Delete,
                    Id = i
                });

            //warning! nothing to update
            if (updateList.Count == 0)
                return;

            //perform update operation
            var list = await _service.UpdateAsync(updateList, ct);
            //refresh values
            UpdateItems(list);
        }

        /// <summary>
        /// Update list of items
        /// </summary>
        /// <param name="list"></param>
        private void UpdateItems(IEnumerable<T> list) {
            if (Items != null)
                Items.UnSubscribe(ItemChanged);

            Items = new ObservableCollection<IItemWrapper>(list.Select(i => _wrap(i)));
            _deletedItems.Clear();

            Items.Subscribe(ItemChanged);
            Items.CollectionChanged += Items_CollectionChanged;
        }

        /// <summary>
        /// Called when item was changed
        /// </summary>
        private void ItemChanged(object sender, PropertyChangedEventArgs args) {
            IsChanged = true;
            IItemWrapper item = (sender as IItemWrapper);
            if (item.UpdateStatus == null)
                item.UpdateStatus = BunchOperation.Update;
        }

        /// <summary>
        /// Called when collection of items was changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (e.NewItems != null) {
                foreach (var i in e.NewItems.Cast<IItemWrapper>()) {
                    i.UpdateStatus = BunchOperation.Insert;
                    IsChanged = true;
                }
            }

            if (e.OldItems != null) {
                foreach (var i in e.OldItems.Cast<IItemWrapper>()) {
                    //if this item is not new
                    if (i.UpdateStatus != BunchOperation.Insert) {
                        _deletedItems.Add(i.Id);
                        IsChanged = true;
                    } else {
                        IsChanged = Items.Where(x => x.UpdateStatus != null).Any() || _deletedItems.Count > 0;
                    }
                }
            }
        }
    }
}
