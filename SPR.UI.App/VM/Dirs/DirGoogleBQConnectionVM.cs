﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.VM.Dirs
{
    public class DirGoogleBQConnectionVM : DirBaseVM<string, GoogleBQConnectionModel>
    {
        public DirGoogleBQConnectionVM(
            IDictionaryCRUDService<GoogleBQConnectionModel> service, 
            ILogger logger,
            IWindowFactory wndFactory = null, 
            IResourceStringProvider resource = null) 
            : base(service, Wrap, logger, wndFactory, resource) {
        }

        public class ItemWrapper : NotifyPropertyChangedBase, IItemWrapper
        {
            public ItemWrapper() : this(new GoogleBQConnectionModel()) {
            }

            public ItemWrapper(GoogleBQConnectionModel data) {
                Id = data.Id;
                Item = data;
            }

            public int Id { get; }

            public string Key => Item.Name;

            public BunchOperation? UpdateStatus { get; set; } = null;

            public GoogleBQConnectionModel Item { get; }

            public bool IsUsed => Item.Used;

            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public string Credentials { get => Item.Credentials; set { Item.Credentials = value; NotifyPropertyChanged(); } }

            public string ProjectId { get => Item.ProjectId; set { Item.ProjectId = value; NotifyPropertyChanged(); } }
        }

        private static IItemWrapper Wrap(GoogleBQConnectionModel item) => new ItemWrapper(item);
    }
}
