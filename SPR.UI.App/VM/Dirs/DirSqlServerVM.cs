﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.App.Model.Dir;
using SPR.UI.App.Services;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;

namespace SPR.UI.App.VM.Dirs
{
    public class DirSqlServerVM : DirBaseVM<string, SqlServerModel>
    {
        /// <summary>
        /// Database systems source
        /// </summary>
        private readonly IDictionaryService<string, DatabaseSystemModel> _databaseSystemsService;        

        public DirSqlServerVM(
            IDictionaryCRUDService<SqlServerModel> service, 
            IDictionaryService<string, DatabaseSystemModel> databaseSystemsService,
            ILogger logger,
            IWindowFactory wndFactory = null, 
            IResourceStringProvider resource = null) 
            : base(service, Wrap, logger, wndFactory, resource) 
        {
            _databaseSystemsService = databaseSystemsService;
        }

        public class ItemWrapper : NotifyPropertyChangedBase, IItemWrapper
        {
            public ItemWrapper() {
                Item = new SqlServerModel();
            }

            public ItemWrapper(SqlServerModel item) {
                Id = item.Id;
                Item = item;
            }


            public int Id { get; }

            public string Key => Item.Name;

            public BunchOperation? UpdateStatus { get; set; }

            public SqlServerModel Item { get; }

            public bool IsUsed => Item.Used;

            public string Name { get => Item.Name; set { Item.Name = value; NotifyPropertyChanged(); } }

            public string ConnectionString { get => Item.Connection; set { Item.Connection = value; NotifyPropertyChanged(); } }

            public string DatabaseSystem { get => Item.DatabaseSystem; set { Item.DatabaseSystem = value; NotifyPropertyChanged(); } }
        }

        public override async Task Initialize(bool fresh)
        {
            DatabaseSystemsList = await _databaseSystemsService.GetItemsAsync(ct);
            await base.Initialize(fresh);            
        }

        public IEnumerable<DatabaseSystemModel> DatabaseSystemsList { get; private set; } 

        private static IItemWrapper Wrap(SqlServerModel item) => new ItemWrapper(item);
    }
}
