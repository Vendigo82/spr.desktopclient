﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Tests.Model
{ 
    public class TestErrorModel
    {
        public long? FormulaId { get; set; }

        public long? ParentFormulaId { get; set; }

        public string Description { get; set; }
    }
}
