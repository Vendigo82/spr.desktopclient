﻿namespace SPR.UI.App.VM.Tests.Model
{
    public class PostConditionsIssueModel
    {
        public string Message { get; set; }

        public bool IsError { get; set; }
    }
}
