﻿using Newtonsoft.Json.Linq;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Tests;
using SPR.UI.App.VM.Base;
using SPR.WebService.TestModule.Client;
using SPR.WebService.TestModule.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Tests
{
    public class TestExpresstionVM : BusyVMBase
    {
        private readonly ITestInputDataProvider inputDataProvider;
        private readonly ITestModuleClient testClient;

        private string input = null;
        private string expression = string.Empty;
        private string resultValue = null;
        private string resultType = null;

        public TestExpresstionVM(ITestInputDataProvider inputDataProvider, ITestModuleClient testClient, ILogger logger) 
            : base(logger, changeable: false, refreshable: false, resetIsChangedOnRefresh : false)
        {
            this.inputDataProvider = inputDataProvider ?? throw new ArgumentNullException(nameof(inputDataProvider));
            this.testClient = testClient ?? throw new ArgumentNullException(nameof(testClient));

            TestCmd = new AsyncCommand(
                () => PerformBusyOperation(() => EvaluateExpressionRoutine()),
                canExecute: () => IsBusy == false,
                ErrorHandler);
        }

        /// <summary>
        /// Run test expression
        /// </summary>
        public ICommand TestCmd { get; }

        /// <summary>
        /// Test expression input data in json format
        /// </summary>
        public string Input { get => input; set { input = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// Testing expression
        /// </summary>
        public string Expression { get => expression; set { expression = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// Evaluate expression result
        /// </summary>
        public string ResultValue { get => resultValue; private set { resultValue = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// Evaluate expression result's type
        /// </summary>
        public string ResultType { get => resultType; private set { resultType = value; NotifyPropertyChanged(); } }

        private async Task EvaluateExpressionRoutine()
        {
            ResultValue = null;
            ResultType = null;

            var response = await testClient.TestExpressionAsync(new TestExpressionRequest {
                Expression = expression,
                Input = new JRaw(input)
            });

            if (!response.Success) {
                throw new Exceptions.TestExpressionErrorException(response.ErrorMessage);
            }

            ResultValue = response.Result.ToString();
            ResultType = response.Type;
        }

        protected override Task Refresh()
        {
            if (input == null)
                input = inputDataProvider.GetInputData();

            return Task.CompletedTask;
        }
    }
}
