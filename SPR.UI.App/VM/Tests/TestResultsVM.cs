﻿using Newtonsoft.Json.Linq;
using SPR.UI.App.Abstractions.Tests;
using SPR.UI.App.DataContract;
using SPR.UI.App.VM.Tests.Model;
using SPR.WebService.TestModule.Client.DataContract.Response;
using SPR.WebService.TestModule.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

namespace SPR.UI.App.VM.Tests
{
    public class TestResultsVM : NotifyPropertyChangedBase, ITestResultsAcceptor
    {
        private readonly object _lock = new object();
        private readonly DispatcherTimer timer;

        public TestResultsVM()
        {
            timer = new DispatcherTimer {
                Interval = TimeSpan.FromSeconds(1)
            };
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (InProgress) {
                CalculatedTime = (CalculatedTime ?? TimeSpan.Zero) + timer.Interval;
                NotifyPropertyChanged(nameof(CalculatedTime));
            }
        }

        /// <summary>
        /// Calculation in progress
        /// </summary>
        public bool InProgress { get; private set; } = false;

        /// <summary>
        /// Test calculation was success or not
        /// </summary>
        public bool Success { get; private set; } = true;

        /// <summary>
        /// Type of tested object
        /// </summary>
        public ObjectType ObjectType { get; private set; }

        /// <summary>
        /// Which item was calculated
        /// </summary>
        public long? ObjectId { get; private set; }

        /// <summary>
        /// Which object was tested
        /// </summary>
        public ShownObjectEnum? ShownObject { get; private set; }

        /// <summary>
        /// Moment of time, when results was received
        /// </summary>
        public DateTime? Timestamp { get; private set; }

        /// <summary>
        /// Formula calculated time
        /// </summary>
        public TimeSpan? CalculatedTime { get; private set; }

        /// <summary>
        /// Type of calculated result
        /// </summary>
        public string ResultType { get; private set; }

        /// <summary>
        /// Resulting value
        /// </summary>
        public JToken ResultValue { get; private set; }

        /// <summary>
        /// Nested formula results
        /// </summary>
        public IEnumerable<ResultRecordInfo> Results { get; private set; }

        /// <summary>
        /// Existed registry values
        /// </summary>
        public IEnumerable<ValueInfo> Registry { get; private set; }

        /// <summary>
        /// Error information
        /// </summary>
        public TestErrorModel Error { get; private set; }

        /// <summary>
        /// Scenario was passed
        /// </summary>
        public bool? Passed { get; private set; }

        /// <summary>
        /// Scenario next step
        /// </summary>
        public string NextStep { get; private set; }

        /// <summary>
        /// Scenario reject codes
        /// </summary>
        public IEnumerable<int> RejectCodes { get; private set; }

        /// <summary>
        /// List of scenario results
        /// </summary>
        public IEnumerable<KeyValuePairDto> ScenarioResults { get; private set; }

        /// <summary>
        /// List of post conditions issues
        /// </summary>
        public IEnumerable<PostConditionsIssueModel> PostConditionsIssues { get; private set; } = Enumerable.Empty<PostConditionsIssueModel>();

        public bool HavePostConditionsError => PostConditionsIssues.Any(i => i.IsError);

        public IEnumerable<KeyValuePair<string, object>> AdditionalData { get; private set; }

        /// <inheritdoc/>
        public void PushResult(long formulaId, ShownObjectEnum shownObject, TestFormulaResponse response,
            IEnumerable<KeyValuePair<string, object>> additionalData = null) 
        {
            try {
                lock (_lock) {
                    Clear();

                    Success = response.Success;
                    Timestamp = DateTime.Now;
                    ObjectType = ObjectType.Formula;
                    ObjectId = formulaId;
                    ShownObject = shownObject;
                    CalculatedTime = response.CalculatedTime;
                    Results = response.Results;
                    Registry = response.Registry;
                    Error = ToModel(response.Error, formulaId);

                    ResultType = response.Type;
                    ResultValue = response.Result;

                    AdditionalData = additionalData;                    
                }
            } finally {
                NotifyAllPropertiesChanged();
            }
        }

        public void PushResult(long scenarioCode, ShownObjectEnum shownObject, TestResponse<ScenarioResultDto> response)
        {
            try {
                lock (_lock) {
                    Clear();

                    Success = response.Success;
                    Timestamp = DateTime.Now;
                    ObjectType = ObjectType.Scenario;
                    ObjectId = scenarioCode;
                    ShownObject = shownObject;
                    CalculatedTime = response.CalculatedTime;
                    Results = response.Results;
                    Registry = response.Registry;
                    Error = ToModel(response.Error, scenarioCode);

                    if (response.Data != null) {
                        Passed = response.Data.Passed;
                        NextStep = response.Data.NextStep;
                        RejectCodes = response.Data.RejectCodes;
                        ScenarioResults = response.Data.Values;
                    }

                    PostConditionsIssues = Convert(response.PostConditions);
                }
            } finally {
                NotifyAllPropertiesChanged();
            }
        }

        public void StartTesting(ObjectType objectType, long id, ShownObjectEnum shownObject)
        {
            lock (_lock) {
                Clear();
                InProgress = true;
                timer.Start();

                Success = true;
                ObjectType = objectType;
                ObjectId = id;
                ShownObject = shownObject;
                CalculatedTime = null;

                NotifyAllPropertiesChanged();
            }
        }

        public void Cancel(ObjectType objectType, long id, ShownObjectEnum shownObject)
        {
            lock (_lock) {
                Clear();
                InProgress = false;
                timer.Stop();

                Success = false;
                ObjectType = objectType;
                ObjectId = id;
                ShownObject = shownObject;
                CalculatedTime = null;
                NotifyAllPropertiesChanged();
            }
        }

        private TestErrorModel ToModel(ErrorInfoDto dto, long formulaId)
        {
            if (dto == null)
                return null;

            return new TestErrorModel {
                FormulaId = dto.FormulaId == formulaId || dto.FormulaId == 0 ? (long?)null : dto.FormulaId,
                ParentFormulaId = dto.ParentFormulaId,
                Description = dto.Description
            };
        }

        private void Clear()
        {
            InProgress = false;
            timer.Stop();

            Success = true;
            Timestamp = null;
            Results = null;
            Registry = null;
            Error = null;
            ResultType = null;
            ResultValue = null;
            Passed = null;
            NextStep = null;
            RejectCodes = null;
            ScenarioResults = null;

            AdditionalData = null;
            PostConditionsIssues = Enumerable.Empty<PostConditionsIssueModel>();
        }

        private static IEnumerable<PostConditionsIssueModel> Convert(IEnumerable<PostConditionIssueDto> dto) => dto != null 
            ? dto.Select(i => new PostConditionsIssueModel { IsError = i.IsError, Message = i.Message }).ToArray() 
            : Enumerable.Empty<PostConditionsIssueModel>();
    }
}
