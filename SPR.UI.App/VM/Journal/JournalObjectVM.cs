﻿using SPR.DAL.Enums;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Journal;
using System;

namespace SPR.UI.App.VM.Journal
{
    public class JournalObjectVM : JournalBaseVM<JournalRecordSlimModel>
    {
        private readonly Guid guid;
        private readonly SerializedObjectType objectType;

        public JournalObjectVM(IJournalSource<JournalRecordSlimModel> source, Guid guid, SerializedObjectType objectType, ILogger logger)
            : base(source, logger)
        {
            this.guid = guid;
            this.objectType = objectType;
        }

        protected override void PrepareFilter(JournalFilter filter)
        {
            filter.ObjectType = objectType;
            filter.Guid = guid;
        }
    }
}
