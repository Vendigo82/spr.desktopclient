﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Journal;
using System;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Journal
{
    public class JournalCommonVM : JournalBaseVM<JournalRecordModel>
    {
        public JournalCommonVM(IJournalSource<JournalRecordModel> source, ILogger logger) : base(source, logger)
        {
            ResetPeriod();
        }

        protected override void PrepareFilter(JournalFilter filter)
        {            
            if (period != null) {
                filter.DateFrom = period.Item1 < period.Item2 ? period.Item1 : period.Item2;
                filter.DateTo = (period.Item1 < period.Item2 ? period.Item2 : period.Item1);// + TimeSpan.FromDays(1);
            }
        }

        private Tuple<DateTime, DateTime> period;

        private void ResetPeriod()
        {
            var dt = DateTime.Now.Date;
            period = new Tuple<DateTime, DateTime>(dt - TimeSpan.FromDays(6), dt + TimeSpan.FromDays(1));
        }

        public Tuple<DateTime, DateTime> Period {
            get => period;
            set {
                bool update = false;
                if (period != null && value != null) {
                    if (!period.Equals(value))
                        update = true;
                } else if (period != null || value != null)
                    update = true;

                if (update) {
                    period = value;
                    Reload();
                }
            }
        }
    }
}
