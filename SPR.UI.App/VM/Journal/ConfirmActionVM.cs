﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.DAL.Enums;

namespace SPR.UI.App.VM.Journal
{
    public class ConfirmActionVM : NotifyPropertyChangedBase
    {
        private string _comment = null;

        public ConfirmActionVM(JournalAction action, SerializedObjectType objectType, long? id, int? prevVersion = null) {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{GetDescription(action)} {GetDescription(objectType)}");
            if (id.HasValue)
                sb.Append(" #").Append(id.Value.ToString());
            if (action == JournalAction.Rollback)
                sb.Append($" к версии #{prevVersion.Value.ToString()}");
            ActionDescr = sb.ToString();

            Action = action;
            ObjectType = objectType;
            ObjectId = id;
            PreviousVersion = prevVersion;
        }

        public JournalAction Action { get; }

        public SerializedObjectType ObjectType { get; }

        public long? ObjectId { get; }

        public int? PreviousVersion { get; }

        public string ActionDescr { get; }

        public string Comment { 
            get => _comment; 
            set { 
                _comment = value; 
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(AcceptEnabled));
            } 
        }

        public bool IsCommentRequired { get; set; } = true;

        public bool AcceptEnabled => IsCommentRequired == false || (Comment != null && Comment.Length > 0);

        private static string GetDescription(JournalAction val) {
            switch (val) {
                case JournalAction.Release: return "Релиз";
                case JournalAction.Rollback: return "Откат";
                case JournalAction.Import: return "Импорт";
                case JournalAction.Delete: return "Удаление";
                default: return val.ToString();
            }
        }

        private static string GetDescription(SerializedObjectType val) {
            switch (val) {
                case SerializedObjectType.Formula: return "формулы";
                case SerializedObjectType.Scenario: return "сценария";
                default: return val.ToString();
            }
        }
    }
}
