﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataModel;
using SPR.UI.App.Services.Utils;
using SPR.UI.App.VM.Base;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Journal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Journal
{
    public abstract class JournalBaseVM<T> : BusyVMBase where T : JournalRecordSlimModel
    {
        private readonly IJournalSource<T> source;

        /// <summary>
        /// loading data was exhausted
        /// </summary>
        private bool isLoadCompleted = false;

        /// <summary>
        /// loading records
        /// </summary>
        volatile private bool isLoading = false;

        private readonly TaskManager<PartialListContainer<T>> taskManager = new TaskManager<PartialListContainer<T>>();

        private bool firstLoading = true;

        private CancellationTokenSource cts = null;

        public JournalBaseVM(IJournalSource<T> source, ILogger logger) : base(logger)
        {
            this.source = source ?? throw new ArgumentNullException(nameof(source));
            LoadNextCmd = new AsyncCommand(
                execute: () => PerformBackgroudOperation(() => LoadWrapper(() => taskManager.RunTask(LoadPart, (r) => ApplyPartialLoad(r, false)))),
                canExecute: () => IsLoadCompleted == false && IsBusy == false && IsLoading == false,
                ErrorHandler);

            LoadAllCmd = new AsyncCommand(
                execute: () => PerformBusyOperation(() => LoadWrapper(LoadAll)),
                canExecute: () => IsLoadCompleted == false && IsBusy == false && IsLoading == false,
                ErrorHandler);

            CancelLoadCmd = new RelayCommand(
                execute: () => cts?.Cancel(),
                canExecute: () => isLoading);
        }

        /// <summary>
        /// Load next part or records
        /// </summary>
        public ICommand LoadNextCmd { get; }

        /// <summary>
        /// Load all records
        /// </summary>
        public ICommand LoadAllCmd { get; }

        /// <summary>
        /// Cancel loading records
        /// </summary>
        public ICommand CancelLoadCmd { get; }

        /// <summary>
        /// List of records
        /// </summary>
        public ObservableCollection<T> Items { get; } = new ObservableCollection<T>();

        /// <summary>
        /// Loading data was complete
        /// </summary>
        public bool IsLoadCompleted {
            get => isLoadCompleted;
            private set {
                isLoadCompleted = value;
                NotifyPropertyChanged();
                RefreshCommands();
            }
        }

        /// <summary>
        /// Is currently loading records
        /// </summary>
        public bool IsLoading {
            get => isLoading;
            private set {
                isLoading = value;
                NotifyPropertyChanged();
                RefreshCommands();
            }
        }

        /// <summary>
        /// Reload records within selected period
        /// </summary>
        protected void Reload()
        {
            var task = PerformBackgroudOperation(() => LoadWrapper(() => taskManager.RunTask(ReloadPart, (r) => ApplyPartialLoad(r, true))));
        }

        protected abstract void PrepareFilter(JournalFilter filter);

        private JournalFilter CreateFilter()
        {
            var filter = new JournalFilter();
            PrepareFilter(filter);
            return filter;
        }

        protected override async Task Refresh()
        {
            taskManager.Cancel();
            if (firstLoading) {
                await taskManager.RunTask(LoadPart, (r) => ApplyPartialLoad(r, false));
                firstLoading = false;
            } else {
                //loading new records
                bool exhausted;
                long? prevId = null;
                do {
                    long? requestId = Items.FirstOrDefault()?.Id;
                    var resp = await source.LoadAsync(CreateFilter(), requestId, true, ct);

                    InsertRange(resp.Items);

                    if (requestId == prevId)
                        break;
                    prevId = requestId;
                    exhausted = resp.Exhausted || (resp.Items.Any() == false);
                } while (exhausted == false);
            }
        }

        /// <summary>
        /// Perform loading operation
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        private async Task LoadWrapper(Func<Task> func)
        {
            try {
                IsLoading = true;

                using (cts = new CancellationTokenSource()) {
                    await func();
                }
            } catch (OperationCanceledException) {
                if (cts.IsCancellationRequested)
                    return;
                throw;
            } finally {
                IsLoading = false;
                cts = null;
            }
        }

        /// <summary>
        /// Load all records from source
        /// </summary>
        /// <returns></returns>
        private async Task LoadAll()
        {
            while (!IsLoadCompleted && !cts.Token.IsCancellationRequested) {
                await taskManager.RunTask(LoadPart, (r) => ApplyPartialLoad(r, false));
            }

        }

        /// <summary>
        /// Load one part of records from source
        /// </summary>
        /// <returns></returns>
        private Task<PartialListContainer<T>> LoadPart()
        {
            long? lastId = null;
            lock (taskManager.Lock) {
                if (Items.Count > 0)
                    lastId = Items[Items.Count - 1].Id;
            }

            return source.LoadAsync(CreateFilter(), lastId, false, cts?.Token ?? CancellationToken.None);
        }

        /// <summary>
        /// Load one part of records from source
        /// </summary>
        /// <returns></returns>
        private Task<PartialListContainer<T>> ReloadPart()
        {
            return source.LoadAsync(CreateFilter(), null, false, cts?.Token ?? CancellationToken.None);
        }

        /// <summary>
        /// Add loaded records to collection. Clear collection if need
        /// </summary>
        /// <param name="resp"></param>
        /// <param name="clear"></param>
        private void ApplyPartialLoad(PartialListContainer<T> resp, bool clear)
        {
            if (clear)
                Items.Clear();

            IsLoadCompleted = resp.Exhausted;
            foreach (var item in resp.Items)
                Items.Add(item);
            //Items.AddRange(resp.Items);
        }

        /// <summary>
        /// Insert records to begin of collection
        /// </summary>
        /// <param name="items"></param>
        private void InsertRange(IEnumerable<T> items)
        {
            int index = 0;
            lock (taskManager.Lock) {
                foreach (var item in items)
                    Items.Insert(index++, item);
            }
        }

        private void RefreshCommands()
        {
            ((AsyncCommand)LoadNextCmd).RaiseCanExecuteChanged();
            ((AsyncCommand)LoadAllCmd).RaiseCanExecuteChanged();
            ((RelayCommand)CancelLoadCmd).RaiseCanExecuteChanged();
        }
    }
}
