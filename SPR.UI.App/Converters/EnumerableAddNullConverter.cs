﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class ValueWrapper
    {
        public ValueWrapper(object item, string nullString = null) {
            Item = item;
            _nullString = nullString;
        }

        private string _nullString;
        public object Item { get; }        

        public override string ToString() {
            return Item != null ? Item.ToString() : _nullString;
        }
    }

    public class EnumerableAddNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var collection = (IEnumerable)value;
            var list = Enumerable.Repeat<object>(new ValueWrapper(null, parameter != null ? (string)parameter : ""), 1).Concat(
                collection.Cast<object>().Select(o => new ValueWrapper(o))).ToArray();
            return list;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
