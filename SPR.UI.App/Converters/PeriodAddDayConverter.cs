﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class PeriodAddDayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Tuple<DateTime, DateTime> tuple)
                return Tuple.Create(tuple.Item1, tuple.Item2 - TimeSpan.FromDays(1));
            else
                return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Tuple<DateTime, DateTime> tuple)
                return Tuple.Create(tuple.Item1.Date, tuple.Item2.Date + TimeSpan.FromDays(1));
            else
                return Binding.DoNothing;
        }
    }
}
