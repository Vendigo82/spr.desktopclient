﻿using SPR.UI.WebService.DataContract.User;
using System;
using System.Globalization;
using System.Windows.Data;

namespace SPR.UI.App.Converters.User
{
    public class UserModelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return string.Empty;

            if (!(value is UserModel u))
                throw new InvalidCastException($"{nameof(UserModelConverter)} expected type of {nameof(UserModel)} but found {value.GetType().Name}");

            if (string.IsNullOrEmpty(u.Name))
                return u.Login;            
            else
                return $"{u.Name} ({u.Login})";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
