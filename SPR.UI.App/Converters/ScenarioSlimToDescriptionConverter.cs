﻿using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class ScenarioSlimToDescriptionConverter : IValueConverter
    {
        public bool ShowCode { get; set; } = true;

        public bool ShowStep { get; set; } = true;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ScenarioSlimModel m) {
                if (ShowCode) {
                    if (ShowStep)
                        return $"{m.Code} - {m.Name} ({m.Step})";
                    else
                        return $"{m.Code} - {m.Name}";
                } else {
                    if (ShowStep)
                        return $"{m.Name} ({m.Step})";
                    else
                        return $"{m.Name}";
                }
            } else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
