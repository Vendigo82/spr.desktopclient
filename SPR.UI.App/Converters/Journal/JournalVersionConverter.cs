﻿using SPR.UI.WebService.DataContract.Journal;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters.Journal
{
    public class JournalVersionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is JournalRecordSlimModel m) {
                if (m.Action == DAL.Enums.JournalAction.Rollback)
                    return $"{m.Version} ({m.RestoredVersion})";
                else
                    return m.Version.ToString();
            } else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
