﻿using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters.Monitoring
{
    public class TransactionAdditionalDataConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TransactionModel m && parameter is string name) {
                if (m.AdditionalData.TryGetValue(name, out var token))
                    return token?.ToString() ?? string.Empty;
                else
                    return string.Empty;
            } else
                return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
