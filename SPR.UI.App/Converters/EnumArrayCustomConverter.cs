﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    /// <summary>
    /// Convert array of enums to string. Each array value on new line
    /// </summary>
    public class EnumArrayCustomConverter : IValueConverter
    {
        private readonly EnumCustomConverter _converter;

        public EnumArrayCustomConverter(EnumCustomConverter.IStringProvider stringProvider) {
            _converter = new EnumCustomConverter(stringProvider);
        }

        public EnumArrayCustomConverter()
            : this((Application.Current as App).TypeResolver.Resolve<EnumCustomConverter.IStringProvider>()) {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null)
                return null;

            Type t = value.GetType();
            StringBuilder sb = new StringBuilder();
            if (t.IsArray && t.GetElementType().IsEnum) {
                Array array = (Array)value;
                for (int i = 0; i < array.Length; ++i) {
                    if (i != 0)
                        sb.AppendLine();
                    sb.Append((string)_converter.Convert(array.GetValue(i), targetType, parameter, culture));
                }
                return sb.ToString();
            } else
                throw new InvalidCastException($"Can't convert {t.Name} to Enum");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
