﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class TimeSpanToSecondConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is null)
                return null;

            if (value is TimeSpan?) {
                TimeSpan? tsn = (TimeSpan?)value;
                if (tsn.HasValue)
                    return Math.Round(tsn.Value.TotalSeconds).ToString();
                else
                    return null;
            } else if (value is TimeSpan ts)
                return Math.Round(ts.TotalSeconds).ToString();
            else
                throw new InvalidCastException($"Expected TimeSpan, but got {value.GetType().Name}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is null)
                return null;
            if (value is string s) {
                if (string.IsNullOrEmpty(s))
                    return null;
                return TimeSpan.FromSeconds(int.Parse(value.ToString()));
            } else if (value is int i)
                return TimeSpan.FromSeconds(i);
            else
                throw new InvalidCastException($"Convert back not support type {value.GetType().Name}");
        }
    }
}
