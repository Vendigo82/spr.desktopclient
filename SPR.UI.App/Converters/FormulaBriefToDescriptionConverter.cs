﻿using System;
using System.Globalization;
using System.Windows.Data;
using SPR.UI.WebService.DataContract.Formula;

namespace SPR.UI.App.Converters
{
    public class FormulaBriefToDescriptionConverter : IValueConverter
    {
        private readonly EnumCustomConverter _conv = new EnumCustomConverter();

        public bool ShowCode { get; set; } = true;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null)
                return null;

            if (value is FormulaBriefModel f) {
                if (ShowCode)
                    return $"{f.Code} - {f.Name} ({_conv.Convert(f.Type, null, null, null)})";
                else
                    return $"{f.Name} ({_conv.Convert(f.Type, null, null, null)})";
            } else
                return null;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
