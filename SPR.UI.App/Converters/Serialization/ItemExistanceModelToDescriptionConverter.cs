﻿using SPR.UI.WebService.DataContract.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters.Serialization
{
    public class ItemExistanceModelToDescriptionConverter<T> : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IEnumerable<ItemExistanceModel<T>> list) {
                var strings = list.Select(i => $"{i.Item}{(i.Exists ? string.Empty : "(+)")}");
                return string.Join("; ", strings);
            } else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ItemExistanceModelIntToDescriptionConverter : ItemExistanceModelToDescriptionConverter<int> { }

    public class ItemExistanceModelStringToDescriptionConverter : ItemExistanceModelToDescriptionConverter<string> { }
}
