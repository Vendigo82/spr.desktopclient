﻿using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.WebService.DataContract.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters.Serialization
{
    public class StdResultListToDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IEnumerable<ObjectExistanceModel<StdResultBriefModel>> list) {
                var strings = list.Select(i => {
                    if (i.Exists && i.Existed != null) {
                        if (i.Item.Name != i.Existed.Name)
                            return $"{i.Existed.Name} -> {i.Item.Name}";
                        else
                            return i.Existed.Name;
                    } else
                        return $"{i.Item.Name}(+)";
                });
                return string.Join("; ", strings);
            } else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
