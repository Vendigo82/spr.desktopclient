﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class RadioButtonCheckedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, 
            System.Globalization.CultureInfo culture) 
            => value.Equals(parameter);

        public object ConvertBack(object value, Type targetType, object parameter, 
            System.Globalization.CultureInfo culture) 
            => value.Equals(true) ? parameter : Binding.DoNothing;
    }
}
