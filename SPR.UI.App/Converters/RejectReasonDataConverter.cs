﻿using SPR.UI.WebService.DataContract.Dir;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class RejectReasonDataConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null)
                return string.Empty;

            if (value is RejectReasonModel d)
                return $"{d.Code} - {d.Name ?? string.Empty}";
            else
                throw new ArgumentException($"RejectReasonDataConverter expected object of type {nameof(RejectReasonModel)}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
