﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using SPR.UI.WebService.DataContract.Formula;

namespace SPR.UI.App.Converters.Formulas
{
    public class FormulaBriefListToDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is IEnumerable<FormulaBriefModel> f)
                return string.Join("; ", f.Select(i => $"{i.Code} - {i.Name.TrimMaxLength(50)}"));
            else
                return null;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => new NotImplementedException();
    }
}
