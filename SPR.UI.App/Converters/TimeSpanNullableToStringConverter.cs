﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class TimeSpanNullableToStringConverter : IValueConverter
    {
        private const string DefaultFormat = "c";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is null)
                return null;

            if (value is TimeSpan?) {
                TimeSpan? ts = (TimeSpan?)value;
                if (ts == null)
                    return null;
                if (!(parameter != null && parameter is string format))
                    format = DefaultFormat;

                return ts.Value.ToString(format, culture);                
            } else
                throw new InvalidCastException("TimeSpanNullableToStringConverter expected value of TimeSpan? type");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value is string str) {
                if (string.IsNullOrWhiteSpace(str))
                    return null;
                if (!(parameter != null && parameter is string format))
                    format = DefaultFormat;

                if (TimeSpan.TryParseExact(str, format, culture, out TimeSpan result))
                    return result;
            }

            return null;
        }
    }
}
