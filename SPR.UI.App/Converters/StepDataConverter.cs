﻿using SPR.UI.WebService.DataContract.Dir;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    /// <summary>
    /// Converts <see cref="StepData"/> to text description
    /// </summary>
    public class StepDataConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null)
                return string.Empty;

            if (value is StepModel d)
                return string.IsNullOrEmpty(d.Descr) ? d.Name : $"{d.Name} - {d.Descr}";
            else
                throw new InvalidCastException($"StepDataConverter expected StepData value type, but received {value.GetType().Name}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
