﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SPR.UI.App.Converters.Testing
{
    /// <summary>
    /// Converts testing windows additional data keys to caption
    /// </summary>
    public class AdditionalDataKeyConverter : IValueConverter
    {
        private readonly IStringProvider stringProvider;

        public AdditionalDataKeyConverter(IStringProvider stringProvider)
        {
            this.stringProvider = stringProvider;
        }

        public AdditionalDataKeyConverter() : this((Application.Current as App).TypeResolver.Resolve<IStringProvider>())
        {
        }

        public interface IStringProvider
        {
            string Text(string additionalKey);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is string key)
                return stringProvider.Text(key);
            else
                return stringProvider.Text(value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
