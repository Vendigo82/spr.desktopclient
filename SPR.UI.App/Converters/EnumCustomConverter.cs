﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class EnumCustomConverter : IValueConverter
    {
        private readonly IStringProvider _stringProvider;

        public EnumCustomConverter(IStringProvider stringProvider) {
            _stringProvider = stringProvider;
        }

        public EnumCustomConverter() : this((Application.Current as App).TypeResolver.Resolve<IStringProvider>()) {
        }

        public interface IStringProvider
        {
            string Text(string typeNamespace, string typeName, string value, string param);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null)
                return null;
            Type t = value.GetType();            

            if (value.GetType().IsEnum) {
                return _stringProvider.Text(t.Namespace, t.Name, Enum.GetName(t, value),
                    parameter != null ? (string)parameter : null);
            } else
                throw new InvalidCastException($"Can't convert {t.Name} to Enum");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
