﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class CollectionsIsEmptyArrayToVisibilityConverter : IMultiValueConverter
    {
        /// <summary>
        /// Visibility value if all arrays are empty.  Default value is <see cref="Visibility.Collapsed"/>
        /// </summary>
        public Visibility EmptyValue { get; set; } = Visibility.Collapsed;

        /// <summary>
        /// Visibility value if any of arrays is not empty. Default value is <see cref="Visibility.Visible"/>
        /// </summary>
        public Visibility NotEmptyValue { get; set; } = Visibility.Visible;

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            //bool inverse = false;
            //if (parameter != null && parameter is bool)
            //    inverse = (bool)parameter;

            foreach (var i in values) {
                if (i != null && i is IEnumerable e) {
                    if (e.GetEnumerator().MoveNext())
                        return NotEmptyValue;// inverse ? Visibility.Collapsed : Visibility.Visible;
                } else
                    return NotEmptyValue; // inverse ? Visibility.Collapsed : Visibility.Visible;
            }

            return EmptyValue; //inverse ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetsType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
