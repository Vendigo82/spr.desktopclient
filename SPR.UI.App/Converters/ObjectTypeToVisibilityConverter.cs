﻿using SPR.UI.App.DataContract;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SPR.UI.App.Converters
{
    public class ObjectTypeToVisibilityConverter : IValueConverter
    {
        public ObjectType ExpectedObjectType { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ObjectType t)
                return t == ExpectedObjectType ? Visibility.Visible : Visibility.Collapsed;
            else
                return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {            
            throw new NotImplementedException();
        }
    }
}
