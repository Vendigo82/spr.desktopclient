﻿using SPR.UI.WebService.DataContract.Dir;
using System;
using System.Globalization;
using System.Windows.Data;

namespace SPR.UI.App.Converters.DataModel
{
    public class ServerModelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return string.Empty;

            if (value is ServerModel d) {
                if (d.Name == d.Url)
                    return d.Name ?? string.Empty;
                else
                    return $"{d.Name ?? string.Empty} - {d.Url ?? string.Empty}";
            } else
                throw new ArgumentException($"{nameof(ServerModelConverter)} expected object of type {nameof(ServerModel)}");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
