﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Services;
using SPR.UI.App.View;
using SPR.UI.App.VM.Main;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using Unity;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]
namespace SPR.UI.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IUnityContainer container = new UnityContainer();

        public ITypeResolver TypeResolver { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                Modules.Initialize(container);
                TypeResolver = container.Resolve<ITypeResolver>();

                base.OnStartup(e);
                SelectCulture("ru");

                var ver = TypeResolver.Resolve<IAppVersionProvider>();
                TypeResolver.Resolve<ILogger>().LogInfo($"Start program, version: {ver.AppVersion}");

                var wnd = container.Resolve<MainWindow>();
                wnd.Show();
                (wnd.DataContext as MainVM).Initialize();
            }
            catch (Exception exc)
            {
                container?.Resolve<ILogger>().LogFatal("Error in OnStartup", exc);
            }
        }

        public static void SelectCulture(string culture)
        {
            if (string.IsNullOrEmpty(culture))
                return;

            //Copy all MergedDictionarys into a auxiliar list.
            var dictionaryList = Current.Resources.MergedDictionaries.ToList();

            //Search for the specified culture.     
            string requestedCulture = string.Format("StringResources.{0}.xaml", culture);
            var resourceDictionary = dictionaryList.
                FirstOrDefault(d => d.Source.OriginalString.EndsWith(requestedCulture));

            if (resourceDictionary == null)
            {
                //If not found, select our default language.             
                requestedCulture = "StringResources.xaml";
                resourceDictionary = dictionaryList.
                    FirstOrDefault(d => d.Source.OriginalString.EndsWith(requestedCulture));
            }

            //If we have the requested resource, remove it from the list and place at the end.     
            //Then this language will be our string table to use.      
            if (resourceDictionary != null)
            {
                Current.Resources.MergedDictionaries.Remove(resourceDictionary);
                Current.Resources.MergedDictionaries.Add(resourceDictionary);
            }

            //Inform the threads of the new culture.     
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        }

        protected override void OnExit(ExitEventArgs e) {
            base.OnExit(e);
            container.Resolve<ILogger>().LogInfo("Shutdown");
            container.Dispose();            
        }
    }
}
