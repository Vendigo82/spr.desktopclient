﻿using System;

namespace SPR.UI.App {

    sealed class AppManager {
        private AppManager() {
            //Entities = new SPREntities();
        }

        ///// <summary>
        ///// Called when formula was deleted, parameter is formula id
        ///// </summary>
        //public event EventHandler<Tuple<Guid, long>> FormulaDeleted;
        ///// <summary>
        ///// called when scenario was deleted, parameter is scenario id
        ///// </summary>
        //public event EventHandler<Tuple<Guid, long>> ScenarioDeleted;

        ///// <summary>
        ///// Called when formula dependencies changed: include to other formula or scenario
        ///// </summary>
        //public event EventHandler<DependencyChangedArg> FormulaUsageChanged;


        //public event EventHandler<Tuple<Guid, long>> FormulaDraftDeleted;
        //public event EventHandler<Tuple<Guid, string>> FormulaDraftAdded;
        //public event EventHandler<Tuple<Guid, string>> FormulaDraftModified;

        //public event EventHandler<Tuple<Guid, long>> ScenarioDraftDeleted;
        //public event EventHandler<Tuple<Guid, string>> ScenarioDraftAdded;
        //public event EventHandler<Tuple<Guid, string>> ScenarioDraftModified;

        static private Lazy<AppManager> _inst = new Lazy<AppManager>(() => new AppManager());
        static public AppManager Inst => _inst.Value;
    }
}
