﻿using AutoMapper;
using SPR.DAL.Enums;
using SPR.DocClient;
using SPR.UI.ApiClient.Infra;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.Tests;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Abstractions.WebService;
using SPR.UI.App.Commands;
using SPR.UI.App.Commands.Dialogs;
using SPR.UI.App.Commands.Serialization;
using SPR.UI.App.Converters;
using SPR.UI.App.Converters.Testing;
using SPR.UI.App.Model;
using SPR.UI.App.Model.Dir;
using SPR.UI.App.Model.Main;
using SPR.UI.App.Services;
using SPR.UI.App.Services.Auth;
using SPR.UI.App.Services.Cache;
using SPR.UI.App.Services.Events;
using SPR.UI.App.Services.Factory;
using SPR.UI.App.Services.LocalData;
using SPR.UI.App.Services.LocalData.Files;
using SPR.UI.App.Services.Loggers;
using SPR.UI.App.Services.Options;
using SPR.UI.App.Services.TestModule;
using SPR.UI.App.Services.UI;
using SPR.UI.App.Services.Utils;
using SPR.UI.App.Services.WebService;
using SPR.UI.App.View.Common;
using SPR.UI.App.View.Dirs;
using SPR.UI.App.View.Formulas;
using SPR.UI.App.View.Journal;
using SPR.UI.App.View.Main;
using SPR.UI.App.View.Monitoring;
using SPR.UI.App.View.Scenarios;
using SPR.UI.App.View.Serialization;
using SPR.UI.App.View.Tests;
using SPR.UI.App.VM.Dirs;
using SPR.UI.App.VM.Formulas;
using SPR.UI.App.VM.Journal;
using SPR.UI.App.VM.Main;
using SPR.UI.App.VM.Monitoring;
using SPR.UI.App.VM.Scenarios;
using SPR.UI.App.VM.Serialization;
using SPR.UI.App.VM.Tests;
using SPR.UI.App.Windows.Common.JsonEditor;
using SPR.UI.App.Windows.DictConstants;
using SPR.UI.App.Windows.FullTextSearch;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.Core.UseCases.StepConstraints;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Journal;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.WebService.TestModule.Client;
using System;
using System.Configuration;
using System.Net.Http;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Unity;
using Unity.Lifetime;

namespace SPR.UI.App
{
    public static class Modules
    {
        public const string WND_ABOUT = "about";
        public const string WND_LOGIN = "login";
        public const string WND_TEXT = "textwnd";
        public const string TAB_PROFILE = "profile";
        public const string TAB_CONTAINER = "tab_container";
        public const string TAB_CONTAINER_PARAM_NAME = "content";
        public const string WND_CONTENT = "busy_content";
        public const string WND_JSON_EDITOR = "json_editor_wnd";

        public const string TAB_DIR_STEP = "dirStep";
        public const string TAB_DIR_REJECTREASON = "dirRejectReason";
        public const string TAB_DIR_SERVER = "dirServer";
        public const string TAB_DIR_SQLSERVER = "dirSqlServer";
        public const string TAB_DIR_GOOGLEBQ = "dirGoogleBQConnection";
        public const string WND_SELECT_ITEM = "selectDirItem";
        public const string TAB_STD_RESULTS_LIST = "stdResultsList";
        public const string TAB_STD_RESULTS = "stdResults";
        public const string DIR_ITEM_USAGE = "dirUsage";
        public const string STD_RESULTS_USAGE = "stdResultsUsage";
        public const string TAB_CONSTANTS = "dictConstants";
        public const string TAB_STEP_CONSTRAINTS = "stepConstraints";

        public const string TAB_JOURNAL = "journal";
        public const string TAB_TRANSACTIONS = "transactions";
        public const string TAB_ERRORS = "errors";
        public const string TAB_SUMMARY = "summary";
        public const string WND_TRANS_CALC_RESULTS = "trans_calcresults";
        public const string TAB_TEST_EXPRESSION = "test_expression";

        public const string TAB_FORMULAS_LIST = "formulasList";
        public const string TAB_SCENARIOS_LIST = "scenariosList";
        public const string TAB_FORMULA = "formula";
        public const string TAB_SCENARIO = "scenario";
        public const string VIEW_FORMULAS_LIST = "workingFormulas";
        public const string FORMULA_CHECKUPS_LIST = "formulaCheckups";
        public const string FORMULA_TYPE_SELECT_WND = "formulaSelectType";
        public const string VIEW_SELECT_ARCHIVE_VERSION = "archiveVersions";
        public const string TABL_FULLTEXT_SEARCH = "fullTextSearch";

        public const string TAB_OBJECT_JOURNAL = "obj_journal";
        public const string TAB_FORMULA_USAGE = "formula_usage";

        public const string WND_EXPORT = "export";
        public const string WND_IMPORT = "import";
        public const string WND_COPY = "copy";

        public const string VIEW_DOCUMENTATION = "documentation";
        public const string SELECT_INSTANCE_DLG = "select_instance_dlg";

        public const string ApiHttpClientName = "ApiClient";
        public const string DocHttpClientName = "DocClient";

        public static IUnityContainer Create()
        {
            var container = new UnityContainer();
            Initialize(container);
            return container;
        }

        public static void Initialize(IUnityContainer container) {
            //dependency injection service
            container.RegisterInstance<IUnityContainer>(container);
            container.RegisterSingleton<ITypeResolver, TypeResolver>();

            //log
            container.RegisterSingleton<ILogger, DefaultLogger>();

            //mapping
            var mapper = new AutoMapper.Mapper(new AutoMapper.MapperConfiguration(c => {
                c.AddProfile<Mapping.MappingProfile>();
                c.AddProfile<Mapping.ApiMappingProfile>();
                c.AddProfile<DocClient.Mapping.MappingProfile>();
                c.AddProfile<ApiClient.Mapping.SwaggerMappingProfile>();
            }));
            container.RegisterInstance<AutoMapper.IMapper>(mapper);

            //events
            container.RegisterSingleton<INotificationEventsService, AppManagerEventsProfider>();

            //settings
            Settings settings = new Settings();
            container.RegisterInstance<ISettings>(settings);
            container.RegisterInstance<ISettingsWritter>(settings);
            container.RegisterFactory<CountOptions<JournalFullSource>>(c => new CountOptions<JournalFullSource> {
                Count = int.Parse(ConfigurationManager.AppSettings["journal_records_count"] ?? "30")
            });
            container.RegisterFactory<CountOptions<JournalSlimSource>>(c => new CountOptions<JournalSlimSource> {
                Count = int.Parse(ConfigurationManager.AppSettings["journal_records_count"] ?? "30")
            });
            container.RegisterFactory<CountOptions<MonitoringSourceSwagger>>(c => new CountOptions<MonitoringSourceSwagger> {
                Count = int.Parse(ConfigurationManager.AppSettings["transaction_records_count"] ?? "50")
            });

            container.RegisterWebServiceConfig();
            container.RegisterInstancesSource();

            //web service
            container.RegisterType<IAccessTokenManager, AccessTokenManager>();
            
            container.RegisterFactory<HttpClient>(ApiHttpClientName, (c) => {
                var s = c.Resolve<IWebServiceConfig>();
                var handler = new RequireAuthHttpClientHandler(c.Resolve<INotificationEventsService>(), new HttpClientHandler());
                var httpClient = new HttpClient(handler) {
                    BaseAddress = new Uri(s.BaseUrl.AddTerminateSlash())
                };
                if (s.Timeout != null)
                    httpClient.Timeout = s.Timeout.Value;
                httpClient.DefaultRequestHeaders.Add("X-NoWrap", "1");
                return httpClient;
            }, new Unity.Lifetime.SingletonLifetimeManager());

            #region Documentation service
            container.RegisterFactory<HttpClient>(DocHttpClientName,
                (c) =>
                {
                    var s = c.Resolve<ISettings>();
                    var httpClient = new HttpClient()
                    {
                        BaseAddress = s.Documentation.Uri
                    };
                    return httpClient;
                },
                new Unity.Lifetime.SingletonLifetimeManager());
            //container.RegisterFactory<DocRepository.IOptions>(c => c.Resolve<ISettings>().Documentation);
            container.RegisterFactory<IDocHttpClient>(c =>
            {
                return new DocHttpClient(c.Resolve<HttpClient>(DocHttpClientName));
            });
            container.RegisterType<IDocRepository, DocRepository>();
            #endregion

            container.RegisterFactory<SPR.UI.ApiClient.IApiClient>((c) => 
                new ApiClient.ApiClient(c.Resolve<HttpClient>(ApiHttpClientName))
            );

            container.RegisterSingleton<IConfigurationSource, ConfigurationSource>();
            container.RegisterType<IAuthService, AuthService>();
            container.RegisterType<IProfileService, ProfileService>();
            container.RegisterType<ISerialization, SerializationSource>();

            //test service
            container.RegisterType<AppTestModuleClient.ISettings, AppTestModuleClientSettings>();
            container.RegisterSingleton<ITestModuleClient, AppTestModuleClient>();
            container.RegisterSingleton<TestResultsVM>();
            container.RegisterType<ITestInputDataProvider, StaticFileInputDataProvider>();

            //dictionaries
            container.RegisterType<IDictionarySource, DictionarySource>();
            container.RegisterDictionary(
                c => new DirRejectReasonService(c.Resolve<ApiClient.IApiClient>(), c.Resolve<IMapper>()),
                DirRejectReasonService.KeyFunc);
            container.RegisterDictionary(
                c => new DirStepService(c.Resolve<ApiClient.IApiClient>(), c.Resolve<IMapper>()),
                DirStepService.KeyFunc);
            container.RegisterDictionary(
                c => new DirGoogleBQConnectionService(c.Resolve<ApiClient.IApiClient>(), c.Resolve<IMapper>()),
                DirGoogleBQConnectionService.KeyFunc
                );
            container.RegisterDictionary(
                c => new DirServerService(c.Resolve<ApiClient.IApiClient>(), c.Resolve<IMapper>()),
                DirServerService.KeyFunc);
            container.RegisterDictionary(
                c => new DirSqlServerService(c.Resolve<ApiClient.IApiClient>(), c.Resolve<IMapper>()),
                DirSqlServerService.KeyFunc);
            //database systems dictionary
            container.RegisterType<DirDatabaseSystemsService>();
            container.RegisterFactory<IDictionaryService<string, DatabaseSystemModel>>(
                c => new CachedDictionaryDataList<string, DatabaseSystemModel>(
                    c.Resolve<DirDatabaseSystemsService>(), 
                    DirDatabaseSystemsService.KeyFunc), new Unity.Lifetime.SingletonLifetimeManager());
            container.RegisterType<IConstantsSource, ConstantsSource>();

            //journal
            container.RegisterType<IJournalSource<JournalRecordModel>, JournalFullSource>();
            container.RegisterType<IJournalSource<JournalRecordSlimModel>, JournalSlimSource>();

            // monitoring
            container.RegisterType<IMonitoringSource, MonitoringSourceSwagger>();

            //formula
            container.RegisterType<IDeleteOperation<FormulaBriefModel>, FormulaDeleteSwaggerOperation>();
            container.RegisterType<IFormulaRepository, FormulaRepository>();
            container.RegisterFactory<IDataSource<FormulaModel>>(c => c.Resolve<IFormulaRepository>());
            container.RegisterType<IDataListSource<FormulaBriefModel>, FormulaListSwaggerSource>();
            container.RegisterType<IDataListFilterSource<FormulaBriefModel, FormulaFilter>, FormulaListSwaggerSource>();
            container.RegisterType<IDataListSource<FormulaScenarioCheckupModel>, FormulaCheckupsSwaggerSource>();
            container.RegisterType<IFullTextSearchSource, FullTextSearchSource>();

            //scenario
            container.RegisterType<IDeleteOperation<ScenarioBriefModel>, ScenarioDeleteOperation>();
            container.RegisterType<IStdResultsSource, StdResultsSource>();
            container.RegisterType<IStdResultsListSource, StdResultListSource>();
            container.RegisterFactory<IDataListSource<StdResultSlimModel>>(c => c.Resolve<IStdResultsListSource>());
            container.RegisterType<IDataListSource<ScenarioBriefModel>, ScenarioListSource>();
            container.RegisterType<IDataListFilterSource<ScenarioBriefModel, ScenarioFilter>, ScenarioListSource>();
            container.RegisterType<IScenarioSource, ScenarioSource>();

            //other services
            container.RegisterType<IAppVersionProvider, AppVersionProvider>();
            container.RegisterType<ILogoProvider, LogoProvider>();
            container.RegisterType<IResourceStringProvider, ResourceStringProvider>();
            container.RegisterType<ITabTitleProvider, TabTitleResourceProvider>();
            container.RegisterType<EnumCustomConverter.IStringProvider, EnumConverterStringProvider>();
            //container.RegisterType<IConfirmActionCommentProvider, ConfirmActionCommentProvider>();
            container.RegisterType<IConfirmActionCommentProvider, SilentCommentProvider>();
            container.RegisterType<IStepConstraintsRepository, StepConstraintsRepository>();

            //local data files
            container.RegisterType<IDataFileNamesProvider, DataFileNameProvider>();
            container.RegisterType<ILogginnedHistory, LoginnedHistory>();

            //models
            container.RegisterType<IMainWndModel, MainWndModel>();//new Unity.Lifetime.PerResolveLifetimeManager());            

            //VM
            container.RegisterType<ProfileVM.IBadPasswordReasonInterpretator, ProfileVMReasonInterpretator>();
            container.RegisterType<IDataObjectVM<FormulaModel>, TextualBodyFormulaVM>(FormulaType.Expression.ToString());
            container.RegisterType<IDataObjectVM<FormulaModel>, ScriptTextualBodyFormulaVM>(FormulaType.Script.ToString());
            container.RegisterType<IDataObjectVM<FormulaModel>, GoogleBQFormulaVM>(FormulaType.GoogleBQ.ToString());
            container.RegisterType<IDataObjectVM<FormulaModel>, RestServiceFormulaVM>(FormulaType.RESTService.ToString());
            container.RegisterType<IDataObjectVM<FormulaModel>, SqlQueryFormulaVM>(FormulaType.SqlQuery.ToString());
            container.RegisterType<IDataObjectVM<FormulaModel>, CheckupBunchFormulaVM>(FormulaType.BunchFormula.ToString());
            container.RegisterType<IDataObjectVM<FormulaModel>, ScoreCardFormulaVM>(FormulaType.ScoreCard.ToString());
            container.RegisterType<IDataObjectVMFactory<FormulaModel>, FormulaTabsVMFactory>();
            container.RegisterType<ICommonVM, FormulaUsageVM>(TAB_FORMULA_USAGE);
            container.RegisterType<ICommonVM, StdResultsVM>(TAB_STD_RESULTS);
            container.RegisterType<IDataObjectVMFactory<ScenarioModel>, ScenarioTabsVMFactory>();
            container.RegisterType<IDataObjectVM<ScenarioModel>, ScenarioVM>();
            container.RegisterType<ICommonVM, FormulaCheckupsListVM>(FORMULA_CHECKUPS_LIST);
            container.RegisterType<ICommonVM, JournalObjectVM>(TAB_OBJECT_JOURNAL);
            container.RegisterType<ICommonVM, JournalCommonVM>(TAB_JOURNAL);
            container.RegisterType<ICommonVM, ExportVM>(WND_EXPORT);
            container.RegisterType<ICommonVM, ImportDataVM>(WND_IMPORT);
            container.RegisterType<ICommonVM, CopyVM>(WND_COPY);
            container.RegisterType<ICommonVM, DirUsageVM>(DIR_ITEM_USAGE);
            container.RegisterType<ICommonVM, StdResultsUsageVM>(STD_RESULTS_USAGE);
            container.RegisterType<ICommonVM, TransactionsVM>(TAB_TRANSACTIONS);
            container.RegisterType<ICommonVM, ErrorsVM>(TAB_ERRORS);
            container.RegisterType<ICommonVM, SummaryVM>(TAB_SUMMARY);
            container.RegisterType<ICommonVM, CalculationResultsVM>(WND_TRANS_CALC_RESULTS);
            container.RegisterType<ICommonVM, TestExpresstionVM>(TAB_TEST_EXPRESSION);
            container.RegisterType<ICommonVM, FullTextSearchVM>(TABL_FULLTEXT_SEARCH);
            container.RegisterType<ICommonVM, DictConstantsVM>(TAB_CONSTANTS);
            container.RegisterType<ICommonVM, Windows.StepConstraints.StepConstraintsVM>(TAB_STEP_CONSTRAINTS);
            container.RegisterType<ICommonVM, Windows.Documentation.DocFunctionsListVM>(VIEW_DOCUMENTATION);
            container.RegisterType<ICommonVM, Windows.Main.SelectInstanceDlg.SelectInstanceVM>(SELECT_INSTANCE_DLG);

            //views and windows
            container.RegisterType<Control, BusyIndicatorWrapper>(TAB_CONTAINER); //tab container!
            container.RegisterType<Control, ProfileView>(TAB_PROFILE);
            container.RegisterType<Control, DirStepView>(TAB_DIR_STEP);
            container.RegisterType<Control, DirRejectReasonView>(TAB_DIR_REJECTREASON);
            container.RegisterType<Control, DirServerView>(TAB_DIR_SERVER);
            container.RegisterType<Control, DirSqlServerView>(TAB_DIR_SQLSERVER);
            container.RegisterType<Control, DirGoogleBQConnectionView>(TAB_DIR_GOOGLEBQ);
            container.RegisterType<Control, JournalView>(TAB_JOURNAL);
            container.RegisterType<Control, FormulaListMainView>(TAB_FORMULAS_LIST);
            container.RegisterType<Control, UsageView>(DIR_ITEM_USAGE);
            container.RegisterType<Control, UsageView>(STD_RESULTS_USAGE);
            container.RegisterType<Control, TransactionsView>(TAB_TRANSACTIONS);
            container.RegisterType<Control, ErrorsView>(TAB_ERRORS);
            container.RegisterType<Control, SummaryView>(TAB_SUMMARY);
            container.RegisterType<Control, CalculationResultsView>(WND_TRANS_CALC_RESULTS);
            container.RegisterType<Control, FullTextSearchView>(TABL_FULLTEXT_SEARCH);
            container.RegisterType<Control, DictConstantsView>(TAB_CONSTANTS);
            container.RegisterType<Control, Windows.StepConstraints.StepConstraintsView>(TAB_STEP_CONSTRAINTS);
            container.RegisterType<Control, Windows.Documentation.DocFunctionsListView>(VIEW_DOCUMENTATION);

            container.RegisterType<Control, FormulaMainView>(TAB_FORMULA);
            container.RegisterType<Control, TextualBodyView>(TAB_FORMULA + "_" + FormulaType.Expression.ToString());
            container.RegisterType<Control, TextualBodyView>(TAB_FORMULA + "_" + FormulaType.Script.ToString());
            container.RegisterType<Control, CheckupBunchView>(TAB_FORMULA + "_" + FormulaType.BunchFormula.ToString());
            container.RegisterType<Control, GoogleBQView>(TAB_FORMULA + "_" + FormulaType.GoogleBQ.ToString());
            container.RegisterType<Control, RestServiceView>(TAB_FORMULA + "_" + FormulaType.RESTService.ToString());
            container.RegisterType<Control, ScoreCardView>(TAB_FORMULA + "_" + FormulaType.ScoreCard.ToString());
            container.RegisterType<Control, SqlQueryView>(TAB_FORMULA + "_" + FormulaType.SqlQuery.ToString());

            container.RegisterType<Control, ScenarioMainTabsView>(TAB_SCENARIO);
            container.RegisterType<Control, FormulaListView>(VIEW_FORMULAS_LIST);
            container.RegisterType<Control, ScenarioListMainView>(TAB_SCENARIOS_LIST);
            container.RegisterType<Control, SelectArchivedVersionView>(VIEW_SELECT_ARCHIVE_VERSION);
            container.RegisterType<Control, StdResultsListMainView>(TAB_STD_RESULTS_LIST);
            container.RegisterType<Control, StdResultsView>(TAB_STD_RESULTS);
            container.RegisterType<Control, FormulaCheckupsMultiSelectView>(FORMULA_CHECKUPS_LIST);
            container.RegisterType<Control, TestExpressionView>(TAB_TEST_EXPRESSION);

            container.RegisterType<Window, AboutWnd>(WND_ABOUT);
            container.RegisterType<Window, LoginWnd>(WND_LOGIN);
            container.RegisterType<Window, TextWnd>(WND_TEXT);
            container.RegisterType<Window, SelectItemDlg>(WND_SELECT_ITEM);
            container.RegisterType<Window, BusyContentWnd>(WND_CONTENT);
            container.RegisterType<Window, SelectFormulaTypeDlg>(FORMULA_TYPE_SELECT_WND);
            container.RegisterType<Window, ExportDlg>(WND_EXPORT);
            container.RegisterType<Window, ImportDataDlg>(WND_IMPORT);
            container.RegisterType<Window, CopyWnd>(WND_COPY);
            container.RegisterType<Window, Windows.Main.SelectInstanceDlg.SelectInstanceDlg>(SELECT_INSTANCE_DLG);

            container.RegisterType<Window, JsonEditorWnd>(WND_JSON_EDITOR);
            container.RegisterType<ICommonVM, JsonEditorVM>(WND_JSON_EDITOR);

            //resource accessors 
            container.RegisterType<ScenarioVM.IStringsProvider, ScenarioVMStringsProvider>();
            container.RegisterType<AdditionalDataKeyConverter.IStringProvider, TestingAdditionalDataKeyConverterResourceStringProvider>();

            //main window
            //container.RegisterSingleton<MainWindow>();

            //commands
            container.RegisterAction<OpenScenarioCheckupsDialogAction, object, string>(nameof(GlobalCommands.OpenSelectCheckupsDlgCmd));
            container.RegisterAction<ExportAction, object>(nameof(GlobalCommands.ExportCmd));
            container.RegisterAction<ImportAction>(nameof(GlobalCommands.ImportCmd));
            container.RegisterAction<CopyAction, object>(nameof(GlobalCommands.CopyCmd));
            container.RegisterAction<Commands.Monitoring.OpenCalculationResultsDialogAction, long>(nameof(GlobalCommands.TransactionsCalcResultsDlg));
            container.RegisterAction<Commands.Dialogs.DirItemUsageDialogAction, string, object>(nameof(GlobalCommands.DirItemUsageDialogCmd));
            container.RegisterAction<Commands.Dialogs.StdResultUsageDialogAction, Guid>(nameof(GlobalCommands.StdResultUsageDialogCmd));
            container.RegisterAction<Commands.Common.OpenProfileAction>(nameof(GlobalCommands.OpenProfileCmd));
            container.RegisterAction<Commands.Dialogs.OpenJsonEditDialogAction, object, string>(nameof(GlobalCommands.OpenJsonEditDialogCmd));

            //window factory
            container.RegisterType<IFactory<ICommonVM>, VMFactory>();
            container.RegisterType<IFactory<ICommand>, CommandFactory>();
            container.RegisterType<IWindowFactory, WindowFactory>();

            //test acceptor: contains two instance - one view model itself, second - main window, which need to expand test panel
            container.RegisterFactory<ITestResultsAcceptor>((c) => new TestResultAcceptorRaiseEventWrapper(
                c.Resolve<TestResultsVM>(), c.Resolve<INotificationEventsService>()));
        }

        private static void RegisterDictionary<TKey, T>(this IUnityContainer container, Func<IUnityContainer, IDictionaryCRUDService<T>> sourceFactory,
            Func<T, TKey> keyFunc) 
            where T : class 
        {
            //var cached = new CachedDictionaryCRUDService<TKey, T>(source, keyFunc, container.Resolve<ILogger>());
            container.RegisterFactory<CachedDictionaryCRUDService<TKey, T>>(c => 
                new CachedDictionaryCRUDService<TKey, T>(sourceFactory(c), keyFunc, c.Resolve<ILogger>()), 
                new Unity.Lifetime.SingletonLifetimeManager());

            container.RegisterFactory<IDictionaryCRUDService<T>>(c => c.Resolve<CachedDictionaryCRUDService<TKey, T>>());
            container.RegisterFactory<IDataListSource<T>>(c => c.Resolve<CachedDictionaryCRUDService<TKey, T>>());
            container.RegisterFactory<IDictionaryService<TKey, T>>(c => c.Resolve<CachedDictionaryCRUDService<TKey, T>>());
        }

        private static void RegisterAction<T>(this IUnityContainer container, string name) where T : ICommandAction
        {            
            container.RegisterFactory<ICommand>(name, (c) => {
                var action = container.Resolve<T>();
                var resolver = container.Resolve<ITypeResolver>();
                return action.CreateRelayCommand(resolver);
            });
        }

        private static void RegisterAction<T, T1>(this IUnityContainer container, string name)
            where T : ICommandAction<T1>
        {
            container.RegisterFactory<ICommand>(name, (c) => {
                var action = container.Resolve<T>();
                var resolver = container.Resolve<ITypeResolver>();
                return action.CreateRelayCommand(resolver);
            });
        }

        private static void RegisterAction<T, T1, T2>(this IUnityContainer container, string name) 
            where T : ICommandAction<T1, T2>
        {
            container.RegisterFactory<ICommand>(name, (c) => {
                var action = container.Resolve<T>();
                var resolver = container.Resolve<ITypeResolver>();
                return action.CreateRelayCommand(resolver);
            });
        }
    }    

    public static class UnityContainerExtensions
    {
        public static IUnityContainer RegisterWebServiceConfig(this IUnityContainer container)
        {
            var settings = new WebServiceConfig();
            container.RegisterInstance<IWebServiceConfig>(settings);
            container.RegisterInstance<IWebServiceConfigWritter>(settings);
            return container;
        }

        public static IUnityContainer RegisterInstancesSource(this IUnityContainer container) => container.RegisterFactory<IInstancesSource>(c 
            => new InstanceCombineSource(
                new InstancesJsonSource("appsettings.json"), 
                new InstanceConfigSource()
                ), new SingletonLifetimeManager());

    }
}
