﻿using System;

namespace SPR.UI.App.Extensions
{

    public static class ExceptionExtensions
    {
        public static string MessageWithInnerExceptions(this Exception exception)
        {
            if (exception.InnerException == null)
                return exception.Message;

            return exception.Message + " " + exception.InnerException.MessageWithInnerExceptions();
        }
    }
}
