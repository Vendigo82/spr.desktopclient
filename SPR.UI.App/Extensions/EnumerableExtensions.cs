﻿using System.Collections.Generic;
using System.Linq;

namespace SPR.UI.App.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> AppendIf<T>(this IEnumerable<T> list, T value, bool predicate) 
            => predicate ? list.Concat(Enumerable.Repeat(value, 1)) : list;
    }
}
