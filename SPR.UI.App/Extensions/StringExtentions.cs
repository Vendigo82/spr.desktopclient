﻿using System;
using System.Linq;

namespace SPR.UI.App
{
    public static class StringExtentions
    {
        /// <summary>
        /// Decode time span value:
        ///     dddx.
        /// Where 
        ///     ddd - digits, 
        ///     x - one of characters: d - days, h - hours, m - minutes, s - seconds.
        /// if x is emitted, then using seconds
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TimeSpan ToTimeSpan(this string value) {
            switch (value.Last()) {
                case 'd': return TimeSpan.FromDays(int.Parse(value.Substring(0, value.Length - 1)));
                case 'h': return TimeSpan.FromHours(int.Parse(value.Substring(0, value.Length - 1)));
                case 'm': return TimeSpan.FromMinutes(int.Parse(value.Substring(0, value.Length - 1)));
                case 's': return TimeSpan.FromDays(int.Parse(value.Substring(0, value.Length - 1)));
                default: return TimeSpan.FromSeconds(int.Parse(value));
            }
        }

        /// <summary>
        /// If <paramref name="value"/> is empty string, then return null
        /// Otherwise returns origin value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string NullIfEmpty(this string value) => string.IsNullOrEmpty(value) ? null : value;

        public static string AddTerminateSlash(this string url) => string.Concat((url ?? string.Empty).TrimEnd('/'), "/");

        public static string TrimMaxLength(this string value, int maxLength) => value.Length > maxLength ? value.Substring(0, maxLength - 3) + "..." : value;
    }
}
