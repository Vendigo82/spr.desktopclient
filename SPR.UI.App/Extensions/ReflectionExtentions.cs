﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App
{
    public static class ReflectionExtentions
    {
        /// <summary>
        /// Assign action with parameter to ICommand property
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="commandName">command property name</param>
        /// <param name="action">delegate action</param>
        /// <returns>true if property exists and value type is ICommand</returns>
        public static bool TrySetCommandDelegate(this object obj, string commandName, Action<object> action) {
            PropertyInfo pi = obj.GetCommandProperty(commandName);
            if (pi == null)
                return false;

            pi.SetValue(obj, new RelayCommand<object>(action));
            return true;
        }

        /// <summary>
        /// Assign action without parameter to ICommand property
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="commandName">command property name</param>
        /// <param name="action">delegate action</param>
        /// <returns>true if property exists and value type is ICommand</returns>
        public static bool TrySetCommandDelegate(this object obj, string commandName, Action action) {
            PropertyInfo pi = obj.GetCommandProperty(commandName);
            if (pi == null)
                return false;

            pi.SetValue(obj, new RelayCommand(action));
            return true;
        }

        /// <summary>
        /// Returns PropertyInfo for property with name <paramref name="commandName"/> and type ICommand
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="commandName"></param>
        /// <returns>PropertyInfo</returns>
        private static PropertyInfo GetCommandProperty(this object obj, string commandName) {
            PropertyInfo pi = obj.GetType().GetProperty(commandName);
            if (pi == null)
                return null;

            Type ptype = pi.PropertyType;            
            if (ptype != typeof(ICommand))
                return null;

            return pi;
        }

        /// <summary>
        /// Try to set value <paramref name="value"/> to property with name <paramref name="propertyName"/>
        /// Type of property must be equals to type of value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool TrySetPropertyValue<T>(this object obj, string propertyName, T value) {
            PropertyInfo pi = obj.GetPropertyOfType(propertyName, typeof(T), PropertyTypeComparision.AssignableToProperty);
            if (pi == null)
                return false;

            pi.SetValue(obj, value);
            return true;
        }

        /// <summary>
        /// Try  to get value from property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool TryGetPropertyValue<T>(this object obj, string propertyName, out T value) {
            PropertyInfo pi = obj.GetPropertyOfType(propertyName, typeof(T), PropertyTypeComparision.AssignableToValue);
            if (pi == null) {
                value = default;
                return false;
            }

            value = (T)pi.GetValue(obj);
            return true;
        }

        /// <summary>
        /// Get value of property of specific type or throws exception
        /// </summary>
        /// <typeparam name="T">Property type</typeparam>
        /// <param name="obj"></param>
        /// <param name="propertyName">Property name</param>
        /// <returns>Property value</returns>
        /// <exception cref="InvalidOperationException">If property not found or has different type</exception>
        public static T GetPropertyValue<T>(this object obj, string propertyName)
        {
            if (TryGetPropertyValue<T>(obj, propertyName, out var value))
                return value;
            else
                throw new InvalidOperationException($"Type '{obj.GetType().Name}' does not contains property '{propertyName}' of type '{typeof(T).Name}'");
        }

        /// <summary>
        /// Set property value or throws exception
        /// </summary>
        /// <typeparam name="T">Property type</typeparam>
        /// <param name="obj"></param>
        /// <param name="propertyName">Property name</param>
        /// <param name="value">Value to set</param>
        /// <exception cref="InvalidOperationException">If property not found or has different type</exception>
        public static void SetPropertyValue<T>(this object obj, string propertyName, T value)
        {
            if (!TrySetPropertyValue<T>(obj, propertyName, value))
                throw new InvalidOperationException($"Type '{obj.GetType().Name}' does not contains property '{propertyName}' of type '{typeof(T).Name}'");
        }

        private enum PropertyTypeComparision {
            /// <summary>
            /// Value and property types should be scrict equal
            /// </summary>
            Strict,

            /// <summary>
            /// Type of value should can be assigned to property 
            /// (Value type is inherited from property type)
            /// </summary>
            AssignableToProperty,

            /// <summary>
            /// Type of property should can be assigned to value
            /// (Property type is inherited form value type)
            /// </summary>
            AssignableToValue
        }

        /// <summary>
        /// Returns PropertyInfo for property with name <paramref name="commandName"/> and type <paramref name="t"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="commandName"></param>
        /// <returns>PropertyInfo</returns>       
        private static PropertyInfo GetPropertyOfType(
            this object obj, 
            string propertyName, 
            Type t,
            PropertyTypeComparision comparision)
        {
            PropertyInfo pi = obj.GetType().GetProperty(propertyName);
            if (pi == null)
                return null;

            switch (comparision) {
                case PropertyTypeComparision.Strict:
                    if (pi.PropertyType != t)
                        return null;
                    break;

                case PropertyTypeComparision.AssignableToProperty:
                    if (!pi.PropertyType.IsAssignableFrom(t))
                        return null;
                    break;

                case PropertyTypeComparision.AssignableToValue:
                    if (!t.IsAssignableFrom(pi.PropertyType))
                        return null;
                    break;
            }
            //if (!t.IsAssignableFrom(pi.PropertyType))
            

            return pi;
        }
    }
}
