﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.Commands
{

    public static class GlobalCommands
    {        

        static GlobalCommands() 
        {
            //CloseWindowCmd = new DelegateCommand<Window>(w => w.Close());
            CloseDialogCmd = new RelayCommand<Window>(w => { w.DialogResult = true; w.Close(); });

            CopyTextToClipboardCmd = new RelayCommand<string>((s) => Clipboard.SetText(s));

            //ShowParameterCmd = new DelegateCommand<object>(o => MessageBox.Show(o == null ? "null" : o.ToString()));
        }

        static public ICommand OpenSelectFormulaDialogCmd { get; } = new RelayCommand<Tuple<object, object>>(
            execute: (t) => {

                SelectFormula(t.Item1, (string)t.Item2);
            });

        /// <summary>
        /// Add new item to list object. Require parameter of <see cref="IList{T}"/> type.
        /// New item created with reflection using parameterless constructor
        /// </summary>
        static public ICommand AddNewListItemCmd { get; } = new RelayCommand<object>(AddNewListItem);

        /// <summary>
        /// Remove selected rows from Grid and other objects
        /// Require parameter of <see cref="MultiSelector"/> type
        /// </summary>
        static public ICommand MultiSelectorDeleteCmd { get; } //= new RelayCommand<MultiSelector>(MultiSelectorDelete);
            = new RelayCommand<MultiSelector>((arg) => ErrorHandler(() => MultiSelectorDelete(arg)));

        /// <summary>
        /// Remove selected item from selector (ListView etc)
        /// Require parameter of <see cref="Selector"/> type
        /// </summary>
        static public ICommand SelectorDeleteCmd { get; } = new RelayCommand<Selector>((arg) => ErrorHandler(() => SelectorDelete(arg)));

        /// <summary>
        /// Clear context property value. Command parameter is Tuple, where Item1 = context, Item2 = property name
        /// </summary>
        static public ICommand ClearValueCmd { get; } = new RelayCommand<Tuple<object, object>>((t) =>
            PropertySetValue(t.Item1, t.Item2 as string, null));

        //static public ICommand CloseWindowCmd { get; }
        [Obsolete]
        static public ICommand CloseDialogCmd { get; }

        static public ICommand CopyTextToClipboardCmd { get; }

        /// <summary>
        /// Show dialog box with parameter as text
        /// </summary>
        static public ICommand ShowTextWndCmd { get; } = new RelayCommand<object>(o => ShowTextWnd(o?.ToString() ?? ""));

        /// <summary>
        /// Select dictionary value and assign to DataContent property
        /// Command arg: Tuple
        ///     Value 1: DataContext, object
        ///     Value 2: PropertyName, string
        ///     Value 3: Name of the dictionary
        /// </summary>
        static public ICommand SelectDictionaryItemCmd { get; } = new RelayCommand<Tuple<object, object, object>>((t) =>
            ErrorHandler(() => SelectDictionaryItem(t.Item1, t.Item2 as string, t.Item3 as string)));

        /// <summary>
        /// Select archive version
        /// Command argument is Tuple with values:
        ///     Value 1: main DataContext, object
        ///     Value 2: Property name in data context which holds selected archive version, string
        ///     Value 3: Select archive version data context
        /// </summary>
        static public ICommand SelectArchiveVersionCmd { get; } = new RelayCommand<Tuple<object, object, object>>((t) =>
            ErrorHandler(() => {
                Window wnd = WindowFactory.CreateWindowOwned(Modules.WND_SELECT_ITEM, Modules.VIEW_SELECT_ARCHIVE_VERSION);                
                wnd.DataContext = t.Item3;
                ShowSelectDialog(wnd, t.Item1, t.Item2 as string, "SelectedValue");
            }));

        static public ICommand OpenSelectCheckupsDlgCmd => CreateCommand();
        static public ICommand ExportCmd => CreateCommand();
        static public ICommand ImportCmd => CreateCommand();
        static public ICommand CopyCmd => CreateCommand();
        static public ICommand TransactionsCalcResultsDlg => CreateCommand();
        static public ICommand DirItemUsageDialogCmd => CreateCommand();
        static public ICommand StdResultUsageDialogCmd => CreateCommand();

        static public ICommand OpenProfileCmd => CreateCommand();

        static public ICommand OpenJsonEditDialogCmd => CreateCommand();

        private static void MultiSelectorDelete(MultiSelector selector) {
            if (selector.SelectedItems.Count > 0) {
                foreach (var item in selector.SelectedItems.Cast<object>().ToArray())
                    (selector.ItemsSource as IList).Remove(item);
            }
        }     
        
        /// <summary>
        /// Remove selected item from selector
        /// </summary>
        /// <param name="selector"></param>
        private static void SelectorDelete(Selector selector)
        {
            if (selector.SelectedItem != null)
                (selector.ItemsSource as IList).Remove(selector.SelectedItem);
        }

        private static void AddNewListItem(object list) {
            System.Type t = list.GetType();
            if (!(list is IList && t.IsGenericType))
                return;
            t = list.GetType().GetGenericArguments()[0];
            object newObj = Activator.CreateInstance(t);
            (list as IList).Add(newObj);
        }
        
        /// <summary>
        /// Assign null to context property. First tuple item is context, second is property name
        /// </summary>
        /// <param name="param"></param>
        private static void PropertyAssignNull(Tuple<object, object> param) {
            PropertySetValue(param.Item1, param.Item2 as string, null);
        }

        private static void PropertySetValue(object obj, string propName, object value) {
            System.Type t = obj.GetType();
            t.GetProperty(propName).SetValue(obj, value);
        }

        private static object PropertyGetValue(object obj, string propName) {
            System.Type t = obj.GetType();
            return t.GetProperty(propName).GetValue(obj);
        }

        private static void ShowTextWnd(string text) 
        {
            Window wnd = WindowFactory.CreateWindowOwned(Modules.WND_TEXT);
            wnd.DataContext = text;
            wnd.ShowDialog();
        }

        private static void SelectDictionaryItem(object dataContext, string propertyName, string dictionaryViewName) {
            Window wnd = WindowFactory.CreateWindowOwned(Modules.WND_SELECT_ITEM, dictionaryViewName);
            wnd.Title = string.Concat(wnd.Title, " ", TitleProvider.Title(dictionaryViewName));
            ShowSelectDialog(wnd, dataContext, propertyName, "SelectedValue");
        }

        /// <summary>
        /// Show select formula dialog and assign selected value to data context
        /// </summary>
        /// <param name="dataContext">Data context, will contains property with name <paramref name="propertyName"/></param>
        /// <param name="propertyName">Property in dataContext, must contains property with name "Code"</param>
        private static void SelectFormula(object dataContext, string propertyName) {
            //create dialog window
            Window wnd = WindowFactory.CreateWindowOwned(Modules.WND_SELECT_ITEM, Modules.VIEW_FORMULAS_LIST);

            //get current selected formula code
            object selectedValue = null;
            PropertyInfo dcpi = dataContext.GetType().GetProperty(propertyName) 
                ?? throw new ApplicationException($"Property {propertyName} has not found in {dataContext.GetType().Name}");
            object formula = dcpi.GetValue(dataContext);
            if (formula != null) {
                PropertyInfo codepi = formula.GetType().GetProperty("Code")
                    ?? throw new ApplicationException($"Property Code has not found in {formula.GetType().Name}");
                selectedValue = codepi.GetValue(formula);
            }
                            
            //initialize dialog
            (wnd.DataContext as IDataInitializer)
                ?.Initialize(false)
                .ContinueWith((t) => {
                    //assign current selected value
                    if (selectedValue != null) {
                        PropertyInfo wndValuePi = wnd.DataContext.GetType().GetProperty("SelectedValue");
                        if (wndValuePi != null)
                            wndValuePi.SetValue(wnd.DataContext, selectedValue);
                    }                    
                }, TaskContinuationOptions.ExecuteSynchronously);

            //show dialog
            if (wnd.ShowDialog() == true) {
                //assign selected item
                PropertyInfo wndItemPi = wnd.DataContext.GetType().GetProperty("SelectedItem")
                    ?? throw new ApplicationException($"Property SelectedItem has not found in {wnd.DataContext.GetType().Name}"); ;
                if (wndItemPi != null) {
                    object item = wndItemPi.GetValue(wnd.DataContext);
                    dcpi.SetValue(dataContext, item);
                }
            }
        }

        private static void ShowSelectDialog(Window wnd, object dataContext, string propertyName, string wndPropertyName) {
            PropertyInfo wndpi = wnd.DataContext.GetType().GetProperty(wndPropertyName)
                ?? throw new InvalidOperationException($"Property SelectedValue has not found in {wnd.DataContext.GetType().Name}");
            PropertyInfo dcpi = dataContext.GetType().GetProperty(propertyName)
                ?? throw new InvalidOperationException($"Property {propertyName} has not found in {dataContext.GetType().Name}");

            (wnd.DataContext as IDataInitializer)
                ?.Initialize(false)
                .ContinueWith((t) => {
                    wndpi.SetValue(wnd.DataContext, dcpi.GetValue(dataContext));
                }, TaskContinuationOptions.ExecuteSynchronously);

            if (wnd.ShowDialog() == true) {
                var selected = wndpi.GetValue(wnd.DataContext);
                dcpi.SetValue(dataContext, selected);
            }
            
        }

        private static void ErrorHandler(Action action) {
            try {
                action();
            } catch (Exception e) {
                Logger.Value.LogError("Perform command error", e);
                WindowFactory.ShowErrorDialog("Internal error");
            }
        }

        private static IFactory<ICommand> factory;
        public static IFactory<ICommand> Factory { 
            get => factory ?? TypeResolver.Resolve<IFactory<ICommand>>();
            set => factory = value;
        }

        private static ICommand CreateCommand([CallerMemberName] string name = null)
            => Factory.Create(name ?? throw new ArgumentNullException(nameof(name)));

        private static ITypeResolver TypeResolver => (Application.Current as App).TypeResolver;

        private static IWindowFactory WindowFactory => TypeResolver.Resolve<IWindowFactory>();

        private static ITabTitleProvider TitleProvider => TypeResolver.Resolve<ITabTitleProvider>();

        private static Lazy<ILogger> Logger { get; } = new Lazy<ILogger>(
            () => TypeResolver.Resolve<ILogger>(),
            true);
    }
}
