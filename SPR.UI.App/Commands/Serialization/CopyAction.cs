﻿using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Services;
using System;

namespace SPR.UI.App.Commands.Serialization
{
    public class CopyAction : DialogActionBase, ICommandAction<object>
    {
        private readonly ITypeResolver resolver;

        public CopyAction(ITypeResolver resolver, IWindowFactory wndFactory) : base(wndFactory)
        {
            this.resolver = resolver ?? throw new ArgumentNullException(nameof(resolver));
        }

        public void Action(object arg)
        {
            var context = resolver.Resolve<ICommonVM, object>(Modules.WND_COPY, arg);
            var dlg = wndFactory.CreateWindowOwned(Modules.WND_COPY);
            dlg.DataContext = context;

            if (context is IDataInitializer di)
                di.Initialize(true);

            ShowDialog(dlg);
        }
    }
}
