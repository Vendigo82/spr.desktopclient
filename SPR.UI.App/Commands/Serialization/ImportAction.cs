﻿using Microsoft.Win32;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Commands.Serialization
{
    public class ImportAction : DialogActionBase, ICommandAction
    {
        private readonly ITypeResolver typeResolver;
        private readonly IResourceStringProvider resourceString;
        private readonly ILogger logger;

        public ImportAction(
            ITypeResolver typeResolver, 
            IWindowFactory wndFactory,
            IResourceStringProvider resourceString,
            ILogger logger) : base(wndFactory)
        {
            this.typeResolver = typeResolver ?? throw new ArgumentNullException(nameof(typeResolver));
            this.resourceString = resourceString ?? throw new ArgumentNullException(nameof(resourceString));
            this.logger = logger;
        }

        public void Action()
        {
            var dlg = new OpenFileDialog() {
                //FileName = DataContext.GetPropertyValue<string>("FileName"),
                Filter = "Json files (.json)|*.json",
                DefaultExt = ".json",
                CheckPathExists = true,
                Title = resourceString.GetString("Serialization.SelectImportFileDlg.Title")
            };
            if (dlg.ShowDialog() != true)
                return;

            logger?.LogInfo($"File '{dlg.FileName}' was selected to import");

            var data = File.ReadAllText(dlg.FileName);
            var vm = typeResolver.Resolve<ICommonVM, string>(Modules.WND_IMPORT, data);
            var wnd = wndFactory.CreateWindowOwned(Modules.WND_IMPORT);
            wnd.DataContext = vm;
            if (vm is IDataInitializer di)
                di.Initialize();

            ShowDialog(wnd);
        }
    }
}
