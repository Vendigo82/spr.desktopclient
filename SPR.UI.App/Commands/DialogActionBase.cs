﻿using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using System;
using System.Windows;

namespace SPR.UI.App.Commands
{
    public class DialogActionBase
    {
        protected readonly IWindowFactory wndFactory;

        public DialogActionBase(IWindowFactory wndFactory)
        {
            this.wndFactory = wndFactory ?? throw new ArgumentNullException(nameof(wndFactory));
        }

        protected virtual bool? ShowDialog(Window wnd) => wnd.ShowDialog();

        protected void OpenBusyContentWnd(string viewName, Action<Window> adjust = null)
        {
            var context = wndFactory.CreateVM(viewName);
            var dlg = wndFactory.CreateWindowOwned(Modules.WND_CONTENT, viewName);
            dlg.DataContext = context;

            if (context is IDataInitializer di)
                di.Initialize(true);

            adjust?.Invoke(dlg);

            ShowDialog(dlg);
        }
    }
}
