﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SPR.UI.App.Commands
{
    public static class CommandsLibrary
    {
        private static readonly object syncRoot = new object();

        public const string ParamBegin = "begin";
        public const string ParamEnd = "end";
        public const string DateFormat = "yyyyMMdd";

        public const string DIALOG = "dlg";
        public const string TAB = "tab";

        public const string OpenCmdMonitoring = TAB + ":" + Modules.TAB_SUMMARY;// "monitoring";
        public const string OpenCmdTransactions = TAB + ":" + Modules.TAB_TRANSACTIONS;// "transactions";
        public const string OpenCmdErrors = TAB + ":" + Modules.TAB_ERRORS;//"errors";

        public const string OpenAbount = DIALOG + ":" + Modules.WND_ABOUT;
        public const string OpenProfile = TAB + ":" + Modules.TAB_PROFILE;
        public const string Login = "login";

        public const string OpenDirRejectReason = TAB + ":" + Modules.TAB_DIR_REJECTREASON;
        public const string OpenDirSqlServer = TAB + ":" + Modules.TAB_DIR_SQLSERVER;
        public const string OpenDirStep = TAB + ":" + Modules.TAB_DIR_STEP;
        public const string OpenDirServer = TAB + ":" + Modules.TAB_DIR_SERVER;
        public const string OpenDirGooleBQ = TAB + ":" + Modules.TAB_DIR_GOOGLEBQ;
        public const string OpenStdResults = TAB + ":" + Modules.TAB_STD_RESULTS_LIST;

        public const string OpenJournal = TAB + ":" + Modules.TAB_JOURNAL;
        public const string OpenTestExpression = TAB + ":" + Modules.TAB_TEST_EXPRESSION;

        public const string OpenFormulasList = TAB + ":" + Modules.TAB_FORMULAS_LIST;
        public const string OpenScenariosList = TAB + ":" + Modules.TAB_SCENARIOS_LIST;
        public const string FullTextSearch = TAB + ":" + Modules.TABL_FULLTEXT_SEARCH;

        public const string OpenDictConstants = TAB + ":" + Modules.TAB_CONSTANTS;
        public const string OpenDocumentation = TAB + ":" + Modules.VIEW_DOCUMENTATION;

        private static RoutedUICommand _openScenarioCmd;
        public static RoutedUICommand OpenScenarioCmd {
            get {
                lock (syncRoot) {
                    if (_openScenarioCmd == null)
                        _openScenarioCmd = new RoutedUICommand("Open scenario", nameof(OpenScenarioCmd), typeof(CommandsLibrary));
                    return _openScenarioCmd;
                }
            }
        }

        private static RoutedUICommand _newScenarioCmd;
        public static RoutedUICommand NewScenarioCmd {
            get {
                lock (syncRoot) {
                    if (_newScenarioCmd == null)
                        _newScenarioCmd = new RoutedUICommand("New scenario", nameof(NewScenarioCmd), typeof(CommandsLibrary));
                    return _newScenarioCmd;
                }
            }
        }

        private static RoutedUICommand _openFormulaCmd;
        public static RoutedUICommand OpenFormulaCmd {
            get {
                lock (syncRoot) {
                    if (_openFormulaCmd == null)
                        _openFormulaCmd = new RoutedUICommand("Open formula", nameof(OpenFormulaCmd), typeof(CommandsLibrary));
                    return _openFormulaCmd;
                }
            }
        }

        private static RoutedUICommand _newFormulaCmd;
        public static RoutedUICommand NewFormulaCmd {
            get {
                lock (syncRoot) {
                    if (_newFormulaCmd == null)
                        _newFormulaCmd = new RoutedUICommand("New formula", nameof(NewFormulaCmd), typeof(CommandsLibrary));
                    return _newFormulaCmd;
                }
            }
        }

        #region Standard results
        private static Lazy<RoutedUICommand> _openStdResultsCmd = new Lazy<RoutedUICommand>(
            () => new RoutedUICommand("Open std results", nameof(OpenStdResultsCmd), typeof(CommandsLibrary)), true);

        public static RoutedUICommand OpenStdResultsCmd => _openStdResultsCmd.Value;

        private static Lazy<RoutedUICommand> _newStdResultsCmd = new Lazy<RoutedUICommand>(
            () => new RoutedUICommand("New std results", nameof(NewStdResultsCmd), typeof(CommandsLibrary)), true);

        public static RoutedUICommand NewStdResultsCmd => _newStdResultsCmd.Value;
        #endregion

        #region Step constraints
        private static Lazy<RoutedUICommand> _openStepConstraintsCmd = new Lazy<RoutedUICommand>(
            () => new RoutedUICommand("Open step constraints", nameof(OpenStepConstraintsCmd), typeof(CommandsLibrary)), true);

        public static RoutedUICommand OpenStepConstraintsCmd => _openStepConstraintsCmd.Value;
        #endregion

        [Obsolete]
        private static RoutedUICommand _deleteFormulaCmd;
        [Obsolete]
        public static RoutedUICommand DeleteFormulaCmd {
            get {
                lock (syncRoot) {
                    if (_deleteFormulaCmd == null)
                        _deleteFormulaCmd = new RoutedUICommand("Delete formula", nameof(DeleteFormulaCmd), typeof(CommandsLibrary));
                    return _deleteFormulaCmd;
                }
            }
        }

        [Obsolete]
        private static RoutedUICommand _deleteScenarioCmd;
        [Obsolete]
        public static RoutedUICommand DeleteScenarioCmd {
            get {
                lock (syncRoot) {
                    if (_deleteScenarioCmd == null)
                        _deleteScenarioCmd = new RoutedUICommand("Delete scenario", nameof(DeleteScenarioCmd), typeof(CommandsLibrary));
                    return _deleteScenarioCmd;
                }
            }
        }

        private static RoutedUICommand _openCmd;
        public static RoutedUICommand OpenCmd {
            get {
                lock (syncRoot) {
                    if (_openCmd == null)
                        _openCmd = new RoutedUICommand("Open", nameof(OpenCmd), typeof(CommandsLibrary));
                    return _openCmd;
                }
            }
        }
    }
}
