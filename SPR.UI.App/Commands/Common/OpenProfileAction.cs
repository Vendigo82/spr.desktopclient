﻿using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Services;
using System;

namespace SPR.UI.App.Commands.Common
{
    public class OpenProfileAction : ICommandAction
    {
        private readonly ISettings settings;

        public OpenProfileAction(ISettings settings)
        {
            this.settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public void Action()
        {
            if (settings.UsingExternalProfile)
            {
                System.Diagnostics.Process.Start(settings.ProfileUrl);
            } 
            else
            {
                CommandsLibrary.OpenCmd.Execute(CommandsLibrary.OpenProfile, null);
            }
        }
    }
}
