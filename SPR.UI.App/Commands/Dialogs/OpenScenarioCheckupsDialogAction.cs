﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

using SPR.UI.WebService.DataContract.Formula;
using System.Collections;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.App.Abstractions.UI;

namespace SPR.UI.App.Commands.Dialogs
{
    /// <summary>
    /// Open select checkups dialog.
    /// First argument is DataContext
    /// Second argument is DataContext property name which contains checkups collection
    /// </summary>
    public class OpenScenarioCheckupsDialogAction : DialogActionBase, ICommandAction<object, string>
    {
        public const string SelectedItemsPropertyName = "SelectedItems";

        private readonly IFactory<ICommonVM> vmFactory;

        public OpenScenarioCheckupsDialogAction(IFactory<ICommonVM> vmFactory, IWindowFactory wndFactory) : base(wndFactory)
        {
            this.vmFactory = vmFactory ?? throw new ArgumentNullException(nameof(vmFactory));
        }

        public void Action(object dataContext, string propertyName)
        {
            //create view model
            var vm = vmFactory.Create(Modules.FORMULA_CHECKUPS_LIST);

            //create view
            var wnd = wndFactory.CreateWindowOwned(Modules.WND_SELECT_ITEM, Modules.FORMULA_CHECKUPS_LIST);
            wnd.DataContext = vm;

            //initialize dialog and select items
            if (vm is IDataInitializer di)
                di.Initialize().ContinueWith(t => {
                    vm.SetPropertyValue(SelectedItemsPropertyName, dataContext.GetPropertyValue<IEnumerable<FormulaScenarioCheckupModel>>(propertyName));
                });

            if (ShowDialog(wnd) == true) {
                //get selected and update context items
                var selected = vm.GetPropertyValue<IEnumerable<FormulaScenarioCheckupModel>>(SelectedItemsPropertyName);
                dataContext.SetPropertyValue(propertyName, selected);
            }
        }
    }
}
