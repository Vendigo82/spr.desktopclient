﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using System;

namespace SPR.UI.App.Commands.Dialogs
{
    public class StdResultUsageDialogAction : DialogActionBase, ICommandAction<Guid>
    {
        private readonly IFactory<ICommonVM> vmFactory;

        public StdResultUsageDialogAction(IFactory<ICommonVM> vmFactory, IWindowFactory wndFactory) : base(wndFactory)
        {
            this.vmFactory = vmFactory ?? throw new ArgumentNullException(nameof(vmFactory));
        }

        public void Action(Guid itemId)
        {
            var window = wndFactory.CreateWindowOwned(Modules.WND_CONTENT, Modules.STD_RESULTS_USAGE);
            var vm = vmFactory.Create(Modules.STD_RESULTS_USAGE, itemId);
            if (vm is IDataInitializer di)
                di.Initialize();

            window.DataContext = vm;

            ShowDialog(window);
        }
    }
}
