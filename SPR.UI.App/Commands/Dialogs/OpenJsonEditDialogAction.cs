﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Abstractions.UI;

namespace SPR.UI.App.Commands.Dialogs
{
    public class OpenJsonEditDialogAction : DialogActionBase, ICommandAction<object, string>
    {
        private readonly ILogger logger;

        public OpenJsonEditDialogAction(IWindowFactory wndFactory, ILogger logger) : base(wndFactory)
        {
            this.logger = logger;
        }

        public void Action(object dataContext, string propertyName)
        {
            if (!dataContext.TryGetPropertyValue<string>(propertyName, out var propertyValue))
            {
                logger.LogError($"{nameof(OpenJsonEditDialogAction)}: Give data context of type '{dataContext.GetType().Name}' does not contains property with name '{propertyName}'");
                return;
            }

            var wnd = wndFactory.CreateWindowOwned(Modules.WND_JSON_EDITOR);
            var vm = wndFactory.CreateVM(Modules.WND_JSON_EDITOR, propertyValue);
            wnd.DataContext = vm;
            if (wnd.ShowDialog() == true)
            {
                if (!vm.TryGetPropertyValue<string>("Value", out var updatedValue)) 
                {
                    logger.LogError($"Can't get property value with name 'Value' from '{vm.GetType().Name}'");
                    wndFactory.ShowErrorDialog("Error on extracting value from editing dialog");
                    return;
                }
                dataContext.SetPropertyValue(propertyName, updatedValue);
            }
        }
    }
}
