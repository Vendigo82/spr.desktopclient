﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using System;

namespace SPR.UI.App.Commands.Dialogs
{
    public class DirItemUsageDialogAction : DialogActionBase, ICommandAction<string, object>
    {
        private readonly IFactory<ICommonVM> vmFactory;

        public DirItemUsageDialogAction(IFactory<ICommonVM> vmFactory, IWindowFactory wndFactory) : base(wndFactory)
        {
            this.vmFactory = vmFactory ?? throw new ArgumentNullException(nameof(vmFactory));
        }

        public void Action(string dictionaryName, object itemId)
        {
            var window = wndFactory.CreateWindowOwned(Modules.WND_CONTENT, Modules.DIR_ITEM_USAGE);
            var vm = vmFactory.Create(Modules.DIR_ITEM_USAGE, Tuple.Create(dictionaryName, itemId.ToString()));
            if (vm is IDataInitializer di)
                di.Initialize();

            window.DataContext = vm;

            ShowDialog(window);
        }
    }
}
