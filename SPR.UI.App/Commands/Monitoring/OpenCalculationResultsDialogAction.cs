﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Commands;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using System;

namespace SPR.UI.App.Commands.Monitoring
{
    public class OpenCalculationResultsDialogAction : DialogActionBase, ICommandAction<long>
    {
        private readonly IFactory<ICommonVM> vmFactory;

        public OpenCalculationResultsDialogAction(IFactory<ICommonVM> vmFactory, IWindowFactory wndFactory) : base(wndFactory)
        {
            this.vmFactory = vmFactory ?? throw new ArgumentNullException(nameof(vmFactory));
        }

        public void Action(long transactionId)
        {
            var vm = vmFactory.Create(Modules.WND_TRANS_CALC_RESULTS, transactionId);
            var wnd = wndFactory.CreateWindowOwned(Modules.WND_CONTENT, Modules.WND_TRANS_CALC_RESULTS);

            wnd.DataContext = vm;
            if (vm is IDataInitializer di)
                di.Initialize(true);
            ShowDialog(wnd);
        }
    }
}
