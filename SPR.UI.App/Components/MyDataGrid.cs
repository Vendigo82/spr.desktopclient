﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SPR.UI.App.Components
{
    public class MyDataGrid : DataGrid
    {
        public MyDataGrid() {
            SelectedCellsChanged += MyDataGrid_SelectedCellsChanged;
        }

        public static readonly DependencyProperty SelectedCellItemProperty = DependencyProperty.Register(
            nameof(SelectedCellItem), typeof(object), typeof(MyDataGrid));

        public object SelectedCellItem { get => GetValue(SelectedCellItemProperty); set => SetValue(SelectedCellItemProperty, value); }

        private void MyDataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e) {
            DataGridCellInfo cell = SelectedCells.FirstOrDefault();
            SelectedCellItem = cell != null ? cell.Item : null;
            //SelectedValue = cell != null ? cell.Item : null;
            //            SelectedItem = cell.
        }        
    }
}
