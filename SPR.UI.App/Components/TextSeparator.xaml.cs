﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SPR.UI.App.Components
{
    /// <summary>
    /// Interaction logic for TextSeparator.xaml
    /// </summary>
    public partial class TextSeparator : UserControl
    {
        public TextSeparator() {
            InitializeComponent();
        }

        #region Text property
        public static DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text), typeof(string), typeof(TextSeparator),
            new FrameworkPropertyMetadata(new PropertyChangedCallback(TextChanged)));

        public string Text {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        private static void TextChanged(DependencyObject o, DependencyPropertyChangedEventArgs args) {
            TextSeparator obj = (TextSeparator)o;
            obj.TextLabel.Content = args.NewValue as string;
        }
        #endregion
    }
}
