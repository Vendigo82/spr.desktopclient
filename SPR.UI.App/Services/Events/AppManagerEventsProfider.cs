﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.UI.App.Services;
using SPR.UI.App.Services.Events.Args;

namespace SPR.UI.App.Services.Events
{
    using Abstractions;

    /// <summary>
    /// Read events from AppManager and raise own events
    /// </summary>
    public class AppManagerEventsProfider : INotificationEventsService
    {
        private readonly ILogger _logger;

        public event EventHandler NeedAuthentification;
        public event EventHandler TestComplete;

        //public event EventHandler<FormulaBrief> FormulaAdded;

        public AppManagerEventsProfider(ILogger logger) {
            _logger = logger;
            //AppManager.Inst.FormulaDeleted += (s, l) => RaiseEvent(s, FormulaDeleted, new GuidArgs(l.Item1), nameof(FormulaDeleted));
            //AppManager.Inst.FormulaDraftDeleted += (s, l) => RaiseEvent(s, FormulaDraftDeleted, new GuidArgs(l.Item1), nameof(FormulaDraftDeleted));
            //AppManager.Inst.ScenarioDeleted += (s, l) => RaiseEvent(s, ScenarioDeleted, new GuidArgs(l.Item1), nameof(ScenarioDeleted));
            //AppManager.Inst.ScenarioDraftDeleted += (s, l) => RaiseEvent(s, ScenarioDraftDeleted, new GuidArgs(l.Item1), nameof(ScenarioDraftDeleted));
        }

        private void RaiseEvent<T>(object sender, EventHandler<T> e, T args, string eventName) {
            if (_logger?.IsEnabled(LogLevel.Debug) == true)
                _logger.LogDebug($"Raise event {eventName} with args {args.ToString()}");

            try {
                e?.Invoke(sender, args);
            } catch (Exception exc) {
                _logger?.LogError($"Exception on raise event {eventName} with args {args.ToString()}", exc);
            }
        }

        private void RaiseEvent(object sender, EventHandler e, string eventName) {
            if (_logger?.IsEnabled(LogLevel.Debug) == true)
                _logger.LogDebug($"Raise event {eventName}");

            try {
                System.Windows.Application.Current.Dispatcher.Invoke(() => e?.Invoke(sender, null));
            } catch (Exception exc) {
                _logger?.LogError($"Exception on raise event {eventName}", exc);
            }
        }

        public void RaiseNeedAuthentification(object sender) => RaiseEvent(sender, NeedAuthentification, nameof(NeedAuthentification));

        public void RaiseTestComplete(object sender) => RaiseEvent(sender, TestComplete, nameof(TestComplete));
    }
}
