﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.UI.App.Services.Events.Args;

namespace SPR.UI.App.Services.Events
{
    using SPR.WebService.TestModule.DataContract;

    /// <summary>
    /// Provide events for SPR object
    /// </summary>
    public interface INotificationEventsService
    {
        /// <summary>
        /// Unauthorized error was received
        /// </summary>
        event EventHandler NeedAuthentification;

        /// <summary>
        /// Raise <see cref=" NeedAuthentification"/> event
        /// </summary>
        void RaiseNeedAuthentification(object sender);

        /// <summary>
        /// Some object was tested
        /// </summary>
        event EventHandler TestComplete;

        /// <summary>
        /// Raise <see cref="TestComplete"/> event
        /// </summary>
        /// <param name="sender"></param>
        void RaiseTestComplete(object sender);
    }
}
