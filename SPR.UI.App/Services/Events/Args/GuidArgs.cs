﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Events.Args
{
    /// <summary>
    /// Argument for formula deleted events
    /// </summary>
    public class GuidArgs
    {
        public GuidArgs(Guid guid) {
            Guid = guid;
        }

        public Guid Guid { get; }

        public override string ToString() => $"Id={Guid.ToString()}";
    }
}
