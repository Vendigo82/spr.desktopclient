﻿using SPR.UI.App.Abstractions;
using System;
using System.Collections.Generic;

namespace SPR.UI.App.Services.Factory
{
    public class TypeResolverFactory<T> : IFactory<T> where T : class
    {
        private readonly ITypeResolver resolver;

        public TypeResolverFactory(ITypeResolver resolver)
        {
            this.resolver = resolver ?? throw new ArgumentNullException(nameof(resolver));
        }

        public T Create(string name) => resolver.Resolve<T>(name);

        public T Create(string name, IEnumerable<KeyValuePair<Type, object>> param) => resolver.Resolve<T>(name, param);
    }
}
