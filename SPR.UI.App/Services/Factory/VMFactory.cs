﻿using SPR.UI.App.Abstractions.VM;

namespace SPR.UI.App.Services.Factory
{
    public class VMFactory : TypeResolverFactory<ICommonVM>
    {
        public VMFactory(ITypeResolver resolver) : base(resolver) 
        { 
        }
    }
}
