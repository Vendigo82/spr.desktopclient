﻿using System.Windows.Input;

namespace SPR.UI.App.Services.Factory
{
    public class CommandFactory : TypeResolverFactory<ICommand>
    {
        public CommandFactory(ITypeResolver resolver) : base(resolver)
        {
        }
    }
}
