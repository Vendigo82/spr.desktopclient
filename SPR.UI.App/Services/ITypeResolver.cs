﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    public interface ITypeResolver
    {
        T Resolve<T>();

        T Resolve<T>(string name);

        T Resolve<T>(string name, KeyValuePair<string, object> param);        

        T Resolve<T>(string name, IEnumerable<KeyValuePair<Type, object>> param);
    }

    public static class ITypeResolverExtensions
    {
        public static T Resolve<T>(this ITypeResolver tr, string name, KeyValuePair<Type, object> param)
            => tr.Resolve<T>(name, Enumerable.Repeat(param, 1));

        /// <summary>
        /// Resolve type with overriding constructor argument of specific type
        /// </summary>
        /// <typeparam name="T">Resolving value type</typeparam>
        /// <typeparam name="TArg1">constructor argument's type</typeparam>
        /// <param name="tr"></param>
        /// <param name="name">Name of the object. Can be null</param>
        /// <param name="param">constructor argument's value </param>
        /// <returns></returns>
        public static T Resolve<T, TArg1>(this ITypeResolver tr, string name, TArg1 param)
        {
            var pair = new KeyValuePair<Type, object>(typeof(TArg1), param);
            return tr.Resolve<T>(name, pair);
        }

        public static T Resolve<T, TArg1, TArg2>(this ITypeResolver tr, string name, TArg1 param1, TArg2 param2)
        {
            return tr.Resolve<T>(name, new KeyValuePair<Type, object>[] {
                new KeyValuePair<Type, object>(typeof(TArg1), param1),
                new KeyValuePair<Type, object>(typeof(TArg2), param2),
            });
        }

        public static T Resolve<T, TArg1, TArg2, TArg3>(this ITypeResolver tr, string name, TArg1 param1, TArg2 param2, TArg3 param3)
        {
            return tr.Resolve<T>(name, new KeyValuePair<Type, object>[] {
                new KeyValuePair<Type, object>(typeof(TArg1), param1),
                new KeyValuePair<Type, object>(typeof(TArg2), param2),
                new KeyValuePair<Type, object>(typeof(TArg3), param3),
            });
        }
    }
}
