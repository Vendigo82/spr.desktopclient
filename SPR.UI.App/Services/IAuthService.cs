﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    /// <summary>
    /// Provide login authentification service
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// Perform login
        /// </summary>
        /// <param name="user">user name</param>
        /// <param name="psw">password</param>
        /// <returns>true if login was success, false if user or password was incorrect</returns>
        /// <exception cref="Exception">on other login errors</exception>
        Task<bool> LoginAsync(string user, string psw, CancellationToken ct);
    }
}
