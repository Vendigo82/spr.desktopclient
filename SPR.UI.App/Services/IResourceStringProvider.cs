﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    /// <summary>
    /// Provide string from resources
    /// </summary>
    public interface IResourceStringProvider
    {
        /// <summary>
        /// Returns string value from resources or null if not found
        /// </summary>
        /// <param name="name">name of the string</param>
        /// <returns>value or null</returns>
        string GetString(string name);
    }
}
