﻿using Newtonsoft.Json;
using SPR.UI.App.Abstractions.DataSources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SPR.UI.App.Services.Options
{
    public class InstancesJsonSource : IInstancesSource
    {
        private readonly string fileName;

        public InstancesJsonSource(string fileName)
        {
            this.fileName = fileName ?? throw new ArgumentNullException(nameof(fileName));
        }

        public IEnumerable<InstanceSettingsModel> GetInstances()
        {
            if (!File.Exists(fileName))
                return Enumerable.Empty<InstanceSettingsModel>();

            var fileContent = File.ReadAllText(fileName);
            var json = JsonConvert.DeserializeObject<FileModel>(fileContent);
            return json.Instances.Select(i => new InstanceSettingsModel { 
                Name = i.Key, 
                BaseUrl = i.Value.BaseUrl,
                Timeout = string.IsNullOrEmpty(i.Value.Timeout) ? (TimeSpan?)null : TimeSpan.Parse(i.Value.Timeout)
            }).ToArray();
        }

        private class FileModel
        {
            [JsonProperty(Required = Required.Always)]
            public Dictionary<string, InstanceModel> Instances { get; set; }
        }

        private class InstanceModel
        {
            [JsonProperty(Required = Required.Always)]
            public string BaseUrl { get; set; }

            public string Timeout { get; set; }
        }
    }
}
