﻿using SPR.UI.App.Abstractions.DataSources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SPR.UI.App.Services.Options
{
    public class InstanceCombineSource : IInstancesSource
    {
        private readonly IInstancesSource primary;
        private readonly IInstancesSource secondary;

        public InstanceCombineSource(IInstancesSource primary, IInstancesSource secondary)
        {
            this.primary = primary ?? throw new ArgumentNullException(nameof(primary));
            this.secondary = secondary ?? throw new ArgumentNullException(nameof(secondary));
        }

        public IEnumerable<InstanceSettingsModel> GetInstances()
        {
            var result = primary.GetInstances();
            if (result.Any())
                return result;

            return secondary.GetInstances();
        }
    }
}
