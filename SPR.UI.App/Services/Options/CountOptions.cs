﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Options
{
    public class CountOptions
    {
        public int Count { get; set; }
    }

    public class CountOptions<T> : CountOptions
    {        
    }
}
