﻿using SPR.UI.App.Abstractions.DataSources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SPR.UI.App.Services.Options
{
    public class InstanceConfigSource : IInstancesSource
    {
        public IEnumerable<InstanceSettingsModel> GetInstances() => Enumerable.Repeat(CreateSettingsModel(), 1);

        public const string PREFIX = "ui_service_";
        public const string URL = PREFIX + "url";
        public const string TIMEOUT = PREFIX + "timeout";

        private InstanceSettingsModel CreateSettingsModel() => new InstanceSettingsModel()
        {
            BaseUrl = ConfigBaseUrl,
            Name = "Config",
            Timeout = GetConfigTimeout()
        };

        private string ConfigBaseUrl => ConfigurationManager.AppSettings[URL];

        private TimeSpan? GetConfigTimeout()
        {
            try
            {
                string value = ConfigurationManager.AppSettings[TIMEOUT];
                if (string.IsNullOrWhiteSpace(value))
                    return null;
                return value.ToTimeSpan();
            }
            catch (Exception e)
            {
                throw new ConfigurationErrorsException($"Configuration value {TIMEOUT} is invalid: {e.Message}", e);
            }
        }
    }
}
