﻿using SPR.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    public interface IConfirmActionCommentProvider
    {
        bool GetComment(
            JournalAction action, 
            SerializedObjectType objectType, 
            long? id, 
            int? prevVersion, 
            out string comment);
    }
}
