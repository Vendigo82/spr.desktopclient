﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Cache
{
    /// <summary>
    /// Cached data list for dictionary service
    /// </summary>
    /// <typeparam name="TKey">type of dictionary element key</typeparam>
    /// <typeparam name="T">Type of dictionary element</typeparam>
    public class CachedDictionaryDataList<TKey, T> : IDataListSource<T>, IDictionaryService<TKey, T> where T : class
    {
        private readonly IDataListSource<T> _service;
        private IEnumerable<T> _items = null;
        private Dictionary<TKey, T> _dict = null;
        private readonly Func<T, TKey> _keyFunc = null;
        private readonly ILogger logger;

        public CachedDictionaryDataList(IDataListSource<T> service, Func<T, TKey> keyFunc = null, ILogger logger = null) {
            _service = service;
            _keyFunc = keyFunc;
            this.logger = logger;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<T>> GetItemsAsync(CancellationToken ct = default) 
        {
            if (_items == null) {
                Log("missing cache, loading items");
                return await LoadItemsAsync(ct);
            } else {
                Log("returns items from cache");
                return _items;
            }
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<T>> LoadItemsAsync(CancellationToken ct = default) 
        {
            Log("loading items");
            var items = await _service.LoadItemsAsync(ct);
            return SetCacheItem(items);
        }

        /// <inheritdoc/>
        public async Task<T> GetItemAsync(TKey key, CancellationToken ct = default) {
            if (_keyFunc == null)
                throw new NotImplementedException("Not support for this type");

            if (_dict == null) {
                Log("missing cache, loading items");
                await LoadItemsAsync(ct);
            }
            if (_dict.TryGetValue(key, out T result))
                return result;
            else
                return default;
        }

        protected IEnumerable<T> SetCacheItem(IEnumerable<T> items = default)
        {
            Log("save items to cache");
            _items = items;
            if (_keyFunc != null)
                _dict = items.ToDictionary(i => _keyFunc(i));
            return _items;
        }

        protected void Log(string message, [CallerMemberName] string method = "")
            => logger?.LogDebug($"{GetType().Name}[{typeof(T).Name}].{method}: {message}");
    }
}
