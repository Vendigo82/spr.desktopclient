﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Cache
{
    public class CachedDictionaryCRUDService<TKey, T> : CachedDictionaryDataList<TKey, T>, IDictionaryCRUDService<T>
        where T : class
    {
        private readonly IDictionaryCRUDService<T> service;

        public CachedDictionaryCRUDService(IDictionaryCRUDService<T> service, Func<T, TKey> keyFunc = null, ILogger logger = null) 
            : base(service, keyFunc, logger)
        {
            this.service = service;
        }

        public string DictionaryName => service.DictionaryName;

        public Task<ItemInfoModel<T>> LoadItemInfoAsync(int id, CancellationToken ct = default)
            => service.LoadItemInfoAsync(id, ct);

        public async Task<IEnumerable<T>> UpdateAsync(IEnumerable<BunchUpdateItemModel<T>> items, CancellationToken ct = default)
        {
            Log("update items");
            var result = await service.UpdateAsync(items, ct);
            return SetCacheItem(result);
        }
    }
}
