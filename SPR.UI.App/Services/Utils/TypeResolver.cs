﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Resolution;

namespace SPR.UI.App.Services.Utils
{
    public class TypeResolver : ITypeResolver
    {
        private IUnityContainer _container;

        public TypeResolver(IUnityContainer container) {
            _container = container;
        }

        public T Resolve<T>() => _container.Resolve<T>();

        public T Resolve<T>(string name) => _container.Resolve<T>(name);

        public T Resolve<T>(string name, KeyValuePair<string, object> param)
            => _container.Resolve<T>(name, new ResolverOverride[] { new ParameterOverride(param.Key, param.Value) });

        public T Resolve<T>(string name, IEnumerable<KeyValuePair<Type, object>> param)
            => _container.Resolve<T>(name, param.Select(i => new ParameterOverride(i.Key, i.Value)).ToArray());
    }
}
