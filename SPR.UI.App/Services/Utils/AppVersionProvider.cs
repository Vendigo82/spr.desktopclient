﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Utils
{
    public class AppVersionProvider : IAppVersionProvider
    {
        public AppVersionProvider() {
            Version ver = Assembly.GetExecutingAssembly().GetName().Version;
            AppVersion = $"{ver.Major}.{ver.Minor}.{ver.Build}.{ver.Revision}";
        }

        public string AppVersion { get; }
    }
}
