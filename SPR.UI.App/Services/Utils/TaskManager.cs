﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Utils
{
    /// <summary>
    /// Provide Task running and apply results pattern. If several tasks was running, then only last running tasks result will be applied
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class TaskManager<TResult>
    {
        private volatile int lastId = 0;
        private volatile int loadingCount = 0;

        public object Lock { get; } = new object();

        public bool IsLoading => loadingCount > 0;

        public event EventHandler LoadingCountChanged;

        /// <summary>
        /// Run task and apply tasks results. Apply task results running in lock block
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        public async Task RunTask(Func<Task<TResult>> operation, Action<TResult> applyAction)
        {
            int currentId = Interlocked.Increment(ref lastId);

            try {
                Interlocked.Increment(ref loadingCount);
                LoadingCountChanged?.Invoke(this, EventArgs.Empty);

                var result = await operation();
                if (currentId == lastId) {
                    lock (Lock) {
                        if (currentId == lastId)
                            applyAction(result);
                    }
                }
            } catch (OperationCanceledException) {
                return;
            } finally {
                Interlocked.Decrement(ref loadingCount);
                LoadingCountChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public void Cancel()
        {
            Interlocked.Increment(ref lastId);
        }
    }
}
