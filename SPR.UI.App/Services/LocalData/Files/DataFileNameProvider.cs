﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SPR.UI.App.Services.LocalData.Files
{
    public class DataFileNameProvider : IDataFileNamesProvider
    {
        public string Path => string.Concat(AppDomain.CurrentDomain.BaseDirectory, "\\local\\");

        public string LogginnedHistory => "users";
    }
}
