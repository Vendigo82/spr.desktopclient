﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace SPR.UI.App.Services.LocalData.Files
{
    using Abstractions;

    /// <summary>
    /// Save logginned history in file
    /// </summary>
    public class LoginnedHistory : ILogginnedHistory
    {
        private readonly IDataFileNamesProvider _fn;
        private readonly ILogger _logger;

        public LoginnedHistory(IDataFileNamesProvider fn, ILogger logger = null) {
            _fn = fn;
            _logger = logger;
        }

        public IEnumerable<string> LogginnedUsers => GetContent(out _);

        public void AddLogin(string login) {
            try {
                Directory.CreateDirectory(_fn.Path);
                List<string> users = GetContent(out string fn);

                if (users.Contains(login))
                    users.Remove(login);

                string json = JsonConvert.SerializeObject(Enumerable.Repeat(login, 1).Concat(users.Take(9)));
                File.WriteAllText(fn, json);
            } catch (Exception e) {
                _logger?.LogError("Serialize saved logginned users error", e);
            }
        }

        private List<string> GetContent(out string fn) {
            fn = string.Concat(_fn.Path, _fn.LogginnedHistory);
            if (File.Exists(fn)) {
                try {
                    return JsonConvert.DeserializeObject<List<string>>(File.ReadAllText(fn));
                } catch (Exception e) {
                    _logger?.LogError("Deserialize saved logginned users error", e);
                    return new List<string>();
                }
            } else
                return new List<string>();
        }
    }
}
