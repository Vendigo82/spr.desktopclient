﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.LocalData.Files
{
    public interface IDataFileNamesProvider
    {
        string Path { get; }

        string LogginnedHistory { get; }
    }
}
