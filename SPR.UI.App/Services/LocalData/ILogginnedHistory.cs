﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.LocalData
{
    /// <summary>
    /// Provide access to logginned users history
    /// </summary>
    public interface ILogginnedHistory
    {
        /// <summary>
        /// List of early logginned users
        /// </summary>
        IEnumerable<string> LogginnedUsers { get; }

        /// <summary>
        /// Add new login to history
        /// </summary>
        /// <param name="login"></param>
        void AddLogin(string login);
    }
}
