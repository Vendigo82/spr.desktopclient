﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    /// <summary>
    /// Provide access to logo
    /// </summary>
    public interface ILogoProvider
    {
        /// <summary>
        /// Path to logo image
        /// </summary>
        string PathToLogo { get; }
    }
}
