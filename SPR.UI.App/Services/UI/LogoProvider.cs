﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.UI
{
    public class LogoProvider : ILogoProvider
    {
        public string PathToLogo => "/Resources/logo_small.png";
    }
}
