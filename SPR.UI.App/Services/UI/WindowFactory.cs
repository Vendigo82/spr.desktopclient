﻿using SPR.DAL.Enums;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.VM.Base;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SPR.UI.App.Services.UI
{
    /// <summary>
    /// Provide window factory by using ITypeResolver
    /// </summary>
    public class WindowFactory : IWindowFactory
    {
        public const string INFO_TITLE = "Common.InformationMsgBox.Title";
        public const string CONFIRM_TITLE = "Common.ConfirmMsgBox.Title";
        public const string ERROR_TITLE = "Common.ErrorMsgBox.Title";

        private readonly ITypeResolver _resolver;
        private readonly IResourceStringProvider _resource;

        public WindowFactory(ITypeResolver resolver, IResourceStringProvider resource = null) {
            _resolver = resolver;
            _resource = resource;
        }

        public Window MainWindow => Application.Current.MainWindow;

        public Window CreateWindow(string name) => _resolver.Resolve<Window>(name);

        public Window CreateWindow(string name, string contentName) {
            Control ctrl = CreateView(contentName);
            Window wnd = _resolver.Resolve<Window>(name, new KeyValuePair<Type, object>(typeof(UIElement), ctrl));
            wnd.DataContext = ctrl.DataContext;
            return wnd;
        }

        public Control CreateView(string name)
        {
            Control view = _resolver.Resolve<Control>(name);
            return view;
        }

        public ICommonVM CreateVM(string name) => _resolver.Resolve<ICommonVM>(name);

        public ICommonVM CreateVM<T>(string name, T constructorParam) => _resolver.Resolve<ICommonVM, T>(name, constructorParam);

        public Control CreateFormulaView(FormulaType type, Guid? id) 
        {
            var vm = _resolver.Resolve<IDataObjectVM<FormulaModel>, Guid?>(type.ToString(), id);
            Control view = _resolver.Resolve<Control>(Modules.TAB_FORMULA + "_" + type.ToString());
            var tabsVM = _resolver.Resolve<DataObjectTabsVM<FormulaModel>, IDataObjectVM<FormulaModel>, FormulaType>(null, vm, type);
            return _resolver.Resolve<Control, DataObjectTabsVM<FormulaModel>, Control>(Modules.TAB_FORMULA, tabsVM, view);
        }

        public Control CreateScenarioView(Guid? id)
        {            
            var vm = _resolver.Resolve<IDataObjectVM<ScenarioModel>, Guid?>(null, id);
            var tabsVM = _resolver.Resolve<DataObjectTabsVM<ScenarioModel>, IDataObjectVM<ScenarioModel>>(null, vm);
            return _resolver.Resolve<Control, DataObjectTabsVM<ScenarioModel>>(Modules.TAB_SCENARIO, tabsVM);
        }

        public Control CreateStdResultsView(Guid? id)
        {
            var vm = _resolver.Resolve<ICommonVM>(Modules.TAB_STD_RESULTS, new KeyValuePair<Type, object>(typeof(Guid?), id));
            var control = _resolver.Resolve<Control>(Modules.TAB_STD_RESULTS);
            control.DataContext = vm;
            return control;
        }

        public Control CreateTabContainer(UIElement content)
            => _resolver.Resolve<Control>(
                Modules.TAB_CONTAINER,
                new KeyValuePair<string, object>(Modules.TAB_CONTAINER_PARAM_NAME, content));

        public void ShowInformationDialog(string text, string caption = null) 
        {
            MessageBox.Show(
                text,
                caption ?? _resource.GetString(INFO_TITLE),
                MessageBoxButton.OK,
                MessageBoxImage.Information);
        }

        public void ShowErrorDialog(string error) 
        { 
            MessageBox.Show(
                error, 
                _resource.GetString(ERROR_TITLE), 
                MessageBoxButton.OK, 
                MessageBoxImage.Error);            
        }

        public bool ShowConfirmDialog(string text, string caption = null)
        {
            var result = MessageBox.Show(
                text, 
                caption ?? _resource.GetString(CONFIRM_TITLE), 
                MessageBoxButton.YesNo, 
                MessageBoxImage.Question);
            return result == MessageBoxResult.Yes;
        }
    }
}
