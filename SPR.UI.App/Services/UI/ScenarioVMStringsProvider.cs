﻿using SPR.UI.App.VM.Scenarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.UI
{
    public class ScenarioVMStringsProvider : ScenarioVM.IStringsProvider
    {
        private readonly IResourceStringProvider _strings;

        public ScenarioVMStringsProvider(IResourceStringProvider strings)
        {
            _strings = strings ?? throw new ArgumentNullException(nameof(strings));
        }

        public string ActivateConfirmation => _strings.GetString("ScenarioView.Action.Activation.Confirm");

        public string DeactivateConfirmation => _strings.GetString("ScenarioView.Action.Deactivation.Confirm");
    }
}
