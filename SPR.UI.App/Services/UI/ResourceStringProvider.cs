﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SPR.UI.App.Services.UI
{
    using Abstractions;

    public class ResourceStringProvider : IResourceStringProvider
    {
        private readonly ILogger logger;

        public ResourceStringProvider(ILogger logger = null) {
            this.logger = logger;
        }

        public string GetString(string name) {
            string result = (string)Application.Current.TryFindResource(name);
            if (result == null)
                logger?.LogWarn($"Resource with name <{name}> has not found");
            return result;
        }
    }
}
