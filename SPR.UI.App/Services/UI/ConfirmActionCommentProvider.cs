﻿using SPR.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.UI.App.View.Journal;
using SPR.UI.App.VM.Journal;

namespace SPR.UI.App.Services.UI
{
    public class ConfirmActionCommentProvider : IConfirmActionCommentProvider
    {
        public bool GetComment(
            JournalAction action,
            SerializedObjectType objectType,
            long? id,
            int? prevVersion,
            out string comment) 
        {
            ConfirmActionVM vm = new ConfirmActionVM(action, objectType, id, prevVersion);
            ConfirmActionWnd wnd = new ConfirmActionWnd() {
                DataContext = vm
            };
            if (wnd.ShowDialog() == true) {
                comment = vm.Comment;
                return true;
            } else {
                comment = null;
                return false;
            }
        }
    }
}
