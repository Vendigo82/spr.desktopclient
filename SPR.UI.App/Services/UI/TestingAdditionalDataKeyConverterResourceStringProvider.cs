﻿using SPR.UI.App.Converters.Testing;
using System;

namespace SPR.UI.App.Services.UI
{
    /// <summary>
    /// Provide string to obtain captions for additional data kets from testing result's window 
    /// </summary>
    public class TestingAdditionalDataKeyConverterResourceStringProvider : AdditionalDataKeyConverter.IStringProvider
    {
        private readonly IResourceStringProvider stringProvider;

        public TestingAdditionalDataKeyConverterResourceStringProvider(IResourceStringProvider stringProvider)
        {
            this.stringProvider = stringProvider ?? throw new ArgumentNullException(nameof(stringProvider));
        }

        public string Text(string additionalKey) 
            => stringProvider.GetString("TestResultsView.AdditionalData.Keys." + additionalKey) ?? additionalKey;
    }
}
