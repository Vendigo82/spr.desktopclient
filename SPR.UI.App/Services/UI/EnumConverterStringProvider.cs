﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.UI.App.Converters;

namespace SPR.UI.App.Services.UI
{
    /// <summary>
    /// Get string strategy for <see cref="EnumCustomConverter"/>
    /// Looking for resource with key Enum.<EnumName>.<EnumValue>.<Param> or Enum.<EnumName>.<EnumValue> if param is null
    /// If resource string has not found, then returns enum value as string
    /// </summary>
    public class EnumConverterStringProvider : EnumCustomConverter.IStringProvider
    {
        private readonly IResourceStringProvider _stringProvider;

        public EnumConverterStringProvider(IResourceStringProvider stringProvider) {
            _stringProvider = stringProvider ?? new ResourceStringProvider();
        }

        public string Text(string typeNamespace, string typeName, string value, string param) {
            string name = $"Enum.{typeName}.{value}{ (param != null ? string.Concat(".", param) : string.Empty)}";
            return _stringProvider.GetString(name) ?? name;
        }
    }
}
