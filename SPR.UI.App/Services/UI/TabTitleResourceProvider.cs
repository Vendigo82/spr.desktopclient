﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;

namespace SPR.UI.App.Services.UI
{
    /// <summary>
    /// Provide title from resource with key: <name>Tab.Title, for example ProfileTab.Title
    /// </summary>
    public class TabTitleResourceProvider : ITabTitleProvider
    {
        private readonly IResourceStringProvider _stringProvider;

        private const string TitleSuffix = "Tab.Title";

        public TabTitleResourceProvider(IResourceStringProvider stringProvider) {
            _stringProvider = stringProvider;
        }

        public string Title(string name) 
            => _stringProvider.GetString(string.Concat(name.Substring(0, 1).ToUpper(), name.Substring(1), TitleSuffix)) ?? name;
    }
}
