﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.TestModule
{
    using SPR.WebService.TestModule.Client;
    using SPR.WebService.TestModule.DataContract;
    using SPR.WebService.TestModule.DataContract.Rest;

    public class AppTestModuleClient : ITestModuleClient
    {
        private readonly Lazy<TestModuleClient> _client;
        private readonly ISettings _settings;

        public interface ISettings
        {
            string Url { get; }
        }

        public AppTestModuleClient(ISettings settings)
        {
            _settings = settings;
            _client = new Lazy<TestModuleClient>(() =>
            {
                if (string.IsNullOrEmpty(_settings.Url))
                    throw new InvalidOperationException("Can't create test client, because test service url is not provided");
                return new TestModuleClient(_settings.Url);
            }, true);
        }

        public async Task<TestFormulaResponse> TestFormula(TestMainFormulaRequest request, CancellationToken ct)
            => await _client.Value.TestFormula(request, ct);

        public async Task<TestFormulaResponse> TestFormula(TestDraftFormulaRequest request, CancellationToken ct) 
            => await _client.Value.TestFormula(request, ct);

        public Task<TestResponse<RestFormulaRequestInfoDto>> InitialTestRestFormulaAsync(TestDraftFormulaRequest request, CancellationToken ct = default)
            => _client.Value.InitialTestRestFormulaAsync(request, ct);

        public Task<TestResponse<FormulaResultDto>> FinalTestRestFormulaAsync(FinalRestFormulaRequest request, CancellationToken ct = default)
            => _client.Value.FinalTestRestFormulaAsync(request, ct);

        public Task<TestResponse<ScenarioResultDto>> TestScenario(TestMainScenarioRequest request, CancellationToken ct)
            => _client.Value.TestScenario(request, ct);

        public Task<TestResponse<ScenarioResultDto>> TestScenario(TestDraftScenarioRequest request, CancellationToken ct)
            => _client.Value.TestScenario(request, ct);

        public Task<TestExpressionResponse> TestExpressionAsync(TestExpressionRequest request, CancellationToken ct = default)
            => _client.Value.TestExpressionAsync(request, ct);

        public void Dispose()
        {
            if (_client.IsValueCreated)
                _client.Value.Dispose();
        }
    }
}
