﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.TestModule
{
    using Abstractions.Tests;
    using DataContract;
    using SPR.WebService.TestModule.DataContract;
    using Services.Events;

    public class TestResultAcceptorRaiseEventWrapper : ITestResultsAcceptor
    {
        private readonly ITestResultsAcceptor origin;
        private readonly INotificationEventsService events;

        public TestResultAcceptorRaiseEventWrapper(ITestResultsAcceptor origin, INotificationEventsService events) {
            this.origin = origin ?? throw new ArgumentNullException(nameof(origin));
            this.events = events ?? throw new ArgumentNullException(nameof(events));
        }

        public void PushResult(long formulaId, ShownObjectEnum shownObject, TestFormulaResponse response, 
            IEnumerable<KeyValuePair<string, object>> additionalData = null) 
        {
            events.RaiseTestComplete(this);
            origin.PushResult(formulaId, shownObject, response, additionalData);
        }

        public void PushResult(long scenarioCode, ShownObjectEnum shownObject, TestResponse<ScenarioResultDto> response)
        {
            events.RaiseTestComplete(this);
            origin.PushResult(scenarioCode, shownObject, response);
        }

        public void StartTesting(ObjectType objectType, long id, ShownObjectEnum shownObject)
            => origin.StartTesting(objectType, id, shownObject);

        public void Cancel(ObjectType objectType, long id, ShownObjectEnum shownObject)
            => origin.Cancel(objectType, id, shownObject);
    }
}
