﻿using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.Tests;
using System;
using System.IO;

namespace SPR.UI.App.Services.TestModule
{
    /// <summary>
    /// Load test input from static file
    /// </summary>
    public class StaticFileInputDataProvider : ITestInputDataProvider
    {
        private readonly ILogger _logger;

        private static readonly string EmptyData = "{" + Environment.NewLine + "}";

        public StaticFileInputDataProvider(ILogger logger) {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public string GetInputData() {
            try {
                if (File.Exists("TestInput.json")) {
                    return File.ReadAllText("TestInput.json");
                }
            } catch (Exception e) {
                _logger.LogWarn("Error on loading 'TestInput.json' file", e);
            }
            return EmptyData;
        }
    }
}
