﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.TestModule
{  
    public class AppTestModuleClientSettings : AppTestModuleClient.ISettings
    {
        private readonly ISettings _settings;

        public AppTestModuleClientSettings(ISettings settings) {
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        public string Url => _settings.TestModuleUrl;
    }
}
