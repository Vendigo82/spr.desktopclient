﻿using System;

namespace SPR.UI.App.Services.Auth
{
    public class RequireAuthorizationException : Exception
    {
        public RequireAuthorizationException() : base("Require authorization")
        {
        }
    }
}
