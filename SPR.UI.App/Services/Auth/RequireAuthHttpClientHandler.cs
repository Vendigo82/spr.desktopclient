﻿using SPR.UI.App.Services.Events;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Auth
{
    public class RequireAuthHttpClientHandler : DelegatingHandler
    {
        private readonly INotificationEventsService events;

        public RequireAuthHttpClientHandler(INotificationEventsService events, HttpMessageHandler innnerHandler) : base(innnerHandler)
        {
            this.events = events ?? throw new ArgumentNullException(nameof(events));
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var _ = Task.Run(() => events.RaiseNeedAuthentification(this));
                throw new RequireAuthorizationException();
            }
            return response;
        }
    }
}
