﻿using SPR.UI.App.Abstractions.WebService;
using System;
using System.Net.Http;

namespace SPR.UI.App.Services.WebService
{
    public class AccessTokenManager : IAccessTokenManager
    {
        private readonly ITypeResolver _typeResolver;

        public AccessTokenManager(ITypeResolver typeResolver)
        {
            _typeResolver = typeResolver ?? throw new ArgumentNullException(nameof(typeResolver));
        }

        public void SetToken(string token)
        {
            //_typeResolver.Resolve<IUIWebClientHandler>().Client.Token = token;
            var httpClient = _typeResolver.Resolve<HttpClient>(Modules.ApiHttpClientName);
            httpClient.DefaultRequestHeaders.Remove("Authorization");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
        }
    }
}
