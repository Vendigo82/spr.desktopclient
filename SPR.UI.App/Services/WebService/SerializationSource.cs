﻿using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataContract;
using SPR.UI.WebClient;
using SPR.UI.WebService.DataContract.Serialization;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    public class SerializationSource : ISerialization
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public SerializationSource(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper;
        }

        public async Task<string> SerializeFormulaAsync(Guid formulaGuid, CancellationToken ct = default)
        {
            var response = await client.SerializationSerializePostAsync(new ApiClient.SerializeRequestModel { Formulas = new List<Guid> { formulaGuid } }, ct);
            return response.Item;            
        }

        public async Task<string> SerializeScenarioAsync(Guid scenarioGuid, CancellationToken ct = default)
        {
            var response = await client.SerializationSerializePostAsync(new ApiClient.SerializeRequestModel { Scenarios = new List<Guid> { scenarioGuid } }, ct);
            return response.Item;
        }

        public async Task<ExistanceResponseModel> CheckExistanceAsync(string data, CancellationToken ct = default)
        { 
            var response = await client.SerializationExistancePostAsync(new ApiClient.StringObjectContainer { Item = data }, ct);
            var result = mapper.Map<ExistanceResponseModel>(response);
            return result;
        }

        public async Task<ImportResultModel> ImportAsync(string data, ImportSettingsModel settings, string comment, CancellationToken ct = default)
        {
            var request = new ApiClient.ImportRequestModel { 
                Data = data,
                Settings = mapper.Map<ApiClient.ImportSettingsModel>(settings),
                Comment = comment,
            };
            var response = await client.SerializationImportPostAsync(request, ct);
            var result = mapper.Map<ImportResultModel>(response);
            return result;
        }

        public async Task<ImportObjectResultModel> ShallowCopyAsync(ObjectType type, Guid guid, CancellationToken ct = default)
        {
            var response = await ShallowCopyRequestAsync(type, guid, ct);
            var result = mapper.Map<ImportObjectResultModel>(response);
            return result;
        }

        private Task<ApiClient.ImportObjectResultModel> ShallowCopyRequestAsync(ObjectType type, Guid guid, CancellationToken ct = default)
        {
            switch (type)
            {
                case ObjectType.Formula:
                    return client.SerializationCopyFormulaIdPostAsync(guid, ct);
                case ObjectType.Scenario:
                    return client.SerializationCopyScenarioIdPostAsync(guid, ct);
                default:
                    throw new InvalidOperationException($"Unknown object type {type}");
            }
        }

        public async Task<ImportResultModel> DeepCopyAsync(ObjectType type, Guid guid, CancellationToken ct = default)
        {
            var response = await DeepCopyRequestAsync(type, guid, ct);
            var result = mapper.Map<ImportResultModel>(response);
            return result;
        }

        private Task<ApiClient.ImportResultModel> DeepCopyRequestAsync(ObjectType type, Guid guid, CancellationToken ct = default)
        {
            switch (type)
            {
                case ObjectType.Formula:
                    return client.SerializationDeepcopyFormulaIdPostAsync(guid, ct);
                case ObjectType.Scenario:
                    return client.SerializationDeepcopyScenarioIdPostAsync(guid, ct);
                default:
                    throw new InvalidOperationException($"Unknown object type {type}");
            }
        }
    }
}
