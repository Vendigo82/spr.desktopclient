﻿using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Common;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    public class ScenarioSource : IScenarioSource
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public ScenarioSource(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client;
            this.mapper = mapper;
        }

        public async Task<ScenarioModel> GetArchiveVersionAsync(Guid id, int ver, CancellationToken ct)
        {
            try
            {
                var response = await client.ScenarioIdArchiveVersionGetAsync(id, ver, ct);
                var result = mapper.Map<ScenarioModel>(response.Item);
                return result;
            }
            catch (ApiClient.ApiException e) when (e.StatusCode == 404)
            {
                return null;
            }
        }

        public async Task<ItemsList<VersionInfo>> GetArchiveVersionsListAsync(Guid id, CancellationToken ct)
        { 
            var response = await client.ScenarioIdArchiveVersionsGetAsync(id, ct);
            var result = mapper.Map<IEnumerable<VersionInfo>>(response.Items);
            return new ItemsList<VersionInfo> { Items = result };
        }

        public async Task<ObjectContainer<ScenarioModel>> GetMain(Guid id, CancellationToken ct)
        {
            try
            {
                var response = await client.ScenarioIdGetAsync(id, ct);
                var result = mapper.Map<ScenarioModel>(response.Item);
                return new ObjectContainer<ScenarioModel> { Item = result };
            }
            catch (ApiClient.ApiException e) when (e.StatusCode == 404)
            {
                return new ObjectContainer<ScenarioModel>() { Item = null };
            }
        }

        public async Task<ScenarioModel> RestoreAsync(Guid id, int version, string comment, CancellationToken ct)
        {
            var response = await client.ScenarioIdArchiveVersionRestorePostAsync(id, version, new ApiClient.RestoreObjectRequest { Comment = comment }, ct);
            var result = mapper.Map<ScenarioModel>(response.Item);
            return result;
        }

        public async Task<ObjectContainer<ScenarioModel>> Save(ScenarioModel item, string comment, CancellationToken ct)
        {
            var requestModel = mapper.Map<ApiClient.ScenarioModel>(item);
            var response = await client.ScenarioSavePostAsync(new ApiClient.ScenarioModelSaveObjectRequest {
                Item = requestModel,
                Comment = comment
            });
            var result = mapper.Map<ScenarioModel>(response.Item);
            return new ObjectContainer<ScenarioModel> { Item = result };
        }

        public async Task ActivateAsync(Guid id, bool enable)
        {
            await client.ScenarioIdActivationPostAsync(id, new ApiClient.ScenarioActivationRequest { Enable = enable });            
        }
    }
}
