﻿using AutoMapper;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataModel;
using SPR.UI.App.Services.Options;
using SPR.UI.WebClient;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Configuration;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    public class MonitoringSourceSwagger : IMonitoringSource
    {
        private readonly ApiClient.IApiClient _client;
        private readonly IMapper _mapper;
        private readonly CountOptions<MonitoringSourceSwagger> _options;
        private readonly ILogger _logger;

        public MonitoringSourceSwagger(ApiClient.IApiClient client, IMapper mapper, CountOptions<MonitoringSourceSwagger> options, ILogger logger)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _logger = logger;
        }

        /// <summary>
        /// Load information about additional transaction columns
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TransactionColumnModel>> LoadAdditionalColumnsInfoAsync(CancellationToken ct = default)
        {
            _logger?.LogDebug($"Loading transaction additional columns info");
            var result = await _client.ConfigurationTransactionAdditionalColumnsGetAsync(ct);
            //var result = await client.ConfigTransactionAdditionalColumnsAsync(ct);
            return _mapper.Map<IEnumerable<TransactionColumnModel>>(result.Items);
        }


        public async Task<PartialListContainer<TransactionModel>> LoadAsync(TransactionsFilter filter, long? currentId, bool fresh, CancellationToken ct = default)
        {
            _logger?.LogDebug($"Loading transaction records fromId: {currentId}, fresh: {fresh}, filter: {FilterToString(filter)}");

            filter = filter ?? new TransactionsFilter();
            // use server's time, because records writting on server's time, not on UTC
            //if (filter.DateFrom != null)
            //    filter.DateFrom = filter.DateFrom.Value.ToUniversalTime();
            //if (filter.DateTo != null)
            //    filter.DateTo = filter.DateTo.Value.ToUniversalTime();

            //logger?.LogDebug($"Loading journal records after adjust time fromId: {currentId}, fresh: {fresh}, filter: {FilterToString(filter)}");

            var filterValues = new List<KeyValuePair<string, object>>();
            if (filter.Values != null)
                filterValues.AddRange(filter.Values.Select(i => new KeyValuePair<string, object>(TransformFilterFieldName(i.Key), i.Value)));

            if (filter.WithError != null)
                filterValues.Add(new KeyValuePair<string, object>("with_errors", filter.WithError.Value));
            if (filter.Step != null)
                filterValues.Add(new KeyValuePair<string, object>(nameof(ApiClient.TransactionItemDto.Step), filter.Step));
            if (filter.IpAddress != null)
                filterValues.Add(new KeyValuePair<string, object>(nameof(ApiClient.TransactionItemDto.IpAddress), filter.IpAddress));
            if (filter.ScenarioCode != null)
                filterValues.Add(new KeyValuePair<string, object>(nameof(ApiClient.TransactionItemDto.ScenarioID), filter.ScenarioCode));

            var response = await _client.MonitoringTransactionsGetAsync(
                count: _options.Count,
                next_id: currentId,
                fresh: fresh,
                from: filter.DateFrom?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                to: filter.DateTo?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                filterValues,
                //with_errors: filter.WithError,
                //step: filter.Step,
                //ip_address: filter.IpAddress,
                //scenario: filter.ScenarioCode,
                ct);
                //filter, new Pagination { Count = options.Count, StartFromId = currentId }, fresh, ct);

            //foreach (var item in response.Items) {
            //    item.DateTime = item.DateTime.ToLocalTime();
            //}

            _logger?.LogDebug($"Receive items. Exhaused: {response.Exhausted}");

            var list = response.IsAscending ?? false ? response.Items.Reverse() : response.Items;
            return new PartialListContainer<TransactionModel> {
                Items = _mapper.Map<IEnumerable<TransactionModel>>(list),
                Exhausted = response.Exhausted
            };
        }

        private string TransformFilterFieldName(string name)
        {
            switch (name) {
                case nameof(TransactionModel.StepOut):
                    return "step_out";
                default:
                    return name;
            }
        }

        /// <summary>
        /// Reload finished transactions
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TransactionModel>> ReloadAsync(IEnumerable<long> ids, CancellationToken ct = default)
        {
            var response = await _client.MonitoringTransactionsPostAsync(new ApiClient.Int64ItemsList { Items = ids.ToList() }, ct);
            return _mapper.Map<IEnumerable<TransactionModel>>(response.Items);
        }


        /// <summary>
        /// Load transactions data
        /// </summary>
        /// <param name="transactionid"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<TransactionDataResponseModel> LoadDataAsync(long transactionId, CancellationToken ct = default)
        {
            var result = await _client.MonitoringTransactionsIdDataGetAsync(transactionId, ct);
            return _mapper.Map<TransactionDataResponseModel>(result);
        }

        /// <summary>
        /// Load calculation results for transaction
        /// </summary>
        /// <param name="transactionId"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CalculationResultModel>> LoadCalculationResulsAsync(long transactionId, CancellationToken ct = default)
        {
            var result = await _client.MonitoringTransactionsIdResultsGetAsync(transactionId, ct);
            return _mapper.Map<IEnumerable<CalculationResultModel>>(result.Items);
        }

        /// <summary>
        /// Load list of errors
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="currentId"></param>
        /// <param name="fresh"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<PartialListContainer<TransactionErrorModel>> LoadErrorsAsync(PeriodFilter filter, long? currentId, bool fresh, CancellationToken ct = default)
        {
            _logger?.LogDebug($"Loading error records fromId: {currentId}, fresh: {fresh}, from: {filter.DateFrom}, to: {filter.DateTo}");

            filter = filter ?? new PeriodFilter();
            var response = await _client.MonitoringErrorsGetAsync(
                count: _options.Count,
                next_id: currentId,
                fresh: fresh,
                from: filter.DateFrom?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                to: filter.DateTo?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                ct);
                //(filter, new Pagination { Count = options.Count, StartFromId = currentId }, fresh, ct);

            _logger?.LogDebug($"Receive items. Exhaused: {response.Exhausted}");

            var list = response.IsAscending ?? false ? response.Items.Reverse() : response.Items;
            return new PartialListContainer<TransactionErrorModel> {
                Items = _mapper.Map<IEnumerable<TransactionErrorModel>>(list),
                Exhausted = response.Exhausted
            };
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<SummaryRecordModel>> LoadSummaryAsync(PeriodFilter filter, CancellationToken ct = default)
        {
            _logger?.LogDebug($"Loading summary records from: {filter.DateFrom}, to: {filter.DateTo}");
            var response = await _client.MonitoringSummaryGetAsync(
                from: filter.DateFrom?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                to: filter.DateTo?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture), 
                ct);
            _logger?.LogDebug($"Summary records was loaded");
            return _mapper.Map<IEnumerable<SummaryRecordModel>>(response.Items);
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<SummaryErrorModel>> LoadSummaryErrorsAsync(PeriodFilter filter, CancellationToken ct = default)
        {
            _logger?.LogDebug($"Loading summary errors from: {filter.DateFrom}, to: {filter.DateTo}");
            var response = await _client.MonitoringErrorsSummaryGetAsync(
                from: filter.DateFrom?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                to: filter.DateTo?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                ct);
            _logger?.LogDebug($"Summary errors was loaded");
            return _mapper.Map<IEnumerable<SummaryErrorModel>>(response.Items);
        }

        private string FilterToString(TransactionsFilter filter)
        {
            if (filter == null)
                return string.Empty;

            var sb = new StringBuilder();
            if (filter.DateFrom.HasValue)
                sb.Append("From: " + filter.DateFrom.Value.ToString() + "; ");
            if (filter.DateTo.HasValue)
                sb.Append("To: " + filter.DateTo.Value.ToString() + "; ");
            if (filter.IpAddress != null)
                sb.Append("Ip address: " + filter.IpAddress + "; ");
            if (filter.ScenarioCode != null)
                sb.Append("Guid: " + filter.ScenarioCode.Value.ToString() + "; ");
            if (filter.Step != null)
                sb.Append("Step: " + filter.Step + "; ");
            if (filter.WithError != null)
                sb.Append("With error: " + filter.WithError.Value.ToString() + "; ");
            return sb.ToString();
        }

    }
}
