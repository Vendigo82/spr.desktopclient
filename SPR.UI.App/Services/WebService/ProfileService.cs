﻿using SPR.UI.App.Abstractions.WebService;
using SPR.UI.WebService.DataContract.Profile;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Provide to access user's profile web-service part
    /// </summary>
    public class ProfileService : IProfileService
    {
        private readonly ApiClient.IApiClient client;
        private readonly AutoMapper.IMapper mapper;

        public ProfileService(ApiClient.IApiClient client, AutoMapper.IMapper mapper)
        {
            this.client = client;
            this.mapper = mapper;
        }

        public async Task<Profile> Profile(CancellationToken ct)
        {
            var response = await client.ProfileGetAsync(ct);
            var result = mapper.Map<Profile>(response);
            return result;
        }

        public async Task<Profile> Edit(string name, CancellationToken ct)
        { 
            var response = await client.ProfilePostAsync(new ApiClient.ProfileEditRequest() { Name = name }, ct);
            var result = mapper.Map<Profile>(response);
            return result;
        }

        public async Task<ChangePasswordResult> ChangePassword(string oldpsw, string newpsw, CancellationToken ct)
        {
            try
            {
                var request = new ApiClient.ChangePasswordRequest { OldPassword = oldpsw, NewPassword = newpsw };
                await client.ProfilePasswordPostAsync(request, ct);
                return new ChangePasswordResult { Result = ChangePasswordResultType.Success };
            } 
            catch (ApiClient.ApiException<ApiClient.ProblemDetails> e) when (e.StatusCode == 400)
            {
                if (e.Result.Title == "Invalid old password")
                    return new ChangePasswordResult { Result = ChangePasswordResultType.InvalidOldPassword };
                else if (e.Result.Title == "Invalid new password")
                {
                    e.Result.AdditionalProperties.TryGetValue("reason", out var reason);
                    return new ChangePasswordResult { Result = ChangePasswordResultType.InvalidNewPassword, ReasonCode = reason?.ToString() };
                }
                else
                    throw;
            }
        }
    }
}
