﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Services.WebService
{
    public class DirServerService : IDictionaryCRUDService<ServerModel>
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public string DictionaryName => WebClient.DictionaryNames.Server;

        public DirServerService(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<ServerModel>> GetItemsAsync(CancellationToken ct) => await LoadItemsAsync(ct);

        public async Task<ItemInfoModel<ServerModel>> LoadItemInfoAsync(int id, CancellationToken ct)
        { 
            var response =  await client.DirServerIdGetAsync(id, ct);
            var result = mapper.Map<ItemInfoModel<ServerModel>>(response);
            return result;
        }

        public async Task<IEnumerable<ServerModel>> LoadItemsAsync(CancellationToken ct)
        {
            var response = await client.DirServerGetAsync(ct);
            var result = mapper.Map<IEnumerable<ServerModel>>(response.Items);
            return result;
        }

        public async Task<IEnumerable<ServerModel>> UpdateAsync(IEnumerable<BunchUpdateItemModel<ServerModel>> items, CancellationToken ct)
        {
            var requestItems = mapper.Map<ICollection<ApiClient.ServerModelBunchUpdateItemModel>>(items);
            var request = new ApiClient.ServerModelBunchUpdateRequest { Items = requestItems };
            var response = await client.DirServerPutAsync(request, ct);
            var result = mapper.Map<IEnumerable<ServerModel>>(response.Items);
            return result;
        }

        public static string KeyFunc(ServerModel s) => s.Name ?? "";
    }
}
