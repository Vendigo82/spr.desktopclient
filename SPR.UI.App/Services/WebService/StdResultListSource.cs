﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Standard result's list data source
    /// </summary>
    public class StdResultListSource : IStdResultsListSource
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public StdResultListSource(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task<bool> DeleteAsync(StdResultSlimModel model, CancellationToken ct)
        {
            try
            {
                await client.StdResultIdDeleteAsync(model.Id, ct);
                return true;
            }
            catch (ApiClient.ApiException<ApiClient.ProblemDetails> e) when (e.Result.Title == "CanNotDelete")
            {
                return false;
            }
        }

        public Task<IEnumerable<StdResultSlimModel>> GetItemsAsync(CancellationToken ct = default) => LoadItemsAsync(ct);

        public async Task<ItemUsageModel> GetUsageAsync(StdResultSlimModel item, CancellationToken ct = default)
        { 
            var response = await client.StdResultIdUsageGetAsync(item.Id, ct);
            var result = mapper.Map<ItemUsageModel>(response);
            return result;
        }

        public async Task<IEnumerable<StdResultSlimModel>> LoadItemsAsync(CancellationToken ct = default)
        {
            var response = await client.StdResultGetAsync(ct);
            var result = mapper.Map<IEnumerable<StdResultSlimModel>>(response.Items);
            return result;
        }

        public async Task SetDefaultAsync(StdResultSlimModel model, CancellationToken ct)
        {
            await client.StdResultIdDefaultPostAsync(model.Id, ct);
            //=>client.Client.StdResultSetDefaultAsync(model.Id, ct);
        }
    }
}
