﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Services.WebService
{
    public class DirGoogleBQConnectionService : IDictionaryCRUDService<GoogleBQConnectionModel>
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public string DictionaryName => WebClient.DictionaryNames.GoogleBQ;

        public DirGoogleBQConnectionService(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<GoogleBQConnectionModel>> GetItemsAsync(CancellationToken ct)        
            => await LoadItemsAsync(ct);

        public async Task<ItemInfoModel<GoogleBQConnectionModel>> LoadItemInfoAsync(int id, CancellationToken ct)
        { 
            var response = await client.DirGooglebqIdGetAsync(id, ct);
            var result = mapper.Map<ItemInfoModel<GoogleBQConnectionModel>>(response);
            return result;
        }

        public async Task<IEnumerable<GoogleBQConnectionModel>> LoadItemsAsync(CancellationToken ct)
        {
            var response = await client.DirGooglebqGetAsync(ct);
            var result = mapper.Map<IEnumerable<GoogleBQConnectionModel>>(response.Items);
            return result;
        }

        public async Task<IEnumerable<GoogleBQConnectionModel>> UpdateAsync(IEnumerable<BunchUpdateItemModel<GoogleBQConnectionModel>> items, CancellationToken ct)
        {
            var requestItems = mapper.Map<ICollection<ApiClient.GoogleBQConnectionModelBunchUpdateItemModel>>(items);
            var request = new ApiClient.GoogleBQConnectionModelBunchUpdateRequest { Items = requestItems };
            var response = await client.DirGooglebqPutAsync(request, ct);
            var result = mapper.Map<IEnumerable<GoogleBQConnectionModel>>(response.Items);
            return result;
        }

        public static string KeyFunc(GoogleBQConnectionModel s) => s.Name ?? "";
    }
}
