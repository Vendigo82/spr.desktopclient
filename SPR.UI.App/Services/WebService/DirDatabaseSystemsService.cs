﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Model.Dir;

namespace SPR.UI.App.Services.WebService
{
    public class DirDatabaseSystemsService : IDataListSource<DatabaseSystemModel>
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public DirDatabaseSystemsService(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IEnumerable<DatabaseSystemModel>> GetItemsAsync(CancellationToken ct)
            => LoadItemsAsync(ct);

        public async Task<IEnumerable<DatabaseSystemModel>> LoadItemsAsync(CancellationToken ct)
        {
            var response = await client.DirSystemDatabaseSystemsGetAsync(ct);
            var items = mapper.Map<IEnumerable<SPR.UI.WebService.DataContract.Dir.KeyDescriptionModel>>(response.Items);
            return items.Select(i => new DatabaseSystemModel(i)).ToArray();
        }

        public static string KeyFunc(DatabaseSystemModel model) => model.Key;
    }
}
