﻿using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    public class DictionarySource : IDictionarySource
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public DictionarySource(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ItemUsageModel> DirItemInfo(string dictionaryName, string id, CancellationToken ct = default)
        {
            var item = await client.DictionaryItemUsageGetAsync(dictionaryName, id, ct);
            var result = mapper.Map<ItemUsageModel>(item.Usage);
            return result;
        }
    }
}
