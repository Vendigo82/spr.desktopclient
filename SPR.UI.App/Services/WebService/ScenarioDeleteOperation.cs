﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.App.Abstractions.DataSources;
using System.Threading;
using SPR.UI.WebClient;
using AutoMapper;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Provide delete scenario operation
    /// </summary>
    public class ScenarioDeleteOperation : IDeleteOperation<ScenarioBriefModel>
    {
        private readonly ApiClient.IApiClient client;

        public ScenarioDeleteOperation(ApiClient.IApiClient client)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
        }

        /// <summary>
        /// Delete scenario. Returns true if scenario was deleted and false, if can't delete because formula is used
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(ScenarioBriefModel item, CancellationToken ct = default)
        {
            try
            {
                await client.ScenarioIdDeleteAsync(item.Guid, ct);
                return true;
            }
            catch (ApiClient.ApiException e) when (e.StatusCode == 412)
            {
                return false;
            }
        }
    }
}
