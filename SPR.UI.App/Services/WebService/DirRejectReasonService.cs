﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Services.WebService
{
    public class DirRejectReasonService : IDictionaryCRUDService<RejectReasonModel>
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public string DictionaryName => WebClient.DictionaryNames.RejectReason;

        public DirRejectReasonService(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<RejectReasonModel>> GetItemsAsync(CancellationToken ct) => await LoadItemsAsync(ct);

        public async Task<ItemInfoModel<RejectReasonModel>> LoadItemInfoAsync(int id, CancellationToken ct)
        {
            var response = await client.DirRejectreasonIdGetAsync(id, ct);
            var result = mapper.Map<ItemInfoModel<RejectReasonModel>>(response);
            return result;
        }

        public async Task<IEnumerable<RejectReasonModel>> LoadItemsAsync(CancellationToken ct)
        {
            var response = await client.DirRejectreasonGetAsync(ct);
            var result = mapper.Map<IEnumerable<RejectReasonModel>>(response.Items);
            return result;
        }

        public async Task<IEnumerable<RejectReasonModel>> UpdateAsync(IEnumerable<BunchUpdateItemModel<RejectReasonModel>> items,
            CancellationToken ct)
        {
            var requestItems = mapper.Map<ICollection<ApiClient.RejectReasonModelBunchUpdateItemModel>>(items);
            var request = new ApiClient.RejectReasonModelBunchUpdateRequest { Items = requestItems };
            var response = await client.DirRejectreasonPutAsync(request, ct);
            var result = mapper.Map<IEnumerable<RejectReasonModel>>(response.Items);
            return result;
        }

        public static int KeyFunc(RejectReasonModel rr) => rr.Code;
    }
}
