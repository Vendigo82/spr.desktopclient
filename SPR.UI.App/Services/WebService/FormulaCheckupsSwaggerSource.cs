﻿using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Provide access to checkups via web-service
    /// </summary>
    public class FormulaCheckupsSwaggerSource : IDataListSource<FormulaScenarioCheckupModel>
    {
        private readonly ApiClient.IApiClient _client;
        private readonly IMapper _mapper;

        public FormulaCheckupsSwaggerSource(ApiClient.IApiClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IEnumerable<FormulaScenarioCheckupModel>> GetItemsAsync(CancellationToken ct = default) 
            => LoadItemsAsync(ct);

        public async Task<IEnumerable<FormulaScenarioCheckupModel>> LoadItemsAsync(CancellationToken ct = default)
        {
            var response = await _client.FormulaCheckupsPostAsync(new ApiClient.ExcludeList { });
            return _mapper.Map<IEnumerable<FormulaScenarioCheckupModel>>(response.Items);
            // _client.Client.FormulaGetCheckups(null, ct);
        }
    }
}
