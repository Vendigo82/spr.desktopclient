﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Provide service for dictionary Step
    /// </summary>
    public class DirStepService : IDictionaryCRUDService<StepModel>
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public string DictionaryName => WebClient.DictionaryNames.Step;

        public DirStepService(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client;
            this.mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<StepModel>> LoadItemsAsync(CancellationToken ct)
        {
            var response = await client.DirStepGetAsync(ct);
            var result = mapper.Map<IEnumerable<StepModel>>(response.Items);
            return result;
        }

        /// <inheritdoc/>
        public async Task<ItemInfoModel<StepModel>> LoadItemInfoAsync(int id, CancellationToken ct)
        { 
            var response = await client.DirStepIdGetAsync(id, ct);
            var result = mapper.Map<ItemInfoModel<StepModel>>(response);
            return result;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<StepModel>> UpdateAsync(IEnumerable<BunchUpdateItemModel<StepModel>> items, CancellationToken ct)
        {
            var requestItems = mapper.Map<ICollection<ApiClient.StepModelBunchUpdateItemModel>>(items);
            var request = new ApiClient.StepModelBunchUpdateRequest { Items = requestItems };
            var response = await client.DirStepPutAsync(request, ct);
            var result = mapper.Map<IEnumerable<StepModel>>(response.Items);
            return result;
        }

        public async Task<IEnumerable<StepModel>> GetItemsAsync(CancellationToken ct) => await LoadItemsAsync(ct);
        
        public static string KeyFunc(StepModel s) => s.Name;
    }
}
