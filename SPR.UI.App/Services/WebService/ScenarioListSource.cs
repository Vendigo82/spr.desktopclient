﻿using SPR.UI.WebService.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebClient;
using AutoMapper;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Provide access to web-service scenario end-point
    /// </summary>
    public class ScenarioListSource : IDataListFilterSource<ScenarioBriefModel, ScenarioFilter>
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public ScenarioListSource(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client;
            this.mapper = mapper;
        }

        public Task<IEnumerable<ScenarioBriefModel>> GetItemsAsync(CancellationToken ct = default) => LoadItemsAsync(ct);

        public Task<IEnumerable<ScenarioBriefModel>> LoadItemsAsync(CancellationToken ct = default) => LoadItemsAsync(null, ct);

        public async Task<IEnumerable<ScenarioBriefModel>> LoadItemsAsync(ScenarioFilter filter, CancellationToken ct = default)
        {
            var response = await client.ScenarioGetAsync(name: filter.NamePart, code: filter.Code, step: filter.Step, ct);
            var result = mapper.Map<IEnumerable<ScenarioBriefModel>>(response.Items);            
            return result;
        }
    }
}
