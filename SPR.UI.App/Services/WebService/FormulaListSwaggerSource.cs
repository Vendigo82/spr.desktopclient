﻿using AutoMapper;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient.DataTypes;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Source for list of formulas and drafts
    /// </summary>
    public class FormulaListSwaggerSource : IDataListFilterSource<FormulaBriefModel, FormulaFilter>
    {
        private readonly ApiClient.IApiClient _client;
        private readonly IMapper _mapper;

        public FormulaListSwaggerSource(ApiClient.IApiClient client, IMapper mapper) 
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IEnumerable<FormulaBriefModel>> GetItemsAsync(CancellationToken ct = default)
            => LoadItemsAsync(null, ct);

        public Task<IEnumerable<FormulaBriefModel>> LoadItemsAsync(CancellationToken ct = default) 
            => LoadItemsAsync(null, ct);

        public async Task<IEnumerable<FormulaBriefModel>> LoadItemsAsync(FormulaFilter filter, CancellationToken ct = default)
        {
            var response = await _client.FormulaGetAsync(
                name: filter?.NamePart, 
                code: filter?.Code, 
                type: filter?.FormulaType?.ToString(), 
                checkup: filter?.CanBeCheckup, 
                notused: filter?.NotUsed, 
                ct);


            return _mapper.Map<IEnumerable<FormulaBriefModel>>(response.Items);
        }
    }
}
