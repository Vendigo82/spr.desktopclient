﻿using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    public class FullTextSearchSource : IFullTextSearchSource
    {
        private readonly ApiClient.IApiClient _client;
        private readonly IMapper _mapper;

        public FullTextSearchSource(ApiClient.IApiClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<FormulaBriefModel>> SearchAsync(string text, CancellationToken ct = default)
        {
            var response = await _client.FormulaSearchTextGetAsync(text, ct);
            var result = _mapper.Map<IEnumerable<FormulaBriefModel>>(response.Items);
            return result;
        }

        public async Task<FullTextSearchOptionsModel> GetOptionsAsync(CancellationToken ct = default)
        {
            var response = await _client.FormulaSearchOptionsAsync();
            var result = _mapper.Map<FullTextSearchOptionsModel>(response);
            return result;
        }

        public async Task<IEnumerable<string>> GetFormulaFragments(Guid formulaId, string text, CancellationToken ct = default)
        {
            var response = await _client.FormulaIdFragmentTextGetAsync(formulaId, text, ct);
            return response.Items;
        }
    }
}
