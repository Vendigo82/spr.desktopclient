﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SPR.UI.WebService.DataContract.Journal;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataModel;
using SPR.UI.App.Services.Options;
using SPR.UI.WebClient;
using System.Threading;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract;
using SPR.UI.App.Abstractions;
using System.Text;
using AutoMapper;

namespace SPR.UI.App.Services.WebService
{

    public abstract class JournalSourceBase<T> : IJournalSource<T> where T : JournalRecordSlimModel
    {
        private readonly ApiClient.IApiClient client;
        protected readonly IMapper mapper;
        private readonly CountOptions options;
        private readonly ILogger logger;

        public JournalSourceBase(ApiClient.IApiClient client, IMapper mapper, CountOptions options, ILogger logger)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.options = options ?? throw new ArgumentNullException(nameof(options));
            this.logger = logger;
        }

        public async Task<PartialListContainer<T>> LoadAsync(JournalFilter filter, long? currentId, bool fresh, CancellationToken ct = default)
        {
            logger?.LogDebug($"Loading journal records fromId: {currentId}, fresh: {fresh}, filter: {FilterToString(filter)}");

            filter = filter ?? new JournalFilter();
            if (filter.DateFrom != null)
                filter.DateFrom = filter.DateFrom.Value.ToUniversalTime();
            if (filter.DateTo != null)
                filter.DateTo = filter.DateTo.Value.ToUniversalTime();

            logger?.LogDebug($"Loading journal records after adjust time fromId: {currentId}, fresh: {fresh}, filter: {FilterToString(filter)}");

            var response = await LoadMethodAsync(client, filter, new Pagination { Count = options.Count, StartFromId = currentId }, fresh, ct);

            foreach (var item in response.Items) {                
                item.DateTime = item.DateTime.ToLocalTime();
            }

            logger?.LogDebug($"Receive items. Exhaused: {response.Exhausted}");

            return new PartialListContainer<T> {
                Items = response.IsAscending ?? false ? response.Items.Reverse() : response.Items,
                Exhausted = response.Exhausted
            };
        }

        protected abstract Task<ItemsListPartial<T>> LoadMethodAsync(ApiClient.IApiClient client, JournalFilter filter, Pagination pagination, bool fresh, CancellationToken ct);

        private string FilterToString(JournalFilter filter)
        {
            if (filter == null)
                return string.Empty;

            var sb = new StringBuilder();
            if (filter.ObjectType.HasValue)
                sb.Append("ObjectType: " + filter.ObjectType.Value.ToString() + "; ");
            if (filter.Guid.HasValue)
                sb.Append("Guid: " + filter.Guid.Value.ToString() + "; ");
            if (filter.DateFrom.HasValue)
                sb.Append("From: " + filter.DateFrom.Value.ToString() + "; ");
            if (filter.DateTo.HasValue)
                sb.Append("To: " + filter.DateTo.Value.ToString() + "; ");
            return sb.ToString();
        }    
    }
}
