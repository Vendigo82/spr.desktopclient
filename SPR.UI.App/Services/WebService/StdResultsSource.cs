﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.UI.App.Services.WebService
{
    public class StdResultsSource : IStdResultsSource
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public StdResultsSource(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper;
        }

        public async Task<StdResultModel> GetItemAsync(Guid id, CancellationToken ct = default)
        {
            var response = await client.StdResultIdGetAsync(id, ct);
            var result = mapper.Map<StdResultModel>(response.Item);
            return result;
        }

        public async Task<ItemUsageModel> GetUsageAsync(Guid id, CancellationToken ct = default)
        { 
            var response = await client.StdResultIdUsageGetAsync(id, ct);
            var result = mapper.Map<ItemUsageModel>(response);
            return result;
        }


        public async Task<StdResultModel> SaveAsync(StdResultModel model, CancellationToken ct = default)
        {
            var request = mapper.Map<ApiClient.StdResultModel>(model);
            var response = await client.StdResultPostAsync(request, ct);
            var result = mapper.Map<StdResultModel>(response.Item);
            return result;
        }

        public async Task SetDefault(Guid id, CancellationToken ct = default)
        {
            await client.StdResultIdDefaultPostAsync(id, ct);            
        }        
    }
}
