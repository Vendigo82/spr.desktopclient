﻿using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    public class ConstantsSource : IConstantsSource
    {
        private readonly ApiClient.IApiClient _client;
        private readonly IMapper _mapper;

        public ConstantsSource(ApiClient.IApiClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<Tuple<IEnumerable<ParamModel>, string>> GetItemsAsync(string token, int? maxCount, CancellationToken ct = default)
        {
            var response = await _client.ConstantGetAsync(token, maxCount, ct);
            var items = _mapper.Map<IEnumerable<ParamModel>>(response.Items);
            return Tuple.Create(items, response.NextToken);
        }

        public async Task BulkUpdateAsync(IEnumerable<BunchUpdateItemModel<string, ParamModel>> items, CancellationToken ct = default)
        {
            var modelItems = _mapper.Map<ICollection<ApiClient.StringParamModelBunchUpdateItemModel>>(items);
            try {
                await _client.ConstantPostAsync(new ApiClient.StringParamModelBunchUpdateItemModelItemsList {
                    Items = modelItems
                }, ct);
            } catch (ApiClient.ApiException<ApiClient.ProblemDetails> e) when (e.StatusCode == 404 || e.StatusCode == 409) {
                throw new Exceptions.UpdateItemsException(e.Result.AdditionalProperties["key"].ToString(), e.Result.Detail);
            } 
        }
    }
}
