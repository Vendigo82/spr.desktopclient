﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.ApiClient;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.WebService;
using SPR.UI.WebService;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Provide auth service for SPR UI Web-service
    /// </summary>
    public class AuthService : IAuthService
    {
        private readonly IApiClient client;
        private readonly IAccessTokenManager accessTokenManager;
        private readonly ILogger logger;

        public AuthService(IApiClient client, IAccessTokenManager accessTokenManager, ILogger logger) 
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.accessTokenManager = accessTokenManager ?? throw new ArgumentNullException(nameof(accessTokenManager));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> LoginAsync(string user, string psw, CancellationToken ct) 
        {
            try
            {
                //perform request
                LoginResponse response = await client.AuthLoginPostAsync(new LoginRequest() {
                    Username = user,
                    Password = psw
                }, ct);

                //save token
                accessTokenManager.SetToken(response.AccessToken);
                return true;
            }
            catch (ApiException<ProblemDetails> e) when (e.StatusCode == 400)
            {
                logger.LogInfo($"Login forbidden for user {user}: {e.Result.Title} - {e.Result.Detail}");
                return false;
            }
        }
    }
}
