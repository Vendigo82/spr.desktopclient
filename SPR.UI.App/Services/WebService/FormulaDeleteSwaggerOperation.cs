﻿using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    public class FormulaDeleteSwaggerOperation : IDeleteOperation<FormulaBriefModel>
    {
        private readonly ApiClient.IApiClient _client;

        public FormulaDeleteSwaggerOperation(ApiClient.IApiClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        /// <summary>
        /// Delete formula. Returns true if formula was deleted and false, if can't delete because formula is used
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(FormulaBriefModel item, CancellationToken ct = default)
        {
            try
            {
                await _client.FormulaIdDeleteAsync(item.Guid, ct);
                return true;
            }
            catch (ApiClient.ApiException e) when (e.StatusCode == 412)
            {
                return false;
            }
        }
    }
}
