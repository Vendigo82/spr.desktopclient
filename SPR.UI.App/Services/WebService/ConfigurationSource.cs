﻿using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    /// <summary>
    /// Provide UI Web service access.
    /// Adapt UIWebClient class for ISPRDataSource interface
    /// </summary>
    public class ConfigurationSource : IConfigurationSource
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public ConfigurationSource(ApiClient.IApiClient client, IMapper mapper) 
        {
            this.client = client;
            this.mapper = mapper;
        }

        public async Task<InstanceResponse> GetInstanceAsync(CancellationToken ct)
        {
            var response = await client.ConfigurationInstanceGetAsync(ct);
            var result = mapper.Map<InstanceResponse>(response);
            return result;
        }
    }
}
