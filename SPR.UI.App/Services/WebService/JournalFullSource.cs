﻿using AutoMapper;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Services.Options;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Journal;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService
{
    public class JournalFullSource : JournalSourceBase<JournalRecordModel>
    {
        public JournalFullSource(ApiClient.IApiClient client, IMapper mapper, CountOptions<JournalFullSource> options, ILogger logger)
            : base(client, mapper, options, logger)
        {
        }

        protected override async Task<ItemsListPartial<JournalRecordModel>> LoadMethodAsync(ApiClient.IApiClient client, JournalFilter filter, Pagination pagination, bool fresh, CancellationToken ct)
        {
            var response = await client.JournalFullGetAsync(
                type: filter.ObjectType?.ToString(), 
                guid: filter.Guid, 
                count: pagination.Count, 
                next_id: pagination.StartFromId, 
                fresh: fresh,
                from: filter.DateFrom?.ToString(QueryParams.UriDateTimeFormat, CultureInfo.InvariantCulture),
                to: filter.DateTo?.ToString(QueryParams.UriDateTimeFormat, CultureInfo.InvariantCulture)
                );

            return mapper.Map<ItemsListPartial<JournalRecordModel>>(response);
            //return client.JournalLoadFullAsync(filter, pagination, fresh, ct);
        }
    }
}
