﻿using System;

namespace SPR.UI.App.Services.WebService
{
    public interface IWebServiceConfig
    {
        string BaseUrl { get; }

        TimeSpan? Timeout { get; }
    }

    public interface IWebServiceConfigWritter : IWebServiceConfig
    {
        void SetValues(string baseUrl, TimeSpan? timeout);
    }

    public class WebServiceConfig : IWebServiceConfig, IWebServiceConfigWritter
    {
        public string BaseUrl { get; private set; } = string.Empty;

        public TimeSpan? Timeout { get; private set; }

        public void SetValues(string baseUrl, TimeSpan? timeout)
        {
            BaseUrl = baseUrl;
            Timeout = timeout;
        }
    }
}
