﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Services.WebService
{
    public class DirSqlServerService : IDictionaryCRUDService<SqlServerModel>
    {
        private readonly ApiClient.IApiClient client;
        private readonly IMapper mapper;

        public string DictionaryName => WebClient.DictionaryNames.SqlServer;

        public DirSqlServerService(ApiClient.IApiClient client, IMapper mapper)
        {
            this.client = client;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<SqlServerModel>> GetItemsAsync(CancellationToken ct) => await LoadItemsAsync(ct);

        public async Task<ItemInfoModel<SqlServerModel>> LoadItemInfoAsync(int id, CancellationToken ct)
        { 
            var response = await client.DirSqlserverIdGetAsync(id, ct);
            var result = mapper.Map<ItemInfoModel<SqlServerModel>>(response);
            return result;
        }

        public async Task<IEnumerable<SqlServerModel>> LoadItemsAsync(CancellationToken ct)
        {
            var response = await client.DirSqlserverGetAsync(ct);
            var result = mapper.Map<IEnumerable<SqlServerModel>>(response.Items);
            return result;
        }

        public async Task<IEnumerable<SqlServerModel>> UpdateAsync(IEnumerable<BunchUpdateItemModel<SqlServerModel>> items, CancellationToken ct)
        {
            var requestItems = mapper.Map<ICollection<ApiClient.SqlServerModelBunchUpdateItemModel>>(items);
            var request = new ApiClient.SqlServerModelBunchUpdateRequest { Items = requestItems };
            var response = await client.DirSqlserverPutAsync(request, ct);
            var result = mapper.Map<IEnumerable<SqlServerModel>>(response.Items);
            return result;
        }

        public static string KeyFunc(SqlServerModel s) => s.Name ?? "";
    }
}
