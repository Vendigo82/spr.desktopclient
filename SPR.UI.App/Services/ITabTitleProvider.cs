﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    /// <summary>
    /// Provide titles for tabs
    /// </summary>
    public interface ITabTitleProvider
    {
        /// <summary>
        /// Returns tab title by name
        /// </summary>
        /// <param name="name">tab's name</param>
        /// <returns>tab's title</returns>
        string Title(string name);
    }
}
