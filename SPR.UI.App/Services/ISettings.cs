﻿
using System;

namespace SPR.UI.App.Services
{
    public interface ISettings
    {
        string TestModuleUrl { get; }

        bool UsingExternalProfile { get; }

        string ProfileUrl { get; }

        /// <summary>
        /// Documentation service settings. Can be null
        /// </summary>
        DocumentationServiceSettings Documentation { get; }
    }

    public interface ISettingsWritter : ISettings
    {
        void SetTestModuleUrl(string url);

        void SetProfile(bool usingExternalProfile, string url);

        void SetDocumentationService(DocumentationServiceSettings uri);
    }

    public class Settings : ISettingsWritter
    {
        public string TestModuleUrl { get; private set; }

        public bool UsingExternalProfile { get; set; }

        public string ProfileUrl { get; set; }

        public DocumentationServiceSettings Documentation { get; private set; }

        public void SetDocumentationService(DocumentationServiceSettings uri)
        {
            Documentation = uri;
        }

        public void SetProfile(bool usingExternalProfile, string url)
        {
            UsingExternalProfile = usingExternalProfile;
            ProfileUrl = url;
        }

        public void SetTestModuleUrl(string url) 
        {
            TestModuleUrl = url;
        }
    }

    public class DocumentationServiceSettings// : DocClient.DocRepository.IOptions
    {
        public DocumentationServiceSettings(Uri uri)
        {
            Uri = uri ?? throw new ArgumentNullException(nameof(uri));
        }

        public Uri Uri { get; }

        //public string Project { get; }

        //public string Version { get; }

        //public string Language => "ru";
    }
}
