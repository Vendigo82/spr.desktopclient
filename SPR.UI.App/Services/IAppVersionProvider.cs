﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    /// <summary>
    /// Provide information about app version
    /// </summary>
    public interface IAppVersionProvider 
    {
        string AppVersion { get; }
    }
}
