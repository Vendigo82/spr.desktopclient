﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using log4net;

namespace SPR.UI.App.Services.Loggers
{
    using Abstractions;

    public class DefaultLogger : ILogger
    {
        private static readonly ILog slog = LogManager.GetLogger("root");
        private readonly ILog log;

        public DefaultLogger() {
            log = slog;
        }

        public DefaultLogger(string name) {
            log = LogManager.GetLogger(name);
        }

        public bool IsEnabled(LogLevel logLevel) {
            switch (logLevel) {
                case LogLevel.Debug:
                    return log.IsDebugEnabled;

                case LogLevel.Information:
                    return log.IsInfoEnabled;

                case LogLevel.Warning:
                    return log.IsWarnEnabled;

                case LogLevel.Error:
                    return log.IsErrorEnabled;

                case LogLevel.Critical:
                    return log.IsFatalEnabled;

                default:
                    return true;
            }
        }

        public void Log(LogLevel logLevel, string message, Exception exception) {
            switch (logLevel) {
                case LogLevel.Debug:
                    log.Debug(message, exception);
                    break;

                case LogLevel.Information:
                    log.Info(message, exception);
                    break;

                case LogLevel.Warning:
                    log.Warn(message, exception);
                    break;

                case LogLevel.Error:
                    log.Error(message, exception);
                    break;

                case LogLevel.Critical:
                    log.Fatal(message, exception);
                    break;
            }
        }
    }
}
