﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using log4net;

//using SPR.UI.App.Services.WebService;
//using SPR.UI.WebClient;
//using SPR.UI.WebClient.EventArgs;
//using SPR.UI.WebService;

//namespace SPR.UI.App.Services.Loggers
//{
//    /// <summary>
//    /// Provide UI SRP Web-service log via log4net
//    /// </summary>
//    public class WebServiceLog : IWebServiceLog
//    {
//        private static readonly ILog log = LogManager.GetLogger("UIWebService");

//        public void ApiError(ExceptionEventArgs args) {
//            ApiException apiException = args.Exception as ApiException;
//            if (apiException is null) {
//                Error(args);
//                return;
//            }

//            string message = $"{args.RequestArgs.RequestInfo()}: API Error [kind] {apiException.Error.ToString()}; [message]: {apiException.Message};";
//            switch (apiException.Error) {
//                case ErrorKind.InternalServerError:
//                case ErrorKind.Unsupported:
//                case ErrorKind.InputDataError:
//                    log.Fatal($"{message}; [trace]: {apiException.Trace ?? ""}");
//                    break;

//                case ErrorKind.BadNewPassword:
//                case ErrorKind.InvalidClient:
//                    log.Info(message);
//                    break;

//                default:
//                    if (log.IsDebugEnabled)
//                        log.Warn($"{message}; [trace]: {apiException.Trace ?? ""}");
//                    else
//                        log.Warn(message);
//                    break;
//            }

//        }

//        public void Error(ExceptionEventArgs args) {
//            log.Error($"{args.RequestArgs.RequestInfo()}", args.Exception);
//        }

//        public void Request(RequestEventArgs args) {
//            if (log.IsDebugEnabled)
//                log.Debug($"REQUEST  {args.RequestInfo()}: {args.HttpContent?.ReadAsStringAsync().Result ?? ""}");
//        }

//        public void Response(ResponseEventArgs args) {
//            if (log.IsDebugEnabled)
//                log.Debug($"RESPONSE {args.RequestArgs.RequestInfo()}: Http status code: {args.HttpStatusCode.ToString()}; {args.Body ?? ""}");
//            else if (args.HttpStatusCode != System.Net.HttpStatusCode.OK)
//                log.Warn($"{args.RequestArgs.RequestInfo()}: Http status code: {args.HttpStatusCode.ToString()}; {args.Body ?? ""}");
//        }
//    }

//    public static class RequestArgsExtentions
//    {
//        public static string RequestInfo(this RequestEventArgs args) => $"{args.Method} {args.Url}";
//    }
//}
