﻿using System;

namespace SPR.UI.App.Exceptions
{
    public class TestExpressionErrorException : Exception
    {
        public TestExpressionErrorException(string message) : base(message)
        {
        }
    }
}
