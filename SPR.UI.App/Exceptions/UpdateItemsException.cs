﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Exceptions
{
    public class UpdateItemsException : Exception
    {
        public UpdateItemsException(IEnumerable<KeyValuePair<string, string>> errors) : base("Multiple errors on update")
        {
            Errors = errors;
        }

        public UpdateItemsException(string key, string error) : base(error)
        {
            Errors = new KeyValuePair<string, string>[] { new KeyValuePair<string, string>(key, error) };
        }

        /// <summary>
        /// List of errors.
        /// Key - item's key
        /// Value - error message
        /// </summary>
        public IEnumerable<KeyValuePair<string, string>> Errors { get; }


    }
}
