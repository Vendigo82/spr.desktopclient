﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App {
    public static class EntityExtensions {
        public static void DisposeIf(this object obj) {
            if (obj is IDisposable)
                ((IDisposable)obj).Dispose();
        }
    }
}
