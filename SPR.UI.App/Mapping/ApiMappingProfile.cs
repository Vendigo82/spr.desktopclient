﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;
using System;

namespace SPR.UI.App.Mapping
{
    public class ApiMappingProfile : Profile
    {
        public ApiMappingProfile()
        {
            #region Instance
            CreateMap<ApiClient.DocumentationServiceModel, WebService.DataContract.Configuration.DocumentationSettings>();
            CreateMap<ApiClient.InstanceResponse, WebService.DataContract.Configuration.InstanceResponse>()
                .ForMember(d => d.DocumentationSettings, m => m.MapFrom(s => s.DocumentationService));
            #endregion

            #region Formula
            CreateMap<ApiClient.FormulaType, DAL.Enums.FormulaType>()
                .ConvertUsingEnumMapping(s => s.MapByName())
                .ReverseMap();
            CreateMap<ApiClient.QueryResultType, SPR.UI.ApiClient.QueryResultType>()
                .ConvertUsingEnumMapping(s => s.MapByName())
                .ReverseMap();

            CreateMap<ApiClient.FormulaBriefModel, WebService.DataContract.Formula.FormulaBriefModel>().ReverseMap();
            CreateMap<ApiClient.FormulaScenarioCheckupModel, WebService.DataContract.Formula.FormulaScenarioCheckupModel>().ReverseMap();

            //CreateMap<ApiClient.FormulaModel, WebService>
            CreateMap<ApiClient.FormulaModel, WebService.DataContract.Formula.FormulaModel>()
                .ForMember(d => d.ChildFormulas, m => m.MapFrom(s => s.ChildFormulas))
                .ReverseMap()
                .ForMember(d => d.ChildFormulas, m => m.MapFrom(s => s.ChildFormulas));
            CreateMap<ApiClient.NestedFormulaModel, WebService.DataContract.Formula.NestedFormulaModel>().ReverseMap();
            CreateMap<ApiClient.TextualBodyModel, WebService.DataContract.Formula.TextualBodyModel>().ReverseMap();
            CreateMap<ApiClient.BunchModel, WebService.DataContract.Formula.BunchModel>().ReverseMap();
            CreateMap<ApiClient.BunchRowModel, WebService.DataContract.Formula.BunchRowModel>().ReverseMap();
            CreateMap<ApiClient.RestServiceModel, WebService.DataContract.Formula.RestServiceModel>().ReverseMap();
            CreateMap<ApiClient.ScoreCardModel, WebService.DataContract.Formula.ScoreCardModel>().ReverseMap();
            CreateMap<ApiClient.ScoreCardConditionModel, WebService.DataContract.Formula.ScoreCardConditionModel>().ReverseMap();
            CreateMap<ApiClient.ScoreCardVarModel, WebService.DataContract.Formula.ScoreCardVarModel>().ReverseMap();
            CreateMap<ApiClient.SqlQueryModel, WebService.DataContract.Formula.SqlQueryModel>().ReverseMap();
            CreateMap<ApiClient.SqlQueryParamModel, WebService.DataContract.Formula.SqlQueryParamModel>().ReverseMap();
            CreateMap<ApiClient.GoogleBQModel, WebService.DataContract.Formula.GoogleBQModel>().ReverseMap();

            CreateMap<ApiClient.VersionInfoItemsList, WebService.DataContract.ItemsList<WebService.DataContract.Common.VersionInfo>>();
            CreateMap<ApiClient.VersionInfo, WebService.DataContract.Common.VersionInfo>();
            CreateMap<ApiClient.FullTextSearchOptionsModel, WebService.DataContract.Formula.FullTextSearchOptionsModel>();
            
            CreateMap<ApiClient.FormulaUsageResponse, WebService.DataContract.Formula.FormulaUsageResponse>();
            CreateMap<ApiClient.UsageScenarioPair, WebService.DataContract.Formula.UsageScenarioPair>();
            CreateMap<ApiClient.FormulaUsageType, WebService.DataContract.Formula.FormulaUsageType>()
                .ConvertUsingEnumMapping(m => m.MapByName());

            CreateMap<ApiClient.FormulaModelObjectContainer, WebService.DataContract.ObjectContainer<WebService.DataContract.Formula.FormulaModel>>();
            #endregion

            #region Scenario 
            CreateMap<ApiClient.ScenarioSlimModel, WebService.DataContract.Scenario.ScenarioSlimModel>();
            CreateMap<ApiClient.ScenarioBriefModel, WebService.DataContract.Scenario.ScenarioBriefModel>();

            CreateMap<ApiClient.ScenarioModel, WebService.DataContract.Scenario.ScenarioModel>().ReverseMap();
            CreateMap<ApiClient.CheckupModel, WebService.DataContract.Scenario.CheckupModel>().ReverseMap();
            CreateMap<ApiClient.CalcResultModel, WebService.DataContract.Scenario.CalcResultModel>().ReverseMap();            
            CreateMap<ApiClient.CalculateTime, SPR.DAL.Enums.CalculateTime>().ConvertUsingEnumMapping(o => o.MapByName()).ReverseMap();
            #endregion

            #region StdResults
            CreateMap<ApiClient.StdResultBriefModel, WebService.DataContract.Scenario.StdResultBriefModel>().ReverseMap();
            CreateMap<ApiClient.StdResultSlimModel, WebService.DataContract.Scenario.StdResultSlimModel>().ReverseMap();
            CreateMap<ApiClient.StdResultModel, WebService.DataContract.Scenario.StdResultModel>().ReverseMap();
            CreateMap<ApiClient.StdResultValueModel, WebService.DataContract.Scenario.StdResultValueModel>().ReverseMap();
            #endregion

            #region Monitoring
            CreateMap<ApiClient.TransactionColumnModel, WebService.DataContract.Configuration.TransactionColumnModel>();
            CreateMap<ApiClient.TransactionItemDto, WebService.DataContract.Monitoring.TransactionModel>()
                .ForMember(d => d.AdditionalData, m => m.MapFrom(s => s.AdditionalProperties))
                ;
            CreateMap<ApiClient.TransactionDataModel, WebService.DataContract.Monitoring.TransactionDataModel>();
            CreateMap<ApiClient.TransactionErrorModel, WebService.DataContract.Monitoring.TransactionErrorModel>();
            CreateMap<ApiClient.TransactionItemDtoTransactionDataResponseModel, WebService.DataContract.Monitoring.TransactionDataResponseModel>();
            CreateMap<ApiClient.CalculationResultModel, WebService.DataContract.Monitoring.CalculationResultModel>();
            CreateMap<ApiClient.SummaryRecordModel, WebService.DataContract.Monitoring.SummaryRecordModel>();
            CreateMap<ApiClient.SummaryErrorModel, WebService.DataContract.Monitoring.SummaryErrorModel>();
            #endregion

            #region Parameters
            CreateMap<ApiClient.SPRDataType, WebService.DataContract.Enums.SPRDataType>()
                .ConvertUsingEnumMapping(s => s.MapByName());
            CreateMap<ApiClient.ParamModel, WebService.DataContract.Formula.ParamModel>()
                .ReverseMap();

            CreateMap<WebService.DataContract.Dir.BunchOperation, ApiClient.BunchOperation>().ConvertUsingEnumMapping(o => o.MapByName());
            CreateMap<WebService.DataContract.Dir.BunchUpdateItemModel<string, WebService.DataContract.Formula.ParamModel>, ApiClient.StringParamModelBunchUpdateItemModel>();
            #endregion

            #region Journal
            CreateMap<ApiClient.UserModel, WebService.DataContract.User.UserModel>();
            CreateMap<ApiClient.JournalAction, SPR.DAL.Enums.JournalAction>().ConvertUsingEnumMapping(o => o.MapByName());
            CreateMap<ApiClient.SerializedObjectType, SPR.DAL.Enums.SerializedObjectType>().ConvertUsingEnumMapping(o => o.MapByName());
            CreateMap<ApiClient.JournalRecordSlimModel, WebService.DataContract.Journal.JournalRecordSlimModel>();
            CreateMap<ApiClient.JournalRecordModel, WebService.DataContract.Journal.JournalRecordModel>()
                .ForMember(d => d.ObjectType, m => m.MapFrom(s => s.ObjType));
            CreateMap<ApiClient.JournalRecordSlimModelItemsListPartial, WebService.DataContract.ItemsListPartial<WebService.DataContract.Journal.JournalRecordSlimModel>>();
            CreateMap<ApiClient.JournalRecordModelItemsListPartial, WebService.DataContract.ItemsListPartial<WebService.DataContract.Journal.JournalRecordModel>>();
            #endregion

            #region Dirs
            CreateMap<ApiClient.ItemUsageModel, WebService.DataContract.ItemUsageModel>();
            CreateMap<ApiClient.KeyDescriptionModel, WebService.DataContract.Dir.KeyDescriptionModel>();

            CreateMap<ApiClient.GoogleBQConnectionModel, WebService.DataContract.Dir.GoogleBQConnectionModel>().ReverseMap();
            CreateMap<ApiClient.GoogleBQConnectionModelItemInfoModel, WebService.DataContract.Dir.ItemInfoModel<WebService.DataContract.Dir.GoogleBQConnectionModel>>();
            CreateMap<WebService.DataContract.Dir.BunchUpdateItemModel<WebService.DataContract.Dir.GoogleBQConnectionModel>, ApiClient.GoogleBQConnectionModelBunchUpdateItemModel>();

            CreateMap<ApiClient.RejectReasonModel, WebService.DataContract.Dir.RejectReasonModel>().ReverseMap();
            CreateMap<ApiClient.RejectReasonModelItemInfoModel, WebService.DataContract.Dir.ItemInfoModel<WebService.DataContract.Dir.RejectReasonModel>>();
            CreateMap<WebService.DataContract.Dir.BunchUpdateItemModel<WebService.DataContract.Dir.RejectReasonModel>, ApiClient.RejectReasonModelBunchUpdateItemModel>();

            CreateMap<ApiClient.ServerModel, WebService.DataContract.Dir.ServerModel>().ReverseMap();
            CreateMap<ApiClient.ServerModelItemInfoModel, WebService.DataContract.Dir.ItemInfoModel<WebService.DataContract.Dir.ServerModel>>();
            CreateMap<WebService.DataContract.Dir.BunchUpdateItemModel<WebService.DataContract.Dir.ServerModel>, ApiClient.ServerModelBunchUpdateItemModel>();

            CreateMap<ApiClient.SqlServerModel, WebService.DataContract.Dir.SqlServerModel>().ReverseMap();
            CreateMap<ApiClient.SqlServerModelItemInfoModel, WebService.DataContract.Dir.ItemInfoModel<WebService.DataContract.Dir.SqlServerModel>>();
            CreateMap<WebService.DataContract.Dir.BunchUpdateItemModel<WebService.DataContract.Dir.SqlServerModel>, ApiClient.SqlServerModelBunchUpdateItemModel>();

            CreateMap<ApiClient.StepModel, WebService.DataContract.Dir.StepModel>().ReverseMap();
            CreateMap<ApiClient.StepScopeEnum, SPR.DAL.Enums.StepScopeEnum>().ConvertUsingEnumMapping(o => o.MapByName()).ReverseMap();
            CreateMap<ApiClient.StepModelItemInfoModel, WebService.DataContract.Dir.ItemInfoModel<WebService.DataContract.Dir.StepModel>>();
            CreateMap<WebService.DataContract.Dir.BunchUpdateItemModel<WebService.DataContract.Dir.StepModel>, ApiClient.StepModelBunchUpdateItemModel>();
            #endregion

            #region Profile
           // CreateMap<ApiClient.UserLoginTypeEnum, SPR.DAL.Enums.UserLoginTypeEnum>().ConvertUsingEnumMapping(o => o.MapByName());
            //CreateMap<ApiClient.UserRoleEnum, SPR.DAL.Enums.UserRoleEnum>().ConvertUsingEnumMapping(o => o.MapByName());
            CreateMap<ApiClient.ProfileModel, WebService.DataContract.Profile.Profile>();
            //CreateMap<ApiClient.Role, WebService.DataContract.Profile.Role>()
            //    .ForMember(d => d.UserRole, m => m.MapFrom(s => s.Role1));
            #endregion

            #region Serialization
            CreateMap<ApiClient.Int32ItemExistanceModel, WebService.DataContract.Serialization.ItemExistanceModel<int>>();
            CreateMap<ApiClient.StringItemExistanceModel, WebService.DataContract.Serialization.ItemExistanceModel<string>>();
            CreateMap<ApiClient.StdResultBriefModelObjectExistanceModel, WebService.DataContract.Serialization.ObjectExistanceModel<WebService.DataContract.Scenario.StdResultBriefModel>>();
            CreateMap<ApiClient.FormulaBriefModelObjectExistanceModel, WebService.DataContract.Serialization.ObjectExistanceModel<WebService.DataContract.Formula.FormulaBriefModel>>();
            CreateMap<ApiClient.ScenarioSlimModelObjectExistanceModel, WebService.DataContract.Serialization.ObjectExistanceModel<WebService.DataContract.Scenario.ScenarioSlimModel>>();

            CreateMap<ApiClient.ExistanceResponseModel, WebService.DataContract.Serialization.ExistanceResponseModel>();
            CreateMap<WebService.DataContract.Serialization.ImportUpdateKind, ApiClient.ImportUpdateKind>().ConvertUsingEnumMapping(m => m.MapByName());
            CreateMap<WebService.DataContract.Serialization.ImportSettingsModel, ApiClient.ImportSettingsModel>()
                .ForMember(d => d.CreateDublicates, m => m.MapFrom(s => s.CreateDublicateForMainObjects))
                .ForMember(d => d.UpdateNestedFormulas, m => m.MapFrom(s => s.UpdateNestedFormulasKind))
                ;

            CreateMap<ApiClient.Int32ImportDictionaryItemResultModel, WebService.DataContract.Serialization.ImportDictionaryItemResultModel<int>>();
            CreateMap<ApiClient.StringImportDictionaryItemResultModel, WebService.DataContract.Serialization.ImportDictionaryItemResultModel<string>>();
            CreateMap<ApiClient.GuidImportDictionaryItemResultModel, WebService.DataContract.Serialization.ImportDictionaryItemResultModel<Guid>>();
            CreateMap<ApiClient.ImportResultModel, WebService.DataContract.Serialization.ImportResultModel>();
            CreateMap<ApiClient.ImportObjectResultModel, WebService.DataContract.Serialization.ImportObjectResultModel>();
            #endregion
        }
    }
}
