﻿using AutoMapper;
using SPR.WebService.TestModule.DataContract;

namespace SPR.UI.App.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TestResponse<RestFormulaRequestInfoDto>, TestFormulaResponse>()
                .ForMember(d => d.Data, m => m.Ignore())
                .ForMember(d => d.Result, m => m.Ignore())
                .ForMember(d => d.Type, m => m.Ignore());
        }
    }
}
