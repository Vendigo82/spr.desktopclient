﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    public class RejectReasonDataConverterTests
    {
        [TestMethod]
        public void Convert_Test() {
            RejectReasonDataConverter conv = new RejectReasonDataConverter();
            Assert.AreEqual(string.Empty, conv.Convert(null, null, null, null));
            Assert.AreEqual("1 - ", conv.Convert(new RejectReasonModel() { Code = 1 }, null, null, null));
            Assert.AreEqual("1 - abc", conv.Convert(new RejectReasonModel() { Code = 1, Name = "abc" }, null, null, null));
        }
    }
}
