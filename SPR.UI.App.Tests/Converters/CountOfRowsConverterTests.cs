﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    public class CountOfRowsConverterTests
    {
        readonly CountOfRowsConverter converter = new CountOfRowsConverter();

        [DataTestMethod]
        [DataRow(1)]
        [DataRow(5)]
        public void ObservableCollection_Test(int cnt)
        {
            // setup
            var c = new ObservableCollection<string>(Enumerable.Range(1, cnt).Select(i => i.ToString()));

            // action
            var result = converter.Convert(c, null, null, null);

            // asserts
            result.ToString().Should().Be(cnt.ToString());
        }
    }
}
