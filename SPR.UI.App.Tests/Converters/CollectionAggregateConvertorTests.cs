﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    public class CollectionAggregateConvertorTests
    {
        readonly CollectionAggregateConvertor conv = new CollectionAggregateConvertor();

        [DataTestMethod, DynamicData(nameof(ConvertCases))]
        public void Convert_Test(object argument, string expected)
        {
            // action
            var result = conv.Convert(argument, null, null, null);

            // asserts
            result.Should().Be(expected);
        }

        public static IEnumerable<object[]> ConvertCases {
            get {
                yield return new object[] { new int[] { }, "" };
                yield return new object[] { new int[] { 1 }, "1" };
                yield return new object[] { new int[] { 1, 2, 3 }, "1;2;3" };
                yield return new object[] { new List<int> { 1, 2, 3 }, "1;2;3" };
                yield return new object[] { new List<int> { 1, 2, 3 }.Select(i => i), "1;2;3" };
                yield return new object[] { new string[] { "1", "2", "3" }, "1;2;3" };
            }
        }
    }
}
