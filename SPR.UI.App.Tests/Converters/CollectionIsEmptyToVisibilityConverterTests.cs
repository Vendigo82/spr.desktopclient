﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    public class CollectionIsEmptyToVisibilityConverterTests
    {
        [TestMethod]
        public void Convert() {
            var c = new CollectionIsEmptyToVisibilityConverter();
            Assert.AreEqual(Visibility.Collapsed, c.Convert(null, null, null, null));
            Assert.AreEqual(Visibility.Collapsed, c.Convert(new int[] { }, null, null, null));
            Assert.AreEqual(Visibility.Collapsed, c.Convert(new List<int> { }, null, null, null));
            Assert.AreEqual(Visibility.Visible, c.Convert(new List<int> { 1 }, null, null, null));
            Assert.AreEqual(Visibility.Visible, c.Convert(new int[] { 1 }, null, null, null));
            Assert.AreEqual(Visibility.Visible, c.Convert(new int[] { 1, 2, 3 }.Where(i => i > 0), null, null, null));
            Assert.AreEqual(Visibility.Collapsed, c.Convert(new int[] { 1, 2, 3 }.Where(i => i < 0), null, null, null));

            Assert.AreEqual(Visibility.Hidden, c.Convert(new int[] { }, null, Visibility.Hidden, null));
        }
    }
}
