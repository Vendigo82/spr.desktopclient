﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.WebService.DataContract.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Converters.User.Tests
{
    [TestClass]
    public class UserModelConverterTests
    {
        private readonly Fixture fixture = new Fixture();
        private readonly UserModelConverter conv = new UserModelConverter();

        [TestMethod]
        public void ConvertTest()
        {
            // setup
            var model = fixture.Create<UserModel>();

            // action
            var result = conv.Convert(model, null, null, null);

            // asserts
            result.Should()
                .BeOfType<string>().Which.Should()
                .Contain(model.Login).And
                .Contain(model.Name);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public void ConvertWithoutNameTest(bool empty)
        {
            // setup
            var model = fixture.Create<UserModel>();
            model.Name = empty ? string.Empty : null;

            // action
            var result = conv.Convert(model, null, null, null);

            // asserts
            result.Should()
                .BeOfType<string>().Which.Should()
                .Contain(model.Login);
        }

        [TestMethod]
        public void ConvertNullTest()
        {
            // setup
            UserModel model = null;

            // action
            var result = conv.Convert(model, null, null, null);

            // asserts
            result.Should().BeOfType<string>().Which.Should().BeEmpty();
        }
    }
}
