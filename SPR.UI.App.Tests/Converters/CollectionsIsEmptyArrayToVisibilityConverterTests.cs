﻿using System;
using System.Windows;
using System.Windows.Navigation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    public class CollectionsIsEmptyArrayToVisibilityConverterTests
    {
        [TestMethod]
        public void Convert_Test()
        {
            var empty = new object[] { };
            var notEmpty = new object[] { new object() };
            var conv = new CollectionsIsEmptyArrayToVisibilityConverter();

            Assert.AreEqual(Visibility.Visible, conv.Convert(new object[] { empty, notEmpty }, null, null, null));
            Assert.AreEqual(Visibility.Visible, conv.Convert(new object[] { notEmpty, empty }, null, null, null));
            Assert.AreEqual(Visibility.Visible, conv.Convert(new object[] { notEmpty, notEmpty }, null, null, null));
            Assert.AreEqual(Visibility.Collapsed, conv.Convert(new object[] { empty, empty }, null, null, null));
        }
    }
}
