﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Converters.DataModel.Tests
{
    [TestClass]
    public class ServerModelConverterTests
    {
        readonly ServerModelConverter converter = new ServerModelConverter();
        readonly Fixture fixture = new Fixture();

        [TestMethod]
        public void ConvertTest()
        {
            // setup
            var item = fixture.Create<ServerModel>();

            // action
            var result = converter.Convert(item, null, null, null);

            // asserts
            result.Should()
                .NotBeNull().And
                .BeOfType<string>().Which.Should()
                .ContainAll(item.Name, item.Url);
        }

        [TestMethod]
        public void ConvertEqualTest()
        {
            // setup
            var item = fixture.Create<ServerModel>();
            item.Name = item.Url;

            // action
            var result = converter.Convert(item, null, null, null);

            // asserts
            result.Should()
                .NotBeNull().And
                .BeOfType<string>().Which.Should()
                .Contain(item.Name, Exactly.Once());
        }

        [TestMethod]
        public void NullConvertTest()
        {
            // action
            var result = converter.Convert(null, null, null, null);

            // asserts
            result.Should()
                .NotBeNull().And
                .BeOfType<string>().Which.Should()
                .BeEmpty();
        }

        [TestMethod]
        public void NullValuesTest()
        {
            // setup
            var item = new ServerModel();

            // action
            var result = converter.Convert(item, null, null, null);

            // asserts
            result.Should()
                .NotBeNull().And
                .BeOfType<string>();
        }
    }
}
