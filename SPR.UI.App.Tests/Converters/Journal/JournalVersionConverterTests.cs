﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.DAL.Enums;
using SPR.UI.WebService.DataContract.Journal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;

namespace SPR.UI.App.Converters.Journal.Tests
{
    public class JournalVersionConverterTests
    {
        readonly JournalVersionConverter converter = new JournalVersionConverter();

        [DataTestMethod]
        [DataRow(JournalAction.Rollback, 1, 2, "1 (2)")]
        [DataRow(JournalAction.Release, 1, 2, "1")]
        public void Convert_Test(JournalAction action, int verion, int? restored, string expected)
        {
            // setup
            var model = new JournalRecordSlimModel {
                Action = action,
                Version = verion,
                RestoredVersion = restored
            };

            // action
            var str = (string)converter.Convert(model, null, null, null);

            // asserts
            str.Should().Be(expected);
        }
    }
}
