﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    public class TimeSpanToSecondConverterTests
    {
        private readonly TimeSpanToSecondConverter conv = new TimeSpanToSecondConverter();

        [TestMethod]
        public void ConvertTest() {
            Assert.AreEqual("10", conv.Convert(TimeSpan.FromSeconds(10), null, null, null));
            Assert.AreEqual("10", conv.Convert((TimeSpan?)TimeSpan.FromSeconds(10), null, null, null));
            Assert.IsNull(conv.Convert((TimeSpan?)null, null, null, null));
        }

        [TestMethod]
        public void ConvertBackTest() {
            Assert.AreEqual(TimeSpan.FromSeconds(10), conv.ConvertBack("10", null, null, null));
            Assert.AreEqual(TimeSpan.FromSeconds(10), conv.ConvertBack(10, null, null, null));
            Assert.IsNull(conv.ConvertBack("", null, null, null));
            Assert.IsNull(conv.ConvertBack(null, null, null, null));
        }
    }
}
