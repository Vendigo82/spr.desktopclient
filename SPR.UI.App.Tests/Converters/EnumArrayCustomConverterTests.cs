﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    [TestCategory(Const.TestCategory)]
    public class EnumArrayCustomConverterTests
    {
        public enum MyEnum { Value1, Value2 }

        public class StringProvider : EnumCustomConverter.IStringProvider
        {
            public string Text(string typeNamespace, string typeName, string value, string param) {
                return typeName +  value + param ?? "";
            }
        }

        [TestMethod]
        public void TestMethod1() {
            EnumArrayCustomConverter conv = new EnumArrayCustomConverter(new StringProvider());
            Assert.AreEqual("MyEnumValue1" + Environment.NewLine + "MyEnumValue2",
                conv.Convert(new MyEnum[] { MyEnum.Value1, MyEnum.Value2 }, null, null, null));
            Assert.AreEqual("MyEnumValue1",
                conv.Convert(new MyEnum[] { MyEnum.Value1 }, null, null, null));
            Assert.AreEqual("",
                conv.Convert(new MyEnum[] { }, null, null, null));
            Assert.AreEqual(null,
                conv.Convert(null, null, null, null));
            Assert.AreEqual("MyEnumValue1Title" + Environment.NewLine + "MyEnumValue2Title",
                conv.Convert(new MyEnum[] { MyEnum.Value1, MyEnum.Value2 }, null, "Title", null));
            Assert.AreEqual("MyEnumValue1Title",
                conv.Convert(new MyEnum[] { MyEnum.Value1 }, null, "Title", null));
        }
    }
}
