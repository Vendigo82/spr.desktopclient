﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoFixture;
using FluentAssertions;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    public class PeriodAddDayConverterTests
    {
        readonly PeriodAddDayConverter converter = new PeriodAddDayConverter();
        readonly Fixture fixture = new Fixture();

        [TestMethod]
        public void Convert()
        {
            // setup
            var tuple = Tuple.Create(fixture.Create<DateTime>(), fixture.Create<DateTime>());

            // action
            var result = converter.Convert(tuple, null, null, null);

            // asserts
            result.Should()
                .BeOfType<Tuple<DateTime, DateTime>>().Which.Should()
                .BeEquivalentTo(new {
                    Item1 = tuple.Item1,
                    Item2 = tuple.Item2 - TimeSpan.FromDays(1)
                });
        }

        [TestMethod]
        public void ConvertBack()
        {
            // setup
            var tuple = Tuple.Create(fixture.Create<DateTime>(), fixture.Create<DateTime>());

            // action
            var result = converter.ConvertBack(tuple, null, null, null);

            // asserts
            result.Should()
                .BeOfType<Tuple<DateTime, DateTime>>().Which.Should()
                .BeEquivalentTo(new {
                    Item1 = tuple.Item1.Date,
                    Item2 = tuple.Item2.Date + TimeSpan.FromDays(1)
                });
        }
    }
}
