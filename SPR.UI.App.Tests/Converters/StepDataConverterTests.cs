﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    public class StepDataConverterTests
    {
        [TestMethod]
        public void Convert_Test() {
            var conv = new StepDataConverter();
            Assert.AreEqual(string.Empty, conv.Convert(null, null, null, null));
            Assert.AreEqual("1", conv.Convert(new StepModel() { Name = "1" }, null, null, null));
            Assert.AreEqual("1 - abc", conv.Convert(new StepModel() { Name = "1", Descr = "abc" }, null, null, null));
        }
    }
}
