﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.WebService.DataContract.Serialization;
using System.Linq;

namespace SPR.UI.App.Converters.Serialization.Tests
{
    [TestClass]
    public class StdResultListToDescriptionConverterTests
    {
        readonly Fixture fixture = new Fixture();
        readonly StdResultListToDescriptionConverter conv = new StdResultListToDescriptionConverter();

        [TestMethod]
        public void WithoutExisted_Test()
        {
            // setup
            var item = fixture
                .Build<ObjectExistanceModel<StdResultBriefModel>>()
                .Without(i => i.Existed)
                .With(i => i.Exists, false)
                .Create();

            // action
            var result = conv.Convert(new[] { item }, null, null, null);

            // asserts
            result.Should().NotBeNull().And.BeOfType<string>().Which.Should().Be(item.Item.Name + "(+)");
        }

        [TestMethod]
        public void WithExisted_SameNames_Test()
        {
            // setup
            var item = fixture
                .Build<ObjectExistanceModel<StdResultBriefModel>>()
                .With(i => i.Exists, true)
                .Create();

            item.Existed.Name = item.Item.Name;

            // action
            var result = conv.Convert(new[] { item }, null, null, null);

            // asserts
            result.Should().NotBeNull().And.BeOfType<string>().Which.Should().Be(item.Item.Name);
        }

        [TestMethod]
        public void WithExisted_DifferentNames_Test()
        {
            // setup
            var item = fixture
                .Build<ObjectExistanceModel<StdResultBriefModel>>()
                .With(i => i.Exists, true)
                .Create();

            // action
            var result = conv.Convert(new[] { item }, null, null, null);

            // asserts
            result.Should().NotBeNull().And.BeOfType<string>().Which.Should().Match($"{item.Existed.Name}*{item.Item.Name}");
        }

        [TestMethod]
        public void WithExisted_Many_Test()
        {
            // setup
            var items = fixture.CreateMany<ObjectExistanceModel<StdResultBriefModel>>();

            // action
            var result = conv.Convert(items, null, null, null);

            // asserts
            result.Should().NotBeNull().And.BeOfType<string>().Which.Should()
                .Match("*" + string.Join("*;*", items.Select(i => i.Item.Name)) + "*");
                //.And.Match("*;*;*");
        }
    }
}
