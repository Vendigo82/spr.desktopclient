﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.WebService.DataContract.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Converters.Serialization.Tests
{
    [TestClass]
    public class ItemExistanceModelToDescriptionConverterTests
    {
        readonly Fixture fixture = new Fixture();
        readonly ItemExistanceModelIntToDescriptionConverter conv = new ItemExistanceModelIntToDescriptionConverter();

        [TestMethod]
        public void Exists_Test()
        {
            // setup
            var item = fixture.Build<ItemExistanceModel<int>>().With(i => i.Exists, true).Create();

            // action
            var result = conv.Convert(new[] { item }, null, null, null);

            // asserts
            result.Should().NotBeNull().And.BeOfType<string>().And.Be(item.Item.ToString());
        }

        [TestMethod]
        public void NotExists_Test()
        {
            // setup
            var item = fixture.Build<ItemExistanceModel<int>>().With(i => i.Exists, false).Create();

            // action
            var result = conv.Convert(new[] { item }, null, null, null);

            // asserts
            result.Should().NotBeNull().And.BeOfType<string>().And.Be(item.Item.ToString() + "(+)");
        }

        [TestMethod]
        public void List_Test()
        {
            // setup
            var items = fixture.CreateMany<ItemExistanceModel<int>>();

            // action
            var result = conv.Convert(items, null, null, null);

            // asserts
            result.Should().NotBeNull().And.BeOfType<string>().Which.Should().Match("*" + string.Join("*;*", items.Select(i => i.Item.ToString())) + "*");
        }
    }
}
