﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.Converters.Tests
{
    [TestClass]
    [TestCategory(Const.TestCategory)]
    public class EnumCustomConverterTests
    {
        public enum MyEnum { Value1, Value2 }

        public class StringProvider : EnumCustomConverter.IStringProvider
        {
            public string Namespace { get; private set; }
            public string TypeName { get; private set; }
            public string Value { get; private set; }
            public string Param { get; private set; }

            public string Text(string typeNamespace, string typeName, string value, string param) {
                Namespace = typeNamespace;
                TypeName = typeName;
                Value = value;
                Param = param;
                return Value + "OK";
            }
        }

        [TestMethod]
        public void ConvertTest() {
            var sp = new StringProvider();
            EnumCustomConverter conv = new EnumCustomConverter(sp);

            Assert.AreEqual("Value1OK", (string)conv.Convert(MyEnum.Value1, null, null, null));
            Assert.AreEqual("SPR.UI.App.Converters.Tests", sp.Namespace);
            Assert.AreEqual("MyEnum", sp.TypeName);
            Assert.AreEqual("Value1", sp.Value);
            Assert.IsNull(sp.Param);

            Assert.AreEqual("Value2OK", (string)conv.Convert(MyEnum.Value2, null, null, null));
            Assert.AreEqual("SPR.UI.App.Converters.Tests", sp.Namespace);
            Assert.AreEqual("MyEnum", sp.TypeName);
            Assert.AreEqual("Value2", sp.Value);
            Assert.IsNull(sp.Param);

            Assert.AreEqual("Value1OK", (string)conv.Convert(MyEnum.Value1, null, "p1", null));
            Assert.AreEqual("p1", sp.Param);
        }
    }
}
