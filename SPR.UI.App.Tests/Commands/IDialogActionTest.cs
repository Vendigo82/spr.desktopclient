﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SPR.UI.App.Commands
{
    public interface IDialogActionTest
    {
        bool? ShowDialog(Window wnd);
    }
}
