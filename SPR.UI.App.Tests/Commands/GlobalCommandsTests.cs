﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using FakeItEasy;
using SPR.UI.App.Abstractions;
using System.Windows.Input;

namespace SPR.UI.App.Commands.Tests
{
    [TestClass]
    public class GlobalCommandsTests
    {
        [TestMethod]
        public void AddNewListItemCmd_ObservableCollection_Test() {
            var c = new System.Collections.ObjectModel.ObservableCollection<object>();
            GlobalCommands.AddNewListItemCmd.Execute(c);
            Assert.AreEqual(1, c.Count);

            GlobalCommands.AddNewListItemCmd.Execute(c);
            Assert.AreEqual(2, c.Count);
        }

        [DataTestMethod]
        [DataRow(nameof(GlobalCommands.OpenSelectCheckupsDlgCmd))]
        public void CreateCommandTest(string commandName)
        {
            //setup
            var cmd = A.Fake<ICommand>();
            IFactory<ICommand> fakeFactory = A.Fake<IFactory<ICommand>>();
            A.CallTo(() => fakeFactory.Create(A<string>._)).Returns(cmd);

            var p = typeof(GlobalCommands).GetProperty(commandName, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
            p.Should().NotBeNull();

            GlobalCommands.Factory = fakeFactory;

            //action
            var result = p.GetValue(null);

            //asserts
            result.Should()
                .NotBeNull().And
                .BeSameAs(cmd);

            A.CallTo(() => fakeFactory.Create(A<string>.That.IsEqualTo(commandName)))
                .MustHaveHappenedOnceExactly();
        }
    }
}
