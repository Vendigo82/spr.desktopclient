﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using FluentAssertions;
using Moq.Protected;
using SPR.UI.App.Services;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using System.Collections.Generic;
using System.Windows;

namespace SPR.UI.App.Commands.Serialization.Tests
{
    [TestClass]
    public class ExportActionTests
    {
        readonly Mock<ITypeResolver> fakeResolver = new Mock<ITypeResolver>();
        readonly Mock<IWindowFactory> fakeWndFactory = new Mock<IWindowFactory>();
        readonly Mock<ExportAction> action;

        public ExportActionTests()
        {
            action = new Mock<ExportAction>(() => new ExportAction(fakeResolver.Object, fakeWndFactory.Object));
        }

        [TestMethod]
        public void Action_Test()
        {
            // setup
            var fakeVm = new Mock<ICommonVM>();
            var fakeVmInitializer = fakeVm.As<IDataInitializer>();

            var list = new List<IEnumerable<KeyValuePair<Type, object>>>();
            fakeResolver.Setup(f => f.Resolve<ICommonVM>(Modules.WND_EXPORT, Capture.In(list))).Returns(fakeVm.Object);

            var wnd = new Window();
            fakeWndFactory.Setup(f => f.CreateWindow(Modules.WND_EXPORT)).Returns(wnd);

            var argument = new object();

            // action
            action.Object.Action(argument);

            // asserts
            fakeResolver.Verify(f => f.Resolve<ICommonVM>(Modules.WND_EXPORT, It.IsAny<IEnumerable<KeyValuePair<Type, object>>>()), Times.Once);
            fakeWndFactory.Verify(f => f.CreateWindow(Modules.WND_EXPORT), Times.Once);

            fakeVmInitializer.Verify(f => f.Initialize(It.IsAny<bool>()), Times.Once);

            action.Protected().As<IDialogActionTest>().Verify(f => f.ShowDialog(wnd), Times.Once());
        }
    }
}
