﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using FluentAssertions;
using AutoFixture;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Services;
using SPR.UI.App.Abstractions.VM;
using System.Windows;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.App.Abstractions.UI;

namespace SPR.UI.App.Commands.Dialogs.Tests
{
    [TestClass]
    public class OpenScenarioCheckupsDialogActionTests
    {
        readonly Fixture fixture;
        readonly Mock<IFactory<ICommonVM>> fakeVMFactory;
        readonly Mock<IWindowFactory> fakeWndFactory;
        readonly Mock<IDialogVM> fakeDialogVM;
        readonly Mock<IListProperty> fakeScenarioVM;
        readonly Mock<Window> fakeWnd;

        readonly Mock<OpenScenarioCheckupsDialogAction> fakeAction;

        /// <summary>
        /// Scenario VM with property
        /// </summary>
        public interface IListProperty
        {
            IEnumerable<FormulaScenarioCheckupModel> List { get; set; }
        }

        /// <summary>
        /// Dialog window VM should have property with name SelectedItems
        /// </summary>
        public interface IDialogVM : ICommonVM, IDataInitializer
        {
            IEnumerable<FormulaScenarioCheckupModel> SelectedItems { get; set; }
        }

        public OpenScenarioCheckupsDialogActionTests()
        {
            fixture = new Fixture();
            
            fakeVMFactory = new Mock<IFactory<ICommonVM>>();
            fakeDialogVM = new Mock<IDialogVM>();
            fakeDialogVM.SetupProperty(i => i.SelectedItems);
            fakeVMFactory.Setup(f => f.Create(It.IsAny<string>())).Returns(fakeDialogVM.Object);//Modules.FORMULA_CHECKUPS_LIST

            fakeWnd = new Mock<Window>(); //A.Dummy<Window>();
            fakeWndFactory = new Mock<IWindowFactory>();
            fakeWndFactory.Setup(f => f.CreateWindow(It.IsAny<string>(), It.IsAny<string>()))//Modules.WND_SELECT_ITEM, Modules.FORMULA_CHECKUPS_LIST
                .Returns(fakeWnd.Object);

            fakeScenarioVM = new Mock<IListProperty>();
            fakeScenarioVM.SetupProperty(i => i.List);

            fakeAction = new Mock<OpenScenarioCheckupsDialogAction>(() 
                => new OpenScenarioCheckupsDialogAction(fakeVMFactory.Object, fakeWndFactory.Object));
            fakeAction.Protected()
                .Setup<bool?>("ShowDialog", false, ItExpr.IsAny<Window>())
                .Callback(() => Task.Delay(50).Wait())
                .Returns(true);
        }        

        [TestMethod]
        public void Action_DialogConfirmed_Test()
        {
            // setup
            var sequence = new MockSequence();
            var initialCheckups = fixture.CreateMany<FormulaScenarioCheckupModel>(2); //already selected items
            var resultedCheckups = initialCheckups.Take(1).Concat(fixture.CreateMany<FormulaScenarioCheckupModel>(1)).ToArray(); //

            //call 1: data initialize
            fakeDialogVM.InSequence(sequence).Setup(f => f.Initialize(false)).Returns(Task.CompletedTask);

            // call 2: read already selected items
            fakeScenarioVM.InSequence(sequence).Setup(f => f.List).Returns(initialCheckups);            

            //call 3: set selected items to dialog vm
            fakeDialogVM.InSequence(sequence).SetupSet(f => f.SelectedItems = initialCheckups);

            //call 4: after close dialog, get selected items
            fakeDialogVM.InSequence(sequence).Setup(i => i.SelectedItems).Returns(resultedCheckups);

            // call 5: set selected items to scenario view model
            fakeScenarioVM.InSequence(sequence).SetupSet(f => f.List = resultedCheckups);            

            //action
            fakeAction.Object.Action(fakeScenarioVM.Object, nameof(IListProperty.List));

            //asserts
            fakeWnd.Object.DataContext.Should().BeSameAs(fakeDialogVM.Object);

            fakeDialogVM.Verify(i => i.Initialize(false));
            fakeScenarioVM.VerifyGet(i => i.List);
            fakeDialogVM.VerifySet(i => i.SelectedItems = initialCheckups);
            fakeDialogVM.VerifyGet(i => i.SelectedItems);
            fakeScenarioVM.VerifySet(i => i.List = resultedCheckups);
        }

        [TestMethod]
        public void Action_DialogCancelled_Test()
        {
            // setup
            fakeAction.Protected()
                .Setup<bool?>("ShowDialog", false, ItExpr.IsAny<Window>())
                .Callback(() => Task.Delay(50).Wait())
                .Returns(false);

            // action
            fakeAction.Object.Action(fakeScenarioVM.Object, nameof(IListProperty.List));

            // asserts
            fakeDialogVM.Verify(i => i.Initialize(false), Times.Once);
            fakeScenarioVM.VerifyGet(i => i.List, Times.Once);
            fakeDialogVM.VerifySet(i => i.SelectedItems = It.IsAny<IEnumerable<FormulaScenarioCheckupModel>>(), Times.Once);
            fakeDialogVM.VerifyGet(i => i.SelectedItems, Times.Never);
            fakeScenarioVM.VerifySet(i => i.List = It.IsAny<IEnumerable<FormulaScenarioCheckupModel>>(), Times.Never);
        }
    }
}
