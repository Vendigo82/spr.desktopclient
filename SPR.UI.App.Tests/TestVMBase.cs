﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using AutoFixture;
using SPR.UI.App.Services;
using SPR.UI.App.Abstractions.UI;

namespace SPR.UI.App
{
    public class TestVMBase
    {
        protected readonly Fixture fixture;
        protected readonly MockRepository repository;
        protected readonly Mock<IWindowFactory> fakeWndFactory;
        protected readonly Mock<IResourceStringProvider> fakeResources;

        public TestVMBase()
        {
            fixture = new Fixture();

            repository = new MockRepository(MockBehavior.Default);
            fakeWndFactory = repository.Create<IWindowFactory>();
            fakeResources = repository.Create<IResourceStringProvider>();
            fakeResources.Setup(f => f.GetString(It.IsAny<string>())).Returns<string>(s => s);
        }
    }
}
