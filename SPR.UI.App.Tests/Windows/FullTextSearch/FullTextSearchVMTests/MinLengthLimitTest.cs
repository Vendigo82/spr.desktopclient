﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Windows.FullTextSearch.FullTextSearchVMTests
{
    [TestClass]
    public class MinLengthLimitTest : FullTextSearchVMBaseTests
    {
        [DataTestMethod]
        [DataRow(3, "abc", true)]
        [DataRow(3, "abcd", true)]
        [DataRow(3, "ab", false)]
        [DataRow(0, "", true)]
        public async Task MinLengthTest(int minLength, string text, bool expectedCanExecute)
        {
            // setup
            sourceMock.Setup(f => f.GetOptionsAsync(default)).ReturnsAsync(new FullTextSearchOptionsModel { MinLength = minLength });

            // action
            await vm.InitAndWait();
            vm.Text = text;

            // asserts
            sourceMock.Verify(f => f.GetOptionsAsync(default), Times.Once);

            vm.SearchCmd.CanExecute(null).Should().Be(expectedCanExecute);
        }
    }
}
