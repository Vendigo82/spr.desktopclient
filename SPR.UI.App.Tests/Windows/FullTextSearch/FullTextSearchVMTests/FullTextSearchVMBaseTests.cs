﻿using Moq;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Windows.FullTextSearch.FullTextSearchVMTests
{
    public class FullTextSearchVMBaseTests : TestVMBase
    {
        protected readonly FullTextSearchVM vm;
        protected readonly Mock<IFullTextSearchSource> sourceMock = new Mock<IFullTextSearchSource>();

        public FullTextSearchVMBaseTests()
        {
            var logger = new Mock<ILogger>();
            sourceMock.Setup(f => f.GetOptionsAsync(default)).ReturnsAsync(new FullTextSearchOptionsModel { MinLength = 0 });

            vm = new FullTextSearchVM(sourceMock.Object, logger.Object);
        }
    }
}
