﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Windows.FullTextSearch.FullTextSearchVMTests
{
    [TestClass]
    public class SearchTests : FullTextSearchVMBaseTests
    {
        public SearchTests()
        {
            vm.Initialize(true).Wait();
            sourceMock.Invocations.Clear();
        }

        [TestMethod]
        public async Task SearchTest()
        {
            // setup
            vm.Text = "search";
            var formulas = fixture.CreateMany<FormulaBriefModel>();
            sourceMock.Setup(f => f.SearchAsync(vm.Text, default)).ReturnsAsync(formulas);

            var args = new List<Guid>();
            sourceMock.Setup(f => f.GetFormulaFragments(Capture.In(args), vm.Text, default))
                .ReturnsAsync(() => fixture.CreateMany<string>());

            // action
            vm.SearchCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            sourceMock.Verify(f => f.SearchAsync(vm.Text, default), Times.Once);
            sourceMock.Verify(f => f.GetFormulaFragments(It.IsAny<Guid>(), vm.Text, default), Times.Exactly(formulas.Count()));
            sourceMock.VerifyNoOtherCalls();

            vm.Should().WithoutError();

            args.Should().BeEquivalentTo(formulas.Select(i => i.Guid));
            vm.Items.Select(i => i.Formula).Should().BeEquivalentTo(formulas);
            vm.Items.Select(i => i.ErrorMessage).Should().OnlyContain(i => i == null);
            vm.Items.Select(i => i.Fragments).Should().NotContainNulls().And.OnlyContain(i => i.Count() == 3);
        }

        [TestMethod]
        public async Task GetFragmentsErrorTest()
        {
            // setup
            vm.Text = "search";
            var formulas = fixture.CreateMany<FormulaBriefModel>();
            sourceMock.Setup(f => f.SearchAsync(vm.Text, default)).ReturnsAsync(formulas);

            sourceMock.Setup(f => f.GetFormulaFragments(It.IsAny<Guid>(), vm.Text, default))
                .ReturnsAsync(() => fixture.CreateMany<string>());
            sourceMock.Setup(f => f.GetFormulaFragments(formulas.First().Guid, vm.Text, default))
               .ThrowsAsync(new ApplicationException());

            // action
            vm.SearchCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            sourceMock.Verify(f => f.SearchAsync(vm.Text, default), Times.Once);
            sourceMock.Verify(f => f.GetFormulaFragments(It.IsAny<Guid>(), vm.Text, default), Times.Exactly(formulas.Count()));
            sourceMock.VerifyNoOtherCalls();

            vm.Should().WithoutError();

            vm.Items.Select(i => i.Formula).Should().BeEquivalentTo(formulas);
            vm.Items.Select(i => i.ErrorMessage).Should().ContainSingle(i => i != null);
            vm.Items.Select(i => i.Fragments).Where(i => i != null && i.Count() == 3).Should().HaveCount(2);
        }
    }
}
