﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Windows.DictConstants;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Tests.Windows.DictConstantsTests
{
    [TestClass]
    public class SaveTests : DictConstantsVMBaseTests
    {
        [AutoData]
        public async Task InsertTest(IEnumerable<ParamModel> items, ParamModel newItem)
        {
            // setup
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();

            // action
            vm.Items.Add(new DictConstantsVM.ItemWrapper { Name = newItem.Name, Type = newItem.Type, Value = newItem.Value });
            await vm.SaveAndWait();

            // asserts
            var args = new List<IEnumerable<BunchUpdateItemModel<string, ParamModel>>>();
            sourceMock.Verify(f => f.BulkUpdateAsync(Capture.In(args), default), Times.Once);

            args.Should().ContainSingle().Which.Should().ContainSingle().Which.Should().BeEquivalentTo(new BunchUpdateItemModel<string, ParamModel> {
                Id = newItem.Name,
                Item = newItem,
                Operation = BunchOperation.Insert
            });

            vm.DeletingItems.Should().BeEmpty();
            vm.Items.Should().HaveCount(items.Count() + 1).And.OnlyContain(i => i.IsChanged == false);
        }

        [AutoData]
        public async Task DeleteTest(IEnumerable<ParamModel> items)
        {
            // setup
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();

            // action
            vm.Items.RemoveAt(0);
            await vm.SaveAndWait();

            // asserts
            var args = new List<IEnumerable<BunchUpdateItemModel<string, ParamModel>>>();
            sourceMock.Verify(f => f.BulkUpdateAsync(Capture.In(args), default), Times.Once);

            args.Should().ContainSingle().Which.Should().ContainSingle().Which.Should().BeEquivalentTo(new BunchUpdateItemModel<string, ParamModel> {
                Id = items.First().Name,
                Operation = BunchOperation.Delete
            });

            vm.DeletingItems.Should().BeEmpty();
            vm.Items.Should().HaveCount(items.Count() - 1).And.OnlyContain(i => i.IsChanged == false);
        }

        [AutoData]
        public async Task UpdateValuesTest(IEnumerable<ParamModel> items, ParamModel newItem)
        {
            // setup
            newItem.Name = items.First().Name;
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();

            // action
            vm.Items[0].Value = newItem.Value;
            vm.Items[0].Type = newItem.Type;
            await vm.SaveAndWait();

            // asserts
            var args = new List<IEnumerable<BunchUpdateItemModel<string, ParamModel>>>();
            sourceMock.Verify(f => f.BulkUpdateAsync(Capture.In(args), default), Times.Once);

            args.Should().ContainSingle().Which.Should().ContainSingle().Which.Should().BeEquivalentTo(new BunchUpdateItemModel<string, ParamModel> {
                Id = newItem.Name,
                Item = newItem,
                Operation = BunchOperation.Update
            });

            vm.DeletingItems.Should().BeEmpty();
            vm.Items.Should().HaveSameCount(items).And.OnlyContain(i => i.IsChanged == false);
        }

        [AutoData]
        public async Task RenameTest(IEnumerable<ParamModel> items, string newName)
        {
            // setup
            var oldName = items.First().Name;
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();

            // action
            vm.Items[0].Name = newName;
            await vm.SaveAndWait();

            // asserts
            var args = new List<IEnumerable<BunchUpdateItemModel<string, ParamModel>>>();
            sourceMock.Verify(f => f.BulkUpdateAsync(Capture.In(args), default), Times.Once);

            args.Should()
                .ContainSingle().Which.Should()
                .BeEquivalentTo(new[] {
                    new BunchUpdateItemModel<string, ParamModel> {
                        Id = newName,
                        Item = items.First(),
                        Operation = BunchOperation.Insert
                    },
                    new BunchUpdateItemModel<string, ParamModel> {
                        Id = oldName,
                        Operation = BunchOperation.Delete
                    }
                });

            vm.DeletingItems.Should().BeEmpty();
            vm.Items.Should().HaveSameCount(items).And.OnlyContain(i => i.IsChanged == false);
        }

        [AutoData]
        public async Task RemoveAddWithSameNameTest(IEnumerable<ParamModel> items, ParamModel newItem)
        {
            // setup
            newItem.Name = items.First().Name;
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();

            // action
            vm.Items.RemoveAt(0);
            vm.Items.Add(new DictConstantsVM.ItemWrapper { Name = newItem.Name, Type = newItem.Type, Value = newItem.Value });
            await vm.SaveAndWait();

            // asserts
            var args = new List<IEnumerable<BunchUpdateItemModel<string, ParamModel>>>();
            sourceMock.Verify(f => f.BulkUpdateAsync(Capture.In(args), default), Times.Once);

            args.Should().ContainSingle().Which.Should().ContainSingle().Which.Should().BeEquivalentTo(new BunchUpdateItemModel<string, ParamModel> {
                Id = newItem.Name,
                Item = newItem,
                Operation = BunchOperation.Update
            });

            vm.DeletingItems.Should().BeEmpty();
            vm.Items.Should().HaveSameCount(items).And.OnlyContain(i => i.IsChanged == false);
        }
    }
}
