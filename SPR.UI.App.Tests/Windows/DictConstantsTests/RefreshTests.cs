﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Tests.Windows.DictConstantsTests
{
    [TestClass]
    public class RefreshTests : DictConstantsVMBaseTests
    {
        [AutoData]
        public async Task SinglePageTest(IEnumerable<ParamModel> first)
        {
            // setup
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(first, (string)null));

            // action
            await vm.InitAndWait();

            // asserts
            vm.Items.Should().BeEquivalentTo(first);
            sourceMock.Verify(f => f.GetItemsAsync(null, null, default), Times.Once);
            sourceMock.VerifyNoOtherCalls();
        }

        [AutoData]
        public async Task MultiPagesTest(IEnumerable<ParamModel> first, string nextToken, IEnumerable<ParamModel> second)
        {
            // setup
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(first, nextToken));
            sourceMock.Setup(f => f.GetItemsAsync(nextToken, null, default)).ReturnsAsync(Tuple.Create(second, (string)null));

            // action
            await vm.InitAndWait();

            // asserts
            vm.Items.Should().BeEquivalentTo(first.Concat(second));
            sourceMock.Verify(f => f.GetItemsAsync(null, null, default), Times.Once);
            sourceMock.Verify(f => f.GetItemsAsync(nextToken, null, default), Times.Once);
            sourceMock.VerifyNoOtherCalls();
        }
    }
}
