﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Tests.Windows.DictConstantsTests
{
    [TestClass]
    public class ChangingListTests : DictConstantsVMBaseTests
    {
        [AutoData]
        public async Task DeletingTest(IEnumerable<ParamModel> items)
        {
            // setup
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();
            var item = vm.Items.First();

            // action
            vm.Items.RemoveAt(0);

            // asserts
            vm.IsChanged.Should().BeTrue();
            vm.Items.Should().HaveCount(items.Count() - 1).And.NotContain(i => i.Name == item.Name);
            vm.DeletingItems.Should().ContainSingle().Which.Should().BeSameAs(item);
        }

        [AutoData]
        public async Task RetrieveFromDeletedTest(IEnumerable<ParamModel> items)
        {
            // setup
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();
            var item = vm.Items.First();

            // action
            vm.Items.RemoveAt(0);
            vm.DeletingItems.First().RevokeDeleteCmd.Execute(vm.DeletingItems.First());

            // asserts
            vm.IsChanged.Should().BeTrue();
            vm.Items.Should().HaveSameCount(items).And.Contain(i => i.Name == item.Name);
            vm.DeletingItems.Should().BeEmpty();
        }

        [AutoData]
        public async Task InsertingsTest(IEnumerable<ParamModel> items)
        {
            // setup
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();

            // action
            vm.Items.Add(new UI.App.Windows.DictConstants.DictConstantsVM.ItemWrapper());

            // asserts
            vm.IsChanged.Should().BeTrue();
            vm.DeletingItems.Should().BeEmpty();
            var newItem = vm.Items.Should().HaveCount(items.Count() + 1).And.Contain(i => string.IsNullOrEmpty(i.Name)).Which;
            newItem.RevokeDeleteCmd.Should().NotBeNull();
            newItem.IsChanged.Should().BeTrue();
        }

        [AutoData]
        public async Task ChangingTest(IEnumerable<ParamModel> items, string newValue)
        {
            // setup
            sourceMock.Setup(f => f.GetItemsAsync(null, null, default)).ReturnsAsync(Tuple.Create(items, (string)null));
            await vm.InitAndWait();
            var item = vm.Items.First();

            // action
            vm.Items.First().Value = newValue;

            // asserts
            vm.IsChanged.Should().BeTrue();
        }
    }
}
