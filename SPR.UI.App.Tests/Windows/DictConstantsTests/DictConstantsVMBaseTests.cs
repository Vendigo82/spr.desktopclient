﻿using Moq;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Windows.DictConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Tests.Windows.DictConstantsTests
{
    public class DictConstantsVMBaseTests
    {
        protected readonly Mock<IConstantsSource> sourceMock = new Mock<IConstantsSource>();
        protected readonly DictConstantsVM vm;

        public DictConstantsVMBaseTests()
        {
            vm = new DictConstantsVM(sourceMock.Object, Mock.Of<ILogger>());
        }
    }
}
