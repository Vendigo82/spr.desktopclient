﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.DocClient;
using SPR.DocClient.Models;
using SPR.UI.App.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Windows.Documentation
{
    [TestClass]
    public class DocFunctionsListVMTests
    {
        readonly Mock<IDocRepository> repositoryMock = new Mock<IDocRepository>();
        readonly DocFunctionsListVM target;

        public DocFunctionsListVMTests()
        {
            target = new DocFunctionsListVM(repositoryMock.Object, Mock.Of<ILogger>());
        }

        //[AutoData]
        //public async Task ShouldInitializeAndFillItems(FunctionsBriefInfoList functionsList)
        //{
        //    // setup
        //    repositoryMock.Setup(f => f.GetFunctionsAsync(It.IsAny<FunctionsFilter>())).ReturnsAsync(functionsList);

        //    // action
        //    await target.InitAndWait();

        //    // asserts
        //    target.Functions.Should().Equal(functionsList.Items);
        //    target.Should().WithoutError();
        //}
    }
}
