﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    public class FakeLogoProvider : ILogoProvider
    {
        public string PathToLogo { get; set; } = "logo.jpg";
    }
}
