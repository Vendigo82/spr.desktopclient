﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VenSoft.WPFLibrary;

namespace SPR.UI.App.Utils
{
    public class WrappedParameter<T> : IWrappedParameter<T>
    {
        public WrappedParameter(T value) {
            Value = value;
        }

        public T Value { get; set; }
    }
}
