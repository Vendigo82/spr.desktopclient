﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SPR.UI.App.VM.Main
{
    public class TestTabItemVM : ITabItemVM
    {
        public UIElement Content { get; set; }

        public object DataContext { get; set; }

        public string Title { get; set; }

        public bool CanClose { get; set; }

        public bool IsChanged { get; set; }

        public object Key => new object();
    }
}
