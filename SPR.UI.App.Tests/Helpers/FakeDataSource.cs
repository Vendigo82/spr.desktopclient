﻿using SPR.UI.WebService.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.WebService.DataContract.Common;
using SPR.UI.Core.Shared;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.Core.UseCases.FormulaEditor;

namespace SPR.UI.App.Services
{
    public class FakeDataSource<TItem> : IDataSource<TItem>
    {        
        private readonly Func<Guid, ObjectContainer<TItem>> _getMain;
        private readonly Func<Guid, IEnumerable<int>> _versions;
        private readonly Func<Guid, int, TItem> _getVersion;

        public FakeDataSource(            
            Func<Guid, ObjectContainer<TItem>> getMain = null,
            Func<Guid, IEnumerable<int>> versions = null,
            Func<Guid, int, TItem> getVersion = null) {
            _getMain = getMain;
            _versions = versions;
            _getVersion = getVersion;
        }

        public Task<ObjectContainer<TItem>> GetMain(Guid id, CancellationToken ct)
            => Task.FromResult(_getMain(id));

        public Task<ObjectContainer<TItem>> Save(TItem item, string comment, CancellationToken ct) {
            throw new NotImplementedException();
        }

        public Task<ItemsList<VersionInfo>> GetArchiveVersionsListAsync(Guid id, CancellationToken ct)
            => Task.FromResult(new ItemsList<VersionInfo>() { 
                Items = _versions(id).Select(i => new VersionInfo() { Version = i, DateTime = DateTime.Now }) 
            });

        public Task<TItem> GetArchiveVersionAsync(Guid id, int ver, CancellationToken ct)
            => Task.FromResult(_getVersion(id, ver));

        public Task<TItem> RestoreAsync(Guid id, int version, string comment, CancellationToken ct)
        {
            throw new NotImplementedException();
        }
    }

    public class FakeFormulaRepository : FakeDataSource<FormulaModel>, IFormulaRepository
    {
        public FakeFormulaRepository(
            Func<Guid, ObjectContainer<FormulaModel>> getMain = null,
            Func<Guid, IEnumerable<int>> versions = null,
            Func<Guid, int, FormulaModel> getVersion = null) : base(getMain, versions, getVersion)
        {
        }

        public Task<IEnumerable<FormulaBriefModel>> GetRegisterNameUsage(string registerName, CancellationToken ct = default)
        {
            return Task.FromResult(Enumerable.Empty<FormulaBriefModel>());
        }

        public Task<FormulaUsageResponse> GetUsage(Guid id, CancellationToken ct = default)
        {
            throw new NotImplementedException();
        }
    }
}
