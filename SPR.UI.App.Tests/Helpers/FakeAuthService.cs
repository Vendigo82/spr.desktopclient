﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    public class FakeAuthService : IAuthService
    {
        private readonly Func<string, string, CancellationToken, Task<bool>> _func;

        public FakeAuthService(Func<string, string, CancellationToken, Task<bool>> func) {
            _func = func;
        }

        public Task<bool> LoginAsync(string user, string psw, CancellationToken ct) => _func(user, psw, ct);
    }
}
