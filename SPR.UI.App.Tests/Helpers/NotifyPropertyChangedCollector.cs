﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App
{
    public class NotifyPropertyChangedCollector : IEnumerable<string>
    { 
        private INotifyPropertyChanged _item;
        private readonly List<string> _events = new List<string>();

        public NotifyPropertyChangedCollector(INotifyPropertyChanged item) {
            _item = item;
            _item.PropertyChanged += _item_PropertyChanged;
        }

        private void _item_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            _events.Add(e.PropertyName);
        }

        public void Reset() => _events.Clear();

        public IEnumerator<string> GetEnumerator() => _events.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
