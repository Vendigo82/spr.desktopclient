﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    public class FakeResourceStringProvider : IResourceStringProvider
    {
        private readonly Func<string, string> _func;

        public FakeResourceStringProvider() : this(null) {
        }

        public FakeResourceStringProvider(Func<string, string> func) {
            _func = func;
        }

        public string GetString(string name) => _func != null ? _func(name) : name;
    }
}
