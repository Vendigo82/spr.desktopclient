﻿using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.Model.Main
{
    public class FakeMainWndModel : IMainWndModel
    {
        public IAppVersionProvider AppVersion { get; } = Mock.Of< IAppVersionProvider>();

        public string InstanceName => "test";

        public string ServerUrl => "test";

        public string TestServiceUrl => "localhost";

        public bool IsDocumentationEnabled => false;

        public IEnumerable<InstanceSettingsModel> GetInstances() => new List<InstanceSettingsModel>() { new InstanceSettingsModel { BaseUrl = "http://localhost" } };

        public Task InitializeAsync(CancellationToken ct)
            => Task.FromResult(new CheckAppVersionResult(true, 1, 1));

        public void SetInstance(InstanceSettingsModel instance)
        {            
        }
    }
}
