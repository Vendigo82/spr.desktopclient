﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Services
{
    public class FakeDictionaryService<TKey, T> : IDictionaryCRUDService<T>, IDictionaryService<TKey, T> where T : class
    {
        private readonly Func<int, ItemInfoModel<T>> _loadItem;
        private readonly Func<IEnumerable<T>> _loadItems;
        private readonly Func<IEnumerable<T>> _getItems;
        private readonly Func<IEnumerable<BunchUpdateItemModel<T>>, IEnumerable<T>> _update;
        private readonly Func<TKey, T> _getItem;

        public FakeDictionaryService(
            Func<int, ItemInfoModel<T>> loadItem = null, 
            Func<IEnumerable<T>> loadItems = null, 
            Func<IEnumerable<BunchUpdateItemModel<T>>, IEnumerable<T>> update = null,
            Func<IEnumerable<T>> getItems = null,
            Func<TKey, T> getItem = null) {
            _loadItem = loadItem;
            _loadItems = loadItems;
            _update = update;
            _getItems = getItems;
            _getItem = getItem;
        }

        public string DictionaryName => throw new NotImplementedException();

        public Task<T> GetItem(TKey key, CancellationToken ct) => Task.FromResult(_getItem(key));

        public Task<T> GetItemAsync(TKey key, CancellationToken ct) => GetItem(key, ct);

        public Task<IEnumerable<T>> GetItems(CancellationToken ct) => Task.FromResult(_getItems());

        public Task<IEnumerable<T>> GetItemsAsync(CancellationToken ct = default) => GetItems(ct);

        public Task<ItemInfoModel<T>> LoadItemInfo(int key, CancellationToken ct) => Task.FromResult(_loadItem(key));

        public Task<ItemInfoModel<T>> LoadItemInfoAsync(int id, CancellationToken ct = default) => LoadItemInfoAsync(id, ct);

        public Task<IEnumerable<T>> LoadItems(CancellationToken ct) => Task.FromResult(_loadItems());

        public Task<IEnumerable<T>> LoadItemsAsync(CancellationToken ct = default) => LoadItems(ct);

        public Task<IEnumerable<T>> Update(IEnumerable<BunchUpdateItemModel<T>> items, CancellationToken ct)
            => Task.FromResult(_update(items));

        public Task<IEnumerable<T>> UpdateAsync(IEnumerable<BunchUpdateItemModel<T>> items, CancellationToken ct = default) => Update(items, ct);
    }
}
