﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions.Execution;
using FluentAssertions.Primitives;
using SPR.UI.App.Abstractions.VM;

namespace FluentAssertions
{
    public static class CommonVMAssertionsExtensions 
    {
        public static CommonVMAssertions Should(this ICommonVM instance)
        {
            return new CommonVMAssertions(instance);
        }
    }

    public class CommonVMAssertions : ReferenceTypeAssertions<ICommonVM, CommonVMAssertions>
    {
        public CommonVMAssertions(ICommonVM subject) : base(subject)
        {
        }

        protected override string Identifier => "commonVM";

        /// <summary>
        /// Verify that VM operation was success
        /// </summary>
        /// <returns></returns>
        public AndConstraint<CommonVMAssertions> WithoutError()
        {
            Execute.Assertion
                .ForCondition(Subject.Error == null && Subject.ErrorMessage == null && Subject.HasError == false)
                .FailWith("VM operation was thrown exception: {0}", Subject.Error);
            return new AndConstraint<CommonVMAssertions>(this);
        }

        /// <summary>
        /// Verify that VM operation was failed
        /// </summary>
        /// <returns></returns>
        public AndConstraint<CommonVMAssertions> HasError()
        {
            Execute.Assertion
                .ForCondition(Subject.Error != null && Subject.HasError)
                .FailWith("VM operation expected exception, but was't");
            return new AndConstraint<CommonVMAssertions>(this);
        }
    }
}
