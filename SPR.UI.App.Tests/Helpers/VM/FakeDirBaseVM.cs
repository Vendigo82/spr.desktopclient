﻿using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Dir;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Dirs
{
    public class FakeDirItem
    {
        public int Code { get; set; }

        public string Value { get; set; }
    }

    public class FakeDirBaseVM : DirBaseVM<int, FakeDirItem>
    {
        public FakeDirBaseVM(IDictionaryCRUDService<FakeDirItem> service) : base(service, Wrap, null) {
        }

        private static IItemWrapper Wrap(FakeDirItem item) => new Wrapper(item);

        public class Wrapper : NotifyPropertyChangedBase, IItemWrapper
        {           
            public Wrapper(FakeDirItem item) {
                Id = item.Code;
                Item = item;
            }

            public int Id { get; }

            public int Key => Item.Code;

            public BunchOperation? UpdateStatus { get; set; }

            public FakeDirItem Item { get; set; }

            public int Code { get => Item.Code; set { Item.Code = value; NotifyPropertyChanged(); } }

            public string Value { get => Item.Value; set { Item.Value = value; NotifyPropertyChanged(); } }

            public bool IsUsed { get; set; } = false;
        }
    }
}
