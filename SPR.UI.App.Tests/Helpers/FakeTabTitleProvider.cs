﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    class FakeTabTitleProvider : ITabTitleProvider
    {
        private readonly Func<string, string> _func;

        public FakeTabTitleProvider(Func<string, string> func = null) {
            _func = func;
        }

        public string Title(string name) => _func != null ? _func(name) : name;
    }
}
