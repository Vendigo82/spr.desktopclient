﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.LocalData
{
    public class FakeLogginnedHistory : ILogginnedHistory
    {
        private readonly Action<string> _add;

        public FakeLogginnedHistory(Action<string> add = null) {
            _add = add;
        }

        public IEnumerable<string> LogginnedUsers { get; set; }

        public void AddLogin(string login) => _add(login);
    }
}
