﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services
{
    public class FakeTypeResolver : ITypeResolver
    {
        private readonly Func<Type, object> _resolve;
        private readonly Func<Type, string, object> _resolveByName;

        public FakeTypeResolver(
            Func<Type, object> resolve = null, 
            Func<Type, string, object> resolveByName = null) {
            _resolve = resolve;
            _resolveByName = resolveByName;
        }

        public T Resolve<T>() => (T)_resolve(typeof(T));

        public T Resolve<T>(string name) => (T)_resolveByName(typeof(T), name);

        public T Resolve<T>(string name, KeyValuePair<string, object> param) {
            throw new NotImplementedException();
        }

        public T Resolve<T>(string name, KeyValuePair<Type, object> param) {
            throw new NotImplementedException();
        }

        public T Resolve<T>(string name, IEnumerable<KeyValuePair<Type, object>> param)
        {
            throw new NotImplementedException();
        }
    }
}
