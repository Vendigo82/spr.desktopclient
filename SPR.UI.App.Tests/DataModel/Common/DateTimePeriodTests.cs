﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.WebClient.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.DataModel.Common.Tests
{
    [TestClass]
    public class DateTimePeriodTests
    {
        readonly Fixture fixture = new Fixture();

        [TestMethod]
        public void Set_Test()
        {
            // setup
            var period = new DateTimePeriod();
            var newPeriod = fixture.Create<Tuple<DateTime, DateTime>>();

            // action
            var result = period.Set(newPeriod);

            // asserts
            result.Should().BeTrue();
            period.Period.Should().Be(newPeriod);
        }

        [TestMethod]
        public void SetEqual_Test()
        {
            // setup
            var period = new DateTimePeriod();
            var newPeriod = new Tuple<DateTime, DateTime>(period.Period.Item1, period.Period.Item2);

            // action
            var result = period.Set(newPeriod);

            // asserts
            result.Should().BeFalse();
            period.Period.Should().Be(newPeriod);
        }

        [TestMethod]
        public void Reset_Test()
        {
            // setup
            var period = new DateTimePeriod();
            period.Set(fixture.Create<Tuple<DateTime, DateTime>>());

            // action
            var result = period.Reset();

            // asserts
            result.Should().BeTrue();
            period.Period.Item1.Should().BeCloseTo(DateTime.Now.Date - TimeSpan.FromDays(6), TimeSpan.FromSeconds(1));
            period.Period.Item2.Should().BeCloseTo(DateTime.Now.Date + TimeSpan.FromDays(1), TimeSpan.FromSeconds(1));
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public void FillFilter(bool reverse)
        {
            // setup
            var period = new DateTimePeriod();
            var dt1 = fixture.Create<DateTime>();
            var dt2 = reverse ? dt1 - TimeSpan.FromDays(2) : dt1 + TimeSpan.FromDays(2);
            
            period.Set(new Tuple<DateTime, DateTime>(dt1, dt2));

            // action
            var filter = new PeriodFilter();
            period.FillFilter(filter);

            // asserts
            filter.DateFrom.Should().Be(reverse ? dt2 : dt1);
            filter.DateTo.Should().Be(reverse ? dt1 : dt2);
            (filter.DateTo - filter.DateFrom).Should().BePositive();
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public void DateProperties_Test(bool reverse)
        {
            // setup
            var period = new DateTimePeriod();
            var dt1 = fixture.Create<DateTime>();
            var dt2 = reverse ? dt1 - TimeSpan.FromDays(2) : dt1 + TimeSpan.FromDays(2);

            // action
            period.Set(new Tuple<DateTime, DateTime>(dt1, dt2));

            // asserts
            period.DateFrom.Should().Be(reverse ? dt2 : dt1);
            period.DateTo.Should().Be(reverse ? dt1 : dt2);
        }
    }
}
