﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Mapping.Tests
{
    [TestClass]
    public class MappingProfileTests
    {
        [TestMethod]
        public void ConfigurationIsValidTest()
        {
            var cfg = new AutoMapper.MapperConfiguration(c => c.AddProfile<MappingProfile>());

            cfg.AssertConfigurationIsValid();
        }
    }
}
