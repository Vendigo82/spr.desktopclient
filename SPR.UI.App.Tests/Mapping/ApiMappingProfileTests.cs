﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Mapping.Tests
{
    [TestClass]
    public class ApiMappingProfileTests
    {
        [TestMethod]
        public void ConfigurationIsValidTest()
        {
            var cfg = new AutoMapper.MapperConfiguration(c => {
                c.AddProfile<ApiMappingProfile>();
                c.EnableEnumMappingValidation();
            });
            
            cfg.AssertConfigurationIsValid();
        }
    }
}
