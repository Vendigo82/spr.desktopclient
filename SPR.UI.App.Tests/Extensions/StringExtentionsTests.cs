﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Extensions
{
    [TestClass]
    public class StringExtentionsTests
    {
        [DataTestMethod]
        [DataRow("http://localhost", "http://localhost/")]
        [DataRow("http://localhost/", "http://localhost/")]
        public void AddTerminateSlashTests(string value, string expectedResult)
        {
            var result = value.AddTerminateSlash();
            result.Should().Be(expectedResult);
        }
    }
}
