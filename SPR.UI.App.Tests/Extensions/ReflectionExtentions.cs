﻿using System;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;

namespace SPR.UI.App.Extensions.Tests
{
    [TestClass]
    public class ReflectionExtentions
    {
        readonly CmdTestClass item = new CmdTestClass();

        class CmdTestClass
        {
            public ICommand MyCommand { get; set; }
            public bool BoolValue { get; set; }
            public List<int> ListValue { get; set; }
            public IEnumerable<int> EnumValue { get; set; }
        }

        [TestMethod]
        public void TrySetCommandDelegate_Test() {
            CmdTestClass c = new CmdTestClass();
            Assert.IsNull(c.MyCommand);

            int called = 0;
            Assert.IsTrue(c.TrySetCommandDelegate(nameof(CmdTestClass.MyCommand), () => called += 1));
            Assert.IsNotNull(c.MyCommand);

            Assert.AreEqual(0, called);
            c.MyCommand.Execute(null);
            Assert.AreEqual(1, called);

            Assert.IsFalse(c.TrySetCommandDelegate(nameof(CmdTestClass.BoolValue), () => { }));
            Assert.IsFalse(c.TrySetCommandDelegate("propertyname", () => { }));
        }

        [TestMethod]
        public void TrySetCommandDelegateWithParam_Test() {
            CmdTestClass c = new CmdTestClass();
            Assert.IsNull(c.MyCommand);

            int called = 0;
            object arg = null;
            Assert.IsTrue(c.TrySetCommandDelegate(nameof(CmdTestClass.MyCommand), (o) => { arg = o; called += 1; }));
            Assert.IsNotNull(c.MyCommand);

            object argument = new object();
            Assert.AreEqual(0, called);
            c.MyCommand.Execute(argument);
            Assert.AreEqual(1, called);
            Assert.AreSame(argument, arg);

            Assert.IsFalse(c.TrySetCommandDelegate(nameof(CmdTestClass.BoolValue), (o) => { }));
            Assert.IsFalse(c.TrySetCommandDelegate("propertyname", (o) => { }));
        }

        [TestMethod]
        public void TrySetPropertyValue_Bool_Test()
        {
            // setup
            item.BoolValue = false;

            // action
            var result = item.TrySetPropertyValue(nameof(item.BoolValue), true);

            // asserts
            result.Should().BeTrue();
            item.BoolValue.Should().BeTrue();
        }

        [TestMethod]
        public void TrySetPropertyValue_UnknownProperty_Test()
        {
            // action
            var result = item.TrySetPropertyValue("unknown", true);

            // asserts
            result.Should().BeFalse();
        }

        [TestMethod]
        public void TrySetPropertyValue_InvalidType_Test()
        {
            // setup
            item.BoolValue = false;

            // action
            var result = item.TrySetPropertyValue(nameof(item.BoolValue), "xyz");

            // asserts
            result.Should().BeFalse();
            item.BoolValue.Should().BeFalse();
        }

        [TestMethod]
        public void TryGetPropertyValue_Test()
        {
            // setup
            item.BoolValue = true;

            // action
            var result = item.TryGetPropertyValue(nameof(item.BoolValue), out bool value);

            // asserts
            result.Should().BeTrue();
            value.Should().BeTrue();
        }

        [TestMethod]
        public void TryGetPropertyValue_UnknownProperty_Test()
        {
            // action
            var result = item.TryGetPropertyValue("xyz", out bool _);

            // asserts
            result.Should().BeFalse();
        }

        [TestMethod]
        public void TryGetPropertyValue_InvalidType_Test()
        {
            // action
            var result = item.TryGetPropertyValue(nameof(item.BoolValue), out string _);

            // asserts
            result.Should().BeFalse();
        }

        [TestMethod]
        public void TryGetPropertyValue_AssignableType_Test()
        {
            // setup
            item.ListValue = new List<int>();

            // action
            var result = item.TryGetPropertyValue(nameof(item.ListValue), out ICollection<int> value);

            // asserts
            result.Should().BeTrue();
            value.Should().BeSameAs(item.ListValue);
        }

        [TestMethod]
        public void TryGetPropertyValue_NotAssignableType_Test()
        {
            // action
            var result = item.TryGetPropertyValue(nameof(item.EnumValue), out List<int> value);

            // asserts
            result.Should().BeFalse();
        }

        [TestMethod]
        public void TrySetPropertyValue_AssignableType_Test()
        {
            // setup
            var list = new List<int>();

            // action
            var result = item.TrySetPropertyValue(nameof(item.EnumValue), list);

            // asserts
            result.Should().BeTrue();
            item.EnumValue.Should().BeSameAs(list);
        }

        [TestMethod]
        public void TrySetPropertyValue_NotAssignableType_Test()
        {
            // setup
            var enumVal = Enumerable.Range(1, 3);

            // action
            var result = item.TrySetPropertyValue(nameof(item.ListValue), enumVal);

            // asserts
            result.Should().BeFalse();
        }
    }
}
