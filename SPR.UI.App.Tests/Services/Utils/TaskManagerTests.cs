﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace SPR.UI.App.Services.Utils.Tests
{
    [TestClass]
    public class TaskManagerTests
    {
        [DataTestMethod]
        [DataRow(10, 20, 30)]
        public async Task Perform_Test(int delay1, int delay2, int delay3)
        {
            // setup
            var delay = new[] { delay1, delay2, delay3 };

            var operations = Enumerable.Range(0, 3).Select(i => (new object(), new Mock<Func<Task<object>>>(), new Mock<Action<object>>(), delay[i])).ToArray();
            foreach (var item in operations) {
                item.Item2.Setup(f => f()).ReturnsAsync(item.Item1, TimeSpan.FromMilliseconds(item.Item4));
            }

            var tm = new TaskManager<object>();

            // action
            var tasks = new List<Task>(3);
            foreach (var item in operations)
                tasks.Add(tm.RunTask(item.Item2.Object, item.Item3.Object));

            await Task.WhenAll(tasks);

            // asserts
            for (int i = 0; i < operations.Length; ++i) {
                operations[i].Item2.Verify(f => f(), Times.Once);
                if (i == 2)
                    operations[i].Item3.Verify(f => f(operations[i].Item1), Times.Once);
                else
                    operations[i].Item3.Verify(f => f(It.IsAny<object>()), Times.Never);
            }
        }
    }
}
