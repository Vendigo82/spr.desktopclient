﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Options
{
    [TestClass]
    public class InstanceCombineSourceTests
    {
        readonly Mock<IInstancesSource> primaryMock = new Mock<IInstancesSource>();
        readonly Mock<IInstancesSource> secondaryMock = new Mock<IInstancesSource>();
        readonly InstanceCombineSource target;

        public InstanceCombineSourceTests()
        {
            target = new InstanceCombineSource(primaryMock.Object, secondaryMock.Object);
        }

        [AutoData]
        public void GetInstances_WhenPrimaryNotEmpty_ShouldReturnPrimaryItems(IEnumerable<InstanceSettingsModel> items)
        {
            primaryMock.Setup(f => f.GetInstances()).Returns(items);

            var result = target.GetInstances();

            result.Should().BeSameAs(items);
            primaryMock.Verify(f => f.GetInstances(), Times.Once);
            secondaryMock.VerifyNoOtherCalls();
        }

        [AutoData]
        public void GetInstances_WhenPrimaryEmpty_ShouldReturnSecondaryItems(IEnumerable<InstanceSettingsModel> items)
        {
            primaryMock.Setup(f => f.GetInstances()).Returns(Enumerable.Empty<InstanceSettingsModel>());
            secondaryMock.Setup(f => f.GetInstances()).Returns(items);

            var result = target.GetInstances();

            result.Should().BeSameAs(items);
            primaryMock.Verify(f => f.GetInstances(), Times.Once);
            secondaryMock.Verify(f => f.GetInstances(), Times.Once);
        }
    }
}
