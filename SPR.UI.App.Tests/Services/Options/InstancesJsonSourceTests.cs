﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Abstractions.DataSources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Options
{
    [TestClass]
    public class InstancesJsonSourceTests
    {        
        [TestMethod]
        public void GetInstances_ShouldReturnInstancesFromFile()
        {
            var target = new InstancesJsonSource(Path.Combine("JsonFiles", "appsettings.test.json"));

            var instances = target.GetInstances();

            var expected = Enumerable.Range(1, 3).Select(i => new InstanceSettingsModel { Name = $"instance{i}", BaseUrl = $"http://localhost:{i}" });
            instances.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        public void GetInstances_WhenFileNotFound_ShouldReturnEmptyList()
        {
            var target = new InstancesJsonSource(Path.Combine("JsonFiles", "appsettings.unknown.json"));

            var instances = target.GetInstances();

            instances.Should().BeEmpty();
        }
    }
}
