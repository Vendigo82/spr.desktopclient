﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Services.Utils;
using Unity;

namespace SPR.UI.App.Services.UI.Tests
{
    [TestClass]
    public class WindowFactoryTests
    {
        [TestMethod]
        public void CreateWindowTest() 
        {
            Type type = null;
            Window wnd = new Window();
            string name = null;
            FakeTypeResolver resolver = new FakeTypeResolver(resolveByName: (t, n) => { type = t; name = n; return wnd; });

            WindowFactory factory = new WindowFactory(resolver);
            Window result = factory.CreateWindow("abc");
            Assert.AreSame(wnd, result);
            Assert.AreEqual("abc", name);
            Assert.AreSame(typeof(Window), type);
        }
        
        //[DataTestMethod]
        //[DynamicData(nameof(FormulaTypeTestCases), DynamicDataSourceType.Method)]
        //public void CreateFormulaView_Test(DAL.Enums.FormulaType ft)
        //{
        //    // setup
        //    using (var c = new Unity.UnityContainer().AddExtension(new Diagnostic())) {
        //        Modules.Initialize(c);
        //        var factory = new WindowFactory(new TypeResolver(c));

        //        // action
        //        var view = factory.CreateFormulaView(ft, Guid.NewGuid());

        //        // asserts
        //        ((object)view).Should().NotBeNull();
        //    }
        //}

        //public static IEnumerable<object[]> FormulaTypeTestCases()
        //    => Enum.GetValues(typeof(DAL.Enums.FormulaType)).Cast<DAL.Enums.FormulaType>().Select(t => new object[] { t });
    }
}
