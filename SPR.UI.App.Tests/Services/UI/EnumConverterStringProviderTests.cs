﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SPR.UI.App.Services;

namespace SPR.UI.App.Services.UI.Tests
{
    [TestClass]
    [TestCategory(Const.TestCategory)]
    public class EnumConverterStringProviderTests
    {
        private enum MyEnum { Value1, Value2 }

        [TestMethod]
        public void TextTest() {            
            EnumConverterStringProvider conv = new EnumConverterStringProvider(new FakeResourceStringProvider((name) => {
                return name;
            }));
            Assert.AreEqual("Enum.MyEnum.Value1", conv.Text("SPR.NS", "MyEnum", "Value1", null));
            Assert.AreEqual("Enum.MyEnum.Value2", conv.Text("SPR.NS", "MyEnum", "Value2", null));
            Assert.AreEqual("Enum.MyEnum.Value1.Title", conv.Text("SPR.NS", "MyEnum", "Value1", "Title"));
        }
    }
}
