﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.Services.UI.Tests
{
    [TestClass]
    [TestCategory(Const.TestCategory)]
    public class TabTitleResourceProviderTests
    {
        [TestMethod]
        public void TitleTest() {
            string prev = null;
            TabTitleResourceProvider trp = new TabTitleResourceProvider(new FakeResourceStringProvider((n) => {
                prev = n;
                return n + "1";
            }));

            Assert.AreEqual("MyTab.Title1", trp.Title("My"));
            Assert.AreEqual("MyTab.Title", prev);
            prev = null;

            Assert.AreEqual("MyTab.Title1", trp.Title("my"));
            Assert.AreEqual("MyTab.Title", prev);

            Assert.AreEqual("AnotherTab.Title1", trp.Title("another"));
            Assert.AreEqual("AnotherTab.Title", prev);
        }

        [TestMethod]
        public void TitleResourceNotFoundTest() {           
            TabTitleResourceProvider trp = new TabTitleResourceProvider(new FakeResourceStringProvider((n) => null));

            Assert.AreEqual("My", trp.Title("My"));
            Assert.AreEqual("my", trp.Title("my"));
            Assert.AreEqual("another", trp.Title("another"));
        }
    }
}
