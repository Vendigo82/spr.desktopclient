﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using FluentAssertions;
using Moq;

namespace SPR.UI.App.Services.Tests
{
    [TestClass]
    public class TypeResolverExtensionsTests
    {
        readonly Mock<ITypeResolver> tr = new Mock<ITypeResolver>();

        [TestMethod]
        public void Resolve_GenericArgument1_Test()
        {
            // setup
            var list = new List<IEnumerable<KeyValuePair<Type, object>>>();
            tr.Setup(f => f.Resolve<object>("abc", Capture.In(list)));

            // action
            tr.Object.Resolve<object, int>("abc", 10);

            // asserts
            var arg1 = list.Should().ContainSingle().Which.Should().ContainSingle().Which;
            arg1.Key.Should().Be(typeof(int));
            arg1.Value.Should().Be(10);
        }

        [TestMethod]
        public void Resolve_GenericArgument2_Test()
        {
            // setup
            var list = new List<IEnumerable<KeyValuePair<Type, object>>>();
            tr.Setup(f => f.Resolve<object>("abc", Capture.In(list)));

            // action
            tr.Object.Resolve<object, int, string>("abc", 10, "arg2");

            // asserts
            var arg1 = list.Should().ContainSingle().Which.Should().HaveCount(2).And.SatisfyRespectively(
                (a1) => {
                    a1.Key.Should().Be(typeof(int));
                    a1.Value.Should().Be(10);
                },
                (a2) => {
                    a2.Key.Should().Be(typeof(string));
                    a2.Value.Should().Be("arg2");
                });
        }

        [TestMethod]
        public void Resolve_GenericArgument3_Test()
        {
            // setup
            var list = new List<IEnumerable<KeyValuePair<Type, object>>>();
            tr.Setup(f => f.Resolve<object>("abc", Capture.In(list)));

            // action
            tr.Object.Resolve<object, int, string, bool>("abc", 10, "arg2", true);

            // asserts
            var arg1 = list.Should().ContainSingle().Which.Should().HaveCount(3).And.SatisfyRespectively(
                (a1) => {
                    a1.Key.Should().Be(typeof(int));
                    a1.Value.Should().Be(10);
                },
                (a2) => {
                    a2.Key.Should().Be(typeof(string));
                    a2.Value.Should().Be("arg2");
                },
                a3 => {
                    a3.Key.Should().Be(typeof(bool));
                    a3.Value.Should().Be(true);
                });
        }
    }
}
