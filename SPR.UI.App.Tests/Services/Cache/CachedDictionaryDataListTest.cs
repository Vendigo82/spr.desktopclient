﻿using Moq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using SPR.UI.App.Abstractions.DataSources;

namespace SPR.UI.App.Services.Cache.Tests
{
    [TestClass]
    public class CachedDictionaryDataListTest
    {
        private readonly Mock<IDataListSource<object>> sourceMock = new Mock<IDataListSource<object>>();
        private readonly CachedDictionaryDataList<int, object> source;

        public CachedDictionaryDataListTest()
        {
            source = new CachedDictionaryDataList<int, object>(sourceMock.Object, (o) => (int)o);
        }

        [TestMethod]
        public async Task LoadItems_Test() 
        {
            // setup
            object[] list = new object[] { };
            sourceMock.Setup(f => f.LoadItemsAsync(default)).ReturnsAsync(list);

            // action
            var result1 = await source.LoadItemsAsync();
            var result2 = await source.LoadItemsAsync();
            var result3 = await source.LoadItemsAsync();

            // asserts
            result1.Should().BeSameAs(list);
            result2.Should().BeSameAs(list);
            result3.Should().BeSameAs(list);

            sourceMock.Verify(f => f.LoadItemsAsync(default), Times.Exactly(3));
        }

        [TestMethod]
        public async Task GetItems_Test() 
        {
            // setup
            object[] list = new object[] { };
            sourceMock.Setup(f => f.LoadItemsAsync(default)).ReturnsAsync(list);

            // action
            var result1 = await source.GetItemsAsync();
            var result2 = await source.GetItemsAsync();
            var result3 = await source.GetItemsAsync();

            // asserts
            result1.Should().BeSameAs(list);
            result2.Should().BeSameAs(list);
            result3.Should().BeSameAs(list);

            sourceMock.Verify(f => f.LoadItemsAsync(default), Times.Exactly(1));
        }

        [TestMethod]
        public async Task GetItem_Test() 
        {
            // setup
            object[] items = new object[] { (object)1, (object)2 };
            sourceMock.Setup(f => f.LoadItemsAsync(default)).ReturnsAsync(items);

            // action
            var item1_1 = await source.GetItemAsync(1);
            var item1_2 = await source.GetItemAsync(1);
            var item2 = await source.GetItemAsync(2);

            // asserts
            item1_1.Should().BeSameAs(items[0]);
            item1_2.Should().BeSameAs(items[0]);
            item2.Should().BeSameAs(items[1]);

            sourceMock.Verify(f => f.LoadItemsAsync(default), Times.Once);
        }

        [TestMethod]
        public async Task GetItem_NotFound_Test()
        {
            // setup
            object[] items = new object[] { (object)1, (object)2 };
            sourceMock.Setup(f => f.LoadItemsAsync(default)).ReturnsAsync(items);

            // action
            var item = await source.GetItemAsync(3);

            // asserts
            item.Should().BeNull();

            sourceMock.Verify(f => f.LoadItemsAsync(default), Times.Once);
        }
    }
}
