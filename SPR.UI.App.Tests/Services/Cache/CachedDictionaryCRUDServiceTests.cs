﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Dir;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.Cache.Tests
{
    [TestClass]
    public class CachedDictionaryCRUDServiceTests
    {
        private readonly Fixture fixture = new Fixture();
        private readonly Mock<IDictionaryCRUDService<object>> sourceMock = new Mock<IDictionaryCRUDService<object>>();
        private readonly CachedDictionaryCRUDService<int, object> source;

        public CachedDictionaryCRUDServiceTests()
        {
            source = new CachedDictionaryCRUDService<int, object>(sourceMock.Object, (o) => (int)o);
        }

        [TestMethod]
        public async Task LoadItemInfoTest() 
        { 
            // setup
            var expectedResult = fixture.Create<ItemInfoModel<object>>();
            sourceMock.Setup(f => f.LoadItemInfoAsync(1, default)).ReturnsAsync(expectedResult);

            // action
            var result = await source.LoadItemInfoAsync(1);

            // asserts
            result.Should().BeSameAs(expectedResult);
            sourceMock.Verify(f => f.LoadItemInfoAsync(1, default), Times.Once);
        }

        [TestMethod]
        public async Task UpdateTest()
        {
            // setup
            var list = fixture.CreateMany<int>().Select(i => (object)i);
            var arg = fixture.CreateMany<BunchUpdateItemModel<object>>();
            sourceMock.Setup(f => f.UpdateAsync(arg, default)).ReturnsAsync(list);

            // action
            var result = await source.UpdateAsync(arg);

            // assert
            result.Should().BeSameAs(list);
            (await source.GetItemsAsync()).Should().BeSameAs(list);

            sourceMock.Verify(f => f.UpdateAsync(arg, default), Times.Once);
        }
    }
}
