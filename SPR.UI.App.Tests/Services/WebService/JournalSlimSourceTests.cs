﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using SPR.DAL.Enums;
using SPR.UI.WebClient;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Journal;
using FluentAssertions;
using AutoMapper;
using AutoFixture;

namespace SPR.UI.App.Services.WebService.Tests
{
    [TestClass]
    public class JournalSlimSourceTests
    {
        readonly Mock<ApiClient.IApiClient> client = new Mock<ApiClient.IApiClient>();
        readonly JournalSlimSource source;

        public JournalSlimSourceTests()
        {
            var mapper = new Mapper(new MapperConfiguration(p => p.AddProfile<Mapping.ApiMappingProfile>()));
            source = new JournalSlimSource(client.Object, mapper, new Options.CountOptions<JournalSlimSource> { Count = 30 }, Mock.Of<Abstractions.ILogger>());
        }

        [DataTestMethod]
        [DataRow(null, false)]
        [DataRow(10L, true)]
        public async Task LoadAsync_ClientArguments_Test(long? nextId, bool fresh)
        {
            // setup
            var originResult = new ApiClient.JournalRecordSlimModelItemsListPartial { Items = new ApiClient.JournalRecordSlimModel[] { } };

            client.Setup(f => f.JournalSlimGetAsync(null, null, 30, nextId, fresh, null, null, default))
                .ReturnsAsync(originResult);

            var guid = Guid.NewGuid();

            // action
            var result = await source.LoadAsync(null, nextId, fresh);

            // asserts
            //result.Items.Should().BeSameAs(originResult.Items);
            result.Exhausted.Should().Be(originResult.Exhausted);

            client.Verify(f => f.JournalSlimGetAsync(null, null, 30, nextId, fresh, null, null, default), Times.Once);
        }

        [AutoData]
        public async Task MappingTest(List<ApiClient.JournalRecordSlimModel> items)
        {
            // setup
            var originResult = new ApiClient.JournalRecordSlimModelItemsListPartial { Items = items };

            client.Setup(f => f.JournalSlimGetAsync(null, null, 30, null, It.IsAny<bool>(), null, null, default))
                .ReturnsAsync(originResult);

            var guid = Guid.NewGuid();

            // action
            var result = await source.LoadAsync(null, null, true);

            // asserts
            result.Items.Should().BeEquivalentTo(items, o => o.Excluding(p => p.DateTime).ComparingEnumsByName());
        }
    }
}
