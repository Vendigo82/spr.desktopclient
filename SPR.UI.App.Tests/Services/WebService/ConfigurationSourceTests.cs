﻿using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RichardSzalay.MockHttp;
using SPR.UI.WebService.DataContract.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService.WebServiceInfoSourceTests
{
    [TestClass]
    public class ConfigurationSourceTests
    {
        readonly ConfigurationSource target;
        readonly MockHttpMessageHandler handlerMock = new MockHttpMessageHandler();        

        public ConfigurationSourceTests()
        {
            var mapper = new Mapper(new MapperConfiguration(p => p.AddProfile<Mapping.ApiMappingProfile>()));
            var httpClient = handlerMock.ToHttpClient();
            httpClient.BaseAddress = new Uri("http://localhost");
            var apiClient = new ApiClient.ApiClient(httpClient);
            target = new ConfigurationSource(apiClient, mapper);
        }

        [TestMethod]
        public async Task ShouldSuccess()
        {
            // setup
            handlerMock
                .When("http://localhost/Configuration/Instance")
                .RespondWithJsonFile("ConfigurationResponse.json");

            // action
            var result = await target.GetInstanceAsync(default);

            // asserts
            result.Should().NotBeNull().And.BeEquivalentTo(new InstanceResponse { 
                MaxVersion = 18,
                MinVersion = 11,
                Name = "local",
                ProfileUrl = "http://localhost:8002",
                TestServiceUrl = "http://localhost/SPR/test",
                UsingProfileWebsite = false,
                DocumentationSettings = new DocumentationSettings { 
                    BaseUrl = new Uri("https://doc-webservice.adms.online")
                }
            });
        }
    }
}
