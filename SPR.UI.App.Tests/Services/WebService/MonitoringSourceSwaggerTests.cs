﻿using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Services.Options;
using SPR.UI.App.Services.WebService;
using SPR.UI.WebClient.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Tests.Services.WebService
{
    [TestClass]
    public class MonitoringSourceSwaggerTests
    {
        protected readonly Fixture fixture = new Fixture();
        protected readonly Mock<ApiClient.IApiClient> clientMock = new Mock<ApiClient.IApiClient>();
        protected readonly CountOptions<MonitoringSourceSwagger> options = new CountOptions<MonitoringSourceSwagger>();
        protected readonly MonitoringSourceSwagger target;

        public MonitoringSourceSwaggerTests()
        {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.ApiMappingProfile>()));
            target = new MonitoringSourceSwagger(clientMock.Object, mapper, options, Mock.Of<ILogger>());
        }

        [TestMethod]
        public async Task LoadAdditionalColumnsInfoTest()
        {
            // setup
            var response = fixture.Create<ApiClient.TransactionColumnModelItemsList>();
            clientMock.Setup(f => f.ConfigurationTransactionAdditionalColumnsGetAsync(default)).ReturnsAsync(response);

            // actio
            var result = await target.LoadAdditionalColumnsInfoAsync();

            // setup
            result.Should().BeEquivalentTo(response.Items);
            clientMock.Verify(f => f.ConfigurationTransactionAdditionalColumnsGetAsync(default), Times.Once);
        }

        [TestMethod]
        public async Task ReloadTest()
        {
            // setup
            var ids = fixture.CreateMany<long>();
            var response = fixture.Create<ApiClient.TransactionItemDtoItemsList>();
            clientMock.Setup(f => f.MonitoringTransactionsPostAsync(It.IsAny<ApiClient.Int64ItemsList>(), default)).ReturnsAsync(response);

            // action
            var result = await target.ReloadAsync(ids);

            // asserts
            result.Should().BeEquivalentTo(response.Items, o => o
                .Excluding(p => p.ID)
                .Excluding(p => p.ScenarioID)
                .Excluding(p => p.AdditionalProperties));
            result.Select(i => i.Id).Should().BeEquivalentTo(response.Items.Select(i => i.ID));
            result.Select(i => i.ScenarioId).Should().BeEquivalentTo(response.Items.Select(i => i.ScenarioID));
            result.Select(i => i.AdditionalData).Should().BeEquivalentTo(response.Items.Select(i => i.AdditionalProperties));

            clientMock.Verify(f => f.MonitoringTransactionsPostAsync(Match.Create<ApiClient.Int64ItemsList>(i => Enumerable.SequenceEqual(ids, i.Items)), default), Times.Once);
        }

        [TestMethod]
        public async Task LoadDataAsyncTest()
        {
            // setup
            var transactionId = fixture.Create<long>();
            var response = fixture.Create<ApiClient.TransactionItemDtoTransactionDataResponseModel>();
            clientMock.Setup(f => f.MonitoringTransactionsIdDataGetAsync(transactionId, default)).ReturnsAsync(response);

            // action
            var result = await target.LoadDataAsync(transactionId);

            // asserts
            result.Should().BeEquivalentTo(response, o => o.Excluding(p => p.Transaction));
            result.Transaction.Should().BeEquivalentTo(response.Transaction, o => o
                .Excluding(p => p.ID)
                .Excluding(p => p.ScenarioID)
                .Excluding(p => p.AdditionalProperties));
        }

        [TestMethod]
        public async Task LoadCalculationResulsAsyncTest()
        {
            // setup
            var transactionId = fixture.Create<long>();
            var response = fixture.Create<ApiClient.CalculationResultModelItemsList>();
            clientMock.Setup(f => f.MonitoringTransactionsIdResultsGetAsync(transactionId, default)).ReturnsAsync(response);

            // action
            var result = await target.LoadCalculationResulsAsync(transactionId);

            // asserts
            result.Should().BeEquivalentTo(response.Items);
        }

        [TestMethod]
        public async Task LoadErrorsAsyncTest()
        {
            // setup
            PeriodFilter filter = fixture.Create<PeriodFilter>();

            string from = filter.DateFrom?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
            string to = filter.DateTo?.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
            long? currentId = fixture.Create<long>();
            bool fresh = fixture.Create<bool>();
            int count = fixture.Create<int>();
            var response = fixture.Create<ApiClient.TransactionErrorModelItemsListPartial>();

            options.Count = count;

            clientMock.Setup(f => f.MonitoringErrorsGetAsync(count, currentId, fresh, from, to, default)).ReturnsAsync(response);

            // action
            var result = await target.LoadErrorsAsync(filter, currentId, fresh);

            // asserts
            result.Items.Should().BeEquivalentTo(response.Items);
            result.Exhausted.Should().Be(response.Exhausted);
        }
    }
}
