﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Services.WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;

namespace SPR.UI.App.Tests.Services.WebService
{
    [TestClass]
    public class ConstantsServiceTests
    {
        private readonly Mock<ApiClient.IApiClient> clientMock = new Mock<ApiClient.IApiClient>();
        private readonly ConstantsSource target;

        public ConstantsServiceTests() {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.ApiMappingProfile>()));
            target = new ConstantsSource(clientMock.Object, mapper);
        }

        [AutoData]
        public async Task GetTest(string token, ApiClient.ParamModelItemsListToken response)
        {
            // setup
            clientMock.Setup(f => f.ConstantGetAsync(token, null, default)).ReturnsAsync(response);

            // action
            var result = await target.GetItemsAsync(token, null);

            // asserts
            result.Item1.Should().BeEquivalentTo(response.Items, o => o.ComparingEnumsByName());
            result.Item2.Should().Be(response.NextToken);

            clientMock.Verify(f => f.ConstantGetAsync(token, null, default), Times.Once);
        }

        [AutoData]
        public async Task BulkUpdateTest(IEnumerable<BunchUpdateItemModel<string, ParamModel>> items)
        {
            // setup

            // action
            await target.BulkUpdateAsync(items);

            // asserts
            var args = new List<ApiClient.StringParamModelBunchUpdateItemModelItemsList>();
            clientMock.Verify(f => f.ConstantPostAsync(Capture.In(args), default), Times.Once);
            clientMock.VerifyNoOtherCalls();

            args.Should().ContainSingle().Which.Items.Should().BeEquivalentTo(items);
        }

        [AutoData]
        [InlineData(404)]
        [InlineData(409)]
        public async Task UpdateErrorTest(int statusCode,
            IEnumerable<BunchUpdateItemModel<string, ParamModel>> items, 
            ApiClient.ProblemDetails problemDetails,
            string keyValue)
        {
            // setup
            problemDetails.AdditionalProperties["key"] = keyValue;
            var exception = new ApiClient.ApiException<ApiClient.ProblemDetails>("", statusCode, "", null, problemDetails, null);

            clientMock.Setup(f => f.ConstantPostAsync(It.IsAny<ApiClient.StringParamModelBunchUpdateItemModelItemsList>(), default)).ThrowsAsync(exception);

            // action
            Func<Task> action = () => target.BulkUpdateAsync(items);

            // asserts
            var e = (await action.Should().ThrowExactlyAsync<Exceptions.UpdateItemsException>()).Which;
            e.Errors.Should().ContainSingle().Which.Should().BeEquivalentTo(new {
                Key = keyValue,
                Value = problemDetails.Detail
            });
        }
    }
}
