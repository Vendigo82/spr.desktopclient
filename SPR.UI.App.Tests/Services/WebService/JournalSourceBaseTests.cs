﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Services.Options;
using SPR.UI.WebClient;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Journal;
using AutoFixture;
using FluentAssertions;
using AutoMapper;

namespace SPR.UI.App.Services.WebService.Tests
{
    [TestClass]
    public class JournalSourceBaseTests
    {
        readonly Fixture fixture = new Fixture();
        readonly Mock<ApiClient.IApiClient> clientMock = new Mock<ApiClient.IApiClient>();
        readonly Mock<JournalSourceBase<JournalRecordSlimModel>> source;

        public JournalSourceBaseTests()
        {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.ApiMappingProfile>()));
            source = new Mock<JournalSourceBase<JournalRecordSlimModel>>(clientMock.Object, mapper, new CountOptions { Count = 30 }, (ILogger)null);
        }

        private void SetupResult(ItemsListPartial<JournalRecordSlimModel> response = null)
        {
            source.Protected().Setup<Task<ItemsListPartial<JournalRecordSlimModel>>>(
                "LoadMethodAsync",
                clientMock.Object,
                ItExpr.IsAny<JournalFilter>(),
                ItExpr.IsAny<Pagination>(),
                ItExpr.IsAny<bool>(),
                ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(response ?? fixture.Create<ItemsListPartial<JournalRecordSlimModel>>()));
        }

        [DataTestMethod]
        [DataRow(true, false)]
        [DataRow(false, true)]
        public async Task LoadAsync_Result_Test(bool exhausted, bool asc)
        {
            // setup
            var response = fixture.Build<ItemsListPartial<JournalRecordSlimModel>>()
                .With(i => i.Exhausted, exhausted)
                .With(i => i.IsAscending, asc)
                .Create();
            SetupResult(response);

            // action
            var result = await source.Object.LoadAsync(null, null, true);

            // asserts
            result.Should().NotBeNull();
            result.Exhausted.Should().Be(response.Exhausted);
            result.Items.Should().Equal(asc ? response.Items.Reverse() : response.Items, (i, j) => i.Id == j.Id);
        }

        [TestMethod]
        public async Task LoadAsync_FilterByDate_Test()
        {
            // setup
            SetupResult();

            DateTime dt1 = DateTime.Now;
            DateTime dt2 = DateTime.Now + TimeSpan.FromDays(1);

            var filter = new JournalFilter { DateFrom = dt1, DateTo = dt2 };

            // action
            await source.Object.LoadAsync(filter, null, true);

            // asserts
            source.Protected().Verify<Task<ItemsListPartial<JournalRecordSlimModel>>>(
                "LoadMethodAsync",
                Times.Once(),
                clientMock.Object,
                ItExpr.Is<JournalFilter>(i => i.DateFrom == dt1.ToUniversalTime() && i.DateTo == dt2.ToUniversalTime()),
                ItExpr.IsAny<Pagination>(),
                ItExpr.IsAny<bool>(),
                ItExpr.IsAny<CancellationToken>());
        }

        [DataTestMethod]
        [DataRow(null, true, true)]
        [DataRow(10L, false, false)]
        public async Task LoadAsync_Arguments_Test(long? currentId, bool fresh, bool withFilter)
        {
            // setup
            SetupResult();

            var filter = withFilter ? new JournalFilter() : null;

            // action
            await source.Object.LoadAsync(filter, currentId, fresh);

            // asserts
            source.Protected().Verify<Task<ItemsListPartial<JournalRecordSlimModel>>>(
                "LoadMethodAsync",
                Times.Once(),
                clientMock.Object,
                ItExpr.IsAny<JournalFilter>(),
                ItExpr.Is<Pagination>(i => i.Count == 30 && i.StartFromId == currentId),
                fresh,
                ItExpr.IsAny<CancellationToken>());
        }

        [TestMethod]
        public async Task LoadAsync_UtcToLocal_Test()
        {
            // setup
            var response = fixture.Build<ItemsListPartial<JournalRecordSlimModel>>()
                .With(i => i.IsAscending, false)
                .Create();
            var sourceDt = response.Items.Select(i => i.DateTime);

            SetupResult(response);

            // action
            var result = await source.Object.LoadAsync(null, null, true);

            // asserts
            var timeDiff = DateTime.Now - DateTime.UtcNow;
            timeDiff.Should().NotBeCloseTo(TimeSpan.Zero, TimeSpan.FromSeconds(1));
            result.Items.Select(i => i.DateTime).Should().Equal(sourceDt, (i,j) => (i - j) < timeDiff + TimeSpan.FromSeconds(3));
        }
    }
}
