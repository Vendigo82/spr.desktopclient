﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using SPR.DAL.Enums;
using SPR.UI.WebClient;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Journal;
using FluentAssertions;
using AutoMapper;
using AutoFixture;

namespace SPR.UI.App.Services.WebService.Tests
{
    [TestClass]
    public class JournalSourceTests
    {
        readonly Mock<ApiClient.IApiClient> client = new Mock<ApiClient.IApiClient>();
        readonly JournalFullSource source;

        public JournalSourceTests()
        {
            var mapper = new Mapper(new MapperConfiguration(p => p.AddProfile<Mapping.ApiMappingProfile>()));
            source = new JournalFullSource(client.Object, mapper, new Options.CountOptions<JournalFullSource> { Count = 30 }, Mock.Of<Abstractions.ILogger>());
        }

        [DataTestMethod]
        [DataRow(null, false)]
        [DataRow(10L, true)]
        public async Task LoadAsync_ClientArguments_Test(long? nextId, bool fresh)
        {
            // setup
            var originResult = new ApiClient.JournalRecordModelItemsListPartial { Items = new ApiClient.JournalRecordModel[] { } };

            client.Setup(f => f.JournalFullGetAsync(null, null, 30, nextId, fresh, null, null, default))
                .ReturnsAsync(originResult);

            var guid = Guid.NewGuid();

            // action
            var result = await source.LoadAsync(null, nextId, fresh);

            // asserts
            result.Exhausted.Should().Be(originResult.Exhausted);

            client.Verify(f => f.JournalFullGetAsync(null, null, 30, nextId, fresh, null, null, default),
                Times.Once);
        }

        [AutoData]
        public async Task MappingTest(List<ApiClient.JournalRecordModel> items)
        {
            // setup
            var originResult = new ApiClient.JournalRecordModelItemsListPartial { Items = items };

            client.Setup(f => f.JournalFullGetAsync(null, null, 30, null, It.IsAny<bool>(), null, null, default))
                .ReturnsAsync(originResult);

            var guid = Guid.NewGuid();

            // action
            var result = await source.LoadAsync(null, null, true);

            // asserts
            result.Items.Should().BeEquivalentTo(items, o => o.Excluding(p => p.DateTime).Excluding(p => p.ObjType).ComparingEnumsByName());
        }
    }
}
