﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.ApiClient;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService.AuthServiceTests
{
    [TestClass]
    public class LoginAsyncTests
    {
        readonly AuthService target;
        readonly Mock<ApiClient.IApiClient> clientMock = new Mock<ApiClient.IApiClient>();
        readonly Mock<IAccessTokenManager> accessTokenMock = new Mock<IAccessTokenManager>();

        public LoginAsyncTests()
        {
            target = new AuthService(clientMock.Object, accessTokenMock.Object, Mock.Of<ILogger>());
        }

        [AutoData]
        public async Task ShouldSuccess(string username, string password, LoginResponse response)
        {
            // setup
            clientMock.Setup(f => f.AuthLoginPostAsync(It.Is<LoginRequest>(c => c.Username == username && c.Password == password), default)).ReturnsAsync(response);

            // action
            var result = await target.LoginAsync(username, password, default);

            // asserts
            result.Should().BeTrue();

            clientMock.Verify(f => f.AuthLoginPostAsync(It.Is<LoginRequest>(c => c.Username == username && c.Password == password), default), Times.Once);
            accessTokenMock.Verify(f => f.SetToken(response.AccessToken), Times.Once);
        }

        [AutoData]
        public async Task ShouldBeInvalidUserOrPasswordTest(string username, string password, ProblemDetails responseData)
        {
            // setup
            clientMock.Setup(f => f.AuthLoginPostAsync(It.Is<LoginRequest>(c => c.Username == username && c.Password == password), default))
                .ThrowsAsync(new ApiException<ProblemDetails>("", 400, "", null, responseData, null));

            // action
            var result = await target.LoginAsync(username, password, default);

            // asserts
            result.Should().BeFalse();

            clientMock.Verify(f => f.AuthLoginPostAsync(It.Is<LoginRequest>(c => c.Username == username && c.Password == password), default), Times.Once);
            accessTokenMock.Verify(f => f.SetToken(It.IsAny<string>()), Times.Never);
        }
    }
}
