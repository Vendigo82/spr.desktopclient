﻿using AutoFixture;
using AutoMapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService.FullTextSearchSourceTests
{
    public class FullTextSearchSourceBaseTests
    {
        protected readonly Mock<ApiClient.IApiClient> clientMock = new Mock<ApiClient.IApiClient>();
        protected readonly Fixture fixture = new Fixture();
        protected readonly FullTextSearchSource source;

        public FullTextSearchSourceBaseTests()
        {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.ApiMappingProfile>()));
            source = new FullTextSearchSource(clientMock.Object, mapper);
        }
    }
}
