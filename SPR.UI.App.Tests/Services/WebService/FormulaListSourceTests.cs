﻿using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebClient;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Services.WebService.Tests
{
    [TestClass]
    public class FormulaListSourceTests
    {
        readonly Fixture fixture = new Fixture();
        readonly Mock<ApiClient.IApiClient> clientMock = new Mock<ApiClient.IApiClient>();
        readonly FormulaListSwaggerSource source;

        public FormulaListSourceTests()
        {
            var mapper = new Mapper(new MapperConfiguration(p => p.AddProfile<Mapping.ApiMappingProfile>()));
            source = new FormulaListSwaggerSource(clientMock.Object, mapper);
        }

        [AutoData]
        public async Task LoadItemsWithFilter_Test(ICollection<ApiClient.FormulaBriefModel> items, FormulaFilter filter)
        {
            // setup
            //var items = fixture.CreateMany<FormulaBriefModel>();
            //var filter = fixture.Create<FormulaFilter>();
            clientMock.Setup(f => f.FormulaGetAsync(filter.NamePart, filter.Code, filter.FormulaType.ToString(), filter.CanBeCheckup, filter.NotUsed, default))
                .ReturnsAsync(new ApiClient.FormulaBriefModelItemsList { Items = items });

            // action
            var result = await source.LoadItemsAsync(filter, default);

            // asserts
            result.Should().BeEquivalentTo(items, o => o.ComparingEnumsByName());
            clientMock.Verify(f => f.FormulaGetAsync(filter.NamePart, filter.Code, filter.FormulaType.ToString(), filter.CanBeCheckup, filter.NotUsed, default), Times.Once);
        }

        [AutoData]
        public async Task LoadItemsWithoutFilter_Test(ICollection<ApiClient.FormulaBriefModel> items)
        {
            // setup
            //var items = fixture.CreateMany<FormulaBriefModel>();
            clientMock.Setup(f => f.FormulaGetAsync(null, null, null, null, null, default))
                .ReturnsAsync(new ApiClient.FormulaBriefModelItemsList { Items = items });

            // action
            var result = await source.LoadItemsAsync(null, default);

            // asserts
            result.Should().BeEquivalentTo(items, o => o.ComparingEnumsByName());
            clientMock.Verify(f => f.FormulaGetAsync(null, null, null, null, null, default), Times.Once);
        }

        [AutoData]
        public async Task GetItems_Test(ICollection<ApiClient.FormulaBriefModel> items)
        {
            // setup
            //var items = fixture.CreateMany<FormulaBriefModel>();
            clientMock.Setup(f => f.FormulaGetAsync(null, null, null, null, null, default))
                .ReturnsAsync(new ApiClient.FormulaBriefModelItemsList { Items = items });

            // action
            var result = await source.GetItemsAsync(default);

            // asserts
            result.Should().BeEquivalentTo(items, o => o.ComparingEnumsByName());
            clientMock.Verify(f => f.FormulaGetAsync(null, null, null, null, null, default), Times.Once);
        }
    }
}
