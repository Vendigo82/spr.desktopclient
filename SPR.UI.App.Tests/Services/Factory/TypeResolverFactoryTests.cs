﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using FluentAssertions;
using SPR.UI.App.Services;
using SPR.UI.App.Abstractions.VM;

namespace SPR.UI.App.Services.Factory.Tests
{
    [TestClass]
    public class VMFactoryTests
    {
        readonly ITypeResolver fakeResolver;
        readonly TypeResolverFactory<object> factory;

        public VMFactoryTests()
        {
            fakeResolver = A.Fake<ITypeResolver>();
            factory = new TypeResolverFactory<object>(fakeResolver);
        }

        [DataTestMethod]
        [DataRow("known", false)]
        [DataRow("unknown", true)]
        public void Create_Test(string name, bool expectedNull)
        {
            //setup
            var obj = new object();
            A.CallTo(() => fakeResolver.Resolve<object>(A<string>._)).Returns(null);
            A.CallTo(() => fakeResolver.Resolve<object>(A<string>.That.IsEqualTo("known"))).Returns(obj);            

            //action
            var result = factory.Create(name);

            //asserts
            if (expectedNull)
                result.Should().BeNull();
            else
                result.Should()
                    .NotBeNull().And
                    .BeSameAs(obj);

            A.CallTo(() => fakeResolver.Resolve<object>(A<string>.That.IsEqualTo(name)))
                .MustHaveHappenedOnceExactly();
        }
    }
}
