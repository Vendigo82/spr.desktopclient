﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.Services.LocalData.Files.Tests
{
    [TestClass]
    public class LoginnedHistoryTests
    {
        private class TestFiles : IDataFileNamesProvider
        {
            public string Path => "./testfiles/";

            public string LogginnedHistory => "logginned.json";
        }

        TestFiles files = new TestFiles();

        [TestInitialize]
        public void Init() {
            if (File.Exists(files.Path + files.LogginnedHistory))
                File.Delete(files.Path + files.LogginnedHistory);
        }

        [TestMethod]
        public void LoginnedHistoryTest() {
            LoginnedHistory f = new LoginnedHistory(files);
            Assert.AreEqual(0, f.LogginnedUsers.Count());
            Assert.IsFalse(File.Exists(files.Path + files.LogginnedHistory));

            f.AddLogin("sa");
            Assert.IsTrue(File.Exists(files.Path + files.LogginnedHistory));
            CollectionAssert.AreEqual(new string[] { "sa" }, f.LogginnedUsers.ToArray());

            f.AddLogin("sa");
            CollectionAssert.AreEqual(new string[] { "sa" }, f.LogginnedUsers.ToArray());

            f.AddLogin("another");
            CollectionAssert.AreEqual(new string[] { "another", "sa" }, f.LogginnedUsers.ToArray());

            f.AddLogin("sa");
            CollectionAssert.AreEqual(new string[] { "sa", "another" }, f.LogginnedUsers.ToArray());

            for (int i = 0; i < 10; i++)
                f.AddLogin("u" + i.ToString());
            CollectionAssert.AreEqual(
                Enumerable.Range(0, 10).Reverse().Select(i => "u" + i.ToString()).ToArray(),
                f.LogginnedUsers.ToArray());

            f.AddLogin("sa");
            CollectionAssert.AreEqual(
                new string[] { "sa" }.Concat(Enumerable.Range(1, 9).Reverse().Select(i => "u" + i.ToString())).ToArray(),
                f.LogginnedUsers.ToArray());
        }

        [TestMethod]
        public void ReadFileError() {
            File.WriteAllText(files.Path + files.LogginnedHistory, "123");

            LoginnedHistory f = new LoginnedHistory(files);
            Assert.IsFalse(f.LogginnedUsers.Any());

            f.AddLogin("sa");
            CollectionAssert.AreEqual(new string[] { "sa" }, f.LogginnedUsers.ToArray());
        }
    }
}
