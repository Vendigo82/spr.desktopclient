﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using SPR.UI.App.Abstractions;
using SPR.UI.App.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Base.Tests
{
    [TestClass]
    public class PartialContentVMTests
    {
        readonly Mock<PartialContentVM<string>> vm;
        readonly Fixture fixture = new Fixture();

        public PartialContentVMTests()
        {
            vm = new Mock<PartialContentVM<string>>((ILogger)null);
            vm.Protected().Setup("Refresh").CallBase();
            //vm.Protected().Setup("RefreshRoutine").CallBase();
            vm.Setup(f => f.Initialize(It.IsAny<bool>())).CallBase();
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task Initial_Test(bool exhausted)
        {
            // setup
            var items = fixture.CreateMany<string>();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = exhausted,
                    Items = items
                });

            // action            
            await vm.Object.Initialize(true);
            await vm.Object.WaitBusy();

            // asserts
            vm.Object.Should().WithoutError();
            vm.Object.Items.Should().Equal(items);
            vm.Object.LoadNextCmd.CanExecute(null).Should().Be(!exhausted);
            vm.Object.LoadAllCmd.CanExecute(null).Should().Be(!exhausted);
            vm.Object.IsLoadCompleted.Should().Be(exhausted);

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Once(), ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }

        [TestMethod]
        public async Task Refresh_Test()
        {
            // setup
            var initItems = fixture.CreateMany<string>();
            var refreshItems = fixture.CreateMany<string>();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = initItems
                });

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.Is<string>(i => i == initItems.First()), ItExpr.Is<bool>(i => i == true), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = true,
                    Items = refreshItems
                });

            await vm.Object.InitAndWait();

            // action
            vm.Object.RefreshCmd.Execute(null);
            await vm.Object.WaitBusy();

            // asserts
            vm.Object.Should().WithoutError();
            vm.Object.Items.Should().Equal(refreshItems.Concat(initItems));

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Exactly(2), ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task LongRefresh_Test(bool lastExhausted)
        {
            // setup
            var initItems = fixture.CreateMany<string>();
            var refreshItems = fixture.CreateMany<string>();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = initItems
                });

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.Is<string>(i => i == initItems.First()), ItExpr.Is<bool>(i => i == true), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = refreshItems
                });

            vm.Protected()
            .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.Is<string>(i => i == refreshItems.First()), ItExpr.Is<bool>(i => i == true), ItExpr.IsAny<CancellationToken>())
            .ReturnsAsync(new PartialListContainer<string> {
                Exhausted = lastExhausted,
                Items = new string[] { }
            });

            await vm.Object.InitAndWait();

            // action
            vm.Object.RefreshCmd.Execute(null);
            await vm.Object.WaitBusy();

            // asserts
            vm.Object.Should().WithoutError();
            vm.Object.Items.Should().Equal(refreshItems.Concat(initItems));
            vm.Object.LoadNextCmd.CanExecute(null).Should().BeTrue();
            vm.Object.LoadAllCmd.CanExecute(null).Should().BeTrue();
            vm.Object.IsLoadCompleted.Should().BeFalse();

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Exactly(3), ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }   
        
        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task LoadNext_Test(bool nextExhausted)
        {
            // setup
            var initItems = fixture.CreateMany<string>();
            var nextItems = fixture.CreateMany<string>();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = initItems
                });

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.Is<string>(i => i == initItems.Last()), ItExpr.Is<bool>(i => i == false), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = nextExhausted,
                    Items = nextItems
                });

            await vm.Object.InitAndWait();

            // action
            vm.Object.LoadNextCmd.Execute(null);
            await vm.Object.WaitBusy();

            // asserts
            vm.Object.Should().WithoutError();
            vm.Object.Items.Should().Equal(initItems.Concat(nextItems));
            vm.Object.LoadNextCmd.CanExecute(null).Should().Be(!nextExhausted);
            vm.Object.LoadAllCmd.CanExecute(null).Should().Be(!nextExhausted);
            vm.Object.IsLoadCompleted.Should().Be(nextExhausted);

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Exactly(2), ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }

        [TestMethod]
        public async Task LoadAll_Test()
        {
            // setup
            var initItems = fixture.CreateMany<string>();
            var nextItems = fixture.CreateMany<string>();
            var next2Items = fixture.CreateMany<string>();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = initItems
                });

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.Is<string>(i => i == initItems.Last()), ItExpr.Is<bool>(i => i == false), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = nextItems
                });

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.Is<string>(i => i == nextItems.Last()), ItExpr.Is<bool>(i => i == false), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = true,
                    Items = next2Items
                });

            await vm.Object.InitAndWait();

            // action
            vm.Object.LoadAllCmd.Execute(null);
            await vm.Object.WaitBusy();

            // asserts
            vm.Object.Should().WithoutError();
            vm.Object.Items.Should().Equal(initItems.Concat(nextItems).Concat(next2Items));
            vm.Object.LoadNextCmd.CanExecute(null).Should().BeFalse();
            vm.Object.LoadAllCmd.CanExecute(null).Should().BeFalse();
            vm.Object.IsLoadCompleted.Should().BeTrue();

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Exactly(3), ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task Reload_Test(bool exhausted)
        {
            // setup
            var initItems = fixture.CreateMany<string>();
            var newItems = fixture.CreateMany<string>();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = initItems
                });

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = exhausted,
                    Items = newItems
                });

            await vm.Object.InitAndWait();

            // action
            await vm.Object.Reload();

            // asserts
            vm.Object.Should().WithoutError();
            vm.Object.Items.Should().Equal(newItems);
            vm.Object.LoadNextCmd.CanExecute(null).Should().Be(!exhausted);
            vm.Object.LoadAllCmd.CanExecute(null).Should().Be(!exhausted);
            vm.Object.IsLoadCompleted.Should().Be(exhausted);

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Exactly(2), ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }

        [TestMethod]
        public async Task Reload_Error_Test()
        {
            // setup
            var initItems = fixture.CreateMany<string>();
            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = initItems
                });

            await vm.Object.InitAndWait();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ThrowsAsync(new ApplicationException());

            // action
            await vm.Object.Reload();

            // asserts
            vm.Object.Items.Should().Equal(initItems);
            vm.Object.Should().HasError();

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Exactly(2), ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }

        [TestMethod]
        public async Task RefreshError_Test()
        {
            // setup
            var initItems = fixture.CreateMany<string>();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = initItems
                });

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.Is<string>(i => i == initItems.First()), ItExpr.Is<bool>(i => i == true), ItExpr.IsAny<CancellationToken>())
                .ThrowsAsync(new ApplicationException());

            await vm.Object.InitAndWait();

            // action
            vm.Object.RefreshCmd.Execute(null);
            await vm.Object.WaitBusy();

            // asserts
            vm.Object.Should().HasError();
            vm.Object.Items.Should().Equal(initItems);

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Exactly(2), ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }

        [TestMethod]
        public async Task LoadNext_Error_Test()
        {
            // setup
            var initItems = fixture.CreateMany<string>();
            var nextItems = fixture.CreateMany<string>();

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.IsNull<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new PartialListContainer<string> {
                    Exhausted = false,
                    Items = initItems
                });

            vm.Protected()
                .Setup<Task<PartialListContainer<string>>>("LoadRoutine", ItExpr.Is<string>(i => i == initItems.Last()), ItExpr.Is<bool>(i => i == false), ItExpr.IsAny<CancellationToken>())
                .ThrowsAsync(new ApplicationException());

            await vm.Object.InitAndWait();

            // action
            vm.Object.LoadNextCmd.Execute(null);
            await vm.Object.WaitBusy();

            // asserts
            vm.Object.Should().HasError();
            vm.Object.Items.Should().Equal(initItems);
            vm.Object.LoadNextCmd.CanExecute(null).Should().BeTrue();
            vm.Object.LoadAllCmd.CanExecute(null).Should().BeTrue();
            vm.Object.IsLoadCompleted.Should().BeFalse();

            vm.Protected()
                .Verify<Task<PartialListContainer<string>>>("LoadRoutine", Times.Exactly(2), ItExpr.IsAny<string>(), ItExpr.IsAny<bool>(), ItExpr.IsAny<CancellationToken>());
        }
    }
}
