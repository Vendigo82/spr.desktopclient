﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Services;
using Moq;
using AutoFixture;
using FluentAssertions;

namespace SPR.UI.App.VM.Base.Tests
{
    using Abstractions;
    using SPR.UI.App.Abstractions.DataSources;
    using SPR.UI.App.Abstractions.UI;
    using System.Linq;

    [TestClass]
    public class ObjectsListVMTests
    {
        public class Wrapper
        {
            public int Val { get; set; }
        }

        public class VM : ObjectsListVM<int, Wrapper>
        {
            public VM(IDataListSource<int> source) : base(source, (i) => new Wrapper { Val = i }, null)
            {
            }

            public (Mock<IDeleteOperation<int>>, Mock<IWindowFactory>, Mock<IDeleteStringsProvider>) ConfigureDelete(
                MockRepository repository,
                Func<int, bool> canExecute)
            {
                var fakeDelete = repository.Create<IDeleteOperation<int>>();
                var fakeWndFactory = repository.Create<IWindowFactory>();
                var fakeString = repository.Create<IDeleteStringsProvider>();

                ConfigureDelete(fakeDelete.Object, (o) => o.Val, canExecute, fakeWndFactory.Object, fakeString.Object);
                return (fakeDelete, fakeWndFactory, fakeString);
            }
        }

        readonly Fixture fixture;
        readonly MockRepository repository;
        readonly Mock<IDataListSource<int>> fakeSource;
        readonly VM vm;

        public ObjectsListVMTests()
        {
            fixture = new Fixture();

            repository = new MockRepository(MockBehavior.Default);
            fakeSource = repository.Create<IDataListSource<int>>();
            fakeSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(fixture.CreateMany<int>());

            vm = new VM(fakeSource.Object);
        }

        [TestMethod]
        public async Task Inititialize_Fresh_Test()
        {
            // setup
            var sourceItems = fixture.CreateMany<int>();
            //load items with fresh call
            fakeSource.Setup(f => f.LoadItemsAsync(default)).ReturnsAsync(sourceItems);

            // action
            await vm.Initialize(true);

            // asserts
            vm.Items.Should()
                .NotBeNullOrEmpty().And
                .HaveSameCount(sourceItems);
            vm.Items.Select(i => i.Val).Should().Equal(sourceItems);
            vm.SelectedItem.Should().BeNull();
            vm.Should().WithoutError();

            fakeSource.Verify(f => f.LoadItemsAsync(default), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task Initialize_Cached_Test()
        {
            // setup
            var sourceItems = fixture.CreateMany<int>();
            //get items without fresh call
            fakeSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(sourceItems);

            // action
            await vm.Initialize(false);

            // asserts
            vm.Items.Should()
                .NotBeNullOrEmpty().And
                .HaveSameCount(sourceItems);
            vm.Items.Select(i => i.Val).Should().Equal(sourceItems);
            vm.SelectedItem.Should().BeNull();
            vm.Should().WithoutError();

            fakeSource.Verify(f => f.GetItemsAsync(default), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task DeleteCmd_WithoutConfigure_Test()
        {
            // setup
            await vm.InitAndWait();

            // asserts
            vm.DeleteCmd.Should().BeNull();
        }

        [TestMethod]
        public void DeleteCmd_Configure_Test()
        {
            // action
            vm.ConfigureDelete(repository, null);

            // asserts
            vm.DeleteCmd.Should().NotBeNull();
        }

        [TestMethod]
        [DataRow(false, null, false)]
        [DataRow(true, null, true)]
        [DataRow(true, false, false)]
        [DataRow(true, true, true)]
        public async Task DeleteCmd_CanExecute_Test(bool select, bool? canExecuteAdditionalCondition, bool expected)
        {
            // setup
            await vm.InitAndWait();
            if (canExecuteAdditionalCondition != null)
                vm.ConfigureDelete(repository, (i) => canExecuteAdditionalCondition.Value);
            else
                vm.ConfigureDelete(repository, null);

            if (select)
                vm.SelectedItem = vm.Items.First();

            // action
            bool result = vm.DeleteCmd.CanExecute(null);

            // asserts
            result.Should().Be(expected);
        }

        /// <summary>
        /// Change selection without delete command should not be fails
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DeleteCmd_SelectionChangedWithoutDeleteCmd_Test()
        {
            // setup
            await vm.InitAndWait();
            
            // action
            Action action = () => vm.SelectedItem = vm.Items.First();

            // asserts: not fails
            action.Should().NotThrow();
        }

        [TestMethod]
        public async Task Delete_Success_Test()
        {
            // setup
            await vm.InitAndWait();
            var sequence = new MockSequence();

            (var fakeDelete, var fakeWndFactory, var fakeStrings) = vm.ConfigureDelete(repository, null);
            fakeStrings.InSequence(sequence).Setup(f => f.Confirm(It.IsAny<int>())).Returns("C");
            fakeWndFactory.InSequence(sequence).Setup(f => f.ShowConfirmDialog("C", It.IsAny<string>())).Returns(true);
            fakeDelete.InSequence(sequence).Setup(f => f.DeleteAsync(It.IsAny<int>(), default)).ReturnsAsync(true);

            vm.SelectedItem = vm.Items.First();
            int item = vm.SelectedItem.Val;

            // action
            vm.DeleteCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();

            fakeSource.VerifyAll();
            fakeStrings.Verify(f => f.Confirm(item), Times.Once);
            fakeWndFactory.Verify(f => f.ShowConfirmDialog("C", It.IsAny<string>()), Times.Once);
            fakeDelete.Verify(f => f.DeleteAsync(item, default), Times.Once);
            repository.VerifyNoOtherCalls();

            vm.Items.Should().NotContain(i => i.Val == item);
        }

        [TestMethod]
        public async Task Delete_RejectedByUser_Test()
        {
            // setup
            await vm.InitAndWait();
            var sequence = new MockSequence();

            (var fakeDelete, var fakeWndFactory, var fakeStrings) = vm.ConfigureDelete(repository, null);
            fakeStrings.InSequence(sequence).Setup(f => f.Confirm(It.IsAny<int>())).Returns("C");
            fakeWndFactory.InSequence(sequence).Setup(f => f.ShowConfirmDialog("C", It.IsAny<string>())).Returns(false);

            vm.SelectedItem = vm.Items.First();
            int item = vm.SelectedItem.Val;

            // action
            vm.DeleteCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();

            fakeSource.VerifyAll();
            fakeStrings.Verify(f => f.Confirm(item), Times.Once);
            fakeWndFactory.Verify(f => f.ShowConfirmDialog("C", It.IsAny<string>()), Times.Once);
            repository.VerifyNoOtherCalls();

            vm.Items.Should().Contain(i => i.Val == item);
        }

        [TestMethod]
        public async Task Delete_RejectedByServer_Test()
        {
            // setup
            await vm.InitAndWait();
            var sequence = new MockSequence();

            (var fakeDelete, var fakeWndFactory, var fakeStrings) = vm.ConfigureDelete(repository, null);
            fakeStrings.InSequence(sequence).Setup(f => f.Confirm(It.IsAny<int>())).Returns("C");
            fakeWndFactory.InSequence(sequence).Setup(f => f.ShowConfirmDialog("C", It.IsAny<string>())).Returns(true);
            fakeDelete.InSequence(sequence).Setup(f => f.DeleteAsync(It.IsAny<int>(), default)).ReturnsAsync(false);
            fakeStrings.InSequence(sequence).Setup(f => f.Reject(It.IsAny<int>())).Returns("R");
            fakeWndFactory.InSequence(sequence).Setup(f => f.ShowInformationDialog("R", It.IsAny<string>()));

            vm.SelectedItem = vm.Items.First();
            int item = vm.SelectedItem.Val;

            // action
            vm.DeleteCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();

            fakeSource.VerifyAll();
            fakeStrings.Verify(f => f.Confirm(item), Times.Once);
            fakeWndFactory.Verify(f => f.ShowConfirmDialog("C", It.IsAny<string>()), Times.Once);
            fakeDelete.Verify(f => f.DeleteAsync(item, default), Times.Once);
            fakeStrings.Verify(f => f.Reject(item), Times.Once);
            fakeWndFactory.Verify(f => f.ShowInformationDialog("R", It.IsAny<string>()), Times.Once);
            repository.VerifyNoOtherCalls();

            vm.Items.Should().Contain(i => i.Val == item);
        }

        [TestMethod]
        public async Task Delete_Exception_Test()
        {
            // setup
            await vm.InitAndWait();
            var sequence = new MockSequence();

            (var fakeDelete, var fakeWndFactory, var fakeStrings) = vm.ConfigureDelete(repository, null);
            //fakeStrings.InSequence(sequence).Setup(f => f.Confirm(It.IsAny<int>())).Returns("C");
            fakeWndFactory.InSequence(sequence).Setup(f => f.ShowConfirmDialog(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            fakeDelete.InSequence(sequence).Setup(f => f.DeleteAsync(It.IsAny<int>(), default))
                .ThrowsAsync(new InvalidOperationException("Test exception"));

            vm.SelectedItem = vm.Items.First();
            int item = vm.SelectedItem.Val;

            // action
            vm.DeleteCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().HasError();

            fakeSource.VerifyAll();
            fakeDelete.Verify(f => f.DeleteAsync(item, default), Times.Once);

            vm.Items.Should().Contain(i => i.Val == item);
        }
    }
}
