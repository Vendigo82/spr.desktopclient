﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.App.Services;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Dirs.Tests
{
    [TestClass]
    public class DirBaseVMTests
    {
        [TestMethod]
        public async Task InitialRefreshTest() {
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                });
            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();
            
            Assert.IsNotNull(vm.Items);
            Assert.IsFalse(vm.IsChanged);
            CollectionAssert.AreEquivalent(new int[] { 1, 2 },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Code).ToArray());
            CollectionAssert.AreEquivalent(new string[] { "a", "b" },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Value).ToArray());
        }

        [TestMethod]
        public async Task Initialize_NotFresh_Test() {
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                });
            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.Initialize(false);
        }

        [TestMethod]
        public async Task UpdateItemTest() {
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                },
                update: (list) => {
                    Assert.AreEqual(1, list.Count());
                    var item = list.First();
                    Assert.AreEqual(1, item.Id);
                    Assert.AreEqual(BunchOperation.Update, item.Operation);
                    Assert.AreEqual(5, item.Item.Code);
                    Assert.AreEqual("f", item.Item.Value);

                    return new FakeDirItem[] {
                        new FakeDirItem() { Code = 5, Value = "f" },
                        new FakeDirItem() { Code = 2, Value = "b" }
                    };
                });
            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();

            var wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Id == 1).Single();
            wrapItem.Code = 5;
            wrapItem.Value = "f";

            Assert.IsTrue(vm.IsChanged);
            await vm.SaveAndWait();

            Assert.IsNotNull(vm.Items);
            Assert.IsFalse(vm.IsChanged);
            CollectionAssert.AreEquivalent(new int[] { 5, 2 },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Code).ToArray());
            CollectionAssert.AreEquivalent(new string[] { "f", "b" },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Value).ToArray());
        }

        [TestMethod]
        public async Task InsertItemTest() {
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                },
                update: (list) => {
                    Assert.AreEqual(1, list.Count());
                    var item = list.First();
                    Assert.AreEqual(0, item.Id); //key is default, because for insert operation key is not relevant
                    Assert.AreEqual(BunchOperation.Insert, item.Operation);
                    Assert.AreEqual(3, item.Item.Code);
                    Assert.AreEqual("c", item.Item.Value);

                    return new FakeDirItem[] {
                        new FakeDirItem() { Code = 1, Value = "a" },
                        new FakeDirItem() { Code = 2, Value = "b" },
                        new FakeDirItem() { Code = 3, Value = "c" }
                    };
                });
            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();

            vm.Items.Add(new FakeDirBaseVM.Wrapper(new FakeDirItem() { Code = 3, Value = "c" }));

            Assert.IsTrue(vm.IsChanged);
            await vm.SaveAndWait();

            Assert.IsNotNull(vm.Items);
            Assert.IsFalse(vm.IsChanged);
            CollectionAssert.AreEquivalent(new int[] { 1, 2, 3 },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Code).ToArray());
            CollectionAssert.AreEquivalent(new string[] { "a", "b", "c" },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Value).ToArray());
        }

        [TestMethod]
        public async Task DeleteItemTest() {
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                },
                update: (list) => {
                    Assert.AreEqual(1, list.Count());
                    var item = list.First();
                    Assert.AreEqual(1, item.Id);
                    Assert.AreEqual(BunchOperation.Delete, item.Operation);
                    Assert.IsNull(item.Item);   //item is null, because for delete operation item is not relevant

                    return new FakeDirItem[] {
                        new FakeDirItem() { Code = 2, Value = "b" },
                    };
                });
            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();

            var wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Id == 1).Single();
            vm.Items.Remove(wrapItem);

            Assert.IsTrue(vm.IsChanged);
            await vm.SaveAndWait();

            Assert.IsNotNull(vm.Items);
            Assert.IsFalse(vm.IsChanged);
            CollectionAssert.AreEquivalent(new int[] { 2 },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Code).ToArray());
            CollectionAssert.AreEquivalent(new string[] { "b" },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Value).ToArray());
        }

        [TestMethod]
        public async Task UpdateThenDelete_Test() {
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                },
                update: (list) => {
                    Assert.AreEqual(1, list.Count());
                    var item = list.First();
                    Assert.AreEqual(1, item.Id);
                    Assert.AreEqual(BunchOperation.Delete, item.Operation);
                    Assert.IsNull(item.Item);   //item is null, because for delete operation item is not relevant

                    return new FakeDirItem[] {
                        new FakeDirItem() { Code = 2, Value = "b" },
                    };
                });
            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();

            var wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Id == 1).Single();
            //update
            wrapItem.Code = 5;
            wrapItem.Value = "f";
            Assert.IsTrue(vm.IsChanged);
            //then remove just updated item
            vm.Items.Remove(wrapItem);

            Assert.IsTrue(vm.IsChanged);
            await vm.SaveAndWait();

            Assert.IsNotNull(vm.Items);
            Assert.IsFalse(vm.IsChanged);
            CollectionAssert.AreEquivalent(new int[] { 2 },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Code).ToArray());
            CollectionAssert.AreEquivalent(new string[] { "b" },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Value).ToArray());
        }

        [TestMethod]
        public async Task InsertThenDelete_Test() {
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                },
                update: (list) => {
                    throw new Exception("must be not items for update");
                });

            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();

            //insert
            vm.Items.Add(new FakeDirBaseVM.Wrapper(new FakeDirItem() { Code = 3, Value = "c" }));
            Assert.IsTrue(vm.IsChanged);

            var wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Code == 3).Single();
            //then remove just added item
            vm.Items.Remove(wrapItem);

            //no changes! because we deleted just added item
            Assert.IsFalse(vm.IsChanged);
        }

        [TestMethod]
        public async Task InsertThenUpdate_Test() {
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                },
                update: (list) => {
                    Assert.AreEqual(1, list.Count());
                    var item = list.First();
                    Assert.AreEqual(0, item.Id); //key is default, because for insert operation key is not relevant
                    Assert.AreEqual(BunchOperation.Insert, item.Operation);
                    Assert.AreEqual(3, item.Item.Code);
                    Assert.AreEqual("d", item.Item.Value);

                    return new FakeDirItem[] {
                        new FakeDirItem() { Code = 1, Value = "a" },
                        new FakeDirItem() { Code = 2, Value = "b" },
                        new FakeDirItem() { Code = 3, Value = "d" }
                    };
                });
            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();

            //add item
            vm.Items.Add(new FakeDirBaseVM.Wrapper(new FakeDirItem() { Code = 3, Value = "c" }));
            Assert.IsTrue(vm.IsChanged);

            //edit just added item
            var wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Code == 3).Single();
            wrapItem.Value = "d";

            Assert.IsTrue(vm.IsChanged);
            await vm.SaveAndWait();

            Assert.IsNotNull(vm.Items);
            Assert.IsFalse(vm.IsChanged);
            CollectionAssert.AreEquivalent(new int[] { 1, 2, 3 },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Code).ToArray());
            CollectionAssert.AreEquivalent(new string[] { "a", "b", "d" },
                vm.Items.Cast<FakeDirBaseVM.Wrapper>().Select(i => i.Value).ToArray());
        }

        /// <summary>
        /// insert item, update another item, and remove inserted item.
        /// check IsChanged flag is true
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Insert_UpdateAnother_Delete_Test() {
            bool performed = false;
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                },
                update: (list) => {
                    Assert.AreEqual(1, list.Count());
                    var item = list.First();
                    Assert.AreEqual(2, item.Id);
                    Assert.AreEqual("f", item.Item.Value);
                    Assert.AreEqual(BunchOperation.Update, item.Operation);
                    performed = true;
                    return new FakeDirItem[] { };
                });

            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();

            //insert
            vm.Items.Add(new FakeDirBaseVM.Wrapper(new FakeDirItem() { Code = 3, Value = "c" }));
            Assert.IsTrue(vm.IsChanged);

            //update another item
            var wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Code == 2).Single();
            wrapItem.Value = "f";

            //then remove added item
            wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Code == 3).Single();
            vm.Items.Remove(wrapItem);

            Assert.IsTrue(vm.IsChanged);
            await vm.SaveAndWait();
            Assert.IsFalse(vm.IsChanged);
            Assert.IsTrue(performed);
        }

        /// <summary>
        /// insert item, delete another item, and remove inserted item.
        /// check IsChanged flag is true
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Insert_DeleteAnother_Delete_Test() {
            bool performed = false;
            var service = new FakeDictionaryService<int, FakeDirItem>(
                getItems: () => new FakeDirItem[] {
                    new FakeDirItem() { Code = 1, Value = "a" },
                    new FakeDirItem() { Code = 2, Value = "b" }
                },
                update: (list) => {
                    Assert.AreEqual(1, list.Count());
                    var item = list.First();
                    Assert.AreEqual(2, item.Id);
                    Assert.AreEqual(BunchOperation.Delete, item.Operation);
                    performed = true;
                    return new FakeDirItem[] { };
                });

            FakeDirBaseVM vm = new FakeDirBaseVM(service);
            await vm.InitAndWait();

            //insert
            vm.Items.Add(new FakeDirBaseVM.Wrapper(new FakeDirItem() { Code = 3, Value = "c" }));
            Assert.IsTrue(vm.IsChanged);

            //update another item
            var wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Code == 2).Single();
            vm.Items.Remove(wrapItem);

            //then remove added item
            wrapItem = vm.Items.Cast<FakeDirBaseVM.Wrapper>().Where(i => i.Code == 3).Single();
            vm.Items.Remove(wrapItem);

            Assert.IsTrue(vm.IsChanged);
            await vm.SaveAndWait();
            Assert.IsFalse(vm.IsChanged);
            Assert.IsTrue(performed);
        }
    }
}
