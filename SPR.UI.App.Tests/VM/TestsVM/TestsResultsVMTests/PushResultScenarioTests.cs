﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.DataContract;
using SPR.WebService.TestModule.Client.DataContract.Response;
using SPR.WebService.TestModule.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Tests.TestsResultsVMTests
{
    [TestClass]
    public class PushResultScenarioTests : BaseTests
    {
        [TestMethod]
        public void SuccessResult_Test()
        {
            // setup
            var response = fixture.Build<TestResponse<ScenarioResultDto>>()
                .Without(i => i.Error)
                .With(i => i.Success, true)
                .Without(i => i.PostConditions)
                .Create();
            var id = fixture.Create<long>();
            var shownObject = fixture.Create<ShownObjectEnum>();

            // action
            vm.StartTesting(ObjectType.Scenario, id, shownObject);
            vm.PushResult(id, shownObject, response);

            // asserts
            (vm as object).Should().BeEquivalentTo(response, o => o.Excluding(i => i.Data).Excluding(i => i.IfAnyErrors).Excluding(p => p.PostConditions));
            (vm as object).Should().BeEquivalentTo(response.Data, o => o.Excluding(i => i.Values));
            vm.ScenarioResults.Should().BeEquivalentTo(response.Data.Values);

            vm.AdditionalData.Should().BeNull();
            vm.ResultValue.Should().BeNull();
            vm.ResultType.Should().BeNull();

            vm.AdditionalData.Should().BeNull();
            vm.Error.Should().BeNull();
            vm.InProgress.Should().BeFalse();

            vm.ObjectId.Should().Be(id);
            vm.ObjectType.Should().Be(ObjectType.Scenario);
            vm.ShownObject.Should().Be(shownObject);
            vm.Success.Should().BeTrue();            
            vm.Timestamp.Should().BeCloseTo(DateTime.Now, TimeSpan.FromSeconds(1));

            vm.PostConditionsIssues.Should().BeEmpty();
            vm.HavePostConditionsError.Should().BeFalse();
        }

        [TestMethod]
        public void WithPostConditions_SuccessResult_Test()
        {
            // setup
            var response = fixture.Build<TestResponse<ScenarioResultDto>>()
                .Without(i => i.Error)
                .With(i => i.Success, true)
                .Create();
            var id = fixture.Create<long>();
            var shownObject = fixture.Create<ShownObjectEnum>();

            // action
            vm.StartTesting(ObjectType.Scenario, id, shownObject);
            vm.PushResult(id, shownObject, response);

            // asserts            
            vm.PostConditionsIssues.Should().BeEquivalentTo(response.PostConditions);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public void WithPostConditions_HaveErrors_SuccessResult_Test(bool isError)
        {
            // setup
            var response = fixture.Build<TestResponse<ScenarioResultDto>>()
                .Without(i => i.Error)
                .With(i => i.Success, true)
                .With(i => i.PostConditions, () => new[] { new PostConditionIssueDto { IsError = isError, Message = "" } })
                .Create();
            var id = fixture.Create<long>();
            var shownObject = fixture.Create<ShownObjectEnum>();

            // action
            vm.StartTesting(ObjectType.Scenario, id, shownObject);
            vm.PushResult(id, shownObject, response);

            // asserts            
            vm.PostConditionsIssues.Should().BeEquivalentTo(response.PostConditions);
            vm.HavePostConditionsError.Should().Be(isError);
        }

        [TestMethod]
        public void ErrorResult_Test()
        {
            // setup
            var response = fixture.Build<TestResponse<ScenarioResultDto>>().Without(i => i.Data).With(i => i.Success, false).Without(i => i.PostConditions).Create();
            var id = fixture.Create<long>();
            var shownObject = fixture.Create<ShownObjectEnum>();

            // action
            vm.StartTesting(ObjectType.Scenario, id, shownObject);
            vm.PushResult(id, shownObject, response);

            // asserts
            (vm as object).Should().BeEquivalentTo(response, o => o.Excluding(i => i.Data).Excluding(i => i.IfAnyErrors).Excluding(p => p.PostConditions));
            vm.ScenarioResults.Should().BeNull();
            vm.Passed.Should().BeNull();
            vm.NextStep.Should().BeNull();
            vm.RejectCodes.Should().BeNull();

            vm.AdditionalData.Should().BeNull();
            vm.ResultValue.Should().BeNull();
            vm.ResultType.Should().BeNull();

            vm.AdditionalData.Should().BeNull();
            vm.Error.Should().NotBeNull();
            vm.InProgress.Should().BeFalse();

            vm.ObjectId.Should().Be(id);
            vm.ObjectType.Should().Be(ObjectType.Scenario);
            vm.ShownObject.Should().Be(shownObject);
            vm.Success.Should().BeFalse();
            vm.Timestamp.Should().BeCloseTo(DateTime.Now, TimeSpan.FromSeconds(1));
        }
    }
}
