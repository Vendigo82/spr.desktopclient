﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.DataContract;
using SPR.WebService.TestModule.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Tests.TestsResultsVMTests
{
    [TestClass]
    public class PushResultFormulaTests : BaseTests
    {
        [TestMethod]
        public void SuccessResult_Test()
        {
            // setup
            var response = fixture.Build<TestFormulaResponse>().Without(i => i.Error).With(i => i.Success, true).Create();
            var id = fixture.Create<long>();
            var shownObject = fixture.Create<ShownObjectEnum>();

            // action
            vm.StartTesting(ObjectType.Formula, id, shownObject);
            vm.PushResult(id, shownObject, response, null);

            // asserts
            (vm as object).Should().BeEquivalentTo(response, o => o
                .Excluding(i => i.Result)
                .Excluding(i => i.Type)
                .Excluding(i => i.Data)
                .Excluding(i => i.IfAnyErrors)
                .Excluding(i => i.PostConditions));

            vm.AdditionalData.Should().BeNull();
            vm.ResultValue.Should().BeSameAs(response.Result);
            vm.ResultType.Should().Be(response.Type);

            vm.AdditionalData.Should().BeNull();
            vm.Error.Should().BeNull();
            vm.InProgress.Should().BeFalse();
            vm.NextStep.Should().BeNull();
            vm.ObjectId.Should().Be(id);
            vm.ObjectType.Should().Be(ObjectType.Formula);
            vm.Passed.Should().BeNull();
            vm.ShownObject.Should().Be(shownObject);
            vm.Success.Should().BeTrue();
            vm.RejectCodes.Should().BeNull();
            vm.Timestamp.Should().BeCloseTo(DateTime.Now, TimeSpan.FromSeconds(1));

            vm.PostConditionsIssues.Should().BeEmpty();
        }

        [TestMethod]
        public void ErrorResult_Test()
        {
            // setup
            var response = fixture.Build<TestFormulaResponse>()
                .Without(i => i.Data)
                .Without(i => i.Result)
                .Without(i => i.Type)
                .With(i => i.Success, false)
                .Create();
            var id = fixture.Create<long>();
            var shownObject = fixture.Create<ShownObjectEnum>();

            // action
            vm.StartTesting(ObjectType.Formula, id, shownObject);
            vm.PushResult(id, shownObject, response, null);

            // asserts
            (vm as object).Should().BeEquivalentTo(response, o => o
                .Excluding(i => i.Result)
                .Excluding(i => i.Type)
                .Excluding(i => i.Data)
                .Excluding(i => i.IfAnyErrors)
                .Excluding(i => i.PostConditions));

            vm.AdditionalData.Should().BeNull();
            vm.ResultValue.Should().BeNull();
            vm.ResultType.Should().BeNull();

            vm.AdditionalData.Should().BeNull();
            vm.Error.Should().NotBeNull();
            vm.InProgress.Should().BeFalse();
            vm.NextStep.Should().BeNull();
            vm.ObjectId.Should().Be(id);
            vm.ObjectType.Should().Be(ObjectType.Formula);
            vm.Passed.Should().BeNull();
            vm.ShownObject.Should().Be(shownObject);
            vm.Success.Should().BeFalse();
            vm.Timestamp.Should().BeCloseTo(DateTime.Now, TimeSpan.FromSeconds(1));
        }
    }
}
