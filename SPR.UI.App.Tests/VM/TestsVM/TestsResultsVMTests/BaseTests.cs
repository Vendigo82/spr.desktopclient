﻿using AutoFixture;
using SPR.UI.App.Customizations;
using System.Linq;

namespace SPR.UI.App.VM.Tests.TestsResultsVMTests
{
    public class BaseTests
    {
        protected readonly Fixture fixture = new Fixture();
        protected readonly TestResultsVM vm = new TestResultsVM();

        public BaseTests()
        {
            fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => fixture.Behaviors.Remove(b));
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            fixture.Customize(new JTokenCustomization());

        }
    }
}
