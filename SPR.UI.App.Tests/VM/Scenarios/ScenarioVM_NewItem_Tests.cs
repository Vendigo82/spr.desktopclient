﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using AutoFixture;
using FluentAssertions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.App.Services;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.UI;

namespace SPR.UI.App.VM.Scenarios.Tests
{
    [TestClass]
    public class ScenarioVM_NewItem_Tests
    {
        readonly Mock<IScenarioSource> fakeSource;
        readonly Mock<IDataListSource<StdResultSlimModel>> fakeStdResultsSource;
        readonly Mock<IWindowFactory> fakeWndFactory;
        readonly Mock<ScenarioVM.IStringsProvider> fakeStrings;
        readonly MockRepository repository;
        readonly Fixture fixture;

        readonly ScenarioVM vm;

        public ScenarioVM_NewItem_Tests()
        {
            fixture = new Fixture();

            repository = new MockRepository(MockBehavior.Default);
            fakeSource = repository.Create<IScenarioSource>();
            fakeStdResultsSource = repository.Create<IDataListSource<StdResultSlimModel>>();
            fakeWndFactory = repository.Create<IWindowFactory>();
            fakeStrings = repository.Create<ScenarioVM.IStringsProvider>();

            vm = new ScenarioVM(
                null,
                fakeSource.Object,
                fakeStdResultsSource.Object,
                new Mock<ILogger>().Object,
                new Mock<IConfirmActionCommentProvider>().Object,
                fakeWndFactory.Object,
                fakeStrings.Object
                );
        }

        [TestMethod]
        public async Task InitialModelValues()
        {
            // setup            

            // action
            await vm.InitAndWait();

            // asserts
            vm.Item.Enabled.Should().BeFalse();
            vm.Item.Guid.Should().NotBeEmpty();
            vm.Item.Quota.Should().Be(100);
            vm.Item.CalcResults.Should().NotBeNull();
            vm.Item.Checkups.Should().NotBeNull();
        }

        /// <summary>
        /// If standard results list has not default item, then model's standard result should be nul;;
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Init_WithoutDefaultStdResult_Model()
        {
            // setup
            var list = fixture.Build<StdResultSlimModel>().Without(i => i.Default).CreateMany(2);
            fakeStdResultsSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(list);

            // action
            await vm.InitAndWait();

            // asserts
            vm.StdResultsList.Should().NotBeEmpty();
            vm.StdResult.Should().BeNull();
            vm.StdResultId.Should().BeNull();
            vm.Item.StdResult.Should().BeNull();
        }

        /// <summary>
        /// Default standard result should be assigned to model after initialization
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task Init_WithDefaultStdResult_Model()
        {
            // setup
            var list = fixture.Build<StdResultSlimModel>().Without(i => i.Default).CreateMany(2);
            var defValue = list.First();
            defValue.Default = true;
            list.First().Default = true;
            fakeStdResultsSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(list);

            // action
            await vm.InitAndWait();

            // asserts
            vm.StdResultsList.Should().NotBeEmpty();
            vm.StdResult.Should()
                .NotBeNull().And
                .BeEquivalentTo(defValue, o => o.Excluding(i => i.Usage).Excluding(i => i.Default));
            vm.StdResultId.Should().Be(defValue.Id);
            vm.Item.StdResult.Should()
                .NotBeNull().And
                .BeEquivalentTo(defValue, o => o.Excluding(i => i.Usage).Excluding(i => i.Default));
        }
    }
}
