﻿using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using FluentAssertions;
using AutoFixture;
using SPR.UI.App.Abstractions.DataSources;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Dir;
using System.Threading;

namespace SPR.UI.App.VM.Scenarios.Tests
{
    [TestClass]
    public class ScenarioMainListVMTests
    {
        readonly MockRepository repository = new MockRepository(MockBehavior.Default);
        readonly Mock<IDataListFilterSource<ScenarioBriefModel, ScenarioFilter>> fakeSource;
        readonly Mock<IDataListSource<StepModel>> stepSource;
        readonly Mock<IDeleteOperation<ScenarioBriefModel>> fakeDelete;
        readonly Mock<IWindowFactory> fakeWndFactory;
        readonly Mock<IResourceStringProvider> fakeResources;

        readonly ScenarioMainListVM vm;
        readonly Fixture fixture;

        public ScenarioMainListVMTests()
        {
            fixture = new Fixture();

            fakeSource = repository.Create<IDataListFilterSource<ScenarioBriefModel, ScenarioFilter>>();
            fakeSource.Setup(f => f.LoadItemsAsync(It.IsAny<ScenarioFilter>(), It.IsAny<CancellationToken>())).ReturnsAsync(fixture.CreateMany<ScenarioBriefModel>());

            stepSource = repository.Create<IDataListSource<StepModel>>();
            stepSource.Setup(f => f.GetItemsAsync(It.IsAny<CancellationToken>())).ReturnsAsync(fixture.CreateMany<StepModel>());

            fakeDelete = new Mock<IDeleteOperation<ScenarioBriefModel>>();
            fakeWndFactory = new Mock<IWindowFactory>();
            fakeResources = new Mock<IResourceStringProvider>();
            fakeResources.Setup(f => f.GetString(It.IsAny<string>())).Returns<string>(s => s);

            vm = new ScenarioMainListVM(
                fakeSource.Object, 
                stepSource.Object,
                fakeDelete.Object,  
                fakeWndFactory.Object, 
                fakeResources.Object);
        }

        [TestMethod]
        public async Task DeleteCmd_Test()
        {
            // setup
            await vm.InitAndWait();

            var item = vm.Items.First();
            vm.SelectedItem = item;

            fakeWndFactory.Setup(f => f.ShowConfirmDialog(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            fakeDelete.Setup(f => f.DeleteAsync(item.Item, default)).ReturnsAsync(true);

            // action
            vm.DeleteCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            fakeDelete.Verify(f => f.DeleteAsync(item.Item, default), Times.Once);
            vm.Items.Should().NotContain(item);
            vm.Should().WithoutError();
        }

        [TestMethod]
        public async Task Items_Test()
        {
            // setup
            var items = await fakeSource.Object.LoadItemsAsync(null);
            var steps = await stepSource.Object.GetItemsAsync();
            
            // action
            await vm.InitAndWait();

            // asserts
            vm.Items.Should()
                .NotBeNullOrEmpty().And
                .BeEquivalentTo(items, o => o.Excluding(i => i.Prior));

            vm.Steps.Should().NotBeNullOrEmpty();
            vm.Steps.Should().Equal(Enumerable.Repeat(string.Empty, 1).Concat(steps.Select(i => i.Name)));
        }
    }
}
