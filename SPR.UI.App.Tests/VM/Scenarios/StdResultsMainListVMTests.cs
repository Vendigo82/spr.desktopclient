﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using AutoFixture;
using SPR.UI.WebService.DataContract.Scenario;
using FluentAssertions;

namespace SPR.UI.App.VM.Scenarios.Tests
{
    [TestClass]
    public class StdResultsMainListVMTests : TestVMBase
    {
        readonly Mock<IStdResultsListSource> fakeSource;
        readonly StdResultsMainListVM vm;

        public StdResultsMainListVMTests()
        {
            fakeSource = repository.Create<IStdResultsListSource>();
            fakeSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(fixture.CreateMany<StdResultSlimModel>());

            vm = new StdResultsMainListVM(fakeSource.Object, fakeWndFactory.Object, fakeResources.Object);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task DeleteCmd_CanExecute_Test(bool usage)
        {
            // setup                  
            var items = fixture.Build<StdResultSlimModel>().With(s => s.Usage, () => usage).CreateMany();
            fakeSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(items);

            await vm.InitAndWait();

            // action
            vm.SelectedItem = vm.Items.First();

            // asserts
            vm.DeleteCmd.CanExecute(null).Should().Be(!usage);
        }

        [TestMethod]
        public async Task DeleteCmd_Test()
        {
            // setup                  
            var items = fixture.Build<StdResultSlimModel>().Without(s => s.Usage).CreateMany();
            fakeSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(items);
            fakeWndFactory.Setup(f => f.ShowConfirmDialog(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            await vm.InitAndWait();
            var item = vm.Items.First();

            fakeSource.Setup(f => f.DeleteAsync(item, default)).ReturnsAsync(true);

            vm.SelectedItem = item;

            // action
            vm.DeleteCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            fakeSource.Verify(f => f.DeleteAsync(item, default), Times.Once);
            vm.Items.Should().NotContain(item);
        }

        [TestMethod]
        public async Task Initialize_Test()
        {
            // setup 
            var items = await fakeSource.Object.GetItemsAsync();

            // action
            await vm.InitAndWait();

            // asserts
            vm.Items.Should()
                .NotBeNullOrEmpty().And
                .BeEquivalentTo(items);

            vm.Should().WithoutError();
        }
    }
}
