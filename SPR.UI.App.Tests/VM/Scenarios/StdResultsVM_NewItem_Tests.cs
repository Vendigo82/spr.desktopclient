﻿using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RandomType;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Scenarios.Tests
{
    [TestClass]
    public class StdResultsVM_NewItem_Tests
    {
        readonly IStdResultsSource fakeSource;
        readonly StdResultsVM vm;
        readonly NotifyPropertyChangedCollector notify;

        public StdResultsVM_NewItem_Tests()
        {
            fakeSource = A.Fake<IStdResultsSource>();

            vm = new StdResultsVM(null, fakeSource, null);
            notify = new NotifyPropertyChangedCollector(vm);
        }

        [TestMethod]
        public async Task Initialize_Test()
        {
            await vm.InitAndWait();
            Assert.AreNotEqual(Guid.Empty, vm.Id);
            Assert.IsFalse(vm.Default);
            Assert.AreEqual(string.Empty, vm.Name);
            Assert.AreEqual(0, vm.Values.Count);

            A.CallTo(() => fakeSource.GetItemAsync(A<Guid>._, A<CancellationToken>._))
                .MustNotHaveHappened();
        }

        [TestMethod]
        public async Task Refresh_Test()
        {
            await vm.InitAndWait();
            vm.Name = Guid.NewGuid().ToString();
            Assert.IsTrue(vm.IsChanged);

            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            Assert.IsFalse(vm.IsChanged);
            Assert.AreEqual(string.Empty, vm.Name);

            A.CallTo(() => fakeSource.GetItemAsync(A<Guid>._, A<CancellationToken>._))
                .MustNotHaveHappened();
            A.CallTo(() => fakeSource.SaveAsync(A<StdResultModel>._, A<CancellationToken>._))
                .MustNotHaveHappened();
        }

        [TestMethod]
        public async Task Save_Test()
        {
            await vm.InitAndWait();
            vm.Name = "xxx";
            await vm.SaveAndWait();
            Assert.IsFalse(vm.IsChanged);

            A.CallTo(() => fakeSource.SaveAsync(A<StdResultModel>._, A<CancellationToken>._))
                .MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeSource.GetItemAsync(A<Guid>._, A<CancellationToken>._))
                .MustNotHaveHappened();

            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            A.CallTo(() => fakeSource.GetItemAsync(A<Guid>._, A<CancellationToken>._))
                .MustHaveHappenedOnceExactly();
        }

        [TestMethod]
        public async Task SetAsDefault_Test()
        {
            await vm.InitAndWait();
            Assert.IsFalse(vm.Default);
            Assert.IsFalse(vm.SetDefaultCmd.CanExecute(null));

            vm.Name = "xxx";
            await vm.SaveAndWait();
            Assert.IsFalse(vm.Default);
            Assert.IsTrue(vm.SetDefaultCmd.CanExecute(null));
        }
    }
}
