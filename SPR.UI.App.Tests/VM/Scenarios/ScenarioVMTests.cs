﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.UI.WebService.DataContract;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Common;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Abstractions.UI;

namespace SPR.UI.App.VM.Scenarios.Tests
{
    [TestClass]
    public class ScenarioVMTests
    {
        readonly Mock<IScenarioSource> fakeSource;
        readonly Mock<IDataListSource<StdResultSlimModel>> fakeStdResultsSource;
        readonly Mock<IWindowFactory> fakeWndFactory;
        readonly Mock<ScenarioVM.IStringsProvider> fakeStrings;
        readonly MockRepository repository;

        readonly ScenarioVM vm;
        readonly Guid id;        

        readonly ScenarioModel model;
        readonly StdResultSlimModel[] stdResultsModel;

        readonly Fixture fixture;

        public ScenarioVMTests()
        {
            repository = new MockRepository(MockBehavior.Default);

            fixture = new Fixture();

            id = Guid.NewGuid();
            model = fixture.Build<ScenarioModel>().Without(i => i.StdResult).Create();
            model.Guid = id;
            stdResultsModel = fixture
                .Build<StdResultSlimModel>()
                .Without(i => i.Default)
                .CreateMany(2)
                .ToArray();

            fakeSource = repository.Create<IScenarioSource>();
            fakeSource.Setup(f => f.GetMain(id, default)).ReturnsAsync(new ObjectContainer<ScenarioModel> { Item = model });

            fakeStdResultsSource = repository.Create<IDataListSource<StdResultSlimModel>>();
            fakeStdResultsSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(stdResultsModel);

            fakeWndFactory = repository.Create<IWindowFactory>();
            fakeStrings = repository.Create<ScenarioVM.IStringsProvider>();

            vm = new ScenarioVM(id, fakeSource.Object, fakeStdResultsSource.Object, 
                null, repository.Create<IConfirmActionCommentProvider>().Object,
                fakeWndFactory.Object,
                fakeStrings.Object);
        }

        [TestMethod]
        public void TypeDeclaration_Test()
        {
            // asserts
            vm.GetType().Should()
                .BeAssignableTo<ICommonVM>().And
                .BeAssignableTo<IDataInitializer>().And
                .HaveProperty<IEnumerable<FormulaScenarioCheckupModel>>("CheckupsFormulaList");
        }

        [DataTestMethod]
        [DataRow(50, 50)]
        [DataRow(1, 1)]
        [DataRow(100, 100)]
        [DataRow(101, 100)]
        [DataRow(0, 1)]
        public async Task Quote_Values_Test(int value, int expected) {
            await vm.InitAndWait();
            vm.Quota = (byte)value;
            Assert.AreEqual((byte)expected, vm.Quota);
        }

        [DataTestMethod]
        [DataRow(100, true)]
        [DataRow(50, false)]
        public async Task SetQuote_VerifyIsPrimary_Test(int value, bool expected)
        {
            await vm.InitAndWait();
            var collector = new NotifyPropertyChangedCollector(vm);
            vm.Quota = (byte)value;
            CollectionAssert.Contains(collector.ToArray(), nameof(vm.IsPrimary));
            CollectionAssert.Contains(collector.ToArray(), nameof(vm.Quota));
            Assert.AreEqual(expected, vm.IsPrimary);
        }

        [DataTestMethod]
        [DataRow(true, 100)]
        [DataRow(false, 50)]
        public async Task SetIsPrimary_VerifyQuote_Test(bool value, int expected)
        {
            await vm.InitAndWait();
            var collector = new NotifyPropertyChangedCollector(vm);
            vm.IsPrimary = value;
            CollectionAssert.Contains(collector.ToArray(), nameof(vm.IsPrimary));
            CollectionAssert.Contains(collector.ToArray(), nameof(vm.Quota));
            Assert.AreEqual((byte)expected, vm.Quota);
        }

        [TestMethod]
        public async Task InitAndRefresh_Test()
        {
            // setup
            Assert.IsNull(vm.StdResultsList);

            //using (var monitorVm = vm.Monitor()) {

                //action
                await vm.InitAndWait();

                //asserts
            //    monitorVm.Should().RaisePropertyChangeFor(x => x.StdResult);
            //    monitorVm.Should().RaisePropertyChangeFor(x => x.StdResultId);
            //    monitorVm.Should().RaisePropertyChangeFor(x => x.StdResultsList);
            //}

            fakeSource.Verify(f => f.GetMain(id, default), Times.Once);
            fakeStdResultsSource.Verify(f => f.GetItemsAsync(default), Times.Once);

            vm.StdResultsList.Should().NotBeNull();
            vm.StdResultsList.Should().Equal(stdResultsModel, (i, m) => i.Id == m.Id && i.Name == m.Name);

            //save prev values, Refresh should not update StdResultsList
            var prevStdResultsList = vm.StdResultsList;

            //refresh 
            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            vm.StdResultsList.Should().BeSameAs(prevStdResultsList);

            fakeSource.Verify(f => f.GetMain(id, default), Times.Exactly(2));
            fakeStdResultsSource.Verify(f => f.GetItemsAsync(default), Times.Once);

            //property still same
            vm.StdResultsList.Should().NotBeNull();
            vm.StdResultsList.Should().Equal(stdResultsModel, (i, m) => i.Id == m.Id && i.Name == m.Name);
        }

        /// <summary>
        /// Standard result should net be assigned to existed item's model after initialization
        /// </summary>
        /// <param name="hasDefault"></param>
        /// <returns></returns>
        [TestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task Init_DoesNotAssignStdResult_Test(bool hasDefault)
        {
            // setup
            if (hasDefault)
                stdResultsModel.First().Default = true;

            // action
            await vm.InitAndWait();

            // asserts
            vm.StdResultsList.Should().NotBeEmpty();
            vm.Item.StdResult.Should().BeNull();
            vm.StdResult.Should().BeNull();
            vm.StdResultId.Should().BeNull();
        }

        [TestMethod]
        public async Task StdResult_ValueChanges_Test()
        {
            await vm.InitAndWait();

            vm.StdResult.Should().BeNull();
            vm.StdResultId.Should().BeNull();

            //using (var monitor = vm.Monitor()) {
                vm.StdResultId = stdResultsModel.First().Id;

            //    monitor.Should().RaisePropertyChangeFor(x => x.StdResult);
            //    monitor.Should().RaisePropertyChangeFor(x => x.StdResultId);
            //}

            vm.StdResult.Id.Should().Be(stdResultsModel.First().Id);
            vm.StdResult.Name.Should().Be(stdResultsModel.First().Name);
            vm.StdResult.Should().BeEquivalentTo(stdResultsModel.First(), o => o
                .Excluding(m => m.Usage)
                .Excluding(m => m.Default));

            //using (var monitor = vm.Monitor()) {
                vm.StdResultId = null;

            //    monitor.Should().RaisePropertyChangeFor(x => x.StdResult);
            //    monitor.Should().RaisePropertyChangeFor(x => x.StdResultId);
            //}
            vm.StdResult.Should().BeNull();
            vm.StdResultId.Should().BeNull();
        }

        [TestMethod]
        public async Task EnableValue_DontInfluentFromArchive()
        {
            // setup
            model.Enabled = true;

            //configure achive.
            fakeSource.Setup(f => f.GetArchiveVersionsListAsync(id, default)).ReturnsAsync(new ItemsList<VersionInfo> {
                Items = new List<VersionInfo> { new VersionInfo { Version = 1 } }
            });
            var archiveModel = fixture.Build<ScenarioModel>().With(i => i.Enabled, () => false).Create();
            archiveModel.Enabled.Should().BeFalse();

            fakeSource.Setup(f => f.GetArchiveVersionAsync(id, 1, default)).ReturnsAsync(archiveModel);

            await vm.InitAndWait();

            // action: switch to archive with has Enabled = false
            vm.ShownObject = DataContract.ShownObjectEnum.Archive;
            
            // asserts
            vm.ShownObject.Should().Be(DataContract.ShownObjectEnum.Archive);

            //but VM still Enabled = true
            vm.Enabled.Should().BeTrue();
        }

        [DataTestMethod]
        [DataRow(false, true, true)] //activate
        [DataRow(false, true, false)] //activate, but refuse action
        [DataRow(true, true, true)] //activate already activated
        [DataRow(true, false, true)] //deactivate
        [DataRow(true, false, false)] //deactivate, but refuse action
        [DataRow(false, false, true)] //deactivate already deactivated
        public async Task Activate_Test(bool initialEnabled, bool enabled, bool confirmResult)
        {
            // setup
            model.Enabled = initialEnabled;
            fakeWndFactory.Setup(f => f.ShowConfirmDialog(It.IsAny<string>(), It.IsAny<string>())).Returns(confirmResult);
            fakeStrings.Setup(f => f.ActivateConfirmation).Returns("A");
            fakeStrings.Setup(f => f.DeactivateConfirmation).Returns("D");

            await vm.InitAndWait();

            // action
            vm.Enabled = enabled;
            await vm.WaitBusy();

            // asserts
            if (initialEnabled != enabled) {
                var expectedConfirmDlgMessage = enabled ? "A" : "D";
                fakeWndFactory.Verify(f => f.ShowConfirmDialog(expectedConfirmDlgMessage, It.IsAny<string>()), Times.Once);
            } else
                fakeWndFactory.VerifyNoOtherCalls();

            if (confirmResult && initialEnabled != enabled)
                fakeSource.Verify(f => f.ActivateAsync(id, enabled), Times.Once);
            else
                fakeSource.Verify(f => f.ActivateAsync(id, It.IsAny<bool>()), Times.Never);
        }

        [TestMethod]
        public async Task CalcResults_Test()
        {
            // action
            await vm.InitAndWait();

            // asserts
            vm.CalcResults.Should().BeEquivalentTo(model.CalcResults);
        }

        [TestMethod]
        public async Task Checkups_Test()
        {
            // action
            await vm.InitAndWait();

            //asserts
            vm.Checkups.Should().BeEquivalentTo(model.Checkups);
        }

        [TestMethod]
        public async Task CheckupsFormulaList_Get_Test()
        {
            // setup
            await vm.InitAndWait();

            // action
            var list = vm.CheckupsFormulaList;

            // asserts
            list.Should().BeEquivalentTo(vm.Checkups.Select(i => i.Item.Formula));
        }

        [TestMethod]
        public async Task CheckupsFormulaList_Set_Test()
        {
            // setup: prepare new list which contains one old item and one new item
            await vm.InitAndWait();
            var selected = vm.CheckupsFormulaList
                .Take(1)
                .Concat(new[] { fixture.Create<FormulaScenarioCheckupModel>() })
                .ToArray();

            // action
            vm.CheckupsFormulaList = selected;

            // asserts
            vm.CheckupsFormulaList.Should().BeEquivalentTo(selected);
        }

        [TestMethod]
        public async Task Checkup_WithNestedRejectCodes_Test()
        {
            // setup
            var checkup = model.Checkups.First().Formula;
            checkup.NestedRejectCodes = fixture.CreateMany<int>();

            // action
            await vm.InitAndWait();

            // asserts
            vm.Checkups.First().RejectCodes.Should()
                .HaveCountGreaterThan(1).And
                .Equal(new[] { checkup.RejectCode.Value }.Concat(checkup.NestedRejectCodes));
        }

        [TestMethod]
        public async Task Checkup_WithoutNestedRejectCodes_Test()
        {
            // setup
            var checkup = model.Checkups.First().Formula;
            checkup.NestedRejectCodes = null;

            // action
            await vm.InitAndWait();

            // asserts
            vm.Checkups.First().RejectCodes.Should().ContainSingle().Which.Should().Be(checkup.RejectCode.Value);
        }

        [DataTestMethod]
        [DataRow(true, true)]
        [DataRow(false, true)]
        [DataRow(true, false)]
        [DataRow(false, false)]
        public async Task Checkup_WithNestedSteps_Test(bool withRejectStep, bool withNestedSteps)
        {
            // setup
            var checkup = model.Checkups.First().Formula;
            checkup.RejectStep = withRejectStep ? fixture.Create<string>() : null;
            checkup.NestedSteps = withNestedSteps ? fixture.CreateMany<string>() : null;

            // action
            await vm.InitAndWait();

            // asserts
            var steps = vm.Checkups.First().RejectSteps;
            if (withRejectStep && withNestedSteps)
                steps.Should()
                    .HaveCountGreaterThan(1).And
                    .Equal(new[] { checkup.RejectStep }.Concat(checkup.NestedSteps));
            else if (withRejectStep)
                steps.Should().ContainSingle().Which.Should().Be(checkup.RejectStep);
            else if (withNestedSteps)
                steps.Should().Equal(checkup.NestedSteps);
            else
                steps.Should().BeEmpty();
        }

        [DataTestMethod]
        [DataRow(true, true)]
        [DataRow(false, true)]
        [DataRow(true, false)]
        [DataRow(false, false)]
        public async Task Checkup_AbortScenario_Test(bool abort, bool nestedAbort)
        {
            // setup
            var checkup = model.Checkups.First().Formula;
            checkup.AbortScenario = abort;
            checkup.NestedAbortScenario = nestedAbort;

            // action
            await vm.InitAndWait();

            // asserts
            vm.Checkups.First().AbortScenario.Should().Be(abort || nestedAbort);
        }
    }
}
