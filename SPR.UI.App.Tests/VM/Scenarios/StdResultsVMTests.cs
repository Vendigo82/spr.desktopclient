﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeItEasy;
using RandomType;
using SPR.UI.App.Abstractions.DataSources;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.UI.App.VM.Scenarios.Tests
{
    [TestClass]
    public class StdResultsVMTests
    {
        readonly IStdResultsSource fakeSource;
        readonly StdResultModel model;
        readonly StdResultsVM vm;
        readonly NotifyPropertyChangedCollector notify;

        public StdResultsVMTests()
        {
            fakeSource = A.Fake<IStdResultsSource>();

            model = RandomTypeGenerator.Generate<StdResultModel>(c => { c.Max.ListSize = 3; c.Min.ListSize = 3; });

            A.CallTo(() => fakeSource.GetItemAsync(A<Guid>._, A<CancellationToken>._))
                .Returns(model);
            
            vm = new StdResultsVM(model.Id, fakeSource, null);
            notify = new NotifyPropertyChangedCollector(vm);
        }

        [TestMethod]
        public async Task Initialize_Test()
        {
            await vm.InitAndWait();            
            Assert.AreEqual(model.Id, vm.Id);
            Assert.AreEqual(model.Default, vm.Default);
            Assert.AreEqual(model.Name, vm.Name);
            Assert.AreEqual(model.Values.Count, vm.Values.Count);
            Assert.IsFalse(vm.IsChanged);

            A.CallTo(() => fakeSource.GetItemAsync(A<Guid>.That.IsEqualTo(model.Id), A<CancellationToken>._))
                .MustHaveHappenedOnceExactly();

            CollectionAssert.IsSubsetOf(
                new string[] { nameof(vm.Default), nameof(vm.Name), nameof(vm.Id), nameof(vm.Values) },
                notify.ToArray());
        }

        [TestMethod]
        public async Task Refresh_Test()
        {
            await vm.InitAndWait();
            vm.Name = Guid.NewGuid().ToString();
            Assert.IsTrue(vm.IsChanged);

            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            Assert.IsFalse(vm.IsChanged);
            Assert.AreEqual(model.Name, vm.Name);

            A.CallTo(() => fakeSource.GetItemAsync(A<Guid>.That.IsEqualTo(model.Id), A<CancellationToken>._))
                .MustHaveHappenedTwiceExactly();
            A.CallTo(() => fakeSource.SaveAsync(A<StdResultModel>._, A<CancellationToken>._))
                .MustNotHaveHappened();
        }

        [TestMethod]
        public async Task AddValuesRow_Test()
        {
            await vm.InitAndWait();
            int prevCount = vm.Values.Count;

            vm.Values.Add(new StdResultsVM.ValueWrapper());

            Assert.IsTrue(vm.IsChanged);
            Assert.AreEqual(prevCount + 1, vm.Values.Count);

            await vm.SaveAndWait();

            Assert.IsFalse(vm.IsChanged);
            A.CallTo(() => fakeSource.SaveAsync(A<StdResultModel>._, A<CancellationToken>._))
                .MustHaveHappenedOnceExactly();
            var saveArg = Fake
                .GetCalls(fakeSource)
                .Where(i => i.Method.Name == nameof(fakeSource.SaveAsync))
                .Single()
                .GetArgument<StdResultModel>(0);
            Assert.AreEqual(prevCount + 1, saveArg.Values.Count);
        }

        [TestMethod]
        public async Task RemoveValuesRow_Test()
        {
            await vm.InitAndWait();
            int prevCount = vm.Values.Count;

            vm.Values.RemoveAt(0);

            Assert.IsTrue(vm.IsChanged);
            Assert.AreEqual(prevCount - 1, vm.Values.Count);

            await vm.SaveAndWait();

            Assert.IsFalse(vm.IsChanged);
            A.CallTo(() => fakeSource.SaveAsync(A<StdResultModel>._, A<CancellationToken>._))
                .MustHaveHappenedOnceExactly();
            var saveArg = Fake
                .GetCalls(fakeSource)
                .Where(i => i.Method.Name == nameof(fakeSource.SaveAsync))
                .Single()
                .GetArgument<StdResultModel>(0);
            Assert.AreEqual(prevCount - 1, saveArg.Values.Count);
        }

        [TestMethod]
        public async Task ChangeValuesRow_Test()
        {
            await vm.InitAndWait();
            vm.Values[0].Name = Guid.NewGuid().ToString();
            Assert.IsTrue(vm.IsChanged);
        }

        [DataTestMethod]
        [DataRow(true, false)]
        [DataRow(false, true)]
        public async Task SetAsDefaultCmd_IsEnabled_Test(bool defValue, bool canExecute)
        {
            model.Default = defValue;
            await vm.InitAndWait();
            Assert.AreEqual(canExecute, vm.SetDefaultCmd.CanExecute(null));
        }

        [TestMethod]
        public async Task SetAsDefault_Execute_Test()
        {
            model.Default = false;
            await vm.InitAndWait();

            vm.SetDefaultCmd.Execute(null);
            await vm.WaitBusy();

            Assert.IsTrue(model.Default);
            Assert.IsFalse(vm.SetDefaultCmd.CanExecute(null));
            CollectionAssert.Contains(notify.ToArray(), nameof(model.Default));

            A.CallTo(() => fakeSource.SetDefault(A<Guid>.That.IsEqualTo(model.Id), A<CancellationToken>._))
                .MustHaveHappenedOnceExactly();
        }
    }
}
