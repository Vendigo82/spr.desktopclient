﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.DataContract;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Scenarios.Tests
{
    [TestClass]
    public class ScenarioTestAccessorAdapterTests
    {
        readonly Fixture fixture = new Fixture();
        readonly Mock<IDataObjectVM<ScenarioModel>> fakeVm = new Mock<IDataObjectVM<ScenarioModel>>();
        readonly ScenarioTestAccessorAdapter adapter;

        public ScenarioTestAccessorAdapterTests()
        {
            adapter = new ScenarioTestAccessorAdapter(fakeVm.Object);
        }

        [TestMethod]
        public void Properties_Test()
        {
            // setup
            var id = fixture.Create<long>();
            var guid = fixture.Create<Guid>();
            var shown = fixture.Create<ShownObjectEnum>();
            fakeVm.SetupGet(f => f.ID).Returns(id);
            fakeVm.SetupGet(f => f.Guid).Returns(guid);
            fakeVm.SetupGet(f => f.ShownObject).Returns(shown);

            // asserts
            adapter.Id.Should().Be(id);
            adapter.Guid.Should().Be(guid);
            adapter.ShownObject.Should().Be(shown);
        }

        [DataTestMethod]
        [DataRow(ShownObjectEnum.Main, false)]
        [DataRow(ShownObjectEnum.Archive, true)]
        [DataRow(ShownObjectEnum.Draft, true)]
        public void ItemProperty_Test(ShownObjectEnum shown, bool expectedItem)
        {
            // setup
            var item = new ScenarioModel();
            fakeVm.SetupGet(f => f.ShownObject).Returns(shown);
            fakeVm.SetupGet(f => f.Item).Returns(item);

            // action
            var result = adapter.Item;

            // asserts
            if (expectedItem)
                result.Should().BeSameAs(item);
            else
                result.Should().BeNull();
        }
    }
}
