﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json.Linq;
using SPR.UI.App.Abstractions.Tests;
using SPR.UI.WebService.DataContract.Scenario;
using SPR.WebService.TestModule.Client;
using SPR.WebService.TestModule.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SPR.UI.App.VM.Scenarios.Tests
{
    [TestClass]
    public class ScenarioTestVMTests
    {
        readonly Fixture fixture = new Fixture();
        readonly MockRepository repository = new MockRepository(MockBehavior.Default);
        readonly Mock<ITestItemAccessor<ScenarioModel>> fakeItemAccessor;
        readonly Mock<ITestModuleClient> fakeClient;
        readonly Mock<ITestResultsAcceptor> fakeResultsAcceptor;
        readonly Mock<ITestInputDataProvider> fakeTestInput;
        
        public ScenarioTestVMTests()
        {
            fakeItemAccessor = new Mock<ITestItemAccessor<ScenarioModel>>();
            fakeClient = repository.Create<ITestModuleClient>();
            fakeResultsAcceptor = repository.Create<ITestResultsAcceptor>();
            fakeTestInput = new Mock<ITestInputDataProvider>();

            fakeTestInput.Setup(f => f.GetInputData()).Returns(new JObject().ToString());
        }

        [TestMethod]
        public void RequestDataInitialization_Test()
        {
            // setup
            var data = new JObject();
            fakeTestInput.Setup(f => f.GetInputData()).Returns(data.ToString());

            // action
            var vm = CreateVM();

            // asserts
            vm.RequestData.Should().Be(data.ToString());
            fakeTestInput.Verify(f => f.GetInputData(), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task TestWorkingVersion_Test()
        {
            // setup
            long id = fixture.Create<long>();
            fakeItemAccessor.SetupGet(f => f.ShownObject).Returns(DataContract.ShownObjectEnum.Main);
            fakeItemAccessor.SetupGet(f => f.Item).Returns((ScenarioModel)null);
            fakeItemAccessor.SetupGet(f => f.Id).Returns(id);

            var response = new TestResponse<ScenarioResultDto>();
            fakeClient.Setup(f => f.TestScenario(It.IsAny<TestMainScenarioRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            var vm = CreateVM();

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();
            vm.TestCmd.CanExecute(null).Should().BeTrue();

            fakeClient.Verify(f => f.TestScenario(
                Match.Create<TestMainScenarioRequest>(i => i.ScenarioId == id),
                It.IsAny<CancellationToken>()), Times.Once);

            fakeResultsAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Scenario, id, DataContract.ShownObjectEnum.Main), Times.Once);
            fakeResultsAcceptor.Verify(f => f.PushResult(id, DataContract.ShownObjectEnum.Main, response), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task TestWorkingVersion_Exception_Test()
        {
            // setup
            long id = fixture.Create<long>();
            fakeItemAccessor.SetupGet(f => f.ShownObject).Returns(DataContract.ShownObjectEnum.Main);
            fakeItemAccessor.SetupGet(f => f.Item).Returns((ScenarioModel)null);
            fakeItemAccessor.SetupGet(f => f.Id).Returns(id);

            var response = new TestResponse<ScenarioResultDto>();
            fakeClient.Setup(f => f.TestScenario(It.IsAny<TestMainScenarioRequest>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new InvalidOperationException());                

            var vm = CreateVM();

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().HasError();
            vm.TestCmd.CanExecute(null).Should().BeTrue();

            fakeClient.Verify(f => f.TestScenario(
                Match.Create<TestMainScenarioRequest>(i => i.ScenarioId == id),
                It.IsAny<CancellationToken>()), Times.Once);

            fakeResultsAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Scenario, id, DataContract.ShownObjectEnum.Main), Times.Once);
            fakeResultsAcceptor.Verify(f => f.Cancel(DataContract.ObjectType.Scenario, id, DataContract.ShownObjectEnum.Main), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task TestDraftVersion_Test()
        {
            // setup           
            long id = fixture.Create<long>();
            var model = new ScenarioModel();
            fakeItemAccessor.SetupGet(f => f.ShownObject).Returns(DataContract.ShownObjectEnum.Draft);
            fakeItemAccessor.SetupGet(f => f.Item).Returns(model);
            fakeItemAccessor.SetupGet(f => f.Id).Returns(id);

            var response = new TestResponse<ScenarioResultDto>();
            fakeClient.Setup(f => f.TestScenario(It.IsAny<TestDraftScenarioRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            var vm = CreateVM();

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();
            vm.TestCmd.CanExecute(null).Should().BeTrue();

            fakeClient.Verify(f => f.TestScenario(
                Match.Create<TestDraftScenarioRequest>(i => i.Scenario == model),
                It.IsAny<CancellationToken>()), Times.Once);

            fakeResultsAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Scenario, id, DataContract.ShownObjectEnum.Draft), Times.Once);
            fakeResultsAcceptor.Verify(f => f.PushResult(id, DataContract.ShownObjectEnum.Draft, response), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task TestDraftVersion_Exception_Test()
        {
            // setup           
            long id = fixture.Create<long>();
            var model = new ScenarioModel();
            fakeItemAccessor.SetupGet(f => f.ShownObject).Returns(DataContract.ShownObjectEnum.Draft);
            fakeItemAccessor.SetupGet(f => f.Item).Returns(model);
            fakeItemAccessor.SetupGet(f => f.Id).Returns(id);

            var response = new TestResponse<ScenarioResultDto>();
            fakeClient.Setup(f => f.TestScenario(It.IsAny<TestDraftScenarioRequest>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new InvalidOperationException());

            var vm = CreateVM();

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().HasError();
            vm.TestCmd.CanExecute(null).Should().BeTrue();

            fakeClient.Verify(f => f.TestScenario(
                Match.Create<TestDraftScenarioRequest>(i => i.Scenario == model),
                It.IsAny<CancellationToken>()), Times.Once);

            fakeResultsAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Scenario, id, DataContract.ShownObjectEnum.Draft), Times.Once);
            fakeResultsAcceptor.Verify(f => f.Cancel(DataContract.ObjectType.Scenario, id, DataContract.ShownObjectEnum.Draft), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        //[TestMethod]
        //public async Task TestButtonAccessible_Test()
        //{
        //    // setup
        //    var vm = CreateVM();

        //    // action
        //    using (var monitor = vm.TestCmd.Monitor()) {
        //        vm.TestCmd.Execute(null);
        //        await vm.WaitBusy();

        //        // asserts
        //        monitor.Should().Raise(nameof(ICommand.CanExecuteChanged));
        //    }

        //    vm.TestCmd.CanExecute(null).Should().BeTrue();
        //}

        private ScenarioTestVM CreateVM()
            => new ScenarioTestVM(fakeItemAccessor.Object, fakeClient.Object, fakeResultsAcceptor.Object, fakeTestInput.Object, null);
    }
}
