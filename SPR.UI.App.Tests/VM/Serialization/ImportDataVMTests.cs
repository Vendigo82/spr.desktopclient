﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient;
using SPR.UI.WebService.DataContract.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Serialization.Tests
{
    [TestClass]
    public class ImportDataVMTests
    {
        readonly Fixture fixture;
        readonly string importedData;
        readonly ImportDataVM vm;
        readonly Mock<ISerialization> fakeClient;

        public ImportDataVMTests()
        {
            fixture = new Fixture();
            importedData = fixture.Create<string>();
            fakeClient = new Mock<ISerialization>();
            vm = new ImportDataVM(importedData, fakeClient.Object, null);
        }

        [TestMethod]
        public async Task Initialize_Test()
        {
            // setup
            var response = fixture.Create<ExistanceResponseModel>();
            fakeClient.Setup(f => f.CheckExistanceAsync(importedData, default))
                .ReturnsAsync(response);

            // action
            await vm.InitAndWait();

            // asserts
            vm.ImportCmd.CanExecute(null).Should().BeTrue();
            vm.ImportComplete.Should().BeFalse();

            (vm as object).Should().BeEquivalentTo(response, o => o
                .Excluding(i => i.GoogleBQConnections)
                .Excluding(i => i.Formulas)
                .Excluding(i => i.FormulasIds));
            vm.GoogleBqConnections.Should().BeSameAs(response.GoogleBQConnections);
            vm.DependencesFormulas.Should().BeEquivalentTo(response.Formulas);
            vm.Formulas.Should().BeEmpty();

            vm.Should().WithoutError();
        }

        [TestMethod]
        public async Task Initialize_MainFormulas_Test()
        {
            // setup
            var response = fixture.Create<ExistanceResponseModel>();
            response.FormulasIds = response.Formulas.Select(i => i.Item.Guid).ToArray();
            fakeClient.Setup(f => f.CheckExistanceAsync(importedData, default))
                .ReturnsAsync(response);

            // action
            await vm.InitAndWait();

            // asserts
            vm.DependencesFormulas.Should().BeEmpty();
            vm.Formulas.Should().BeEquivalentTo(response.Formulas);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task Initialize_CanCreateDublicate_Test(bool haveExisted)
        {
            // setup
            var response = fixture.Create<ExistanceResponseModel>();
            if (!haveExisted) {
                foreach (var item in response.Formulas) {
                    item.Existed = null;
                    item.Exists = false;
                }

                foreach (var item in response.Scenarios) {
                    item.Existed = null;
                    item.Exists = false;
                }
            }

            fakeClient.Setup(f => f.CheckExistanceAsync(importedData, default))
                .ReturnsAsync(response);

            // action
            await vm.InitAndWait();

            // asserts
            vm.CanCreateDublicate.Should().Be(haveExisted);
        }

        [TestMethod]
        public async Task Import_Test()
        {
            // setup
            var expectedSettings = fixture.Create<ImportSettingsModel>();

            var settingsArgs = new List<ImportSettingsModel>();
            var result = fakeClient.Setup(f => f.ImportAsync(importedData, Capture.In(settingsArgs), null, default))
                .ReturnsAsync(fixture.Create<ImportResultModel>());

            await vm.InitAndWait();

            vm.CreateDublicate = expectedSettings.CreateDublicateForMainObjects;
            vm.UpdateDependencyKind = expectedSettings.UpdateNestedFormulasKind;
            vm.RewriteStdResults = expectedSettings.RewriteStdResults;
            vm.RewriteSteps = expectedSettings.RewriteSteps;
            vm.RewriteRejectReasons = expectedSettings.RewriteRejectReasons;
            vm.RewriteServers = expectedSettings.RewriteServers;
            vm.RewriteSqlServers = expectedSettings.RewriteSqlServers;
            vm.RewriteGoogleBQConnections = expectedSettings.RewriteGoogleBqConnections;

            // action
            //using (var monitor = vm.Monitor()) {
                vm.ImportCmd.Execute(null);
                await vm.WaitBusy();

                // asserts
                //monitor.Should().RaisePropertyChangeFor(i => i.ImportComplete);
           // }

            // asserts
            vm.Should().WithoutError();
            vm.ImportComplete.Should().BeTrue();

            settingsArgs.Should().ContainSingle().Which.Should().BeEquivalentTo(expectedSettings);
        }
    }
}
