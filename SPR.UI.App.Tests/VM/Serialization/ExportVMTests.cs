﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using FluentAssertions;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataContract;
using SPR.DAL.Enums;
using SPR.UI.App.Abstractions.VM;

namespace SPR.UI.App.VM.Serialization.Tests
{
    [TestClass]
    public class ExportVMTests
    {
        readonly Mock<ISerialization> fakeSerialization = new Mock<ISerialization>();

        public ExportVMTests()
        {
        }

        [TestMethod]
        public void TypeDeclaration_Test()
        {
            typeof(ExportVM).Should()
                .Implement<IDataInitializer>().And
                .Implement<ICommonVM>();
        }

        [TestMethod]
        public async Task ExportFormula_Test()
        {
            // setup
            var guid = Guid.NewGuid();
            var serializedData = Guid.NewGuid().ToString();

            var fakeFormula = new Mock<IFormulaItem>();
            fakeFormula.SetupGet(f => f.Guid).Returns(guid);

            fakeSerialization.Setup(f => f.SerializeFormulaAsync(guid, default)).ReturnsAsync(serializedData);

            // action
            //using (var m = )
            var vm = new ExportVM(fakeFormula.Object, fakeSerialization.Object, null);
            //using (var monitor = vm.Monitor()) {
                await vm.InitAndWait();

                // asserts            
                //monitor.Should().RaisePropertyChangeFor(p => p.DataContent);
            //}

            vm.Should().WithoutError();
            vm.DataContent.Should().Be(serializedData);            

            fakeSerialization.Verify(f => f.SerializeFormulaAsync(guid, default), Times.Once);
        }

        [TestMethod]
        public async Task ExportScenario_Test()
        {
            // setup
            var guid = Guid.NewGuid();
            var serializedData = Guid.NewGuid().ToString();

            var fakeFormula = new Mock<IScenarioItem>();
            fakeFormula.SetupGet(f => f.Guid).Returns(guid);

            fakeSerialization.Setup(f => f.SerializeScenarioAsync(guid, default)).ReturnsAsync(serializedData);
            
            var vm = new ExportVM(fakeFormula.Object, fakeSerialization.Object, null);
            //using (var monitor = vm.Monitor()) {
                await vm.InitAndWait();

                // asserts            
            //    monitor.Should().RaisePropertyChangeFor(p => p.DataContent);
            //}

            vm.Should().WithoutError();
            vm.DataContent.Should().Be(serializedData);

            fakeSerialization.Verify(f => f.SerializeScenarioAsync(guid, default), Times.Once);
        }

        [DataTestMethod]
        [DynamicData(nameof(ItemPropertiesTestSource))]
        public void ItemProperties_Test(object content, long expectedCode, string expectedName, SerializedObjectType expectedType)
        {
            // action
            var vm = new ExportVM(content, fakeSerialization.Object, null);

            // asserts
            vm.Code.Should().Be(expectedCode);
            vm.Name.Should().Be(expectedName);
            vm.ObjectType.Should().Be(expectedType);
        }

        public static IEnumerable<object[]> ItemPropertiesTestSource {
            get {
                var fakeFormula = new Mock<IFormulaItem>();
                fakeFormula.SetupGet(f => f.Id).Returns(1);
                fakeFormula.SetupGet(f => f.Name).Returns("abc");
                yield return new object[] { fakeFormula.Object, 1, "abc", SerializedObjectType.Formula };

                fakeFormula = new Mock<IFormulaItem>();
                fakeFormula.SetupGet(f => f.Id).Returns(1);
                fakeFormula.SetupGet(f => f.Name).Returns((string)null);
                yield return new object[] { fakeFormula.Object, 1, "", SerializedObjectType.Formula };

                var fakeScenario = new Mock<IScenarioItem>();
                fakeScenario.SetupGet(f => f.Id).Returns(1);
                fakeScenario.SetupGet(f => f.Name).Returns("abc");
                yield return new object[] { fakeScenario.Object, 1, "abc", SerializedObjectType.Scenario };

                fakeScenario = new Mock<IScenarioItem>();
                fakeScenario.SetupGet(f => f.Id).Returns(1);
                fakeScenario.SetupGet(f => f.Name).Returns((string)null);
                yield return new object[] { fakeScenario.Object, 1, "", SerializedObjectType.Scenario };
            }
        }
    }
}
