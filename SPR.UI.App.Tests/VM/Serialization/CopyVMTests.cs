﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Serialization.Tests
{
    [TestClass]
    public class CopyVMTests
    {
        readonly Mock<ISerialization> fakeSerialization = new Mock<ISerialization>();        

        [TestMethod]
        public void TypeDeclaration_Test()
        {
            typeof(CopyVM).Should()
                .Implement<IDataInitializer>().And
                .Implement<ICommonVM>();
        }

        [DataTestMethod]
        [DataRow(ObjectType.Formula)]
        [DataRow(ObjectType.Scenario)]
        public async Task Initialize_Test(ObjectType objType)
        {
            // setup
            object obj;
            switch (objType) {
                case ObjectType.Formula:
                    obj = new Mock<IFormulaItem>().Object;
                    break;

                case ObjectType.Scenario:
                    obj = new Mock<IScenarioItem>().Object;
                    break;

                default: throw new InvalidOperationException();
            }

            // action
            var vm = new CopyVM(obj, fakeSerialization.Object, null);
            await vm.InitAndWait();

            // asserts
            vm.ImportComplete.Should().BeFalse();
            vm.ShallowCopyCmd.CanExecute(null).Should().BeTrue();
            vm.DeepCopyCmd.CanExecute(null).Should().BeTrue();
        }

        [DataTestMethod]
        [DataRow(ObjectType.Formula)]
        [DataRow(ObjectType.Scenario)]
        public async Task ShallowCopy_Test(ObjectType objType)
        {
            // setup
            var guid = Guid.NewGuid();
            object obj;
            switch (objType) {
                case ObjectType.Formula:
                    var fakeFormula = new Mock<IFormulaItem>();
                    fakeFormula.SetupGet(f => f.Guid).Returns(guid);
                    obj = fakeFormula.Object;
                    break;

                case ObjectType.Scenario:
                    var fakeScenario = new Mock<IScenarioItem>();
                    fakeScenario.SetupGet(f => f.Guid).Returns(guid);
                    obj = fakeScenario.Object;
                    break;

                default: throw new InvalidOperationException();
            }
            
            var vm = new CopyVM(obj, fakeSerialization.Object, null);

            // action
            await vm.InitAndWait();
            //using (var monitor = ((object)vm).Monitor()) {
                vm.ShallowCopyCmd.Execute(null);
                await vm.WaitBusy();

                // asserts
                vm.Should().WithoutError();
                vm.ImportComplete.Should().BeTrue();

                fakeSerialization.Verify(f => f.ShallowCopyAsync(objType, guid, It.IsAny<CancellationToken>()), Times.Once);

                //monitor.Should().RaisePropertyChangeFor(i => i.ImportComplete);
            //}
        }

        [DataTestMethod]
        [DataRow(ObjectType.Formula)]
        [DataRow(ObjectType.Scenario)]
        public async Task DeepCopy_Test(ObjectType objType)
        {
            // setup
            var guid = Guid.NewGuid();
            object obj;
            switch (objType) {
                case ObjectType.Formula:
                    var fakeFormula = new Mock<IFormulaItem>();
                    fakeFormula.SetupGet(f => f.Guid).Returns(guid);
                    obj = fakeFormula.Object;
                    break;

                case ObjectType.Scenario:
                    var fakeScenario = new Mock<IScenarioItem>();
                    fakeScenario.SetupGet(f => f.Guid).Returns(guid);
                    obj = fakeScenario.Object;
                    break;

                default: throw new InvalidOperationException();
            }

            var vm = new CopyVM(obj, fakeSerialization.Object, null);

            // action
            await vm.InitAndWait();
            //using (var monitor = vm.Monitor()) {
                vm.DeepCopyCmd.Execute(null);
                await vm.WaitBusy();

                // asserts
                vm.Should().WithoutError();
                vm.ImportComplete.Should().BeTrue();

                fakeSerialization.Verify(f => f.DeepCopyAsync(objType, guid, It.IsAny<CancellationToken>()), Times.Once);

                //monitor.Should().RaisePropertyChangeFor(i => i.ImportComplete);
            //}
        }

        [DataTestMethod]
        [DynamicData(nameof(ItemPropertiesTestSource))]
        public void ItemProperties_Test(object content, long expectedCode, string expectedName, ObjectType expectedType)
        {
            // action
            var vm = new CopyVM(content, fakeSerialization.Object, null);

            // asserts
            vm.Code.Should().Be(expectedCode);
            vm.Name.Should().Be(expectedName);
            vm.ObjectType.Should().Be(expectedType);
        }

        public static IEnumerable<object[]> ItemPropertiesTestSource {
            get {
                var fakeFormula = new Mock<IFormulaItem>();
                fakeFormula.SetupGet(f => f.Id).Returns(1);
                fakeFormula.SetupGet(f => f.Name).Returns("abc");
                yield return new object[] { fakeFormula.Object, 1, "abc", ObjectType.Formula };

                fakeFormula = new Mock<IFormulaItem>();
                fakeFormula.SetupGet(f => f.Id).Returns(1);
                fakeFormula.SetupGet(f => f.Name).Returns((string)null);
                yield return new object[] { fakeFormula.Object, 1, "", ObjectType.Formula };

                var fakeScenario = new Mock<IScenarioItem>();
                fakeScenario.SetupGet(f => f.Id).Returns(1);
                fakeScenario.SetupGet(f => f.Name).Returns("abc");
                yield return new object[] { fakeScenario.Object, 1, "abc", ObjectType.Scenario };

                fakeScenario = new Mock<IScenarioItem>();
                fakeScenario.SetupGet(f => f.Id).Returns(1);
                fakeScenario.SetupGet(f => f.Name).Returns((string)null);
                yield return new object[] { fakeScenario.Object, 1, "", ObjectType.Scenario };
            }
        }
    }
}
