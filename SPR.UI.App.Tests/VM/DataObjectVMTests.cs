﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Formula;
using FakeItEasy;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.App.VM.Base;
using SPR.UI.Core.Shared;

namespace SPR.UI.App.VM.Tests
{
    [TestClass]
    public class DataObjectVMTests
    {
        public class MyDataObjectVM : MyDataObjectVMT<object>
        {
            public MyDataObjectVM(Guid guid, IDataSource<object> source) : base(guid, source) {
            }
        };

        public class MyDataObjectVMT<T> : DataObjectVM<T> where T : class
        {
            public MyDataObjectVMT(Guid guid, IDataSource<T> source) : base(guid, DAL.Enums.SerializedObjectType.Formula,
                source, null, null) {
            }

            public new object Item => base.Item;

            public override long ID => throw new NotImplementedException();

            public override int Ver => throw new NotImplementedException();

            public override string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public void SetChanged() { IsChanged = true; }

            protected override T CreateNewItem() {
                throw new NotImplementedException();
            }
        }

        public class TestObjectVM : DataObjectVM<object>
        {
            public TestObjectVM(Guid? guid, IDataSource<object> source) 
                : base(guid, DAL.Enums.SerializedObjectType.Formula, source, null, null)
            {
            }

            public override long ID => throw new NotImplementedException();

            public override int Ver => throw new NotImplementedException();

            public override string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public void SetChanged() { IsChanged = true; }

            protected override object CreateNewItem() => throw new NotImplementedException();
        }

        [TestMethod]
        public void Initialize_WithoutDraft_Test() {
            object item = new object();
            Guid guid = Guid.NewGuid();
            FakeDataSource<object> src = new FakeDataSource<object>(
                getMain: (id) => {
                    Assert.AreEqual(guid, id);
                    return new ObjectResponse<object>() { Item = item, Kind = ObjectVersionKind.Main };
                });

            MyDataObjectVM vm = new MyDataObjectVM(guid, src);
            vm.RefreshCmd.Execute(null);

            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.AreSame(item, vm.Item);
            Assert.IsTrue(vm.IsExistsMain);
            Assert.IsFalse(vm.IsExistsDraft);
            Assert.IsFalse(vm.IsReadOnly);
        }        

        /// <summary>
        /// Load formula without draft and modify main version. Draft version must be created
        /// </summary>
        [TestMethod]
        public void ModifyMain_Test() {
            object item = new object();
            Guid guid = Guid.NewGuid();
            FakeDataSource<object> src = new FakeDataSource<object>(
                getMain: (id) => {
                    Assert.AreEqual(guid, id);
                    return new ObjectResponse<object>() { Item = item, Kind = ObjectVersionKind.Main };
                });

            MyDataObjectVM vm = new MyDataObjectVM(guid, src);
            vm.RefreshCmd.Execute(null);
            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);

            vm.SetChanged();

            Assert.AreEqual(DataContract.ShownObjectEnum.Draft, vm.ShownObject);
            Assert.AreSame(item, vm.Item);
            Assert.IsTrue(vm.IsExistsMain);
            Assert.IsTrue(vm.IsExistsDraft);
            Assert.IsFalse(vm.IsReadOnly);
        }
        

        [TestMethod]
        public void Switch_MainToDraft_WithoutDraft_Test() {
            var item = new object();
            Guid guid = Guid.NewGuid();
            FakeDataSource<object> src = new FakeDataSource<object>(
                getMain: (id) => {
                    Assert.AreEqual(guid, id);
                    return new ObjectResponse<object>() { Item = item, Kind = ObjectVersionKind.Main };
                });

            MyDataObjectVM vm = new MyDataObjectVM(guid, src);
            vm.RefreshCmd.Execute(null);

            vm.ShownObject = DataContract.ShownObjectEnum.Draft;

            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.AreSame(item, vm.Item);
            Assert.IsTrue(vm.IsExistsMain);
            Assert.IsFalse(vm.IsExistsDraft);
            Assert.IsFalse(vm.IsReadOnly);
        }        

        [TestMethod]
        public void CodeProperty() {
            var main = new FormulaModel() { Code = 2 };
            Guid guid = Guid.NewGuid();
            FakeDataSource<FormulaModel> src = new FakeDataSource<FormulaModel>(
                //initialize returns draft
                getMain: (id) => new ObjectResponse<FormulaModel>() { Item = main, Kind = ObjectVersionKind.Main }
                );

            MyDataObjectVMT<FormulaModel> vm = new MyDataObjectVMT<FormulaModel>(guid, src);
            //initialize refresh
            vm.RefreshCmd.Execute(null);

            Assert.AreEqual(2, vm.Code);
        }

        [TestMethod]
        public void SwitchToArchive_Test() {
            object item = new object();
            object archive3 = new object();
            int versionsCalled = 0;
            Guid guid = Guid.NewGuid();
            FakeDataSource<object> src = new FakeDataSource<object>(
                getMain: (id) => {
                    Assert.AreEqual(guid, id);
                    return new ObjectResponse<object>() { Item = item, Kind = ObjectVersionKind.Main };
                },
                versions: (id) => {
                    versionsCalled += 1;
                    Assert.AreEqual(guid, id);
                    return new int[] { 3, 2, 1 };
                },
                getVersion: (id, ver) => {
                    Assert.AreEqual(guid, id);
                    Assert.AreEqual(3, ver);
                    return archive3;
                });

            MyDataObjectVM vm = new MyDataObjectVM(guid, src);
            vm.RefreshCmd.Execute(null);

            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.AreEqual(0, versionsCalled);
            //Assert.IsNull(vm.ArchiveVersions);

            vm.ShownObject = DataContract.ShownObjectEnum.Archive;
            Assert.AreEqual(DataContract.ShownObjectEnum.Archive, vm.ShownObject);
            Assert.AreSame(archive3, vm.Item);
            Assert.AreEqual(1, versionsCalled);
        }

        [TestMethod]
        public void SwitchToArchive_NotFound_Test() {
            object item = new object();
            object archive3 = new object();
            Guid guid = Guid.NewGuid();
            FakeDataSource<object> src = new FakeDataSource<object>(
                getMain: (id) => {
                    Assert.AreEqual(guid, id);
                    return new ObjectResponse<object>() { Item = item, Kind = ObjectVersionKind.Main };
                },
                versions: (id) => {
                    Assert.AreEqual(guid, id);
                    return new int[] { };
                },
                getVersion: (id, ver) => throw new NotImplementedException());

            MyDataObjectVM vm = new MyDataObjectVM(guid, src);
            vm.RefreshCmd.Execute(null);

            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);

            vm.ShownObject = DataContract.ShownObjectEnum.Archive;
            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.AreSame(item, vm.Item);
        }

        [TestMethod]
        public void SwitchToArchiveVersion_Test() {
            object item = new object();
            object archive1 = new object();
            object archive2 = new object();
            object archive3 = new object();
            int versionsCalled = 0;
            Guid guid = Guid.NewGuid();
            FakeDataSource<object> src = new FakeDataSource<object>(
                getMain: (id) => {
                    Assert.AreEqual(guid, id);
                    return new ObjectResponse<object>() { Item = item, Kind = ObjectVersionKind.Main };
                },
                versions: (id) => {
                    versionsCalled += 1;
                    Assert.AreEqual(guid, id);
                    return new int[] { 1, 2, 3 };
                },
                getVersion: (id, ver) => {
                    Assert.AreEqual(guid, id);
                    switch (ver) {
                        case 1: return archive1;
                        case 2: return archive2;
                        case 3: return archive3;
                        default: return null;
                    }
                });

            MyDataObjectVM vm = new MyDataObjectVM(guid, src);
            vm.RefreshCmd.Execute(null);

            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.AreEqual(0, versionsCalled);
            
            vm.SelectedArchiveVersion = 1;            
            Assert.AreEqual(DataContract.ShownObjectEnum.Archive, vm.ShownObject);                        
            Assert.AreSame(archive1, vm.Item);

            vm.SelectedArchiveVersion = 2;
            Assert.AreEqual(DataContract.ShownObjectEnum.Archive, vm.ShownObject);
            Assert.AreSame(archive2, vm.Item);

            vm.SelectedArchiveVersion = 3;
            Assert.AreEqual(DataContract.ShownObjectEnum.Archive, vm.ShownObject);
            Assert.AreSame(archive3, vm.Item);

            vm.SelectedArchiveVersion = 4;
            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.AreSame(item, vm.Item);

            vm.SelectedArchiveVersion = 3;
            vm.ShownObject = DataContract.ShownObjectEnum.Main;
            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.AreSame(item, vm.Item);

            vm.SelectedArchiveVersion = 3;
            Assert.AreEqual(DataContract.ShownObjectEnum.Archive, vm.ShownObject);
            Assert.AreSame(archive3, vm.Item);

            Assert.AreEqual(0, versionsCalled);
        }

        //1. Exist main, exist draft and shown object is draft. Cleare draft and should switch to main
        [TestMethod]
        public async Task ClearDraft_WithMain_SelectedDraft_Test()
        {
            var vm = CreateVM();
            await vm.InitAndWait();

            vm.SetChanged();
            Assert.AreEqual(DataContract.ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsTrue(vm.IsExistsDraft);
            Assert.IsFalse(vm.IsReadOnly);

            vm.ClearDraftCmd.Execute(null);
            await vm.WaitBusy();

            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsFalse(vm.IsExistsDraft);
            Assert.IsFalse(vm.IsReadOnly);
        }

        //2. Exist main, exist draft and draft not selected
        [TestMethod]
        public async Task ClearDraft_WithMain_SelectedMain_Test()
        {
            var vm = CreateVM();
            await vm.InitAndWait();

            vm.SetChanged();
            vm.ShownObject = DataContract.ShownObjectEnum.Main;

            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsTrue(vm.IsExistsDraft);
            Assert.IsTrue(vm.IsReadOnly);

            vm.ClearDraftCmd.Execute(null);
            await vm.WaitBusy();

            Assert.AreEqual(DataContract.ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsFalse(vm.IsExistsDraft);
            Assert.IsFalse(vm.IsReadOnly);
        }

        //3. Without main
        [TestMethod]
        public async Task ClearDraft_WithoutMain_Test()
        {
            var ds = A.Fake<IDataSource<object>>();
            A.CallTo(() => ds.GetMain(A<Guid>._, A<CancellationToken>._)).Returns(new ObjectContainer<object> { Item = null });

            var vm = CreateVM(null, ds);
            A.CallTo(vm).Where(i => i.Method.Name == "CreateNewItem")
                .WithReturnType<object>()
                .Returns(new object());
            await vm.InitAndWait();

            vm.SetChanged();

            Assert.IsTrue(vm.IsExistsDraft);
            Assert.IsFalse(vm.IsExistsMain);
            Assert.AreEqual(DataContract.ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsFalse(vm.IsReadOnly);
            Assert.IsTrue(vm.IsChanged);

            Fake.ClearRecordedCalls(vm);

            vm.ClearDraftCmd.Execute(null);
            await vm.WaitBusy();

            Assert.IsTrue(vm.IsExistsDraft);
            Assert.IsFalse(vm.IsExistsMain);
            Assert.AreEqual(DataContract.ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsFalse(vm.IsReadOnly);
            Assert.IsFalse(vm.IsChanged);

            A.CallTo(vm).Where(i => i.Method.Name == "CreateNewItem").MustHaveHappenedOnceExactly();
        }

        private TestObjectVM CreateVM(IDataSource<object> ds = null) => CreateVM(Guid.NewGuid(), ds);

        private TestObjectVM CreateVM(Guid? guid, IDataSource<object> ds = null)
        {
            if (ds == null) {
                ds = A.Fake<IDataSource<object>>();
                A.CallTo(() => ds.GetMain(A<Guid>._, A<CancellationToken>._)).Returns(new ObjectContainer<object> { Item = new object() });
            }

            var vm = A.Fake<TestObjectVM>(x => {
                x.WithArgumentsForConstructor(new object[] { guid, ds });
                x.CallsBaseMethods();
            });

            return vm;
        }
    }
}
