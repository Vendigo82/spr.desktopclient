﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Services;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.UI.App.VM.Formulas.Tests
{
    [TestClass]
    public class FormulaUsageVMTests
    {
        [TestMethod]
        public async Task Refresh()
        {
            var f = new FormulaBriefModel[] { };
            var sf = new ScenarioSlimModel[] { };
            var scr = new ScenarioSlimModel[] { };
            var sc = new ScenarioSlimModel[] { };

            var source = A.Fake<IFormulaRepository>();
            A.CallTo(() => source.GetUsage(A<Guid>._, A<CancellationToken>._))
                .Returns(new FormulaUsageResponse {
                    Formulas = f,
                    Scenarios = new UsageScenarioPair[] {
                        new UsageScenarioPair { Key = FormulaUsageType.Filter, Value = sf },
                        new UsageScenarioPair { Key = FormulaUsageType.CalcResult, Value = scr },
                        new UsageScenarioPair { Key = FormulaUsageType.Checkup, Value = sc }
                    }
                });

            Guid guid = Guid.NewGuid();
            var vm = new FormulaUsageVM(guid, source, null);
            var npc = new NotifyPropertyChangedCollector(vm);

            vm.RefreshCmd.Execute(null);

            await vm.WaitBusy();

            Assert.AreSame(f, vm.UsedInFormulas);
            Assert.AreSame(scr, vm.UsedInScenarioAsCalcResult);
            Assert.AreSame(sc, vm.UsedInScenarioAsCheckup);
            Assert.AreSame(sf, vm.UsedInScenarioAsFilter);

            A.CallTo(() => source.GetUsage(A<Guid>.That.IsEqualTo(guid), A<CancellationToken>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => source.GetUsage(A<Guid>.That.Not.IsEqualTo(guid), A<CancellationToken>._)).MustNotHaveHappened();

            CollectionAssert.IsSubsetOf(
                new string[] {
                    nameof(FormulaUsageVM.UsedInFormulas),
                    nameof(FormulaUsageVM.UsedInScenarioAsCalcResult),
                    nameof(FormulaUsageVM.UsedInScenarioAsCheckup),
                    nameof(FormulaUsageVM.UsedInScenarioAsFilter)
                },
                npc.ToArray());
        }
    }
}
