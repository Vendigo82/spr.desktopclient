﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using AutoFixture;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.VM;
using Moq;
using SPR.UI.App.Abstractions.DataSources;

namespace SPR.UI.App.VM.Formulas.Tests
{
    [TestClass]
    public class FormulaCheckupsListVMTests
    {
        readonly Fixture fixture;
        readonly Mock<IDataListSource<FormulaScenarioCheckupModel>> fakeSource;

        readonly FormulaCheckupsListVM vm;
        readonly IEnumerable<FormulaScenarioCheckupModel> sourceItems;

        public FormulaCheckupsListVMTests()
        {
            fixture = new Fixture();
            fakeSource = new Mock<IDataListSource<FormulaScenarioCheckupModel>>();

            //set source reult
            sourceItems = fixture.CreateMany<FormulaScenarioCheckupModel>();
            fakeSource.Setup(f => f.GetItemsAsync(default)).ReturnsAsync(sourceItems);
            fakeSource.Setup(f => f.LoadItemsAsync(default)).ReturnsAsync(sourceItems);

            //fixture.cre
            vm = new FormulaCheckupsListVM(fakeSource.Object, new Mock<ILogger>().Object);
        }

        [TestMethod]
        public void TypeDeclaration_Test()
        {
            //asserts
            vm.GetType().Should()
                .BeAssignableTo<ICommonVM>().And
                .BeAssignableTo<IDataInitializer>().And
                .HaveProperty<IEnumerable<FormulaScenarioCheckupModel>>("SelectedItems");               
        }

        [TestMethod]
        public async Task Items_Test()
        {
            // setup            

            // action
            await vm.InitAndWait();

            // asserts
            fakeSource.Verify(f => f.GetItemsAsync(default), Times.Once);
            fakeSource.VerifyNoOtherCalls();

            vm.Items.Should()
                .NotBeNullOrEmpty().And
                .BeEquivalentTo(sourceItems, o => o
                    .Excluding(i => i.Code)
                    .Excluding(i => i.Guid)
                    .Excluding(i => i.Name)
                    .Excluding(i => i.Type)
                    .ExcludingMissingMembers()
                    ).And
                .Equal(sourceItems, (i, j) => ReferenceEquals(i.Item, j)).And
                .OnlyContain(i => i.Checked == false);
        }

        [TestMethod]
        public async Task SelectedItems_Set_Test()
        {
            // setup
            await vm.InitAndWait();

            // action
            vm.SelectedItems = sourceItems.Take(1);

            // asserts
            vm.Items.Where(i => i.Checked).Select(i => i.Item).ToArray().Should()
                .HaveCount(1).And
                .BeEquivalentTo(sourceItems.Take(1));
        }

        [TestMethod]
        public async Task SelectedItems_Get_Test()
        {
            // setup
            await vm.InitAndWait();

            // action
            vm.Items.First().Checked = true;

            // asserts
            vm.SelectedItems.Should().HaveCount(1).And
                .BeEquivalentTo(sourceItems.Take(1));
        }

        [TestMethod]
        public async Task SelectedItem_AfterInitialize_ShouldHaveValue_Test()
        {
            // action
            await vm.InitAndWait();

            // asserts
            vm.SelectedItem.Should().NotBeNull();
        }

        [TestMethod]
        public async Task SelectedItem_AfterRefresh_ShouldHaveValue_Test()
        {
            // setup
            await vm.InitAndWait();
            vm.SelectedItem = null;

            // action
            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.SelectedItem.Should().NotBeNull();
        }

        [TestMethod]
        public async Task SelectedItems_OnRefresh_Test()
        {
            // setup
            await vm.InitAndWait();
            vm.Items.First().Checked = true;
            vm.Items.Last().Checked = true;
            var selected = vm.Items.Where(i => i.Checked).Select(i => i.Item.Guid).ToArray();

            // action
            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Items.Where(i => i.Checked).Select(i => i.Item.Guid).Should().BeEquivalentTo(selected);
        }
    }
}
