﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using SPR.UI.App.Abstractions.Tests;
using SPR.UI.App.Customizations;
using SPR.UI.WebService.DataContract.Formula;
using SPR.WebService.TestModule.Client;
using SPR.WebService.TestModule.DataContract;
using SPR.WebService.TestModule.DataContract.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Formulas.Tests
{
    [TestClass]
    public class FormulaTestVMTests
    {
        readonly MockRepository repository = new MockRepository(MockBehavior.Strict);
        readonly Mock<ITestItemAccessor<FormulaModel>> fakeItemAccessor = new Mock<ITestItemAccessor<FormulaModel>>();
        readonly Mock<ITestModuleClient> fakeClient;// = new Mock<ITestModuleClient>();
        readonly Mock<ITestResultsAcceptor> fakeAcceptor;// = new Mock<ITestResultsAcceptor>();
        readonly Mock<ITestInputDataProvider> fakeTestData = new Mock<ITestInputDataProvider>();
        readonly Mock<HttpClientHandler> httpClientHandler = new Mock<HttpClientHandler>();
        readonly Mock<Func<HttpClient>> httpClientFactory = new Mock<Func<HttpClient>>();

        readonly Fixture fixture = new Fixture();

        public FormulaTestVMTests()
        {
            fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => fixture.Behaviors.Remove(b));
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            fixture.Customize(new AutoMoqCustomization() { ConfigureMembers = true });
            fixture.Customize(new JTokenCustomization());

            fakeClient = repository.Create<ITestModuleClient>();
            fakeAcceptor = repository.Create<ITestResultsAcceptor>();
            
            fakeAcceptor.Setup(f => f.Cancel(It.IsAny<DataContract.ObjectType>(), It.IsAny<long>(), It.IsAny<DataContract.ShownObjectEnum>()));

            fakeItemAccessor.Setup(f => f.Guid).ReturnsUsingFixture(fixture);
            fakeItemAccessor.Setup(f => f.Id).ReturnsUsingFixture(fixture);
            fakeItemAccessor.Setup(f => f.Item).ReturnsUsingFixture(fixture);
            fakeItemAccessor.Setup(f => f.ShownObject).Returns(DataContract.ShownObjectEnum.Main);
            //itemAccessor = fixture.Create<ITestItemAccessor<FormulaModel>>();
            fakeTestData.Setup(f => f.GetInputData()).Returns(new Newtonsoft.Json.Linq.JObject().ToString());            
        }

        [TestMethod]
        public async Task MainTesting_Test()
        {
            // setup
            var vm = CreateVM();
            var response = fixture.Create<TestFormulaResponse>();

            var seq = new MockSequence();
            fakeAcceptor.InSequence(seq).Setup(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main));
            fakeClient.InSequence(seq).Setup(f => f.TestFormula(It.IsAny<TestMainFormulaRequest>(), default)).ReturnsAsync(response);
            fakeAcceptor.InSequence(seq).Setup(f => f.PushResult(fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main, response, null));

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts

            fakeAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main), Times.Once);
            fakeClient.Verify(f => f.TestFormula(
                Match.Create<TestMainFormulaRequest>(i => i.FormulaId == fakeItemAccessor.Object.Id && i.Data.ToString() == vm.RequestData),
                default),
                Times.Once);
            fakeAcceptor.Verify(f => f.PushResult(fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main, response, null), Times.Once);
            repository.VerifyNoOtherCalls();

            vm.Should().WithoutError();
        }

        [TestMethod]
        public async Task MainTesting_Error_Test()
        {
            // setup
            var vm = CreateVM();
            var response = fixture.Create<TestFormulaResponse>();

            var seq = new MockSequence();
            fakeAcceptor.InSequence(seq).Setup(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main));
            fakeClient.InSequence(seq).Setup(f => f.TestFormula(It.IsAny<TestMainFormulaRequest>(), default)).ThrowsAsync(new InvalidOperationException());
            fakeAcceptor.InSequence(seq).Setup(f => f.Cancel(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main));

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts

            fakeAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main), Times.Once);
            fakeClient.Verify(f => f.TestFormula(It.IsAny<TestMainFormulaRequest>(), default), Times.Once);
            fakeAcceptor.Verify(f => f.Cancel(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main), Times.Once);
            repository.VerifyNoOtherCalls();

            vm.Should().HasError();
        }

        [TestMethod]
        public async Task DraftTesting_Test()
        {
            // setup
            var vm = CreateVM();
            var response = fixture.Create<TestFormulaResponse>();
            fakeItemAccessor.Setup(f => f.ShownObject).Returns(DataContract.ShownObjectEnum.Draft);

            var seq = new MockSequence();
            fakeAcceptor.InSequence(seq).Setup(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft));
            fakeClient.InSequence(seq).Setup(f => f.TestFormula(It.IsAny<TestDraftFormulaRequest>(), default)).ReturnsAsync(response);
            fakeAcceptor.InSequence(seq).Setup(f => f.PushResult(fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft, response, null));

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            fakeAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft), Times.Once);
            fakeClient.Verify(f => f.TestFormula(
                Match.Create<TestDraftFormulaRequest>(i => i.Formula == fakeItemAccessor.Object.Item && i.Data.ToString() == vm.RequestData),
                default),
                Times.Once); fakeAcceptor.Verify(f => f.PushResult(fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft, response, null), Times.Once);            
            fakeAcceptor.Verify(f => f.PushResult(fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft, response, null), Times.Once);
            repository.VerifyNoOtherCalls();

            vm.Should().WithoutError();
        }

        [TestMethod]
        public async Task DraftTesting_Error_Test()
        {
            // setup
            var vm = CreateVM();
            var response = fixture.Create<TestFormulaResponse>();
            fakeItemAccessor.Setup(f => f.ShownObject).Returns(DataContract.ShownObjectEnum.Draft);

            var seq = new MockSequence();
            fakeAcceptor.InSequence(seq).Setup(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft));
            fakeClient.InSequence(seq).Setup(f => f.TestFormula(It.IsAny<TestDraftFormulaRequest>(), default)).ThrowsAsync(new InvalidOperationException());
            fakeAcceptor.InSequence(seq).Setup(f => f.Cancel(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft));

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts

            fakeAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft), Times.Once);
            fakeClient.Verify(f => f.TestFormula(It.IsAny<TestDraftFormulaRequest>(), default), Times.Once);
            fakeAcceptor.Verify(f => f.Cancel(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Draft), Times.Once);
            repository.VerifyNoOtherCalls();

            vm.Should().HasError();
        }

        [DataTestMethod]
        [DataRow(DataContract.ShownObjectEnum.Archive)]
        [DataRow(DataContract.ShownObjectEnum.Main)]
        [DataRow(DataContract.ShownObjectEnum.Draft)]
        public async Task Rest_ServerSide_Test(DataContract.ShownObjectEnum shownObject)
        {
            // setup
            var vm = CreateVM(DAL.Enums.FormulaType.RESTService);
            vm.RestTestingMethod = DataContract.Testing.RestTestingMethod.ServerSide;
            fakeItemAccessor.Setup(f => f.ShownObject).Returns(shownObject);

            fakeAcceptor.Setup(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, shownObject));
            fakeAcceptor.Setup(f => f.PushResult(fakeItemAccessor.Object.Id, DataContract.ShownObjectEnum.Main, It.IsAny<TestFormulaResponse>(), null));

            fakeClient.Setup(f => f.TestFormula(It.IsAny<TestDraftFormulaRequest>(), default)).ReturnsUsingFixture(fixture);
            fakeClient.Setup(f => f.TestFormula(It.IsAny<TestMainFormulaRequest>(), default)).ReturnsUsingFixture(fixture);

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            if (shownObject == DataContract.ShownObjectEnum.Main)
                fakeClient.Verify(f => f.TestFormula(It.IsAny<TestMainFormulaRequest>(), default), Times.Once);
           else
                fakeClient.Verify(f => f.TestFormula(It.IsAny<TestDraftFormulaRequest>(), default), Times.Once);
            fakeClient.VerifyNoOtherCalls();
        }

        [DataTestMethod]
        [DataRow("POST", true, DataContract.ShownObjectEnum.Main)]
        [DataRow("POST", true, DataContract.ShownObjectEnum.Draft)]
        [DataRow("GET", false, DataContract.ShownObjectEnum.Draft)]
        public async Task Rest_LocalSide_Test(string httpVerb, bool withBody, DataContract.ShownObjectEnum shownObject)
        {
            // setup
            var vm = CreateVM(DAL.Enums.FormulaType.RESTService);
            vm.RestTestingMethod = DataContract.Testing.RestTestingMethod.LocalSide;
            fakeItemAccessor.Setup(f => f.ShownObject).Returns(shownObject);


            var initialResponse = fixture.Build<TestResponse<RestFormulaRequestInfoDto>>()
                .With(p => p.Success, true)
                .With(p => p.Data, () => fixture.Build<RestFormulaRequestInfoDto>()
                    .With(p => p.Url, () => "http://localhost/" + fixture.Create<string>())
                    .With(p => p.HttpVerb, () => httpVerb)
                    .With(p => p.Body, () => withBody ? fixture.Create<string>() : null)
                    .Create())
                .Create();
            var httpStatusCode = fixture.Create<int>();
            var httpResponseData = fixture.Create<string>();
            var httpResponse = new HttpResponseMessage((System.Net.HttpStatusCode)httpStatusCode) {
                Content = new StringContent(httpResponseData, Encoding.UTF8, "application/json")
            };
            var finalResponse = fixture.Create<TestResponse<FormulaResultDto>>();

            string localRequestVerb = null;
            string localRequestUrl = null;
            string localRequestBody = null;
            var resultList = new List<TestFormulaResponse>();
            var additionalDataList = new List<IEnumerable<KeyValuePair<string, object>>>();

            var seq = new MockSequence();
            fakeAcceptor.InSequence(seq).Setup(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, shownObject));
            fakeClient.InSequence(seq).Setup(f => f.InitialTestRestFormulaAsync(It.IsAny<TestDraftFormulaRequest>(), default)).ReturnsAsync(initialResponse);
            httpClientHandler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(httpResponse))
                .Callback<HttpRequestMessage, CancellationToken>((r, ct) => {
                    localRequestVerb = r.Method.Method;
                    localRequestUrl = r.RequestUri.ToString();
                    localRequestBody = r.Content != null ? r.Content.ReadAsStringAsync().Result : null;
                });
            fakeClient.InSequence(seq).Setup(f => f.FinalTestRestFormulaAsync(It.IsAny<FinalRestFormulaRequest>(), default)).ReturnsAsync(finalResponse);
            fakeAcceptor.InSequence(seq)
                .Setup(f => f.PushResult(fakeItemAccessor.Object.Id, shownObject, Capture.In(resultList), Capture.In(additionalDataList)));

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();

            fakeAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, shownObject), Times.Once);

            fakeClient.Verify(f => f.InitialTestRestFormulaAsync(
                // contains right request data and object's item
                Match.Create<TestDraftFormulaRequest>(i => i.Data.ToString() == vm.RequestData && i.Formula == fakeItemAccessor.Object.Item), default), 
                Times.Once);

            // local http request was performing with right url and data
            httpClientHandler.Protected().Verify<Task<HttpResponseMessage>>("SendAsync", Times.Once(), 
                ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>());
            localRequestVerb.Should().Be(httpVerb);
            localRequestUrl.Should().Be(initialResponse.Data.Url);
            if (withBody)
                localRequestBody.Should().Be(initialResponse.Data.Body);
            else
                localRequestBody.Should().BeNull();

            // final request to testing module with local response
            fakeClient.Verify(f => f.FinalTestRestFormulaAsync(Match.Create<FinalRestFormulaRequest>(i => i.Formula == fakeItemAccessor.Object.Item 
                && i.Response.ResponseData == httpResponseData
                && i.Response.HttpStatusCode == httpStatusCode), default), Times.Once);

            // verify resulting object
            fakeAcceptor.Verify(f => f.PushResult(fakeItemAccessor.Object.Id, shownObject, It.IsAny<TestFormulaResponse>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()), 
                Times.Once);            
            repository.VerifyNoOtherCalls();

            // testing result 
            var result = resultList.Should().ContainSingle().Which;
            result.Data.Should().BeSameAs(finalResponse.Data);
            result.Error.Should().BeSameAs(finalResponse.Error);
            result.Results.Should().BeEquivalentTo(initialResponse.Results);
            result.Success.Should().Be(finalResponse.Success);

            // testing additional data
            var additional = additionalDataList.Should().ContainSingle().Which.Should().NotBeNull().And.Subject;
            additional.Select(i => i.Key).Should().BeEquivalentTo(
                new[] { "HttpRequestUrl", "HttpRequestBody", "HttpResponseStatusCode", "HttpResponseBody", "HttpRequestTime", "HttpRequestMethod" });
            additional.First(i => i.Key == "HttpRequestMethod").Value.Should().Be(initialResponse.Data.HttpVerb);
            additional.First(i => i.Key == "HttpRequestUrl").Value.Should().Be(initialResponse.Data.Url);
            additional.First(i => i.Key == "HttpRequestBody").Value.Should().Be(initialResponse.Data.Body);
            additional.First(i => i.Key == "HttpResponseStatusCode").Value.Should().Be(httpStatusCode);
            additional.First(i => i.Key == "HttpResponseBody").Value.Should().Be(httpResponseData);
            additional.First(i => i.Key == "HttpRequestTime").Value.Should().BeOfType<TimeSpan>();
        }

        [DataTestMethod]
        [DataRow(DataContract.ShownObjectEnum.Main)]
        [DataRow(DataContract.ShownObjectEnum.Draft)]
        public async Task Rest_LocalSide_Failed_Test(DataContract.ShownObjectEnum shownObject)
        {
            // setup
            var vm = CreateVM(DAL.Enums.FormulaType.RESTService);
            vm.RestTestingMethod = DataContract.Testing.RestTestingMethod.LocalSide;
            fakeItemAccessor.Setup(f => f.ShownObject).Returns(shownObject);

            var initialResponse = fixture.Build<TestResponse<RestFormulaRequestInfoDto>>()
                .With(p => p.Success, false)
                .Without(p => p.Data)
                .Create();

            var resultList = new List<TestFormulaResponse>();
            var additionalDataList = new List<IEnumerable<KeyValuePair<string, object>>>();

            fakeAcceptor.Setup(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, shownObject));
            fakeClient.Setup(f => f.InitialTestRestFormulaAsync(It.IsAny<TestDraftFormulaRequest>(), default)).ReturnsAsync(initialResponse);
            fakeAcceptor.Setup(f => f.PushResult(fakeItemAccessor.Object.Id, shownObject, Capture.In(resultList), Capture.In(additionalDataList)));

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();

            fakeAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, shownObject), Times.Once);

            fakeClient.Verify(f => f.InitialTestRestFormulaAsync(
                // contains right request data and object's item
                Match.Create<TestDraftFormulaRequest>(i => i.Data.ToString() == vm.RequestData && i.Formula == fakeItemAccessor.Object.Item), default),
                Times.Once);

            // testing result 
            var result = resultList.Should().ContainSingle().Which;
            result.Data.Should().BeNull();
            result.Error.Should().BeSameAs(initialResponse.Error);
            result.Results.Should().BeEquivalentTo(initialResponse.Results);
            result.Success.Should().BeFalse();

            // testing additional data
            additionalDataList.Should().ContainSingle().Which.Should().BeNull();            
        }

        [DataTestMethod]
        [DataRow(DataContract.ShownObjectEnum.Main)]
        [DataRow(DataContract.ShownObjectEnum.Draft)]
        public async Task Rest_MakeRequest_Test(DataContract.ShownObjectEnum shownObject)
        {
            // setup
            var vm = CreateVM(DAL.Enums.FormulaType.RESTService);
            vm.RestTestingMethod = DataContract.Testing.RestTestingMethod.MakeRequest;
            fakeItemAccessor.Setup(f => f.ShownObject).Returns(shownObject);

            var initialResponse = fixture.Build<TestResponse<RestFormulaRequestInfoDto>>()
                .With(p => p.Success, true)
                .With(p => p.Data, () => fixture.Build<RestFormulaRequestInfoDto>()
                    .With(p => p.Url, () => "http://localhost/" + fixture.Create<string>())
                    .Create())
                .Create();

            var resultList = new List<TestFormulaResponse>();
            var additionalDataList = new List<IEnumerable<KeyValuePair<string, object>>>();

            var seq = new MockSequence();
            fakeAcceptor.InSequence(seq).Setup(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, shownObject));
            fakeClient.InSequence(seq).Setup(f => f.InitialTestRestFormulaAsync(It.IsAny<TestDraftFormulaRequest>(), default)).ReturnsAsync(initialResponse);
            fakeAcceptor.InSequence(seq)
                .Setup(f => f.PushResult(fakeItemAccessor.Object.Id, shownObject, Capture.In(resultList), Capture.In(additionalDataList)));

            // action
            vm.TestCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();

            fakeAcceptor.Verify(f => f.StartTesting(DataContract.ObjectType.Formula, fakeItemAccessor.Object.Id, shownObject), Times.Once);

            fakeClient.Verify(f => f.InitialTestRestFormulaAsync(
                // contains right request data and object's item
                Match.Create<TestDraftFormulaRequest>(i => i.Data.ToString() == vm.RequestData && i.Formula == fakeItemAccessor.Object.Item), default),
                Times.Once);

            // verify resulting object
            fakeAcceptor.Verify(f => f.PushResult(fakeItemAccessor.Object.Id, shownObject, It.IsAny<TestFormulaResponse>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()),
                Times.Once);
            repository.VerifyNoOtherCalls();

            // testing result 
            var result = resultList.Should().ContainSingle().Which;
            result.Error.Should().BeSameAs(initialResponse.Error);
            result.Results.Should().BeEquivalentTo(initialResponse.Results);
            result.Success.Should().Be(initialResponse.Success);

            // testing additional data
            var additional = additionalDataList.Should().ContainSingle().Which.Should().NotBeNull().And.Subject;
            additional.Select(i => i.Key).Should().BeEquivalentTo(
                new[] { "HttpRequestUrl", "HttpRequestBody", "HttpRequestMethod" });
            additional.First(i => i.Key == "HttpRequestMethod").Value.Should().Be(initialResponse.Data.HttpVerb);
            additional.First(i => i.Key == "HttpRequestUrl").Value.Should().Be(initialResponse.Data.Url);
            additional.First(i => i.Key == "HttpRequestBody").Value.Should().Be(initialResponse.Data.Body);
        }

        private FormulaTestVM CreateVM(DAL.Enums.FormulaType formulaType = DAL.Enums.FormulaType.Expression)
        {
            var mapper = new AutoMapper.Mapper(new AutoMapper.MapperConfiguration(c => c.AddProfile<Mapping.MappingProfile>()));

            var httpClient = new HttpClient(httpClientHandler.Object);
            httpClientFactory.Setup(f => f()).Returns(httpClient);

            return new FormulaTestVM(formulaType,
                fakeItemAccessor.Object,
                fakeClient.Object,
                fakeAcceptor.Object,
                fakeTestData.Object,
                mapper,
                httpClientFactory.Object,
                null);
        }
    }
}
