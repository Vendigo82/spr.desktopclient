﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SPR.UI.App.Services;
using SPR.UI.App.DataContract;
using SPR.UI.App.Abstractions;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;

namespace SPR.UI.App.VM.Formulas.Tests
{
    [TestClass]
    public class FormulaVMTests
    {
        public class MyFormulaVM : FormulaVM
        {
            public MyFormulaVM(Guid? guid, IFormulaRepository source, IDictionaryService<int, RejectReasonModel> rejectReasonSource = null, IDictionaryService<string, StepModel> stepSource = null, ILogger logger = null, IConfirmActionCommentProvider commentProvider = null) 
                : base(guid, source, rejectReasonSource, stepSource, logger, commentProvider) {
            }

            protected override FormulaModel CreateNewItem() {
                return base.CreateNewItem();
            }
        }

        [TestMethod]
        public async Task RejectCodeItem_Update_Test() {
            var src = new FakeFormulaRepository(
                getMain: (g) => new WebService.DataContract.ObjectResponse<FormulaModel>() {
                    Item = new FormulaModel()
                });
            var dir = new FakeDictionaryService<int, RejectReasonModel>(
                getItem: (i) => new RejectReasonModel() { Code = i, Name = i.ToString() });
            FormulaVM vm = new MyFormulaVM(Guid.NewGuid(), source: src, rejectReasonSource: dir);
            await vm.InitAndWait();

            NotifyPropertyChangedCollector collector = new NotifyPropertyChangedCollector(vm);

            Assert.IsNull(vm.RejectCode);
            Assert.IsNull(vm.RejectCodeItem);

            collector.Reset();

            vm.RejectCode = 1;
            Assert.IsNotNull(vm.RejectCodeItem);
            Assert.AreEqual(vm.RejectCode, vm.RejectCodeItem.Code);
            Assert.IsNotNull(vm.RejectCodeItem.Name);

            Assert.IsTrue(collector.Contains(nameof(vm.RejectCodeItem)));
        }

        [TestMethod]
        public async Task RejectStepItem_Update_Test() {
            var src = new FakeFormulaRepository(
                getMain: (g) => new WebService.DataContract.ObjectResponse<FormulaModel>() {
                    Item = new FormulaModel()
                });
            var dir = new FakeDictionaryService<string, StepModel>(
                getItem: (i) => new StepModel() { Name = i, Descr = i });
            FormulaVM vm = new MyFormulaVM(Guid.NewGuid(), source: src, stepSource: dir);
            await vm.InitAndWait();

            NotifyPropertyChangedCollector collector = new NotifyPropertyChangedCollector(vm);

            Assert.IsNull(vm.RejectStep);
            Assert.IsNull(vm.RejectStepItem);

            collector.Reset();

            vm.RejectStep = "1";
            Assert.IsNotNull(vm.RejectStepItem);
            Assert.AreEqual(vm.RejectStep, vm.RejectStepItem.Name);
            Assert.IsNotNull(vm.RejectStepItem.Descr);

            Assert.IsTrue(collector.Contains(nameof(vm.RejectStepItem)));
        }

        [TestMethod]
        public async Task Params_Modify_Test() {
            var src = new FakeFormulaRepository(
                getMain: (g) => new WebService.DataContract.ObjectResponse<FormulaModel>() {
                    Item = new FormulaModel() { Params = new List<ParamModel>() { new ParamModel() { Name = "z", Value = "1" } } }
                });            
            FormulaVM vm = new MyFormulaVM(Guid.NewGuid(), source: src);
            await vm.InitAndWait();

            Assert.AreEqual(ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsNotNull(vm.Params);
            CollectionAssert.AreEquivalent(new string[] { "z" }, vm.Params.Select(i => i.Name).ToArray());

            //modify param
            vm.Params.First().Value = "2";
            Assert.AreEqual(ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsTrue(vm.IsChanged);
            CollectionAssert.AreEquivalent(new string[] { "z" }, vm.Params.Select(i => i.Name).ToArray());
        }

        [TestMethod]
        public async Task Params_Remove_Test() {
            var src = new FakeFormulaRepository(
                getMain: (g) => new WebService.DataContract.ObjectResponse<FormulaModel>() {
                    Item = new FormulaModel() { Params = new List<ParamModel>() { 
                        new ParamModel() { Name = "z", Value = "1" },
                        new ParamModel() { Name = "x", Value = "2" }
                    } }
                });            
            FormulaVM vm = new MyFormulaVM(Guid.NewGuid(), source: src);
            await vm.InitAndWait();

            Assert.AreEqual(ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsNotNull(vm.Params);
            CollectionAssert.AreEquivalent(new string[] { "z", "x" }, vm.Params.Select(i => i.Name).ToArray());

            //remove parameter
            vm.Params.RemoveAt(1);
            Assert.AreEqual(ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsTrue(vm.IsChanged);
            CollectionAssert.AreEquivalent(new string[] { "z" }, vm.Params.Select(i => i.Name).ToArray());            
        } 

        [TestMethod]
        public async Task Params_Add_Test() {
            var src = new FakeFormulaRepository(
                getMain: (g) => new WebService.DataContract.ObjectResponse<FormulaModel>() {
                    Item = new FormulaModel() {
                        Params = new List<ParamModel>() {
                        new ParamModel() { Name = "z", Value = "1" },
                    }
                    }
                });            
            FormulaVM vm = new MyFormulaVM(Guid.NewGuid(), source: src);
            await vm.InitAndWait();

            Assert.AreEqual(ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsNotNull(vm.Params);
            CollectionAssert.AreEquivalent(new string[] { "z" }, vm.Params.Select(i => i.Name).ToArray());

            //add parameter
            vm.Params.Add(new FormulaVM.ParamWrapper());

            Assert.AreEqual(ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsTrue(vm.IsChanged);
            CollectionAssert.AreEquivalent(new string[] { "z", null }, vm.Params.Select(i => i.Name).ToArray());
        }

        [TestMethod]
        public async Task NestedFormulas_Modify_Test() {
            var src = new FakeFormulaRepository(
                getMain: (g) => new WebService.DataContract.ObjectResponse<FormulaModel>() {
                    Item = new FormulaModel() { ChildFormulas = new List<NestedFormulaModel>() { 
                        new NestedFormulaModel() { Name = "z" } 
                    } }
                });            
            FormulaVM vm = new MyFormulaVM(Guid.NewGuid(), source: src);
            await vm.InitAndWait();

            Assert.AreEqual(ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsNotNull(vm.NestedFormulas);
            CollectionAssert.AreEquivalent(new string[] { "z" }, vm.NestedFormulas.Select(i => i.Name).ToArray());

            //modify param
            vm.NestedFormulas.First().Name = "zz";
            Assert.AreEqual(ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsTrue(vm.IsChanged);
            CollectionAssert.AreEquivalent(new string[] { "zz" }, vm.NestedFormulas.Select(i => i.Name).ToArray());
        }

        [TestMethod]
        public async Task NestedFormulas_Remove_Test() {
            var src = new FakeFormulaRepository(
                getMain: (g) => new WebService.DataContract.ObjectResponse<FormulaModel>() {
                    Item = new FormulaModel() {
                        ChildFormulas = new List<NestedFormulaModel>() {
                        new NestedFormulaModel() { Name = "z" },
                        new NestedFormulaModel() { Name = "x" }
                    }
                    }
                });            
            FormulaVM vm = new MyFormulaVM(Guid.NewGuid(), source: src);
            await vm.InitAndWait();

            Assert.AreEqual(ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsNotNull(vm.NestedFormulas);
            CollectionAssert.AreEquivalent(new string[] { "z", "x" }, vm.NestedFormulas.Select(i => i.Name).ToArray());

            //remove parameter
            vm.NestedFormulas.RemoveAt(1);
            Assert.AreEqual(ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsTrue(vm.IsChanged);
            CollectionAssert.AreEquivalent(new string[] { "z" }, vm.NestedFormulas.Select(i => i.Name).ToArray());
        }

        [TestMethod]
        public async Task NestedFormulas_Add_Test() {
            var src = new FakeFormulaRepository(
                getMain: (g) => new WebService.DataContract.ObjectResponse<FormulaModel>() {
                    Item = new FormulaModel() {
                        ChildFormulas = new List<NestedFormulaModel>() {
                        new NestedFormulaModel() { Name = "z" },
                    }
                    }
                });            
            FormulaVM vm = new MyFormulaVM(Guid.NewGuid(), source: src);
            await vm.InitAndWait();

            Assert.AreEqual(ShownObjectEnum.Main, vm.ShownObject);
            Assert.IsNotNull(vm.NestedFormulas);
            CollectionAssert.AreEquivalent(new string[] { "z" }, vm.NestedFormulas.Select(i => i.Name).ToArray());

            //add parameter
            vm.NestedFormulas.Add(new FormulaVM.NestedFormulaWrapper());

            Assert.AreEqual(ShownObjectEnum.Draft, vm.ShownObject);
            Assert.IsTrue(vm.IsChanged);
            CollectionAssert.AreEquivalent(new string[] { "z", null }, vm.NestedFormulas.Select(i => i.Name).ToArray());
        }
    }
}
