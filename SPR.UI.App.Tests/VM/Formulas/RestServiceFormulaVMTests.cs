﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.Core.Shared;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract.Dir;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Formulas.Tests
{
    [TestClass]
    public class RestServiceFormulaVMTests 
    {
        readonly Fixture fixture = new Fixture();
        readonly RestServiceFormulaVM vm;
        readonly Guid formulaId = Guid.NewGuid();
        readonly MockRepository repository = new MockRepository(MockBehavior.Default);
        readonly Mock<IFormulaRepository> dataSource;
        readonly Mock<IDictionaryService<int, RejectReasonModel>> rejectSource;
        readonly Mock<IDictionaryService<string, StepModel>> stepSource;
        readonly Mock<IDictionaryService<string, ServerModel>> serverSource;

        public RestServiceFormulaVMTests()
        {
            dataSource = repository.Create<IFormulaRepository>();
            rejectSource = repository.Create<IDictionaryService<int, RejectReasonModel>>();
            stepSource = repository.Create<IDictionaryService<string, StepModel>>();
            serverSource = repository.Create<IDictionaryService<string, ServerModel>>();
            vm = new RestServiceFormulaVM(formulaId, dataSource.Object, rejectSource.Object, stepSource.Object, serverSource.Object, null, null);

            dataSource.Setup(f => f.GetMain(formulaId, default)).ReturnsUsingFixture(fixture);
            serverSource.Setup(f => f.GetItemAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsUsingFixture(fixture);
            
        }

        [TestMethod]
        public async Task ServerChanged_Test()
        {
            // setup
            await vm.InitAndWait();
            serverSource.Invocations.Clear();

            string serverName = fixture.Create<string>();
            var serverModel = fixture.Build<ServerModel>().With(i => i.Name, serverName).Create();
            serverSource.Setup(f => f.GetItemAsync(serverName, It.IsAny<CancellationToken>())).ReturnsAsync(serverModel);

            // action
            vm.Server = serverName;

            // asserts
            vm.ServerItem.Should().NotBeNull().And.BeSameAs(serverModel);
            serverSource.Verify(f => f.GetItemAsync(serverName, default), Times.Once);
        }
    }
}
