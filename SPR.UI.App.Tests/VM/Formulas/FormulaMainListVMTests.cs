﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SPR.UI.App.Services;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services.Events;
using System.Collections.Generic;
using Moq;
using FluentAssertions;
using AutoFixture;
using SPR.UI.WebClient.DataTypes;

namespace SPR.UI.App.VM.Formulas.Tests
{
    [TestClass]
    public class FormulaMainListVMTests : TestVMBase
    {
        readonly Mock<IDataListFilterSource<FormulaBriefModel, FormulaFilter>> fakeSource;
        readonly Mock<IDeleteOperation<FormulaBriefModel>> fakeDelete;

        readonly FormulaMainListVM vm;

        public FormulaMainListVMTests()
        {
            fakeSource = repository.Create<IDataListFilterSource<FormulaBriefModel, FormulaFilter>>();
            fakeSource.Setup(f => f.LoadItemsAsync(It.IsAny<FormulaFilter>(), default)).ReturnsAsync(fixture.CreateMany<FormulaBriefModel>());

            fakeDelete = repository.Create<IDeleteOperation<FormulaBriefModel>>();

            vm = new FormulaMainListVM(fakeSource.Object, fakeDelete.Object, fakeWndFactory.Object, fakeResources.Object);
        }

        [TestMethod]
        public async Task Items_Test() {
            // setup
            var items = await fakeSource.Object.LoadItemsAsync(null);

            // action
            await vm.Initialize(false);

            // asserts
            vm.Items.Should()
                .NotBeNullOrEmpty().And
                .BeEquivalentTo(items);
            vm.Should().WithoutError();
        }

        [TestMethod]
        public async Task DeleteTest()
        {
            // setup
            await vm.Initialize(false);

            var item = vm.Items.First();

            fakeWndFactory.Setup(f => f.ShowConfirmDialog(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            fakeDelete.Setup(f => f.DeleteAsync(item.Item, default)).ReturnsAsync(true);

            vm.SelectedItem = item;

            // action
            vm.DeleteCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            fakeDelete.Verify(f => f.DeleteAsync(item.Item, default), Times.Once);
            vm.Items.Should().NotContain(item);
        }
    }
}
