﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SPR.UI.WebService.DataContract.Profile;
using SPR.UI.App.Utils;
using SPR.UI.App.Abstractions.WebService;
using Moq;
using SPR.UI.App.Abstractions;
using AutoFixture;
using FluentAssertions;
using System.Threading;

namespace SPR.UI.App.VM.Main.Tests
{
    [TestClass]
    public class ProfileVMTests
    {
        readonly Mock<IProfileService> serviceMock = new Mock<IProfileService>();
        readonly Mock<ProfileVM.IBadPasswordReasonInterpretator> reasonMock = new Mock<ProfileVM.IBadPasswordReasonInterpretator>();
        readonly ProfileVM vm;

        public ProfileVMTests()
        {
            serviceMock.Setup(f => f.Profile(It.IsAny<CancellationToken>())).ReturnsAsync(new Profile { Login = "sa", Name = "name" });

            vm = new ProfileVM(serviceMock.Object, Mock.Of<ILogger>(), reasonMock.Object);
        }

        [AutoData]
        public async Task Initialize(Profile profile)
        {
            // setup
            serviceMock.Setup(f => f.Profile(It.IsAny<CancellationToken>())).ReturnsAsync(profile);

            // action
            await vm.InitAndWait();

            // asserts
            vm.Login.Should().Be(profile.Login);
            vm.Name.Should().Be(profile.Name);
            vm.Should().WithoutError();
        }

        [AutoData]
        public async Task Initialize_Error(string message)
        {
            // setup
            serviceMock.Setup(f => f.Profile(It.IsAny<CancellationToken>())).ThrowsAsync(new Exception(message));

            // action
            await vm.InitAndWait();

            // asserts
            vm.Should().HasError();
            vm.ErrorMessage.Should().Be(message);
            vm.Login.Should().BeNull();
        }

        [AutoData]
        public async Task ChangePassword_ShouldSuccess(string oldPassword, string newPassword)
        {
            // setup
            serviceMock.Setup(f => f.ChangePassword(oldPassword, newPassword, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ChangePasswordResult { Result = ChangePasswordResultType.Success });

            await vm.InitAndWait();

            // action
            vm.SwitchToChangePasswordCmd.Execute(null);
            await vm.ChangePassword(oldPassword, newPassword, newPassword);

            // asserts
            serviceMock.Verify(f => f.ChangePassword(oldPassword, newPassword, It.IsAny<CancellationToken>()), Times.Once);

            vm.ChangePasswordMode.Should().BeFalse();
            vm.PasswordsNotEqual.Should().BeFalse();
            vm.InvalidOldPassword.Should().BeFalse();
            vm.InvalidNewPassword.Should().BeFalse();
            vm.InvalidNewPasswordReason.Should().BeNull();
            vm.Should().WithoutError();
        }

        [TestMethod]
        public async Task ChangePassword_PasswordsNotEqual()
        {
            // setup
            await vm.InitAndWait();

            // action
            vm.SwitchToChangePasswordCmd.Execute(null);
            NotifyPropertyChangedCollector changed = new NotifyPropertyChangedCollector(vm);
            await vm.ChangePassword("old password", "new password", "different new password");

            // asserts
            vm.ChangePasswordMode.Should().BeTrue();
            vm.PasswordsNotEqual.Should().BeTrue();
            vm.InvalidOldPassword.Should().BeFalse();
            vm.InvalidNewPassword.Should().BeFalse();
            vm.InvalidNewPasswordReason.Should().BeNull();
            vm.Error.Should().BeNull();

            CollectionAssert.IsSubsetOf(new string[] { nameof(ProfileVM.PasswordsNotEqual) }, changed.ToArray());
        }

        [AutoData]
        public async Task Edit_Success(string newName)
        {
            // setup
            serviceMock.Setup(f => f.Edit(It.IsAny<string>(), default)).ReturnsAsync(new Profile { Name = newName });

            await vm.InitAndWait();
            Assert.IsFalse(vm.IsChanged);

            // action
            vm.Name = newName;
            
            vm.IsChanged.Should().BeTrue();
            vm.SaveCmd.Execute(null);

            await vm.WaitBusy();

            // asserts
            vm.IsChanged.Should().BeFalse();
            vm.Name.Should().Be(newName);

            serviceMock.Verify(f => f.Edit(newName, It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task Edit_Failed()
        {
            // setup
            serviceMock.Setup(f => f.Edit(It.IsAny<string>(), It.IsAny<CancellationToken>())).ThrowsAsync(new Exception());
            await vm.InitAndWait();

            // action
            vm.Name = "name";
            vm.SaveCmd.Execute(null);

            await vm.WaitBusy();
            
            // asserts
            vm.Should().HasError();
        }
    }

    public static class ProfileVMExtentions
    {
        public static async Task ChangePassword(this ProfileVM vm, string oldp, string newp, string repeatp) {
            vm.ChangePasswordCmd.Execute(new Tuple<object, object, object>(
                new WrappedParameter<string>(oldp),
                new WrappedParameter<string>(newp),
                new WrappedParameter<string>(repeatp)));

            while (vm.IsBusy)
                await Task.Delay(TimeSpan.FromMilliseconds(50));
        }
    }
}
