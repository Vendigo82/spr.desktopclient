﻿using System;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VenSoft.WPFLibrary.Commands;

namespace SPR.UI.App.VM.Main.Tests
{
    [TestClass]
    public class TabItemVMExtentionsTests
    {
        public class DataContext
        {
            public ICommand NullCmd { get; set; } = null;

            public ICommand MyCmd { get; set; } = new RelayCommand(() => { });

            public RelayCommand RelayCommand { get; set; } = new RelayCommand(() => { });

            public RelayCommand<int> RelayCommandT { get; set; } = new RelayCommand<int>((i) => { });

            public object NotCmd { get; set; } = new object();
        }

        [TestMethod]
        public void TryGetCommandTest() {
            TestTabItemVM vm = new TestTabItemVM() {
                DataContext = new DataContext()
            };

            Assert.IsNull(vm.TryGetCommand("unknown"));
            Assert.IsNull(vm.TryGetCommand(nameof(DataContext.NullCmd)));
            Assert.ThrowsException<InvalidCastException>(() => (vm.TryGetCommand(nameof(DataContext.NotCmd))));

            Assert.AreSame((vm.DataContext as DataContext).MyCmd, vm.TryGetCommand(nameof(DataContext.MyCmd)));
            Assert.AreSame((vm.DataContext as DataContext).RelayCommand, vm.TryGetCommand(nameof(DataContext.RelayCommand)));
            Assert.AreSame((vm.DataContext as DataContext).RelayCommandT, vm.TryGetCommand(nameof(DataContext.RelayCommandT)));

            vm.DataContext = null;
            Assert.IsNull(vm.TryGetCommand(nameof(DataContext.MyCmd)));

            vm = null;
            Assert.IsNull(vm.TryGetCommand(nameof(DataContext.MyCmd)));
        }
    }
}
