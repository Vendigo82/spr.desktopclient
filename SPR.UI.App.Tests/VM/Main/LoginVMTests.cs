﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SPR.UI.App.Services;
using SPR.UI.App.Services.LocalData;
using SPR.UI.App.Utils;

namespace SPR.UI.App.VM.Main.Tests
{
    [TestClass]
    public class LoginVMTests
    {
        [TestMethod]
        public void LogoTest() {
            FakeLogoProvider logoProvider = new FakeLogoProvider();
            LoginVM vm = new LoginVM(logoProvider, null);
            Assert.AreEqual(logoProvider.PathToLogo, vm.Logo);
        }

        [TestMethod]
        public async Task LoginFlowTest() {
            string user = null;
            string password = null;
            List<string> notify = new List<string>();

            LoginVM vm = new LoginVM(null, new FakeAuthService((u, p, ct) => {
                user = u;
                password = p;
                return Task.FromResult(u == "sa" && p == "123");
            }));
            vm.PropertyChanged += (s, a) => notify.Add(a.PropertyName);

            Assert.AreEqual(LoginVM.LoginResult.Unknown, vm.Result);
            Assert.IsNull(vm.ErrorMessage);
            Assert.IsFalse(vm.IsBusy);

            //first login - must be failed
            await vm.Login("sa1", "456");

            Assert.AreEqual(LoginVM.LoginResult.Failed, vm.Result);
            Assert.IsNull(vm.ErrorMessage);
            Assert.IsFalse(vm.IsBusy);
            Assert.AreEqual("sa1", user);
            Assert.AreEqual("456", password);

            //check notify property changed events: 2 busy, 2 result
            CollectionAssert.AreEquivalent(
                new string[] { nameof(vm.IsBusy), nameof(vm.IsBusy), nameof(vm.Result), nameof(vm.Result) },
                notify.Where(s => new string[] { nameof(vm.IsBusy), nameof(vm.Result) }.Contains(s)).ToArray());
            notify.Clear();

            //second login - success
            await vm.Login("sa", "123");

            Assert.AreEqual(LoginVM.LoginResult.Success, vm.Result);
            Assert.IsNull(vm.ErrorMessage);
            Assert.IsFalse(vm.IsBusy);
            Assert.AreEqual("sa", user);
            Assert.AreEqual("123", password);

            //check notify property changed events: 2 busy, 2 result
            CollectionAssert.AreEquivalent(
                new string[] { nameof(vm.IsBusy), nameof(vm.IsBusy), nameof(vm.Result), nameof(vm.Result) },
                notify.Where(s => new string[] { nameof(vm.IsBusy), nameof(vm.Result) }.Contains(s)).ToArray());

        }

        [TestMethod]
        public async Task LoginError() {
            const string msg = "error message";
            List<string> notify = new List<string>();

            LoginVM vm = new LoginVM(null, new FakeAuthService((u, p, ct) => {
                if (u == "sa" && p == "123")
                    return Task.FromResult(true);
                else
                    throw new Exception(msg);
            }));
            vm.PropertyChanged += (s, a) => notify.Add(a.PropertyName);

            await vm.Login("sa", "456");

            Assert.AreEqual(LoginVM.LoginResult.Error, vm.Result);
            Assert.IsNotNull(vm.ErrorMessage);
            Assert.AreEqual(msg, vm.ErrorMessage);
            Assert.IsFalse(vm.IsBusy);

            //check notify property changed events: 2 busy, 2 result, 2 error message     
            string[] events = new string[] { nameof(vm.IsBusy), nameof(vm.Result), nameof(vm.ErrorMessage) };
            CollectionAssert.AreEquivalent(
                events.Concat(events).ToArray(),
                notify.Where(s => events.Contains(s)).ToArray());
            notify.Clear();

            //second login - success
            await vm.Login("sa", "123");

            Assert.AreEqual(LoginVM.LoginResult.Success, vm.Result);
            Assert.IsNull(vm.ErrorMessage);
            Assert.IsFalse(vm.IsBusy);

        }

        [TestMethod]
        public async Task LoginEmptyPassword() {
            string password = null;

            LoginVM vm = new LoginVM(null, new FakeAuthService((u, p, ct) => {
                password = p;
                return Task.FromResult(true);
            }));

            //first login - must be failed
            await vm.Login("sa1", "");

            Assert.IsNull(password);
        }

        [TestMethod]
        public void LogginnedHistoryTest() {
            FakeLogginnedHistory hist = new FakeLogginnedHistory() {
                LogginnedUsers = new string[] { "sa", "user1" }
            };
            LoginVM vm = new LoginVM(null, null, hist);
            CollectionAssert.AreEqual(hist.LogginnedUsers.ToArray(), vm.UsernameHistory.ToArray());
            Assert.AreEqual("sa", vm.Username);
        }

        [TestMethod]
        public async Task LogginnedHistory_Login_Test() {
            string saveLogin = null;
            FakeLogginnedHistory hist = new FakeLogginnedHistory(add: (s) => { Assert.IsNull(saveLogin); saveLogin = s; });
            LoginVM vm = new LoginVM(null, new FakeAuthService(func: (l, p, ct) => Task.FromResult(true)), hist);

            await vm.Login("sa", "1");
            Assert.AreEqual("sa", saveLogin);
        }

        [TestMethod]
        public async Task LogginnedHistory_LoginFailed_Test() {
            bool added = false;
            bool called = false;
            FakeLogginnedHistory hist = new FakeLogginnedHistory(add: (s) => { added = true; });
            LoginVM vm = new LoginVM(null, new FakeAuthService(func: (l, p, ct) => { called = true; return Task.FromResult(false); }), hist);

            await vm.Login("sa", "1");

            Assert.IsTrue(called);
            Assert.IsFalse(added);
        }
    }

    public static class LoginVMExtentions
    {
        public static async Task Login(this LoginVM vm, string user, string psw) {
            vm.Username = user;
            vm.LoginCmd.Execute(new WrappedParameter<string>(psw));

            while (vm.IsBusy)
                await Task.Delay(TimeSpan.FromMilliseconds(50));            
        }
    }
}
