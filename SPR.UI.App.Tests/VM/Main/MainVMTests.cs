﻿using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Abstractions.UI;
using SPR.UI.App.Abstractions.VM;
using SPR.UI.App.Model.Main;
using SPR.UI.App.Services;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SPR.UI.App.VM.Main.Tests
{
    [TestClass]
    public class MainVMTests
    {
        readonly IWindowFactory wndFactory;
        readonly IFactory<ICommonVM> fakeVmFactory;
        
        public MainVMTests()
        {
            fakeVmFactory = A.Fake<IFactory<ICommonVM>>();
            wndFactory = A.Fake<IWindowFactory>();
            A.CallTo(() => wndFactory.CreateTabContainer(A<UIElement>._)).ReturnsLazily(c => c.GetArgument<Control>(0));
        }

        [TestMethod]
        public void CreateCommands() {
            MainVM vm = new MainVM(new FakeMainWndModel(), null, fakeVmFactory, new FakeTabTitleProvider());
            Assert.IsNotNull(vm.SaveCmd);
            Assert.IsNotNull(vm.RefreshCmd);
        }

        [TestMethod]
        public void AddTab_CallRefresh() {
            bool refreshWasCalled = false;
                       
            var usrControl = new UserControl() {
                DataContext = new ActionBusyVM(refresh: () => {
                    refreshWasCalled = true;
                    return Task.CompletedTask;
                })
            };
            A.CallTo(() => wndFactory.CreateView(A<string>._))
                .Returns(usrControl);

            MainVM vm = new MainVM(new FakeMainWndModel(), wndFactory, fakeVmFactory, new FakeTabTitleProvider());
            vm.ExecOpenCommand(Commands.CommandsLibrary.OpenProfile);

            A.CallTo(() => wndFactory.CreateView(A<string>.That.IsEqualTo(Modules.TAB_PROFILE)))
                .MustHaveHappenedOnceExactly();
            Assert.IsTrue(refreshWasCalled);
        }

        [TestMethod]
        public void AddTab_Title() {
            string tabName = null;
            MainVM vm = new MainVM(new FakeMainWndModel(), wndFactory, fakeVmFactory,
                new FakeTabTitleProvider((name) => { tabName = name; return name; }));
            vm.ExecOpenCommand(Commands.CommandsLibrary.OpenProfile);

            Assert.AreEqual(Modules.TAB_PROFILE, tabName);
        }
    }
}
