﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SPR.UI.App.Services;

namespace SPR.UI.App.VM.Main.Tests
{
    [TestClass]
    public class ProfileVMReasonInterpretatorTests
    {
        [TestMethod]
        public void GetTextTest() {            
            FakeResourceStringProvider sp = new FakeResourceStringProvider((s) => s.EndsWith("unknown") ? null : s + "_xyz");
            ProfileVMReasonInterpretator inter = new ProfileVMReasonInterpretator(sp);
            Assert.AreEqual("ProfileTab.BadNewPassword.123_xyz", inter.GetText("123"));
            Assert.AreEqual("unknown", inter.GetText("unknown"));
        }
    }
}
