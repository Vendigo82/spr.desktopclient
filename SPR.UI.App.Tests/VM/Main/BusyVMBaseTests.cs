﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.VM.Main.Tests
{
    [TestClass]
    public class BusyVMBaseTests
    {
        [TestMethod]
        public async Task Refresh_IsBusy_Test() {
            bool cont = false;
            bool performed = false;

            ActionBusyVM vm = new ActionBusyVM(refresh: async () => {
                while (cont == false)
                    await Task.Delay(50);
                performed = true;                
            });
            //change property
            vm.MyProperty = "123";

            //still not busy
            Assert.IsFalse(vm.IsBusy);
            //has changed state
            Assert.IsTrue(vm.IsChanged);
            //can call Refresh
            Assert.IsTrue(vm.RefreshCmd.CanExecute(null));

            //and perform command
            vm.RefreshCmd.Execute(null);

            //check busy flag
            Assert.IsTrue(vm.IsBusy);
            //cant execute
            Assert.IsFalse(vm.RefreshCmd.CanExecute(null));
            //still has changed state
            Assert.IsTrue(vm.IsChanged);

            //release resource
            cont = true;

            while (vm.IsBusy)
                await Task.Delay(50);

            //out method was performed
            Assert.IsTrue(performed);

            //not busy
            Assert.IsFalse(vm.IsBusy);
            //changed flag was reseted
            Assert.IsFalse(vm.IsChanged);
            //can call Refresh again
            Assert.IsTrue(vm.RefreshCmd.CanExecute(null));

            //has not error
            Assert.IsNull(vm.Error);
            Assert.IsFalse(vm.HasError);
            Assert.IsNull(vm.ErrorMessage);
        }

        [TestMethod]
        public async Task Refresh_Error_Test() {
            bool performed = false;
            string errMsg = Guid.NewGuid().ToString();
            bool bThrow = true;
            ActionBusyVM vm = new ActionBusyVM(refresh: () => {
                performed = true;
                if (bThrow)
                    throw new Exception(errMsg);
                else
                    return Task.CompletedTask;
            });

            //and perform command
            vm.RefreshCmd.Execute(null);

            while (vm.IsBusy)
                await Task.Delay(50);

            //out method was performed
            Assert.IsTrue(performed);

            //not busy
            Assert.IsFalse(vm.IsBusy);
            //can call Refresh again
            Assert.IsTrue(vm.RefreshCmd.CanExecute(null));

            //has error
            Assert.IsNotNull(vm.Error);
            Assert.IsTrue(vm.HasError);
            Assert.AreEqual(errMsg, vm.ErrorMessage);

            bThrow = false;
            vm.RefreshCmd.Execute(null);

            while (vm.IsBusy)
                await Task.Delay(50);

            //error was reseted
            Assert.IsNull(vm.Error);
            Assert.IsFalse(vm.HasError);
            Assert.IsNull(vm.ErrorMessage);
        }

        [TestMethod]
        public async Task Save_IsBusy_Test() {
            bool cont = false;
            bool performed = false;

            ActionBusyVM vm = new ActionBusyVM(save: async () => {
                while (cont == false)
                    await Task.Delay(50);
                performed = true;
            });
            //change property
            vm.MyProperty = "123";

            //still not busy
            Assert.IsFalse(vm.IsBusy);
            //has changed state
            Assert.IsTrue(vm.IsChanged);
            //can call SaveCmd
            Assert.IsTrue(vm.SaveCmd.CanExecute(null));

            //and perform command
            vm.SaveCmd.Execute(null);

            //check busy flag
            Assert.IsTrue(vm.IsBusy);
            //cant execute
            Assert.IsFalse(vm.SaveCmd.CanExecute(null));
            //still has changed state
            Assert.IsTrue(vm.IsChanged);

            //release resource
            cont = true;

            while (vm.IsBusy)
                await Task.Delay(50);

            //out method was performed
            Assert.IsTrue(performed);

            //not busy
            Assert.IsFalse(vm.IsBusy);
            //changed flag was reseted
            Assert.IsFalse(vm.IsChanged);
            //can't call Save again, because have not changes
            Assert.IsFalse(vm.SaveCmd.CanExecute(null));

            vm.MyProperty = "123";
            //can call Save, because has changes
            Assert.IsTrue(vm.SaveCmd.CanExecute(null));

            //has not error
            Assert.IsNull(vm.Error);
            Assert.IsFalse(vm.HasError);
            Assert.IsNull(vm.ErrorMessage);
        }

        [TestMethod]
        public async Task Save_Error_Test() {
            bool performed = false;
            string errMsg = Guid.NewGuid().ToString();
            bool bThrow = true;
            ActionBusyVM vm = new ActionBusyVM(save: () => {
                performed = true;
                if (bThrow)
                    throw new Exception(errMsg);
                else
                    return Task.CompletedTask;
            });

            vm.MyProperty = "123";

            //and perform command
            vm.SaveCmd.Execute(null);

            while (vm.IsBusy)
                await Task.Delay(50);

            //out method was performed
            Assert.IsTrue(performed);

            //not busy
            Assert.IsFalse(vm.IsBusy);
            //can call Refresh again
            Assert.IsTrue(vm.SaveCmd.CanExecute(null));

            //has error
            Assert.IsNotNull(vm.Error);
            Assert.IsTrue(vm.HasError);
            Assert.AreEqual(errMsg, vm.ErrorMessage);

            bThrow = false;
            vm.SaveCmd.Execute(null);

            while (vm.IsBusy)
                await Task.Delay(50);

            //error was reseted
            Assert.IsNull(vm.Error);
            Assert.IsFalse(vm.HasError);
            Assert.IsNull(vm.ErrorMessage);
        }
    }
}
