﻿using SPR.UI.App.VM.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Main.Tests
{
    public class ActionBusyVM : BusyVMBase
    {
        private readonly Func<Task> _refresh;
        private readonly Func<Task> _save;
        private string _myProp = null;

        public ActionBusyVM(Func<Task> refresh = null, Func<Task> save = null) : base(null) {
            _refresh = refresh;
            _save = save;
        }

        public string MyProperty { get => _myProp; set { _myProp = value; NotifyPropertyChanged(); IsChanged = true; } }

        protected override async Task Save() => await _save?.Invoke();

        protected override async Task Refresh() => await _refresh?.Invoke();
    }
}
