﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.View.Scenarios;
using SPR.UI.App.VM.Scenarios;
using FakeItEasy;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Abstractions;
using System.Linq;
using System.Windows.Controls;
using System.Threading;
using RandomType;
using SPR.UI.WebService.DataContract.Scenario;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Main.Tests
{
    [TestClass]
    public class StdResultsTabItemVMTests
    {
        readonly StdResultsVM vm;
        readonly StdResultsTabItemVM tabVM;
        readonly NotifyPropertyChangedCollector notify;

        public StdResultsTabItemVMTests()
        {
            vm = new StdResultsVM(null, A.Fake<IStdResultsSource>(), A.Fake<ILogger>());
            var control = new UserControl() {
                DataContext = vm
            };
            tabVM = new StdResultsTabItemVM(control);
            vm.InitAndWait().Wait();

            notify = new NotifyPropertyChangedCollector(tabVM);
        }

        [TestMethod]
        public void Title_Test()
        {
            Assert.AreEqual("SR-", tabVM.Title);
            vm.Name = "xyz";
            Assert.AreEqual("SR-xyz", tabVM.Title);
            CollectionAssert.Contains(notify.ToArray(), nameof(tabVM.Title));

            notify.Reset();
            vm.Values.Add(new StdResultsVM.ValueWrapper());
            CollectionAssert.DoesNotContain(notify.ToArray(), nameof(tabVM.Title));
        }
    }
}
