﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Configuration;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring.TransactionsVMTests
{
    [TestClass]
    public class InitializeTests : BaseTests
    {
        public InitializeTests() : base(performInitialLoading: false)
        {

        }

        [TestMethod]
        public async Task Initialize_Test()
        {
            // setup
            var columns = fixture.CreateMany<TransactionColumnModel>();
            var items = fixture.Build<TransactionModel>().CreateMany();

            source.Setup(f => f.LoadAdditionalColumnsInfoAsync(It.IsAny<CancellationToken>())).ReturnsAsync(columns);
            source.Setup(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()))
               .ReturnsAsync(new DataModel.PartialListContainer<TransactionModel> { Items = items, Exhausted = true });

            // action
            await vm.Initialize(true);

            // asserts
            vm.Should().WithoutError();
            vm.AdditionalColums.Should().BeSameAs(columns);
            vm.Items.Should().Equal(items);
        }

        //[TestMethod]
        //public async Task Initialize_NotifyPropertyChanged_Test()
        //{
        //    // setup
        //    source.Setup(f => f.LoadAdditionalColumnsInfoAsync(It.IsAny<CancellationToken>())).ReturnsUsingFixture(fixture);
        //    source.Setup(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()))
        //       .ReturnsUsingFixture(fixture);

        //    // action
        //    using (var monitor = vm.Monitor()) {
        //        await vm.Initialize(true);

        //        // asserts
        //        monitor.Should().RaisePropertyChangeFor(vm => vm.AdditionalColums);
        //        monitor.Should().RaisePropertyChangeFor(vm => vm.Items);
        //        monitor.Should().RaisePropertyChangeFor(vm => vm.IsBusy);
        //        monitor.Should().RaisePropertyChangeFor(vm => vm.IsLoading);
        //    }
        //}
    }
}
