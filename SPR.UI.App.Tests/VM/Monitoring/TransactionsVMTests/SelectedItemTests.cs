﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring.TransactionsVMTests
{
    [TestClass]
    public class SelectedItemTests : BaseTests
    {
        [TestMethod]
        public void SelectedTransactionData_Test()
        {
            // setup
            var item = fixture.Create<TransactionModel>();
            var response = fixture.Create<TransactionDataResponseModel>();
            source.Setup(f => f.LoadDataAsync(item.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            // action
            vm.SelectedItem = item;

            // asserts
            vm.Should().WithoutError();

            vm.IsSelectedTransactionDataLoading.Should().BeFalse();
            vm.SelectedTransactionData.Should().BeSameAs(response.Data);
            vm.SelectedTransactionErrors.Should().BeEquivalentTo(response.Errors);

            source.Verify(f => f.LoadDataAsync(item.Id, It.IsAny<CancellationToken>()), Times.Once);
            source.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void ThrowsExceptionOnDataLoading_Test()
        {
            // setup
            var item = fixture.Create<TransactionModel>();
            var response = fixture.Create<TransactionDataResponseModel>();
            var exception = new ApplicationException();
            source.Setup(f => f.LoadDataAsync(item.Id, It.IsAny<CancellationToken>()))
                .ThrowsAsync(exception);

            // action
            vm.SelectedItem = item;

            // asserts
            vm.Should().HasError();
            vm.Error.Should().BeSameAs(exception);
            vm.IsSelectedTransactionDataLoading.Should().BeFalse();
        }

        [TestMethod]
        public async Task DoubleLoading_Test()
        {
            // setup
            var item1 = fixture.Create<TransactionModel>();
            var item2 = fixture.Create<TransactionModel>();
            var response1 = fixture.Create<TransactionDataResponseModel>();
            var response2 = fixture.Create<TransactionDataResponseModel>();

            source.Setup(f => f.LoadDataAsync(item1.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(response1, TimeSpan.FromMilliseconds(50));
            source.Setup(f => f.LoadDataAsync(item2.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(response2);

            // action
            vm.SelectedItem = item1;
            vm.SelectedItem = item2;

            while (vm.IsSelectedTransactionDataLoading)
                await Task.Delay(50);

            // asserts
            vm.Should().WithoutError();

            vm.IsSelectedTransactionDataLoading.Should().BeFalse();
            vm.SelectedTransactionData.Should().BeSameAs(response2.Data);
            vm.SelectedTransactionErrors.Should().BeEquivalentTo(response2.Errors);

            source.Verify(f => f.LoadDataAsync(item1.Id, It.IsAny<CancellationToken>()), Times.Once);
            source.Verify(f => f.LoadDataAsync(item2.Id, It.IsAny<CancellationToken>()), Times.Once);
            source.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void ClearDataOnError_Test()
        {
            // setup
            var item1 = fixture.Create<TransactionModel>();
            var item2 = fixture.Create<TransactionModel>();
            var response1 = fixture.Create<TransactionDataResponseModel>();

            source.Setup(f => f.LoadDataAsync(item1.Id, It.IsAny<CancellationToken>()))
                .ReturnsAsync(response1);
            source.Setup(f => f.LoadDataAsync(item2.Id, It.IsAny<CancellationToken>()))
                .ThrowsAsync(new ApplicationException());

            // action
            vm.SelectedItem = item1;
            vm.SelectedItem = item2;

            // asserts
            vm.Should().HasError();

            vm.IsSelectedTransactionDataLoading.Should().BeFalse();
            vm.SelectedTransactionData.Should().BeNull();
            vm.SelectedTransactionErrors.Should().BeNull();

            source.Verify(f => f.LoadDataAsync(item1.Id, It.IsAny<CancellationToken>()), Times.Once);
            source.Verify(f => f.LoadDataAsync(item2.Id, It.IsAny<CancellationToken>()), Times.Once);
            source.VerifyNoOtherCalls();
        }

        //[TestMethod]
        //public void NotifyPropertyChanged_Test()
        //{
        //    // setup
        //    var item = fixture.Create<TransactionModel>();
        //    var response = fixture.Create<TransactionDataResponseModel>();
        //    source.Setup(f => f.LoadDataAsync(item.Id, It.IsAny<CancellationToken>()))
        //        .ReturnsAsync(response);

        //    // action
        //    using (var monitor = vm.Monitor()) {
        //        vm.SelectedItem = item;

        //        // asserts
        //        monitor.Should().RaisePropertyChangeFor(p => p.SelectedTransactionData);
        //        monitor.Should().RaisePropertyChangeFor(p => p.SelectedTransactionErrors);
        //        monitor.Should().RaisePropertyChangeFor(p => p.IsSelectedTransactionDataLoading);
        //    }
        //}
    }
}
