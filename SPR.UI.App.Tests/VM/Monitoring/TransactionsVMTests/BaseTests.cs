﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring.TransactionsVMTests
{
    public class BaseTests
    {
        protected readonly Fixture fixture = new Fixture();
        protected readonly TransactionsVM vm;
        protected readonly Mock<IMonitoringSource> source;
        protected readonly List<TransactionsFilter> filterArgs = new List<TransactionsFilter>();

        public BaseTests(bool performInitialLoading = true)
        {
            source = new Mock<IMonitoringSource>();
            source.Setup(f => f.LoadAsync(Capture.In(filterArgs), It.IsAny<long?>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsUsingFixture(fixture);
            source.Setup(f => f.LoadAdditionalColumnsInfoAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(new TransactionColumnModel[] { });

            vm = new TransactionsVM(source.Object, null);

            if (performInitialLoading) {
                vm.InitAndWait().Wait();

                source.Invocations.Clear();
                filterArgs.Clear();
            }
        }
    }
}
