﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring.TransactionsVMTests
{
    [TestClass]
    public class TransactionsVMTests : BaseTests
    {
        [TestMethod]
        public void PeriodFilter_Test()
        {
            // setup
            var dt1 = DateTime.Now;
            var dt2 = dt1 + TimeSpan.FromDays(1);

            // action
            vm.Period = new Tuple<DateTime, DateTime>(dt1, dt2);

            // asserts
            vm.Should().WithoutError();

            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()), Times.Once);
            source.VerifyNoOtherCalls();

            filterArgs.Should().ContainSingle().Which.Should().BeEquivalentTo(new TransactionsFilter { 
                DateFrom = dt1,
                DateTo = dt2,
                Values = Enumerable.Empty<KeyValuePair<string, string>>()
            });
        }

        [TestMethod]
        public void ApplyFilter_Test()
        {
            // setup
            bool withError = fixture.Create<bool>();
            string step = fixture.Create<string>();
            string ipAddress = fixture.Create<string>();
            long scenarioId = fixture.Create<long>();

            // action
            vm.FilterErrors = withError;
            vm.FilterIpAddress = ipAddress;
            vm.FilterStep = step;
            vm.FilterScenarioId = scenarioId;
            vm.ApplyFilterCmd.Execute(Enumerable.Empty<KeyValuePair<string, string>>());

            // asserts
            vm.Should().WithoutError();

            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()), Times.Once);
            source.VerifyNoOtherCalls();

            filterArgs.Should().ContainSingle().Which.Should().BeEquivalentTo(new TransactionsFilter {
                DateFrom = vm.Period.Item1,
                DateTo = vm.Period.Item2,
                IpAddress = ipAddress,
                ScenarioCode = scenarioId,
                Step = step,
                WithError = withError ? (bool?)true : null,
                Values = Enumerable.Empty<KeyValuePair<string, string>>()
            });

            (vm as object).Should().BeEquivalentTo(new {
                FilterErrors = withError,
                FilterIpAddress = ipAddress,
                FilterStep = step,
                FilterScenarioId = scenarioId
            });
        }

        [TestMethod]
        public void SetFilterWithoutApply_Test()
        {
            // setup
            bool withError = fixture.Create<bool>();
            string step = fixture.Create<string>();
            string ipAddress = fixture.Create<string>();
            long scenarioId = fixture.Create<long>();

            // action
            vm.FilterErrors = withError;
            vm.FilterIpAddress = ipAddress;
            vm.FilterStep = step;
            vm.FilterScenarioId = scenarioId;

            vm.Period = new Tuple<DateTime, DateTime>(DateTime.Now, DateTime.Now);

            // asserts
            vm.Should().WithoutError();

            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()), Times.Once);
            source.VerifyNoOtherCalls();

            filterArgs.Should().ContainSingle().Which.Should().BeEquivalentTo(new TransactionsFilter {
                DateFrom = vm.Period.Item1,
                DateTo = vm.Period.Item2,
                IpAddress = null,
                ScenarioCode = null,
                Step = null,
                WithError = null,
                Values = Enumerable.Empty<KeyValuePair<string, string>>()
            });

            (vm as object).Should().BeEquivalentTo(new {
                FilterErrors = false,
                FilterIpAddress = (string)null,
                FilterStep = (string)null,
                FilterScenarioId = (long?)null
            });
        }

        [TestMethod]
        public void ResetFilter_Test()
        {
            // setup
            vm.FilterErrors = fixture.Create<bool>();
            vm.FilterIpAddress = fixture.Create<string>();
            vm.FilterStep = fixture.Create<string>();
            vm.FilterScenarioId = fixture.Create<long>();
            vm.ApplyFilterCmd.Execute(Enumerable.Empty<KeyValuePair<string, string>>());

            source.Invocations.Clear();
            filterArgs.Clear();

            // action
            vm.ResetFilterCmd.Execute(null);

            // asserts
            vm.Should().WithoutError();

            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()), Times.Once);
            source.VerifyNoOtherCalls();

            filterArgs.Should().ContainSingle().Which.Should().BeEquivalentTo(new TransactionsFilter {
                DateFrom = vm.Period.Item1,
                DateTo = vm.Period.Item2,
                IpAddress = null,
                ScenarioCode = null,
                Step = null,
                WithError = null,
                Values = Enumerable.Empty<KeyValuePair<string, string>>()
            });

            (vm as object).Should().BeEquivalentTo(new {
                FilterErrors = false,
                FilterIpAddress = (string)null,
                FilterStep = (string)null,
                FilterScenarioId = (long?)null
            });
        }

        [TestMethod]
        public void ResetFilter_OnEmptyFilter_Test()
        {
            // action
            vm.ResetFilterCmd.Execute(null);

            // asserts
            vm.Should().WithoutError();
            source.VerifyNoOtherCalls();
        }
    }
}
