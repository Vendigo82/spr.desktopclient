﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring.TransactionsVMTests
{
    [TestClass]
    public class RefreshTests : BaseTests
    {
        public RefreshTests() : base(false)
        {
        }

        [TestMethod]
        public async Task RefreshWithoutNotFinished_Test()
        {
            // setup
            var initialResponse = fixture.Build<TransactionModel>().With(i => i.Finished, true).CreateMany();

            source.Setup(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new DataModel.PartialListContainer<TransactionModel> { Items = initialResponse, Exhausted = true });
            source.Setup(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), It.IsNotNull<long?>(), true, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new DataModel.PartialListContainer<TransactionModel> { Items = Enumerable.Empty<TransactionModel>(), Exhausted = true });

            //source.Setup(f => f.ReloadAsync(It.IsAny<IEnumerable<long>>(), It.IsAny<CancellationToken>()))
            await vm.InitAndWait();

            // action
            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();
            vm.Items.Should().Equal(initialResponse);

            source.Verify(f => f.LoadAdditionalColumnsInfoAsync(It.IsAny<CancellationToken>()), Times.Once);
            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()), Times.Once);
            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), initialResponse.First().Id, true, It.IsAny<CancellationToken>()), Times.Once());
            source.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task RefreshWithNotFinished_Test()
        {
            // setup
            var initialResponseFinishedPart = fixture.Build<TransactionModel>().With(i => i.Finished, true).CreateMany(2);
            var initialResponseNotFinishedPart = fixture.Build<TransactionModel>().With(i => i.Finished, false).CreateMany(2).ToArray();

            var reloadResponse = fixture.Build<TransactionModel>().With(i => i.Finished, true).CreateMany(2).ToArray();
            for (int i = 0; i < initialResponseNotFinishedPart.Length; ++i)
                reloadResponse[i].Id = initialResponseNotFinishedPart[i].Id;

            source.Setup(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new DataModel.PartialListContainer<TransactionModel> { 
                    Items = initialResponseFinishedPart.Concat(initialResponseNotFinishedPart), 
                    Exhausted = true 
                });
            source.Setup(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), It.IsNotNull<long?>(), true, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new DataModel.PartialListContainer<TransactionModel> { Items = Enumerable.Empty<TransactionModel>(), Exhausted = true });
            source.Setup(f => f.ReloadAsync(It.IsAny<IEnumerable<long>>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(reloadResponse);

            await vm.InitAndWait();

            // action
            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();
            vm.Items.Should().Equal(initialResponseFinishedPart.Concat(reloadResponse));

            source.Verify(f => f.LoadAdditionalColumnsInfoAsync(It.IsAny<CancellationToken>()), Times.Once);
            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()), Times.Once);
            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), initialResponseFinishedPart.First().Id, true, It.IsAny<CancellationToken>()), Times.Once());
            source.Verify(f => f.ReloadAsync(
                Match.Create<IEnumerable<long>>(i => i.SequenceEqual(initialResponseNotFinishedPart.Select(j => j.Id))), 
                It.IsAny<CancellationToken>()), 
                Times.Once);
            source.VerifyNoOtherCalls();
        }

        [TestMethod]
        public async Task RefreshNewItems_Test()
        {
            // setup
            var part1 = fixture.Build<TransactionModel>().With(i => i.Finished, true).CreateMany();
            var part2 = fixture.Build<TransactionModel>().With(i => i.Finished, true).CreateMany();

            source.Setup(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new DataModel.PartialListContainer<TransactionModel> { Items = part1, Exhausted = true });
            source.Setup(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), It.IsNotNull<long?>(), true, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new DataModel.PartialListContainer<TransactionModel> { Items = part2, Exhausted = true });

            await vm.InitAndWait();

            // action
            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Should().WithoutError();
            vm.Items.Should().Equal(part2.Concat(part1));

            source.Verify(f => f.LoadAdditionalColumnsInfoAsync(It.IsAny<CancellationToken>()), Times.Once);
            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()), Times.Once);
            source.Verify(f => f.LoadAsync(It.IsAny<TransactionsFilter>(), part1.First().Id, true, It.IsAny<CancellationToken>()), Times.Once());
            source.VerifyNoOtherCalls();
        }
    }
}
