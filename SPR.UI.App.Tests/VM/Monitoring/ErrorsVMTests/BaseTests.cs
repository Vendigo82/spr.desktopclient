﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebClient.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring.ErrorsVMTests
{
    public class BaseTests
    {
        protected readonly Fixture fixture = new Fixture();
        protected readonly ErrorsVM vm;
        protected readonly Mock<IMonitoringSource> source;
        protected readonly List<PeriodFilter> filterArgs = new List<PeriodFilter>();

        public BaseTests(bool performInitialLoading = true)
        {
            source = new Mock<IMonitoringSource>();
            source.Setup(f => f.LoadErrorsAsync(Capture.In(filterArgs), It.IsAny<long?>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsUsingFixture(fixture);

            vm = new ErrorsVM(source.Object, null);

            if (performInitialLoading) {
                vm.InitAndWait().Wait();

                source.Invocations.Clear();
                filterArgs.Clear();
            }
        }
    }
}
