﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.WebClient.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.App.VM.Monitoring.ErrorsVMTests
{
    [TestClass]
    public class FilterTests : BaseTests
    {
        [TestMethod]
        public void PeriodFilter_Test()
        {
            // setup
            var dt1 = DateTime.Now;
            var dt2 = dt1 + TimeSpan.FromDays(1);

            // action
            vm.Period = new Tuple<DateTime, DateTime>(dt1, dt2);

            // asserts
            vm.Should().WithoutError();

            source.Verify(f => f.LoadErrorsAsync(It.IsAny<PeriodFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()), Times.Once);
            source.VerifyNoOtherCalls();

            filterArgs.Should().ContainSingle().Which.Should().BeEquivalentTo(new PeriodFilter {
                DateFrom = dt1,
                DateTo = dt2
            });
        }
    }
}
