﻿using System;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using AutoFixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Abstractions.DataSources;
using SPR.DAL.Enums;
using System.Threading;
using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Journal;
using System.Linq;
using System.Collections.Generic;
using FluentAssertions;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.App.DataModel;

namespace SPR.UI.App.VM.Journal.Tests
{
    [TestClass]
    public class JournalBaseVMTests
    {
        readonly Fixture fixture = new Fixture();
        readonly Mock<IJournalSource<JournalRecordModel>> fakeSource = new Mock<IJournalSource<JournalRecordModel>>();
        readonly Mock<JournalBaseVM<JournalRecordModel>> mockVm;
        readonly JournalBaseVM<JournalRecordModel> vm;

        public JournalBaseVMTests()
        {
            mockVm = new Mock<JournalBaseVM<JournalRecordModel>>(fakeSource.Object, null) {
                CallBase = true
            };
            vm = mockVm.Object;
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task Initialize_Test (bool exhausted)
        {
            // setup
            var result = fixture.CreateMany<JournalRecordModel>();
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = exhausted,
                    Items = result
                });

            // action
            await vm.InitAndWait();

            // asserts
            fakeSource.Verify(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()), Times.Once);
            fakeSource.VerifyNoOtherCalls();

            vm.Should().WithoutError();

            vm.IsLoadCompleted.Should().Be(exhausted);
            vm.IsLoading.Should().BeFalse();
            vm.Items.Should().Equal(result, (i, j) => i.Id == j.Id);            
        }

        [TestMethod]
        public async Task RefreshAfterInitialize_Test()
        {
            // setup
            var list1 = fixture.CreateMany<JournalRecordModel>();
            var list2 = fixture.CreateMany<JournalRecordModel>();
            var list3 = fixture.CreateMany<JournalRecordModel>();

            var sequence = new MockSequence();
            fakeSource.InSequence(sequence).Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> { Exhausted = false, Items = list1 });
            fakeSource.InSequence(sequence).Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), list1.First().Id, true, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> { Exhausted = false, Items = list2 });
            fakeSource.InSequence(sequence).Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), list2.First().Id, true, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> { Exhausted = true, Items = list3 });

            await vm.InitAndWait();

            // action
            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Items.Should().Equal(list3.Concat(list2).Concat(list1), (i, j) => i.Id == j.Id);

            vm.Should().WithoutError();

            fakeSource.Verify(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()), Times.Once);
            fakeSource.Verify(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), list1.First().Id, true, It.IsAny<CancellationToken>()), Times.Once);
            fakeSource.Verify(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), list2.First().Id, true, It.IsAny<CancellationToken>()), Times.Once);
            fakeSource.VerifyNoOtherCalls();
        }        

        [TestMethod]
        public async Task Refresh_AfterEmptyInitialize_Test()
        {
            // setup
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = true,
                    Items = Enumerable.Empty<JournalRecordModel>()
                });
            await vm.InitAndWait();

            // action
            vm.RefreshCmd.Execute(null);
            await vm.WaitBusy();

            // asserts
            vm.Items.Should().BeEmpty();
            vm.IsLoadCompleted.Should().BeTrue();

            vm.Should().WithoutError();

            fakeSource.Verify(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()), Times.Once);
            fakeSource.Verify(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, true, It.IsAny<CancellationToken>()), Times.Once);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task LoadNext_Test(bool loadComplete)
        {
            // setup
            var list1 = fixture.CreateMany<JournalRecordModel>();
            var list2 = fixture.CreateMany<JournalRecordModel>();

            // action
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = false,
                    Items = list1
                });
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), list1.Last().Id, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = loadComplete,
                    Items = list2
                });

            await vm.InitAndWait();

            // action
            vm.LoadNextCmd.Execute(null);
            while (vm.IsLoading)
                await Task.Delay(10);

            // asserts
            vm.IsLoadCompleted.Should().Be(loadComplete);
            vm.Items.Should().Equal(list1.Concat(list2), (i, j) => i.Id == j.Id);

            vm.Should().WithoutError();

            fakeSource.Verify(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), It.IsAny<long?>(), false, It.IsAny<CancellationToken>()), Times.Exactly(2));
        }

        [TestMethod]
        public async Task LoadAll_Test()
        {
            // setup
            var list1 = fixture.CreateMany<JournalRecordModel>();
            var list2 = fixture.CreateMany<JournalRecordModel>();
            var list3 = fixture.CreateMany<JournalRecordModel>();

            // action
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = false,
                    Items = list1
                });
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), list1.Last().Id, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = false,
                    Items = list2
                });
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), list2.Last().Id, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = true,
                    Items = list3
                });            

            await vm.InitAndWait();

            // action
            vm.LoadAllCmd.Execute(null);
            while (vm.IsLoading)
                await Task.Delay(10);

            // asserts
            vm.IsLoadCompleted.Should().BeTrue();
            vm.Items.Should().Equal(list1.Concat(list2).Concat(list3), (i, j) => i.Id == j.Id);

            vm.Should().WithoutError();

            fakeSource.Verify(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), It.IsAny<long?>(), false, It.IsAny<CancellationToken>()), Times.Exactly(3));
        }

        [TestMethod]
        public async Task RefreshError_Test()
        {
            // setup
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), It.IsAny<long?>(), false, It.IsAny<CancellationToken>()))
                .ThrowsAsync(new InvalidOperationException());

            // action
            await vm.InitAndWait();

            // asserts
            vm.Should().HasError();
        }

        [TestMethod]
        public async Task LoadError_Test()
        {
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = false,
                    Items = fixture.CreateMany<JournalRecordModel>()
                });
            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), It.IsAny<long?>(), false, It.IsAny<CancellationToken>()))
                .ThrowsAsync(new InvalidOperationException());

            await vm.InitAndWait();

            // action            
            vm.LoadNextCmd.Execute(null);
            while (vm.IsLoading)
                await Task.Delay(10);

            // asserts
            vm.Should().HasError();
        }

        [DataTestMethod]
        [DataRow(null, false)]
        [DataRow(SerializedObjectType.Formula, true)]
        public async Task FilterAdjust_Test(SerializedObjectType? type, bool withGuid)
        {
            // setup
            DateTime dt1 = DateTime.Now;
            DateTime dt2 = DateTime.Now.AddDays(1);
            Guid? guid = withGuid ? Guid.NewGuid() : (Guid?)null;

            mockVm.Protected().Setup("PrepareFilter", ItExpr.IsAny<JournalFilter>())
                .Callback<JournalFilter>(f => {
                    f.ObjectType = type;
                    f.Guid = guid;
                    f.DateFrom = dt1;
                    f.DateTo = dt2;
                });

            fakeSource.Setup(f => f.LoadAsync(It.IsNotNull<JournalFilter>(), null, false, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new PartialListContainer<JournalRecordModel> {
                    Exhausted = false,
                    Items = fixture.CreateMany<JournalRecordModel>()
                });

            // action
            await vm.InitAndWait();

            vm.RefreshCmd.Execute(null);
            while (vm.IsLoading)
                await Task.Delay(10);

            vm.LoadNextCmd.Execute(null);
            while (vm.IsLoading)
                await Task.Delay(10);

            // asserts
            fakeSource.Verify(f => f.LoadAsync(
                Match.Create<JournalFilter>(i => i.ObjectType == type && i.Guid == guid && i.DateFrom == dt1 && i.DateTo == dt2),
                It.IsAny<long?>(),
                It.IsAny<bool>(),
                It.IsAny<CancellationToken>()), 
                Times.Exactly(3));
        }
    }
}
