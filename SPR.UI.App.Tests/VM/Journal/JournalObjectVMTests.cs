﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.WebService.DataContract.Journal;
using Moq;
using SPR.DAL.Enums;
using SPR.UI.WebClient.DataTypes;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using SPR.UI.App.DataModel;
using System.Collections.Generic;
using System.Linq;

namespace SPR.UI.App.VM.Journal.Tests
{
    [TestClass]
    public class JournalObjectVMTests
    {
        readonly Fixture fixture = new Fixture();
        readonly Mock<IJournalSource<JournalRecordSlimModel>> fakeSource = new Mock<IJournalSource<JournalRecordSlimModel>>();

        [DataTestMethod]
        [DataRow(SerializedObjectType.Formula)]
        [DataRow(SerializedObjectType.Scenario)]
        public async Task PrepareFilter_Test(SerializedObjectType type)
        {
            //setup
            fakeSource.Setup(f => f.LoadAsync(It.IsAny<JournalFilter>(), It.IsAny<long?>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new DataModel.PartialListContainer<JournalRecordSlimModel> { Items = new JournalRecordSlimModel[] { } });

            Guid guid = Guid.NewGuid();
            var vm = new JournalObjectVM(fakeSource.Object, guid, type, null);

            // action
            await vm.InitAndWait();

            // asserts
            fakeSource.Verify(f => f.LoadAsync(
                Match.Create<JournalFilter>(i => i.Guid == guid && i.ObjectType == type && i.DateFrom == null && i.DateTo == null),
                It.IsAny<long?>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()),
                Times.Once);
        }
    }
}
