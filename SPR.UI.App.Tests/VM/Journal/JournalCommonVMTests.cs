﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.DataModel;
using SPR.UI.WebClient.DataTypes;
using SPR.UI.WebService.DataContract.Journal;
using FluentAssertions;

namespace SPR.UI.App.VM.Journal.Tests
{
    [TestClass]
    public class JournalCommonVMTests
    {
        readonly Fixture fixture = new Fixture();
        readonly Mock<IJournalSource<JournalRecordModel>> fakeSource = new Mock<IJournalSource<JournalRecordModel>>();

        [TestMethod]
        public async Task InitialPeriod_Test()
        {
            // setup
            fakeSource.Setup(f => f.LoadAsync(It.IsAny<JournalFilter>(), It.IsAny<long?>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(fixture.Create<PartialListContainer<JournalRecordModel>>());

            var vm = new JournalCommonVM(fakeSource.Object, null);

            // action
            await vm.InitAndWait();

            // asserts
            fakeSource.Verify(f => f.LoadAsync(
                Match.Create<JournalFilter>(i => i.DateFrom != null && i.DateTo == DateTime.Now.Date + TimeSpan.FromDays(1)),
                null,   // currentId is null, because reload data
                It.IsAny<bool>(),
                It.IsAny<CancellationToken>()),
                Times.Once);
        }

        [TestMethod]
        public async Task SetPeriod_Test()
        {
            // setup
            var results = new List<PartialListContainer<JournalRecordModel>>();

            fakeSource.Setup(f => f.LoadAsync(It.IsAny<JournalFilter>(), It.IsAny<long?>(), It.IsAny<bool>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => {
                    results.Add(fixture.Create<PartialListContainer<JournalRecordModel>>());
                    return results.Last();
                });

            var vm = new JournalCommonVM(fakeSource.Object, null);
            await vm.InitAndWait();

            var dt1 = DateTime.Now - TimeSpan.FromDays(20);
            var dt2 = DateTime.Now - TimeSpan.FromDays(15);

            // action
            vm.Period = new Tuple<DateTime, DateTime>(dt1, dt2);
            while (vm.IsLoading)
                await Task.Delay(10);

            // asserts
            fakeSource.Verify(f => f.LoadAsync(
                Match.Create<JournalFilter>(i => i.DateFrom == dt1 && i.DateTo == dt2),
                null,   // currentId is null, because reload data
                false,  // not relevant, but false
                It.IsAny<CancellationToken>()),
                Times.Once);

            vm.Items.Should().Equal(results.Last().Items, (i, j) => i.Id == j.Id);
        }
    }
}
