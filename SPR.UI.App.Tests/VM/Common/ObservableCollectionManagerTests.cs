﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using AutoFixture;
using FluentAssertions;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace SPR.UI.App.VM.Common.Tests
{
    [TestClass]
    public class ObservableCollectionManagerTests
    {
        public class TestClass : NotifyPropertyChangedBase
        {
            public int Val { get => 0; set => NotifyPropertyChanged(); }
        }

        private readonly TestClass[] items;
        private readonly ObservableCollectionManager<TestClass> manager;

        private readonly Func<IEnumerable<TestClass>> fakeSource;
        private readonly Action<TestClass> addedAction;
        private readonly Action<TestClass> removedAction;
        private readonly Action collectionChangedAction;
        private readonly PropertyChangedEventHandler propChangedHandler;

        private readonly ObservableCollection<TestClass> collection;

        public ObservableCollectionManagerTests()
        {
            items = Enumerable.Range(1, 3).Select(i =>  new TestClass()).ToArray();

            fakeSource = A.Fake<Func<IEnumerable<TestClass>>>();
            A.CallTo(() => fakeSource()).Returns(items);

            addedAction = A.Fake<Action<TestClass>>();
            removedAction = A.Fake<Action<TestClass>>();
            collectionChangedAction = A.Fake<Action>();
            propChangedHandler = A.Fake<PropertyChangedEventHandler>();

            manager = new ObservableCollectionManager<TestClass>(
                fakeSource, addedAction, removedAction, collectionChangedAction, propChangedHandler);

            collection = manager.Create();
        }

        [TestMethod]
        public void Create_Test()
        {
            //var collection = manager.Create();

            A.CallTo(() => fakeSource()).MustHaveHappenedOnceExactly();
            collection.Should().Equal(items, (i, e) => ReferenceEquals(i, e));
        }

        [TestMethod]
        public void SourceReturnNull_Test()
        {
            A.CallTo(() => fakeSource()).Returns(null);
            var collection = manager.Create();
            Assert.IsNull(collection);
        }

        [TestMethod]
        public void AddItem_Test()
        {
            var newItem = new TestClass();
            collection.Add(newItem);

            collection.Should().Equal(items.Concat(new[] { newItem }), (i, e) => ReferenceEquals(i, e));
            A.CallTo(() => addedAction(A<TestClass>.That.IsSameAs(newItem)))
                .MustHaveHappenedOnceExactly();
            A.CallTo(() => removedAction(A<TestClass>._)).MustNotHaveHappened();
            A.CallTo(() => collectionChangedAction()).MustHaveHappenedOnceExactly();
            A.CallTo(() => propChangedHandler(A<object>._, A<PropertyChangedEventArgs>._)).MustNotHaveHappened();

            //change new item
            newItem.Val = 1;

            A.CallTo(() => propChangedHandler(A<object>._, A<PropertyChangedEventArgs>._)).MustHaveHappenedOnceExactly();
        }

        [TestMethod]
        public void RemoveItem_Test()
        {
            collection.RemoveAt(0);

            collection.Should().Equal(items.Skip(1), (i, e) => ReferenceEquals(i, e));
            A.CallTo(() => addedAction(A<TestClass>._)).MustNotHaveHappened();
            A.CallTo(() => removedAction(A<TestClass>.That.IsSameAs(items[0])))
                .MustHaveHappenedOnceExactly();
            A.CallTo(() => collectionChangedAction()).MustHaveHappenedOnceExactly();
            A.CallTo(() => propChangedHandler(A<object>._, A<PropertyChangedEventArgs>._)).MustNotHaveHappened();
        }

        [TestMethod]
        public void ItemChanged_Test()
        {
            items[0].Val = 1;

            A.CallTo(() => addedAction(A<TestClass>._)).MustNotHaveHappened();
            A.CallTo(() => removedAction(A<TestClass>._)).MustNotHaveHappened();
            A.CallTo(() => collectionChangedAction()).MustNotHaveHappened();
            A.CallTo(() => propChangedHandler(A<object>._, A<PropertyChangedEventArgs>._)).MustHaveHappenedOnceExactly();
        }
    }
}
