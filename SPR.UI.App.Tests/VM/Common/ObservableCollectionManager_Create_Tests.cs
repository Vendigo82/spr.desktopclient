﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.Abstractions.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using FluentAssertions;

namespace SPR.UI.App.VM.Common.Tests
{
    [TestClass]
    public class ObservableCollectionManager_Create_Tests
    {       
        readonly List<object> sourceItems;
        readonly Action changedAction;
        readonly ObservableCollectionManager<TestClass> manager;
        readonly ObservableCollection<TestClass> collection;

        public class TestClass : NotifyPropertyChangedBase, IItemContainer<object>
        {
            public object Item { get; set; }

            public int Val { get => 0; set => NotifyPropertyChanged(); }
        }

        public ObservableCollectionManager_Create_Tests()
        {
            changedAction = A.Fake<Action>();
            sourceItems = Enumerable.Range(1, 3).Select(i => new object()).ToList();
            manager = ObservableCollectionManager.Create(() => sourceItems, i => new TestClass { Item = i }, changedAction);
            collection = manager.Create();                 
        }

        [TestMethod]
        public void ItemsEqual_Test()
        {
            collection.Select(i => i.Item).Should().Equal(sourceItems, (i, e) => ReferenceEquals(i, e));
        }

        [TestMethod]
        public void ChangedNotCalledOnCreate_Test()
        {
            A.CallTo(() => changedAction()).MustNotHaveHappened();
        }

        [TestMethod]
        public void AddItem_Test()
        {
            var obj = new object();
            collection.Add(new TestClass { Item = obj });

            sourceItems.Should().HaveCount(4);
            sourceItems.Last().Should().BeSameAs(obj);
            A.CallTo(() => changedAction()).MustHaveHappenedOnceExactly();
        }

        [TestMethod]
        public void RemoveItem_Test()
        {
            var prevList = sourceItems.ToArray();
            collection.RemoveAt(0);
            sourceItems.Should().HaveCount(2);
            sourceItems.Should().Equal(prevList.Skip(1), (i, e) => ReferenceEquals(i, e));
            A.CallTo(() => changedAction()).MustHaveHappenedOnceExactly();
        }

        [TestMethod]
        public void PropertyChanged_Test()
        {
            collection.First().Val = 1;
            sourceItems.Should().HaveCount(3); 
            A.CallTo(() => changedAction()).MustHaveHappenedOnceExactly();
        }
    }
}
