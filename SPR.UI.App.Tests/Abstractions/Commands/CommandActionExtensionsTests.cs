﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using FakeItEasy;
using FluentAssertions;
using Moq;
using SPR.UI.App.Abstractions;
using SPR.UI.App.Services;
using VenSoft.WPFLibrary.Commands;
using AutoFixture;
using SPR.UI.App.Abstractions.UI;

namespace SPR.UI.App.Abstractions.Commands.Tests
{
    [TestClass]
    public class CommandActionExtensionsTests
    {
        readonly Mock<ILogger> fakeLogger;
        readonly Mock<IWindowFactory> fakeWndFactory;

        readonly Mock<ITypeResolver> fakeResolver;
        readonly MockRepository repository;

        public CommandActionExtensionsTests()
        {
            repository = new MockRepository(MockBehavior.Default);

            fakeLogger = repository.Create<ILogger>();
            fakeWndFactory = repository.Create<IWindowFactory>();

            fakeResolver = repository.Create<ITypeResolver>();
            fakeResolver.Setup(f => f.Resolve<ILogger>()).Returns(fakeLogger.Object);
            fakeResolver.Setup(f => f.Resolve<IWindowFactory>()).Returns(fakeWndFactory.Object);
        }

        [TestMethod]
        public void CreateRelayCommand_Create_Test()
        {
            // setup
            var action = repository.Create<ICommandAction>();

            // action
            var cmd = action.Object.CreateRelayCommand(fakeResolver.Object);
            cmd.Execute(null);

            // asserts
            cmd.Should().BeOfType<RelayCommand>();

            action.Verify(f => f.Action(), Times.Once);
            repository.Verify();
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void CreateRelayCommand_ActionError_Test()
        {
            // setup
            var action = repository.Create<ICommandAction>();
            action.Setup(f => f.Action()).Throws<InvalidOperationException>();

            // action
            var cmd = action.Object.CreateRelayCommand(fakeResolver.Object);
            cmd.Execute(null);

            // asserts
            action.Verify(f => f.Action(), Times.Once);
            fakeLogger.Verify(f => f.Log(LogLevel.Error, It.IsAny<string>(), It.IsNotNull<Exception>()), Times.Once);
            fakeWndFactory.Verify(f => f.ShowErrorDialog(It.IsAny<string>()), Times.Once);

            repository.Verify();
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void CreateRelayCommand_Arg_Test()
        {
            // setup
            var action = repository.Create<ICommandAction<string>>();
            string s = "abc";

            // action
            var cmd = action.Object.CreateRelayCommand(fakeResolver.Object);
            cmd.Execute(s);

            // asserts
            cmd.Should().BeOfType<RelayCommand<object>>();
            action.Verify(f => f.Action(s), Times.Once);
            repository.Verify();
            repository.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void CreateRelayCommand_CreateTuple2_Test()
        {
            // setup
            var action = repository.Create<ICommandAction<string, int>>();
            string s = "abc";
            int i = 10;
            var tuple = Tuple.Create<object, object>(s, i);

            // action
            var cmd = action.Object.CreateRelayCommand(fakeResolver.Object);
            cmd.Execute(tuple);

            // asserts
            cmd.Should().BeOfType<RelayCommand<object>>();
            action.Verify(f => f.Action(s, i), Times.Once);
            repository.Verify();
            repository.VerifyNoOtherCalls();
        }
    }
}
