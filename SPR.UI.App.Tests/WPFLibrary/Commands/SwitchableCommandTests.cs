﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VenSoft.WPFLibrary.Commands.Tests
{
    [TestClass]
    public class SwitchableCommandTests
    {
        [TestMethod]
        public void CreateWithNullTest() {
            SwitchableCommand cmd = new SwitchableCommand();
            Assert.IsNull(cmd.Command);
            Assert.IsFalse(cmd.CanExecute(null));
            cmd.Execute(null);            
        }

        [TestMethod]
        public void CreateWithCommandTest() {
            int execute = 0;
            int canExecute = 0;
            RelayCommand<int> c1 = new RelayCommand<int>((i) => { execute = i; }, i => { canExecute = i; return true; });
            SwitchableCommand cmd = new SwitchableCommand(c1);
            Assert.AreSame(c1, cmd.Command);

            Assert.IsTrue(cmd.CanExecute(1));
            Assert.AreEqual(1, canExecute);

            cmd.Execute(2);
            Assert.AreEqual(2, execute);
        }

        [TestMethod]
        public void AsyncCommandTest() {
            int execute = 0;
            int canExecute = 0;
            AsyncCommand<int> c1 = new AsyncCommand<int>(
                i => { execute = i; return Task.CompletedTask; },
                i => { canExecute = i; return true; });
            SwitchableCommand cmd = new SwitchableCommand(c1);
            Assert.AreSame(c1, cmd.Command);

            Assert.IsTrue(cmd.CanExecute(1));
            Assert.AreEqual(1, canExecute);

            cmd.Execute(2);
            Assert.AreEqual(2, execute);
        }

        [TestMethod]
        public void SwitchCommand() {
            int c1cnt = 0;
            int c2cnt = 0;
            RelayCommand c1 = new RelayCommand(() => { c1cnt += 1; });
            RelayCommand c2 = new RelayCommand(() => { c2cnt += 1; });

            int changed = 0;
            SwitchableCommand cmd = new SwitchableCommand();
            cmd.CanExecuteChanged += (s, a) => {
                changed += 1;
            };

            Assert.IsFalse(cmd.CanExecute(null));
            cmd.Execute(null);
            Assert.AreEqual(0, c1cnt);
            Assert.AreEqual(0, c2cnt);

            cmd.Command = c1;
            //Assert.AreEqual(1, changed);
            Assert.IsTrue(cmd.CanExecute(null));
            cmd.Execute(null);
            Assert.AreEqual(1, c1cnt);
            Assert.AreEqual(0, c2cnt);

            cmd.Command = c2;
            //Assert.AreEqual(2, changed);
            Assert.IsTrue(cmd.CanExecute(null));
            cmd.Execute(null);
            Assert.AreEqual(1, c1cnt);
            Assert.AreEqual(1, c2cnt);

            cmd.Command = null;
            //Assert.AreEqual(3, changed);
            Assert.IsFalse(cmd.CanExecute(null));
            cmd.Execute(null);
            Assert.AreEqual(1, c1cnt);
            Assert.AreEqual(1, c2cnt);
        }

        [TestMethod]
        public void SwitchCommand_CanExecute() {
            bool can = false;
            RelayCommand c1 = new RelayCommand(() => { }, () => true);
            RelayCommand c2 = new RelayCommand(() => { }, () => can);

            int changed = 0;
            SwitchableCommand cmd = new SwitchableCommand(c1);
            cmd.CanExecuteChanged += (s, a) => {
                changed += 1;
            };

            Assert.IsTrue(cmd.CanExecute(null));

            cmd.Command = c2;
            //Assert.AreEqual(1, changed);
            Assert.IsFalse(cmd.CanExecute(null));

            can = true;
            c2.RaiseCanExecuteChanged();
            //Assert.AreEqual(2, changed);
            Assert.IsTrue(cmd.CanExecute(null));

            c1.RaiseCanExecuteChanged();
            //Assert.AreEqual(2, changed);
            Assert.IsTrue(cmd.CanExecute(null));
        }
    }
}
