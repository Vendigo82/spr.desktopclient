﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.UI.App.VM.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App
{
    public static class BusyVMBaseExtentions
    {
        public static async Task WaitBusy(this BusyVMBase vm) {
            while (vm.IsBusy)
                await Task.Delay(50);
        }

        public static async Task InitAndWait(this BusyVMBase vm, bool fresh = false) {
            await vm.Initialize(fresh);//true
            await vm.WaitBusy();
        }

        public static async Task SaveAndWait(this BusyVMBase vm) {
            Assert.IsTrue(vm.SaveCmd.CanExecute(null));
            vm.SaveCmd.Execute(null);
            await vm.WaitBusy();
        }
    }
}
