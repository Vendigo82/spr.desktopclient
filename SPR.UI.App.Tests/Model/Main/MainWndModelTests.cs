﻿using AutoFixture;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.UI.App.Abstractions.DataSources;
using SPR.UI.App.Services;
using SPR.UI.App.Services.WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.UI.App.Model.Main
{
    [TestClass]
    public class MainWndModelTests
    {
        readonly MainWndModel target;
        readonly Mock<ITypeResolver> resolverMock = new Mock<ITypeResolver>();
        readonly Mock<IConfigurationSource> confSourceMock = new Mock<IConfigurationSource>();
        readonly Mock<IAppVersionProvider> versionMock = new Mock<IAppVersionProvider>();
        readonly Mock<ISettingsWritter> settingsMock = new Mock<ISettingsWritter>();
        readonly Mock<IWebServiceConfigWritter> webServiceConfig = new Mock<IWebServiceConfigWritter>();
        readonly Mock<IInstancesSource> instancesSourceMock = new Mock<IInstancesSource>();

        public MainWndModelTests()
        {
            resolverMock.Setup(f => f.Resolve<IConfigurationSource>()).Returns(confSourceMock.Object);
            target = new MainWndModel(resolverMock.Object, versionMock.Object, instancesSourceMock.Object, settingsMock.Object, webServiceConfig.Object);
        }

        [DataTestMethod]
        [DataRow("http://localhost/SPR/test", "http://localhost:8050/SPRUI", "http://localhost/SPR/test")]
        [DataRow("testredirect", "http://localhost:8050/SPRUI", "http://localhost:8050/SPRUI/testredirect")]
        [DataRow("testredirect", "http://localhost:8050/SPRUI/", "http://localhost:8050/SPRUI/testredirect")]
        [DataRow("/testredirect", "http://localhost:8050/SPRUI/", "http://localhost:8050/SPRUI/testredirect")]
        public async Task TestServiceUrl_ShouldBeCorrect(string testServiceUrl, string webServiceUrl, string expectedTestUrl)
        {
            confSourceMock.Setup(f => f.GetInstanceAsync(default)).ReturnsAsync(new WebService.DataContract.Configuration.InstanceResponse
            {
                TestServiceUrl = testServiceUrl
            });
            webServiceConfig.Setup(f => f.BaseUrl).Returns(webServiceUrl);

            await target.InitializeAsync(default);

            settingsMock.Verify(f => f.SetTestModuleUrl(expectedTestUrl), Times.Once);
        }

        [AutoData]
        public void GetInstances_ShouldReturnValuesFromSource(IEnumerable<InstanceSettingsModel> items)
        {
            instancesSourceMock.Setup(f => f.GetInstances()).Returns(items);

            var result = target.GetInstances();

            result.Should().BeSameAs(items);
        }

        [AutoData]
        public void SetInstance_ShouldCallConfigWritter(InstanceSettingsModel model)
        {
            target.SetInstance(model);

            webServiceConfig.Verify(f => f.SetValues(model.BaseUrl, model.Timeout), Times.Once);
        }
    }
}
