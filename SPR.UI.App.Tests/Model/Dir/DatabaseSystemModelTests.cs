﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RandomType;
using KellermanSoftware.CompareNetObjects;
using SPR.UI.WebService.DataContract.Dir;

namespace SPR.UI.App.Model.Dir.Tests
{
    [TestClass]
    public class DatabaseSystemModelTests
    {
        [TestMethod]
        public void Construct()
        {
            var data = RandomTypeGenerator.Generate<KeyDescriptionModel>();
            var model = new DatabaseSystemModel(data);

            var logic = new CompareLogic(new ComparisonConfig {
                IgnoreObjectTypes = true
            });

            var result = logic.Compare(data, model);
            Assert.IsTrue(result.AreEqual, result.DifferencesString);
        }
    }
}
