﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;

namespace SPR.UI.App.Model.Special.Tests
{
    [TestClass]
    public class RefContainerTests
    {
        [TestMethod]
        public void ConstructEmpty_Test()
        {
            Assert.IsNull(new RefContainer<object>(null).Value);
        }

        [TestMethod]
        public void ConstructWithItem_Test()
        {
            var obj = new object();
            Assert.AreSame(obj, new RefContainer<object>(obj, null).Value);
        }

        [TestMethod]
        public void Assign_Test()
        {
            var obj1 = new object();
            var func = A.Fake<Action<object>>();

            var holder = new RefContainer<object>(obj1, func);
            holder.Value = obj1; //same object
            A.CallTo(() => func(A<object>._)).MustNotHaveHappened();

            holder.Value = new object(); //other object
            A.CallTo(() => func(A<object>._)).MustHaveHappenedOnceExactly();
            
            holder.Value = null;
            A.CallTo(() => func(A<object>._)).MustHaveHappenedTwiceExactly();

            holder.Value = null;//again null, not changed
            A.CallTo(() => func(A<object>._)).MustHaveHappenedTwiceExactly();

            holder.Value = obj1;
            A.CallTo(() => func(A<object>._)).MustHaveHappened(3, Times.Exactly);
        }
    }
}
