﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SPR.UI.App.View.Formulas.Tests
{
    [TestClass]
    public class SelectFormulaTypeDlgTests
    {
        //readonly SelectFormulaTypeDlg dlg = new SelectFormulaTypeDlg();

        [TestMethod]
        public void TypeDeclaration_Test()
        {
            typeof(SelectFormulaTypeDlg).Should().HaveProperty<object>("SelectedValue");
        }
    }
}
