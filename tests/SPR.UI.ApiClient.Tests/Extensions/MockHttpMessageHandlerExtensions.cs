﻿using System.Collections.Generic;
using System.IO;

namespace RichardSzalay.MockHttp
{
    public static class MockHttpMessageHandlerExtensions
    {
        public static MockedRequest RespondFromJsonFile(this MockedRequest request, string fileName)
        {
            var content = File.ReadAllText(fileName);
            return request.Respond("application/json", content);
        }

        public static MockedRequest CaptureRequestContent(this MockedRequest request, List<string> captureIn)
        {
            return request.With(msg =>
            {
                captureIn.Add(msg.Content!.ReadAsStringAsync().Result);
                return true;
            });
        }
    }
}
