﻿using AutoMapper.Extensions.EnumMapping;
using SPR.UI.ApiClient.Mapping;
using Xunit;

namespace SPR.UI.ApiClient.Tests.Mapping
{
    public class MappingProfileTests
    {
        [Fact]
        public void StepConstraints_ConfigurationIsValidTest()
        {
            var cfg = new AutoMapper.MapperConfiguration(c => {
                c.AddProfile<SwaggerMappingProfile>();
                c.EnableEnumMappingValidation();
            });

            cfg.AssertConfigurationIsValid();
        }
    }
}
