﻿using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using RichardSzalay.MockHttp;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace SPR.UI.ApiClient.Infra.FormulaRepositoryTests
{
    public class FormulaRepositoryTests
    {
        private readonly MockHttpMessageHandler handlerMock = new();
        private readonly FormulaRepository target;

        public FormulaRepositoryTests()
        {
            var httpClient = handlerMock.ToHttpClient();
            httpClient.BaseAddress = new Uri("http://localhost");

            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.SwaggerMappingProfile>()));
            target = new FormulaRepository(new ApiClient(httpClient), mapper);
        }

        [Theory, AutoData]
        public async Task GetRegisterNameUsage_WhenFound_ShouldBeSuccess(string registerName)
        {
            // setup
            var request = handlerMock
                .When(HttpMethod.Get, $"/Formula/registerName/{registerName}/usage")
                .RespondFromJsonFile("Infra/FormulaRepositoryTests/json/GetRegisterNameUsage_200OK.json");

            // action
            var result = await target.GetRegisterNameUsage(registerName);

            // asserts
            result.Should().ContainSingle().Which.Should().BeEquivalentTo(new SPR.UI.WebService.DataContract.Formula.FormulaBriefModel
            {
                Code = 187,
                Guid = Guid.Parse("f3e2d6e4-d038-47b1-bde5-1ecbb705ad40"),
                Name = "Formula with register",
                Type = DAL.Enums.FormulaType.Expression
            });

            handlerMock.GetMatchCount(request).Should().Be(1);
        }

        [Theory, AutoData]
        public async Task GetRegisterNameUsage_WhenNotFound_ShouldBeEmpty(string registerName)
        {
            // setup
            var request = handlerMock
                .When(HttpMethod.Get, $"/Formula/registerName/{registerName}/usage")
                .Respond(HttpStatusCode.NoContent);

            // action
            var result = await target.GetRegisterNameUsage(registerName);

            // asserts
            result.Should().BeEmpty();

            handlerMock.GetMatchCount(request).Should().Be(1);
        }
    }
}
