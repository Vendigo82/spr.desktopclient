﻿using AutoFixture.Xunit2;
using AutoMapper;
using FluentAssertions;
using FluentAssertions.Json;
using Newtonsoft.Json.Linq;
using RichardSzalay.MockHttp;
using SPR.UI.Core.UseCases.StepConstraints;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.UI.ApiClient.Infra.StepConstraintsRepositoryTests
{
    public class StepConstraintsRepositoryTests
    {
        private readonly MockHttpMessageHandler handlerMock = new();
        private readonly StepConstraintsRepository target;

        public StepConstraintsRepositoryTests()
        {
            var httpClient = handlerMock.ToHttpClient();
            httpClient.BaseAddress = new Uri("http://localhost");

            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.SwaggerMappingProfile>()));
            target = new StepConstraintsRepository(new ApiClient(httpClient), mapper);
        }

        [Theory, AutoData]
        public async Task LoadLatestsAsync_ShouldBeSuccess(string stepName)
        {
            // setup
            var request = handlerMock
                .When(HttpMethod.Get, $"/dir/step/{stepName}/postconditions/latest")
                .RespondFromJsonFile("Infra/StepConstraintsRepositoryTests/json/postconditions_response.json");

            // action
            var result = await target.LoadLatestsAsync(stepName, StepConstraintType.PostConditions);

            // asserts
            result.Should().BeEquivalentTo(new StepConstraintsModel
            {
                Header = new StepConstraintsHeaderModel { Version = 2 },
                Items = new List<StepConstraintItemModel>()
                {
                    new StepConstraintItemModel { Error = false, Expression = "form.PostConditionWarning", Message = "Post condition warning message"},
                    new StepConstraintItemModel { Error = true, Expression = "form.PostConditionError", Message = "Post condition error message" }
                }
            });

            handlerMock.GetMatchCount(request).Should().Be(1);
        }

        [Theory, AutoData]
        public async Task SaveAsync_ShouldSuccess(string stepName)
        {
            // setup
            var content = new List<string>();
            var expectedContent = await File.ReadAllTextAsync("Infra/StepConstraintsRepositoryTests/json/postconditions_response.json");

            var request = handlerMock
                .When(HttpMethod.Post, $"/dir/step/{stepName}/postconditions")
                .CaptureRequestContent(content)
                .Respond(HttpStatusCode.OK);

            var body = new StepConstraintsModel
            {
                Header = new StepConstraintsHeaderModel { Version = 2 },
                Items = new List<StepConstraintItemModel>()
                {
                    new StepConstraintItemModel { Error = false, Expression = "form.PostConditionWarning", Message = "Post condition warning message"},
                    new StepConstraintItemModel { Error = true, Expression = "form.PostConditionError", Message = "Post condition error message" }
                }
            };

            // action
            await target.SaveAsync(stepName, StepConstraintType.PostConditions, body);

            // asserts
            handlerMock.GetMatchCount(request).Should().Be(1);

            JToken.Parse(content.Single()).Should().BeEquivalentTo(expectedContent);
        }
    }
}
