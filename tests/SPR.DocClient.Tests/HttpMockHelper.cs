﻿using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.DocClient.Tests
{
    public static class HttpMockHelper
    {
        public static MockedRequest RespondWithJsonFile(this MockedRequest request, string fileName)
        {
            var response = File.ReadAllText(Path.Combine("JsonFiles", fileName));
            return request.Respond("application/json", response);
        }
    }
}
