﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RichardSzalay.MockHttp;
using SPR.DocClient.Dto;
using System;
using System.Net;
using System.Threading.Tasks;

namespace SPR.DocClient.Tests
{
    [TestClass]
    public class DocClientTests
    {
        readonly MockHttpMessageHandler mockHttp = new MockHttpMessageHandler();
        readonly DocHttpClient target;

        public DocClientTests()
        {
            var httpClient = mockHttp.ToHttpClient();
            httpClient.BaseAddress = new Uri("http://localhost/doc/SPR/1.0.0/");
            target = new DocHttpClient(httpClient);
        }

        [TestMethod]
        public async Task ShouldReturnCorrectListOfFunctions()
        {
            // setup
            mockHttp
                .When("http://localhost/doc/SPR/1.0.0/ru/functions")
                .RespondWithJsonFile("FunctionsResponse.json");

            // action
            var result = await target.GetFunctionsAsync(new RequestModel("ru"), null);

            // asserts
            result.Should().HaveCount(2).And.OnlyContain(i => !string.IsNullOrEmpty(i.Title) && !string.IsNullOrEmpty(i.SystemName));
        }

        [DataTestMethod]
        [DataRow(null, "http://localhost/doc/SPR/1.0.0/ru/functions")]
        [DataRow("", "http://localhost/doc/SPR/1.0.0/ru/functions")]
        [DataRow("abc", "http://localhost/doc/SPR/1.0.0/ru/functions?groupFilter=abc")]
        public async Task RequstWithFilters_QueryStringShouldBeCorrect(string group, string expectedUri)
        {
            // setup
            var request = mockHttp
                .When(expectedUri)
                .RespondWithJsonFile("FunctionsResponse.json");

            // action
            await target.GetFunctionsAsync(new RequestModel("ru"), group);

            // asserts
            mockHttp.GetMatchCount(request).Should().Be(1);
        }

        [TestMethod]
        public async Task ShouldReturnCorrectFunction()
        {
            // setup
            mockHttp
                .When("http://localhost/doc/SPR/1.0.0/ru/functions/test_function1")
                .RespondWithJsonFile("FunctionResponse.json");

            // action
            var result = await target.GetFunctionAsync(new RequestModel("ru"), "test_function1");

            // asserts
            result.Should().NotBeNull();
            result.Implementations.Should().HaveCount(2);
        }

        [TestMethod]
        public async Task ShouldReturnNullOnNoContent()
        {
            // setup
            mockHttp
                .When("http://localhost/doc/SPR/1.0.0/ru/functions/test_function1")
                .Respond(HttpStatusCode.NoContent);

            // action
            var result = await target.GetFunctionAsync(new RequestModel("ru"), "test_function1");

            // asserts
            result.Should().BeNull();
        }

        [TestMethod]
        public async Task ShouldReturnCorrentGroups()
        {
            // setup
            mockHttp
                .When("http://localhost/doc/SPR/1.0.0/ru/groups")
                .RespondWithJsonFile("GroupsResponse.json");

            // action
            var result = await target.GetGroupsAsync(new RequestModel("ru"));

            // asserts
            result.Should().BeEquivalentTo(new[] {
                new GroupDto { Group = "group1", Title = "Группа 1" },
                new GroupDto { Group = "group2", Title = "Группа 2" }
            });
        }
    }
}
