﻿using AutoFixture;
using AutoMapper;
using FluentAssertions;
using FluentAssertions.Equivalency;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SPR.DocClient.Dto;
using SPR.DocClient.Exceptions;
using SPR.DocClient.Mapping;
using SPR.DocClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.DocClient.Tests
{
    [TestClass]
    public class DocRepositoryTests
    {
        readonly Mock<IDocHttpClient> clientMock = new Mock<IDocHttpClient>();

        private DocRepository CreateTarget()
        {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<MappingProfile>()));
            return new DocRepository(clientMock.Object, mapper);
        }

        [AutoData, AutoMoq]
        public async Task GetGroupsShouldSuccess(IEnumerable<GroupDto> items)
        {
            // setup
            var target = CreateTarget();
            clientMock.Setup(f => f.GetGroupsAsync(It.IsAny<RequestModel>())).ReturnsAsync(items);

            // action
            var result = await target.GetGroupsAsync();

            // asserts
            AssertionOptions.AssertEquivalencyUsing(c => c.Using(new ModelEquivalencyStep()));
            result.Items.Should()
                .HaveSameCount(items).And
                .BeEquivalentTo(items);

            clientMock.Verify(f => f.GetGroupsAsync(It.IsAny<RequestModel>()), Times.Once);
        }

        [AutoData, AutoMoq]
        public async Task GetFunctionsShouldSuccess(IEnumerable<FunctionTitleDto> items)
        {
            // setup
            var target = CreateTarget();
            clientMock.Setup(f => f.GetFunctionsAsync(It.IsAny<RequestModel>(), null)).ReturnsAsync(items);

            // action
            var result = await target.GetFunctionsAsync();

            // asserts
            AssertionOptions.AssertEquivalencyUsing(c => c.Using(new ModelEquivalencyStep()));
            result.Items.Should()
                .HaveSameCount(items).And
                .BeEquivalentTo(items);

            clientMock.Verify(f => f.GetFunctionsAsync(It.IsAny<RequestModel>(), null), Times.Once);
        }

        [AutoData, AutoMoq]
        public async Task GetFunctionsWithGroupFilter_ShouldCallClientWithGroup(GroupName group)
        {
            // setup
            var target = CreateTarget();
            clientMock.Setup(f => f.GetFunctionsAsync(It.IsAny<RequestModel>(), null)).ReturnsAsync(Enumerable.Empty<FunctionTitleDto>());

            // action
            await target.GetFunctionsAsync(new FunctionsFilter { Group = group });

            // asserts
            clientMock.Verify(f => f.GetFunctionsAsync(It.IsAny<RequestModel>(), group), Times.Once);
        }

        [AutoData, AutoMoq]
        public async Task GetFunctionShouldBeSuccess(FunctionDto function)
        {
            // setup
            var target = CreateTarget();
            clientMock.Setup(f => f.GetFunctionAsync(It.IsAny<RequestModel>(), function.SystemName)).ReturnsAsync(function);

            // action
            var result = await target.GetFunctionAsync((FunctionName)function.SystemName);

            // asserts
            AssertionOptions.AssertEquivalencyUsing(c => c.Using(new ModelEquivalencyStep()));
            result.Should().NotBeNull().And.BeEquivalentTo(function);

            
            clientMock.Verify(f => f.GetFunctionAsync(It.IsAny<RequestModel>(), function.SystemName), Times.Once);
        }

        [AutoData, AutoMoq]
        public async Task GetFunctionThrowsExceptionOnNullResult(string functionName)
        {
            // setup
            var target = CreateTarget();
            clientMock.Setup(f => f.GetFunctionAsync(It.IsAny<RequestModel>(), functionName)).ReturnsAsync((FunctionDto)null);

            // action
            Func<Task> action = () => target.GetFunctionAsync((FunctionName)functionName);

            // asserts
            await action.Should().ThrowAsync<FunctionDocumentationNotFoundException>();
        }

        public class ModelEquivalencyStep : IEquivalencyStep
        {
            public EquivalencyResult Handle(Comparands comparands, IEquivalencyValidationContext context, IEquivalencyValidator nestedValidator)
            {
                if (comparands.Subject is Title t1)
                {
                    ((string)t1).Should().Be((string)comparands.Expectation);
                    return EquivalencyResult.AssertionCompleted;
                }

                if (comparands.Expectation is Title t2)
                {
                    ((string)comparands.Subject).Should().Be(t2);
                    return EquivalencyResult.AssertionCompleted;
                }

                if (comparands.Subject is FunctionName fn1)
                {
                    ((string)fn1).Should().Be((string)comparands.Expectation);
                    return EquivalencyResult.AssertionCompleted;
                }

                if (comparands.Expectation is FunctionName fn2)
                {
                    ((string)comparands.Subject).Should().Be(fn2);
                    return EquivalencyResult.AssertionCompleted;
                }

                if (comparands.Subject is ValueType<string> vt1)
                {
                    ((string)vt1).Should().Be((string)comparands.Expectation);
                    return EquivalencyResult.AssertionCompleted;
                }

                if (comparands.Expectation is ValueType<string> vt2)
                {
                    ((string)comparands.Subject).Should().Be((string)vt2);
                    return EquivalencyResult.AssertionCompleted;
                }

                return EquivalencyResult.ContinueWithNext;
            }
        }
    }
}
