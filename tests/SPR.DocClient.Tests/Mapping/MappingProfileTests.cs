﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SPR.DocClient.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.DocClient.Tests.Mapping
{
    [TestClass]
    public class MappingProfileTests
    {
        [TestMethod]
        public void ConfigurationIsValidTest()
        {
            var cfg = new AutoMapper.MapperConfiguration(c => c.AddProfile<MappingProfile>());

            cfg.AssertConfigurationIsValid();
        }
    }
}
