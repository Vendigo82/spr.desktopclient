﻿using AutoMapper;
using SPR.UI.Core.UseCases.FormulaEditor;
using SPR.UI.WebService.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.ApiClient.Infra
{
    public class FormulaRepository : IFormulaRepository
    {
        private readonly IApiClient _client;
        private readonly IMapper _mapper;

        public FormulaRepository(IApiClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ObjectContainer<WebService.DataContract.Formula.FormulaModel>> GetMain(Guid id, CancellationToken ct)
        {
            try
            {
                var response = await _client.FormulaIdGetAsync(id, ct);
                return _mapper.Map<ObjectContainer<WebService.DataContract.Formula.FormulaModel>>(response);
            }
            catch (ApiException e) when (e.StatusCode == 404)
            {
                return new ObjectContainer<WebService.DataContract.Formula.FormulaModel>() { Item = null };
            }
        }

        public async Task<ObjectContainer<WebService.DataContract.Formula.FormulaModel>> Save(SPR.UI.WebService.DataContract.Formula.FormulaModel item, string comment, CancellationToken ct)
        {
            var requestModel = new FormulaModelSaveObjectRequest
            {
                Item = _mapper.Map<FormulaModel>(item),
                Comment = comment
            };
            var response = await _client.FormulaSavePostAsync(requestModel, ct);
            return _mapper.Map<ObjectContainer<WebService.DataContract.Formula.FormulaModel>>(response);
        }

        public async Task<ItemsList<WebService.DataContract.Common.VersionInfo>> GetArchiveVersionsListAsync(Guid id, CancellationToken ct)
        {
            var response = await _client.FormulaIdArchiveVersionsGetAsync(id, ct);
            return _mapper.Map<ItemsList<WebService.DataContract.Common.VersionInfo>>(response);
        }

        public async Task<WebService.DataContract.Formula.FormulaModel> GetArchiveVersionAsync(Guid id, int ver, CancellationToken ct)
        {
            try
            {
                var response = await _client.FormulaIdArchiveVersionGetAsync(id, ver, ct);
                return _mapper.Map<WebService.DataContract.Formula.FormulaModel>(response.Item);
            }
            catch (ApiException e) when (e.StatusCode == 404)
            {
                return null;
            }
        }

        public async Task<WebService.DataContract.Formula.FormulaUsageResponse> GetUsage(Guid id, CancellationToken ct = default)
        {
            var response = await _client.FormulaIdUsageGetAsync(id, ct);
            return _mapper.Map<WebService.DataContract.Formula.FormulaUsageResponse>(response);
        }

        public async Task<WebService.DataContract.Formula.FormulaModel> RestoreAsync(Guid id, int version, string comment, CancellationToken ct)
        {
            var request = new RestoreObjectRequest
            {
                Comment = comment
            };
            var response = await _client.FormulaIdArchiveVersionRestorePostAsync(id, version, request, ct);
            return _mapper.Map<WebService.DataContract.Formula.FormulaModel>(response.Item);
        }

        public async Task<IEnumerable<WebService.DataContract.Formula.FormulaBriefModel>> GetRegisterNameUsage(string registerName, CancellationToken ct = default)
        {
            try
            {
                var response = await _client.FormulaRegisterNameRegisterNameUsageGetAsync(registerName, ct);
                return _mapper.Map<IEnumerable<WebService.DataContract.Formula.FormulaBriefModel>>(response.Formulas);
            }
            catch (ApiException e) when (e.StatusCode == (int)HttpStatusCode.NoContent)
            {
                return Enumerable.Empty<WebService.DataContract.Formula.FormulaBriefModel>();
            }
        }
    }
}
