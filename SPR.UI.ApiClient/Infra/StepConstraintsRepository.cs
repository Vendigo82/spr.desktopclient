﻿using AutoMapper;
using SPR.UI.Core.UseCases.StepConstraints;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.ApiClient.Infra
{
    public class StepConstraintsRepository : IStepConstraintsRepository
    {
        private readonly IApiClient apiClient;
        private readonly IMapper mapper;

        public StepConstraintsRepository(IApiClient apiClient, IMapper mapper)
        {
            this.apiClient = apiClient ?? throw new ArgumentNullException(nameof(apiClient));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<StepConstraintsModel> LoadLatestsAsync(string stepName, StepConstraintType type, CancellationToken ct = default)
        {
            if (type != StepConstraintType.PostConditions)
                throw new NotSupportedException();

            var response = await apiClient.DirStepStepNamePostconditionsLatestGetAsync(stepName, ct);
            return mapper.Map<StepConstraintsModel>(response);
        }

        public async Task SaveAsync(string stepName, StepConstraintType type, StepConstraintsModel model, CancellationToken ct = default)
        {
            if (type != StepConstraintType.PostConditions)
                throw new NotSupportedException();

            var request = new StepConstraintsUpdateRequestDto
            {
                Header = new StepConstraintsHeaderDto { Version = model.Header.Version },
                Items = mapper.Map<ICollection<StepConstraintsItemDto>>(model.Items)
            };

            await apiClient.DirStepStepNamePostconditionsPostAsync(stepName, request, ct);
        }
    }
}
