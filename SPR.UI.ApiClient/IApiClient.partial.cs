﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.ApiClient
{
    public partial interface IApiClient
    {
        Task<TransactionItemDtoItemsListPartial> MonitoringTransactionsGetAsync(
            int? count = null,
            long? next_id = null,
            bool? fresh = null,
            string from = null,
            string to = null,
            IEnumerable<KeyValuePair<string, object>> filters = null,
            CancellationToken cancellationToken = default);

        Task<Dto.DictionaryItemInfoModel> DictionaryItemUsageGetAsync(string dictionaryName, string id, CancellationToken cancellationToken = default);
    }
}
