﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;

namespace SPR.UI.ApiClient.Mapping
{
    public class SwaggerMappingProfile : Profile
    {
        public SwaggerMappingProfile()
        {
            CreateMap<FormulaTypeDto, DAL.Enums.FormulaType>()
                .ConvertUsingEnumMapping(s => s.MapByName())
                .ReverseMap();
            CreateMap<FormulaBriefDto, WebService.DataContract.Formula.FormulaBriefModel>().ReverseMap();

            CreateMap<StepConstraintsItemDto, Core.UseCases.StepConstraints.StepConstraintItemModel>().ReverseMap();
            CreateMap<StepConstraintsHeaderDto, Core.UseCases.StepConstraints.StepConstraintsHeaderModel>();
            CreateMap<StepConstraintsResponseDto, Core.UseCases.StepConstraints.StepConstraintsModel>();
        }
    }
}
