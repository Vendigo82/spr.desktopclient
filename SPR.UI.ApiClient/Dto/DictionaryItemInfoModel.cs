﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.ApiClient.Dto
{
    [System.CodeDom.Compiler.GeneratedCode("NJsonSchema", "10.4.6.0 (Newtonsoft.Json v9.0.0.0)")]
    public partial class DictionaryItemInfoModel
    {
        /// <summary>Information about item's usage</summary>
        [Newtonsoft.Json.JsonProperty("usage", Required = Newtonsoft.Json.Required.Always)]
        public ItemUsageModel Usage { get; set; } = new ItemUsageModel();


    }
}
