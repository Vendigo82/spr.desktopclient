﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.ApiClient
{
    public partial class ApiClient 
    {
        public async Task<TransactionItemDtoItemsListPartial> MonitoringTransactionsGetAsync(
            int? count = null,
            long? next_id = null,
            bool? fresh = null,
            string from = null,
            string to = null,
            IEnumerable<KeyValuePair<string, object>> filters = null,
            CancellationToken cancellationToken = default)
        {
            var urlBuilder_ = new System.Text.StringBuilder();
            urlBuilder_.Append("Monitoring/transactions?");
            if (count != null) {
                urlBuilder_.Append(System.Uri.EscapeDataString("count") + "=").Append(System.Uri.EscapeDataString(ConvertToString(count, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
            }
            if (next_id != null) {
                urlBuilder_.Append(System.Uri.EscapeDataString("next_id") + "=").Append(System.Uri.EscapeDataString(ConvertToString(next_id, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
            }
            if (fresh != null) {
                urlBuilder_.Append(System.Uri.EscapeDataString("fresh") + "=").Append(System.Uri.EscapeDataString(ConvertToString(fresh, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
            }
            if (from != null) {
                urlBuilder_.Append(System.Uri.EscapeDataString("from") + "=").Append(System.Uri.EscapeDataString(ConvertToString(from, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
            }
            if (to != null) {
                urlBuilder_.Append(System.Uri.EscapeDataString("to") + "=").Append(System.Uri.EscapeDataString(ConvertToString(to, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
            }

            foreach (var item in filters) {
                switch (item.Key) {
                    case nameof(TransactionItemDto.Step):
                        AppendUri(urlBuilder_, "step", item.Value);                        
                        break;

                    case nameof(TransactionItemDto.IpAddress):
                        AppendUri(urlBuilder_, "ip_address", item.Value);
                        break;

                    case nameof(TransactionItemDto.ScenarioID):
                        AppendUri(urlBuilder_, "scenario", item.Value);
                        break;

                    default:
                        AppendUri(urlBuilder_, item.Key, item.Value);
                        break;
                }
            }
            urlBuilder_.Length--;

            var client_ = _httpClient;
            var disposeClient_ = false;
            try {
                using (var request_ = new System.Net.Http.HttpRequestMessage()) {
                    request_.Method = new System.Net.Http.HttpMethod("GET");
                    request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/json"));

                    PrepareRequest(client_, request_, urlBuilder_);

                    var url_ = urlBuilder_.ToString();
                    request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                    PrepareRequest(client_, request_, url_);

                    var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                    var disposeResponse_ = true;
                    try {
                        var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                        if (response_.Content != null && response_.Content.Headers != null) {
                            foreach (var item_ in response_.Content.Headers)
                                headers_[item_.Key] = item_.Value;
                        }

                        ProcessResponse(client_, response_);

                        var status_ = (int)response_.StatusCode;
                        if (status_ == 200) {
                            var objectResponse_ = await ReadObjectResponseAsync<TransactionItemDtoItemsListPartial>(response_, headers_, cancellationToken).ConfigureAwait(false);
                            if (objectResponse_.Object == null) {
                                throw new ApiException("Response was null which was not expected.", status_, objectResponse_.Text, headers_, null);
                            }
                            return objectResponse_.Object;
                        } else
                        if (status_ == 400) {
                            var objectResponse_ = await ReadObjectResponseAsync<ProblemDetails>(response_, headers_, cancellationToken).ConfigureAwait(false);
                            if (objectResponse_.Object == null) {
                                throw new ApiException("Response was null which was not expected.", status_, objectResponse_.Text, headers_, null);
                            }
                            throw new ApiException<ProblemDetails>("Bad Request", status_, objectResponse_.Text, headers_, objectResponse_.Object, null);
                        } else {
                            var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                            throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                        }
                    } finally {
                        if (disposeResponse_)
                            response_.Dispose();
                    }
                }
            } finally {
                if (disposeClient_)
                    client_.Dispose();
            }
        }

        public async Task<Dto.DictionaryItemInfoModel> DictionaryItemUsageGetAsync(string dictionaryName, string id, 
            CancellationToken cancellationToken = default)
        {
            var urlBuilder_ = new System.Text.StringBuilder();
            urlBuilder_.Append($"dir/{dictionaryName}/{id}");

            var client_ = _httpClient;
            var disposeClient_ = false;
            try
            {
                using (var request_ = new System.Net.Http.HttpRequestMessage())
                {
                    request_.Method = new System.Net.Http.HttpMethod("GET");
                    request_.Headers.Accept.Add(System.Net.Http.Headers.MediaTypeWithQualityHeaderValue.Parse("application/json"));

                    PrepareRequest(client_, request_, urlBuilder_);

                    var url_ = urlBuilder_.ToString();
                    request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);

                    PrepareRequest(client_, request_, url_);

                    var response_ = await client_.SendAsync(request_, System.Net.Http.HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
                    var disposeResponse_ = true;
                    try
                    {
                        var headers_ = System.Linq.Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
                        if (response_.Content != null && response_.Content.Headers != null)
                        {
                            foreach (var item_ in response_.Content.Headers)
                                headers_[item_.Key] = item_.Value;
                        }

                        ProcessResponse(client_, response_);

                        var status_ = (int)response_.StatusCode;
                        if (status_ == 200)
                        {
                            var objectResponse_ = await ReadObjectResponseAsync<Dto.DictionaryItemInfoModel>(response_, headers_, cancellationToken).ConfigureAwait(false);
                            if (objectResponse_.Object == null)
                            {
                                throw new ApiException("Response was null which was not expected.", status_, objectResponse_.Text, headers_, null);
                            }
                            return objectResponse_.Object;
                        }
                        else
                        {
                            var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
                            throw new ApiException("The HTTP status code of the response was not expected (" + status_ + ").", status_, responseData_, headers_, null);
                        }
                    }
                    finally
                    {
                        if (disposeResponse_)
                            response_.Dispose();
                    }
                }
            }
            finally
            {
                if (disposeClient_)
                    client_.Dispose();
            }
        }

        private void AppendUri(StringBuilder urlBuilder_, string key, object value)
        {
            urlBuilder_.Append(System.Uri.EscapeDataString(key) + "=").Append(System.Uri.EscapeDataString(ConvertToString(value, System.Globalization.CultureInfo.InvariantCulture))).Append("&");
        }
    }
}
