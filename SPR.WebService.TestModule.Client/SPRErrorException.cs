﻿using SPR.WebService.TestModule.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.Client
{
    public class SPRErrorException : Exception
    {
        public ResponseError Error { get; }

        public SPRErrorException(ResponseError error) : base($"{error.Error}: {error.Descr}") {
            Error = error ?? throw new ArgumentNullException(nameof(error));
        }
    }
}
