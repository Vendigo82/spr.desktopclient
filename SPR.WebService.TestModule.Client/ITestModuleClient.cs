﻿using SPR.WebService.TestModule.DataContract;
using SPR.WebService.TestModule.DataContract.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.Client
{
    public interface ITestModuleClient : IDisposable
    {
        Task<TestFormulaResponse> TestFormula(TestMainFormulaRequest request, CancellationToken ct = default);

        Task<TestFormulaResponse> TestFormula(TestDraftFormulaRequest request, CancellationToken ct = default);

        /// <summary>
        /// Initial step of testing Rest formula with mock http client handler. Returns http request data without performing real http request
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<TestResponse<RestFormulaRequestInfoDto>> InitialTestRestFormulaAsync(TestDraftFormulaRequest request, CancellationToken ct = default);

        /// <summary>
        /// Final step of testing Rest formula with mock http client. Define which data should be returned by HttpClient
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<TestResponse<FormulaResultDto>> FinalTestRestFormulaAsync(FinalRestFormulaRequest request, CancellationToken ct = default);

        Task<TestResponse<ScenarioResultDto>> TestScenario(TestMainScenarioRequest request, CancellationToken ct = default);

        Task<TestResponse<ScenarioResultDto>> TestScenario(TestDraftScenarioRequest request, CancellationToken ct = default);

        /// <summary>
        /// Test expression
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<TestExpressionResponse> TestExpressionAsync(TestExpressionRequest request, CancellationToken ct = default);

    }
}
