﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.Client.EventArgs
{
    /// <summary>
    /// Provide event args for OnResponse event
    /// </summary>
    public class ResponseEventArgs
    {
        // <summary>
        /// Event arguments of this request
        /// </summary>

        public RequestEventArgs RequestArgs { get; set; }

        /// <summary>
        /// Http status code
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; internal set; }

        /// <summary>
        /// HTTP response body received from API
        /// </summary>
        public string ResponseData { get; internal set; }

    }
}
