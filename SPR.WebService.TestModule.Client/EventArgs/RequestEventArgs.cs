﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.Client.EventArgs
{
    /// <summary>
    /// Provides data for Http request event
    /// </summary>
    public class RequestEventArgs
    {
        /// <summary>
        /// Request url
        /// </summary>
        public string Url { get; internal set; }

        /// <summary>
        /// Request content
        /// </summary>
        public string RequestData { get; internal set; }

    }
}
