﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.Client.EventArgs
{
    /// <summary>
    /// Provide on exception
    /// </summary>
    public class ExceptionEventArgs
    {
        /// <summary>
        /// Event arguments for OnRequest event
        /// </summary>
        public RequestEventArgs RequestArgs { get; internal set; }

        /// <summary>
        /// Thrown exception
        /// </summary>
        public Exception Exception { get; internal set; }
    }
}
