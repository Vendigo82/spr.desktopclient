﻿using SPR.WebService.TestModule.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.Client.EventArgs
{
    public class SPRErrorEventArgs
    {
        /// <summary>
        /// Event arguments for OnRequest event
        /// </summary>
        public RequestEventArgs RequestArgs { get; internal set; }

        /// <summary>
        /// Thrown exception
        /// </summary>
        public ResponseError Error { get; internal set; }
    }
}
