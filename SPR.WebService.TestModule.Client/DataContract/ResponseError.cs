﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.DataContract
{
    public class ResponseError
    {
        public string Error { get; set; }
        public string Descr { get; set; }
    }
}
