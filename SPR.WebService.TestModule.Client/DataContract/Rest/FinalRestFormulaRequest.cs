﻿using Newtonsoft.Json;
using SPR.UI.WebService.DataContract.Formula;

namespace SPR.WebService.TestModule.DataContract.Rest
{
    public class FinalRestFormulaRequest
    {
        [JsonProperty(Required = Required.Always)]
        public HttpRequestResponseDto Response { get; set; }

        [JsonProperty(Required = Required.Always)]
        public FormulaModel Formula { get; set; }
    }
}
