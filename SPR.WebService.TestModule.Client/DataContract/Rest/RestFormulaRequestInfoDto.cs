﻿using Newtonsoft.Json;
using SPR.WebService.TestModule.DataContract.Converters;
using System;

namespace SPR.WebService.TestModule.DataContract
{
    public class RestFormulaRequestInfoDto
    {
        [JsonProperty(Required = Required.Always)]
        public string HttpVerb { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Url { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string Body { get; set; }

        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(TimeSpanConverter))]
        public TimeSpan Timeout { get; set; }
    }
}
