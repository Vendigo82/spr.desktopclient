﻿using Newtonsoft.Json;

namespace SPR.WebService.TestModule.DataContract
{
    public class HttpRequestResponseDto
    {
        [JsonProperty(Required = Required.Always)]
        public bool ResponseReceived { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string ResponseData { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int HttpStatusCode { get; set; }
    }
}
