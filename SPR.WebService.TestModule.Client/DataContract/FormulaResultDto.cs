﻿using Newtonsoft.Json.Linq;

namespace SPR.WebService.TestModule.DataContract
{
    public class FormulaResultDto
    {
        public JToken Result { get; set; }

        public string Type { get; set; }
    }
}
