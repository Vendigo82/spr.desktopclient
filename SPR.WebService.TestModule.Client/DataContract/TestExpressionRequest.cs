﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestExpressionRequest
    {
        [JsonProperty(Required = Required.Always)]
        public string Expression { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public JToken Input { get; set; }
    }
}
