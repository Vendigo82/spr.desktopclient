﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace SPR.WebService.TestModule.DataContract
{
    public class ResultRecordInfo
    {
        public long FormulaId { get; set; }

        public long? ParentId { get; set; }

        public JToken Value { get; set; }

        //[JsonConverter(typeof(StringEnumConverter))]
        public string Type { get; set; }

        /// <summary>
        /// Calculate time in milliseconds
        /// </summary>
        public int? Time { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public IEnumerable<ResultRecordInfo> Nested { get; set; }
    }
}
