﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SPR.WebService.TestModule.DataContract
{
    public class ValueInfo
    {
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public JToken Value { get; set; }

        //[JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(Required = Required.Always)]
        public string Type { get; set; }
    }
}
