﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestMainScenarioRequest : TestRequestBase
    {
        [JsonProperty(Required = Required.Always)]
        public long ScenarioId { get; set; }
    }
}
