﻿using Newtonsoft.Json;
using System;

namespace SPR.WebService.TestModule.DataContract.Converters
{
    /// <summary>
    /// TimeSpans are not serialized consistently depending on what properties are present. So this 
    /// serializer will ensure the format is maintained no matter what.
    /// </summary>
    public class TimeSpanConverter : JsonConverter<TimeSpan?>
    {
        /// <summary>
        /// Format: Days.Hours:Minutes:Seconds:Milliseconds
        /// </summary>
        public const string TimeSpanFormatString = @"c";

        public override void WriteJson(JsonWriter writer, TimeSpan? value, JsonSerializer serializer)
        {
            if (value == null)
                writer.WriteNull();
            else {
                var timespanFormatted = $"{value.Value.ToString(TimeSpanFormatString)}";
                writer.WriteValue(timespanFormatted);
            }
        }

        public override TimeSpan? ReadJson(JsonReader reader, Type objectType, TimeSpan? existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            TimeSpan parsedTimeSpan;
            if (reader.Value == null)
                return null;

            TimeSpan.TryParseExact((string)reader.Value, TimeSpanFormatString, null, out parsedTimeSpan);
            return parsedTimeSpan;
        }
    }
}
