﻿namespace SPR.WebService.TestModule.Client.DataContract.Response
{
    public class PostConditionIssueDto
    {
        public string Message { get; set; }

        public bool IsError { get; set; }
    }
}
