﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestExpressionResponse
    {
        [JsonProperty(Required = Required.Always)]
        public bool Success { get; set; }

        public JToken Result { get; set; }

        public string Type { get; set; }

        public string ErrorMessage { get; set; }

        public IEnumerable<ErrorInfoDto> IfAnyErrors { get; set; }
    }
}
