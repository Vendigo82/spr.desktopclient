﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestRequestBase
    {
        /// <summary>
        /// Request data
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public JObject Data { get; set; }
    }
}
