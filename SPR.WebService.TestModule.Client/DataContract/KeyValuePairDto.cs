﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SPR.WebService.TestModule.DataContract
{
    public class KeyValuePairDto
    {
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Type { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public JToken Value { get; set; }
    }
}
