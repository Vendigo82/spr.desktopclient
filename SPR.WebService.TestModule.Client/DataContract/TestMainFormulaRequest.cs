﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace SPR.WebService.TestModule.DataContract
{
    /// <summary>
    /// Test working version of formula
    /// </summary>
    public class TestMainFormulaRequest : TestFormulaRequestBase
    {
        /// <summary>
        /// Formula id
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public long FormulaId { get; set; }        
    }
}
