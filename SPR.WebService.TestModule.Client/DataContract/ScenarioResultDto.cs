﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.WebService.TestModule.DataContract
{
    /// <summary>
    /// Result of performing scenario 
    /// </summary>
    public class ScenarioResultDto
    {
        /// <summary>
        /// Scenario was passed or rejected. Always
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Passed { get; set; }

        /// <summary>
        /// Next step returned by scenario. Always
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string NextStep { get; set; }

        /// <summary>
        /// List of reject codes. Always
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<int> RejectCodes { get; set; }

        /// <summary>
        /// Scenario resulting values. Always
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<KeyValuePairDto> Values { get; set; }
    }
}
