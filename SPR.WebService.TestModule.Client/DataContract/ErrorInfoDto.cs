﻿using Newtonsoft.Json;

namespace SPR.WebService.TestModule.DataContract
{
    public class ErrorInfoDto
    {
        [JsonProperty(Required = Required.Always)]
        public long FormulaId { get; set; }

        public long? ParentFormulaId { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Description { get; set; }
    }
}
