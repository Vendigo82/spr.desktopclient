﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestFormulaResponse : TestResponse<FormulaResultDto>
    {
        public JToken Result { get; set; }

        public string Type { get; set; }
    }
}
