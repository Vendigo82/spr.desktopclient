﻿using Newtonsoft.Json;
using SPR.WebService.TestModule.Client.DataContract.Response;
using System;
using System.Collections.Generic;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestResponse
    {       
        [JsonProperty(Required = Required.Always)]
        public bool Success { get; set; }

        public TimeSpan CalculatedTime { get; set; }

        public IEnumerable<ResultRecordInfo> Results { get; set; }

        public IEnumerable<ValueInfo> Registry { get; set; }

        public ErrorInfoDto Error { get; set; }

        public IEnumerable<ErrorInfoDto> IfAnyErrors { get; set; }

        [JsonProperty(Required = Required.DisallowNull)]
        public IEnumerable<PostConditionIssueDto> PostConditions { get; set; }
    }

    public class TestResponse<TData> : TestResponse
        where TData : class
    {
        public TData Data { get; set; }
    }
}
