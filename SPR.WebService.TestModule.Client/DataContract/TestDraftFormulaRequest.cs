﻿using Newtonsoft.Json;
using SPR.UI.WebService.DataContract.Formula;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestDraftFormulaRequest : TestFormulaRequestBase
    {
        [JsonProperty(Required = Required.Always)]
        public FormulaModel Formula { get; set; }
    }
}
