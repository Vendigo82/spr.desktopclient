﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestFormulaRequestBase : TestRequestBase
    {
        public IEnumerable<ValueInfo> Registry { get; set; }

        public IEnumerable<ValueInfo> Results { get; set; }
    }
}
