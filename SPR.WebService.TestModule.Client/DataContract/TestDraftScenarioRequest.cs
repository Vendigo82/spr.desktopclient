﻿using Newtonsoft.Json;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.WebService.TestModule.DataContract
{
    public class TestDraftScenarioRequest : TestRequestBase
    {
        [JsonProperty(Required = Required.Always)]
        public ScenarioModel Scenario { get; set; }
    }
}
