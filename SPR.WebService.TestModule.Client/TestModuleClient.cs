﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

using SPR.WebService.TestModule.Client.EventArgs;
using SPR.WebService.TestModule.DataContract;
using SPR.WebService.TestModule.DataContract.Rest;

namespace SPR.WebService.TestModule.Client
{
    /// <summary>
    /// Client for SPR UI WebService
    /// </summary>
    public class TestModuleClient : ITestModuleClient, IDisposable
    {
        #region private fields
        private readonly HttpClient _httpClient;
        private readonly string _baseUrl;
        #endregion

        #region Create and destroy
        public TestModuleClient(string baseUrl) {
            _baseUrl = baseUrl.Trim();
            if (_baseUrl.Length > 0 && _baseUrl[_baseUrl.Length - 1] != '/')
                _baseUrl += "/";

            _httpClient = new HttpClient();
        }

        public void Dispose() {
            _httpClient.Dispose();
        }
        #endregion

        #region Events
        /// <summary>
        /// Call when making http request
        /// </summary>
        public event EventHandler<RequestEventArgs> OnRequest;

        /// <summary>
        /// Call when any network error occured
        /// </summary>
        public event EventHandler<ExceptionEventArgs> OnNetworkError;

        /// <summary>
        /// Call when any error occurs on deserializing response
        /// </summary>
        public event EventHandler<ExceptionEventArgs> OnInvalidResponse;

        /// <summary>
        /// Call when received response with error
        /// </summary>
        public event EventHandler<SPRErrorEventArgs> OnSPRError;

        /// <summary>
        /// Called when response was received
        /// </summary>
        public event EventHandler<ResponseEventArgs> OnResponse;
        #endregion

        #region Methods
        public Task<TestFormulaResponse> TestFormula(TestMainFormulaRequest request, CancellationToken ct = default)
            => MakeRequestAsync<TestMainFormulaRequest, TestFormulaResponse>("formula/main", request, ct);

        public Task<TestFormulaResponse> TestFormula(TestDraftFormulaRequest request, CancellationToken ct = default)
            => MakeRequestAsync<TestDraftFormulaRequest, TestFormulaResponse>("formula/draft", request, ct);

        public Task<TestResponse<RestFormulaRequestInfoDto>> InitialTestRestFormulaAsync(TestDraftFormulaRequest request, CancellationToken ct = default)
            => MakeRequestAsync<TestDraftFormulaRequest, TestResponse<RestFormulaRequestInfoDto>>("formula/initial_rest", request, ct);

        public Task<TestResponse<FormulaResultDto>> FinalTestRestFormulaAsync(FinalRestFormulaRequest request, CancellationToken ct = default)
            => MakeRequestAsync<FinalRestFormulaRequest, TestResponse<FormulaResultDto>>("formula/final_rest", request, ct);

        public Task<TestResponse<ScenarioResultDto>> TestScenario(TestMainScenarioRequest request, CancellationToken ct = default)
            => MakeRequestAsync<TestMainScenarioRequest, TestResponse<ScenarioResultDto>>("scenario/main", request, ct);

        public Task<TestResponse<ScenarioResultDto>> TestScenario(TestDraftScenarioRequest request, CancellationToken ct = default)
            => MakeRequestAsync<TestDraftScenarioRequest, TestResponse<ScenarioResultDto>>("scenario/draft", request, ct);

        public Task<TestExpressionResponse> TestExpressionAsync(TestExpressionRequest request, CancellationToken ct = default)
            => MakeRequestAsync<TestExpressionRequest, TestExpressionResponse>("expression", request, ct);
        #endregion

        #region generic request method        
        /// <summary>
        /// Make request
        /// </summary>
        /// <exception cref="ApiException">Throws on api error</exception>
        /// <exception cref="TimeoutException">Throws when timeout was expired</exception>
        public async Task<TResponse> MakeRequestAsync<TRequest, TResponse>(
            string methodUrl,
            TRequest request,
            CancellationToken cancellationToken) 
            where TRequest : class
            where TResponse : class 
        {
            string url = _baseUrl + methodUrl;
            string requestData = JsonConvert.SerializeObject(request);
            var content = new StringContent(requestData, Encoding.UTF8, "application/json");

            var reqArgs = new RequestEventArgs {
                Url = methodUrl,
                RequestData = requestData,
            };
            RaiseOnRequestEvent(reqArgs);

            HttpResponseMessage httpResponse;
            try {
                try {
                    httpResponse = await _httpClient.PostAsync(url, content, cancellationToken)
                        .ConfigureAwait(false);
                } catch (TaskCanceledException e) {
                    if (cancellationToken.IsCancellationRequested)
                        throw;

                    throw new TimeoutException("Request timed out", e);
                }
            } catch (Exception e) {
                RaiseOnNetworkErrorEvent(new ExceptionEventArgs() {
                    RequestArgs = reqArgs,
                    Exception = e
                });
                throw;
            }

            try {
                string responseJson = await httpResponse.Content.ReadAsStringAsync();

                RaiseOnResponseEvent(new ResponseEventArgs() {
                    RequestArgs = reqArgs,
                    ResponseData = responseJson,
                    HttpStatusCode = httpResponse.StatusCode
                });

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    return JsonConvert.DeserializeObject<TResponse>(responseJson);
                else {
                    try {
                        ResponseError error = JsonConvert.DeserializeObject<ResponseError>(responseJson);
                        if (error == null)
                            throw new HttpRequestException($"Http status code: {httpResponse.StatusCode}");
                        RaiseSPRErrorEvent(new SPRErrorEventArgs() { Error = error, RequestArgs = reqArgs });
                        throw new SPRErrorException(error);
                    } catch (JsonException) {
                        throw new HttpRequestException($"Http status code: {httpResponse.StatusCode}");
                    }
                }
            } catch (SPRErrorException) {
                throw;
            } catch (Exception e) {
                RaiseOnInvalidResponseEvent(new ExceptionEventArgs() {
                    RequestArgs = reqArgs,
                    Exception = e
                });
                throw;
            }            
        }
        #endregion

        #region Raise events methods
        private void RaiseOnRequestEvent(RequestEventArgs args) => SafeRiseEvent(OnRequest, args);

        private void RaiseOnNetworkErrorEvent(ExceptionEventArgs args) => SafeRiseEvent(OnNetworkError, args);

        private void RaiseOnInvalidResponseEvent(ExceptionEventArgs args) => SafeRiseEvent(OnInvalidResponse, args);

        private void RaiseSPRErrorEvent(SPRErrorEventArgs args) => SafeRiseEvent(OnSPRError, args);

        private void RaiseOnResponseEvent(ResponseEventArgs args) => SafeRiseEvent(OnResponse, args);

        private void SafeRiseEvent<T>(EventHandler<T> eh, T args) {
            try { eh?.Invoke(this, args); } catch { }
        }
        #endregion
    }
}
