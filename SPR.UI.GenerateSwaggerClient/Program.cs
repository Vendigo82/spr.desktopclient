﻿using NJsonSchema;
using NJsonSchema.CodeGeneration;
using NSwag;
using NSwag.CodeGeneration.CSharp;
using NSwag.CodeGeneration.OperationNameGenerators;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwaggerVerificationQueue
{

    class Program
    {
        static async Task Main(string[] args)
        {
            System.Net.WebClient wclient = new();

            var document = await OpenApiDocument.FromJsonAsync(wclient.DownloadString(args.Length > 0
                ? args[0]
                : "https://localhost:44349/swagger/v1/swagger.json"));

            wclient.Dispose();

            var settings = new CSharpClientGeneratorSettings {
                ClassName = "ApiClient",
                OperationNameGenerator = new DefaultOperationGenerator(),
                GenerateExceptionClasses = true,
                //ExceptionClass = "ApiException",
                GenerateClientClasses = true,
                GenerateBaseUrlProperty = false,
                DisposeHttpClient = false,
                GenerateClientInterfaces = true,
                GenerateOptionalParameters = true,
                GenerateUpdateJsonSerializerSettingsMethod = true,
                InjectHttpClient = true,
                UseBaseUrl = false,
                WrapResponses = false,
                ExposeJsonSerializerSettings = false,
                
                CSharpGeneratorSettings =
                {
                    Namespace = "SPR.UI.ApiClient",
                   // AnyType = "Newtonsoft.Json.Linq.JToken",
                    //DictionaryType = "System.Collections.Generic.Dictionary",
                    DateTimeType = "System.DateTime",
                    DateType = "System.DateTime",
                    PropertyNameGenerator = new CamelCaseNameGenerator(),
                    JsonSerializerSettingsTransformationMethod = null,
                    GenerateJsonMethods = false,
                    EnumNameGenerator = new DefaultEnumNameGenerator(),
                    GenerateDataAnnotations = false,                                                            
                },
                CodeGeneratorSettings = {                    
                    
                },
                UseRequestAndResponseSerializationSettings = false,
            };

            var generator = new CSharpClientGenerator(document, settings);
            var code = generator.GenerateFile();

            System.IO.File.WriteAllText("SwaggerClient.cs", code);
        }

        public class CamelCaseNameGenerator : IPropertyNameGenerator
        {
            public string Generate(JsonSchemaProperty property)
            {
                return property.Name.ToCamelCase();
            }
        }

        public class DefaultOperationGenerator : IOperationNameGenerator
        {
            public bool SupportsMultipleClients => false;

            public string GetClientName(OpenApiDocument document, string path, string httpMethod, OpenApiOperation operation)
            {
                //var pos = path.IndexOf('/');
                //if (pos != -1)
                //    return path[0..pos];// "Verification";
                //else
                //    return path;
                return "ApiClient";
            }

            public string GetOperationName(OpenApiDocument document, string path, string httpMethod, OpenApiOperation operation)
            {
                return string.Concat(path.Split('/').Select(i => i.Replace("{", "").Replace("}", "").ToCamelCase())) + httpMethod.ToCamelCase();
                //var index = path.LastIndexOf('/');
                //if (index == -1)
                    //return path.ToCamelCase() + httpMethod.ToCamelCase();
                //else
                  //  return path[(index + 1)..].Replace("{", "").Replace("}", "").Replace("Id", "").ToCamelCase() + httpMethod.ToCamelCase();
            }
        }
        public class DefaultEnumNameGenerator : IEnumNameGenerator
        {
            public string Generate(int index, string name, object value, JsonSchema schema)
            {
                return name.ToCamelCase();
            }
        }
    }

    public static class StringExtensions
    {
        public static string ToCamelCase(this string name)
        {
            var resultName = new string(name[0], 1).ToUpper() + name[1..];
            int index;
            while ((index = resultName.IndexOf('_')) != -1) {
                resultName = resultName[0..index] + new string(resultName[index + 1], 1).ToUpper() + resultName[(index + 2)..^0];
            }
            return resultName;
        }
    }
}
