﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.DAL.Enums
{
    /// <summary>
    /// Represent values from table [Arch].[JournalAction]
    /// </summary>
    public enum JournalAction : short
    {
        /// <summary>
        /// Release new version of formula or scenario
        /// </summary>
        Release = 1,

        /// <summary>
        /// Rollback to archive version
        /// </summary>
        Rollback = 2,

        /// <summary>
        /// Import new version
        /// </summary>
        Import = 3,

        /// <summary>
        /// Delete object
        /// </summary>
        Delete = 4,

        /// <summary>
        /// Creation new object
        /// </summary>
        New = 5,

        /// <summary>
        /// Dublicate formula or scenario
        /// </summary>
        Dublicate = 6,

        /// <summary>
        /// Enable scenario
        /// </summary>
        Enable = 7, 

        /// <summary>
        /// Disable scenario
        /// </summary>
        Disable = 8
    }
}
