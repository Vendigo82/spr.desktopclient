﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.DAL.Enums
{
    /// <summary>
    /// Represent values from table [Arch].[ObjectType]
    /// </summary>
    public enum SerializedObjectType : byte
    {
        Formula = 1,

        Scenario = 2
    }
}
