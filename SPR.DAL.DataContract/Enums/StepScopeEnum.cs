﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.DAL.Enums
{
    public enum StepScopeEnum
    {
        In = 1,

        Out = 2,

        InOut = 3
    }
}
