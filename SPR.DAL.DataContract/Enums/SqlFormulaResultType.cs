﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.DAL.Enums
{
    ///// <summary>
    ///// Sql formula result's type
    ///// Represent values from table [Dir].[SqlQueryResultType]
    ///// </summary>
    //public enum SqlFormulaResultType
    //{
    //    /// <summary>
    //    /// Result valus is scalar
    //    /// </summary>
    //    Scalar = 0,

    //    /// <summary>
    //    /// Result value is ICompexValue type
    //    /// </summary>
    //    Row = 1,

    //    /// <summary>
    //    /// Result value is IIndexableValue type, array of scalars
    //    /// </summary>
    //    List = 2,

    //    /// <summary>
    //    /// Result value is IIndexableValue type, array of ICompexValues
    //    /// </summary>
    //    Table = 3,

    //    /// <summary>
    //    /// ResultValue is Dictionary(IIndexableValue), where first column is key. If columns more then two, then dict value is ICompexValue
    //    /// </summary>
    //    Dict = 4,

    //    /// <summary>
    //    /// Perform Execute query. Returns count of rows was affected
    //    /// </summary>
    //    Execute = 5,
    //}
}
