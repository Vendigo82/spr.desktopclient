﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.DAL.Enums
{
    public static class FormulaTypeExtentions
    {
        /// <summary>
        /// Returns true if formula to type <paramref name="ft"/> stored in [Formula].[TextualBody] table
        /// </summary>
        /// <param name="ft"></param>
        /// <returns></returns>
        public static bool IsTextualBody(this Enums.FormulaType ft)
            => ft == Enums.FormulaType.Expression || ft == Enums.FormulaType.Script;
    }
}
