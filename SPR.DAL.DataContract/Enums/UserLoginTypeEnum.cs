﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.DAL.Enums
{
    /// <summary>
    /// Represent values in table [Acl].[UserLoginType]
    /// User login type
    /// </summary>
    public enum UserLoginTypeEnum
    {
        /// <summary>
        /// local spr user. Authentificate with login and password
        /// </summary>
        Local = 1,

        /// <summary>
        /// Windown domain user
        /// </summary>
        Domain = 2,
    }
}
