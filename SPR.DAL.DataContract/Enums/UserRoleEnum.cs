﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.DAL.Enums
{
    /// <summary>
    /// Represent values in table [Acl].[UserRole]
    /// </summary>
    public enum UserRoleEnum
    {
        SysAdmin = 1,

        PrjAdmin = 2,

        Master = 3,

        Apprentice = 4,

        Spectrator = 5
    }
}
