﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.DAL.Enums
{
    /// <summary>
    /// Moment of time on scenario performing progress
    /// Represent values from table [System].[CalculationTime]
    /// </summary>
    public enum CalculateTime : byte
    {
        /// <summary>
        /// Before calculating checkups
        /// </summary>
        Before = 0,

        /// <summary>
        /// After calculating checkups
        /// </summary>
        After = 1,

        /// <summary>
        /// Affter scenariod passed
        /// </summary>
        AfterSuccess = 2,

        /// <summary>
        /// Affter scenario declained
        /// </summary>
        AfterFail = 3
    }
}
