﻿
namespace SPR.DAL.Enums
{
    /// <summary>
    /// Represent values from [Formula].[Type] table
    /// </summary>
    public enum FormulaType
    {
        ScoreCard = 1,

        BunchFormula = 2,

        Script = 3,

        Expression = 4,

        /// <summary>
        /// Predictor model
        /// </summary>
        RESTService = 5,

        SqlQuery = 6,

        /// <summary>
        /// Google big query
        /// </summary>
        GoogleBQ = 7
    }
}
