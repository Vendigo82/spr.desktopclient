﻿using SPR.DocClient.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.DocClient
{
    public interface IDocHttpClient
    {
        Task<IEnumerable<GroupDto>> GetGroupsAsync(RequestModel request);

        Task<FunctionDto> GetFunctionAsync(RequestModel request, string functionSystemName);

        Task<IEnumerable<FunctionTitleDto>> GetFunctionsAsync(RequestModel request, string filterGroup = null);
    }
}