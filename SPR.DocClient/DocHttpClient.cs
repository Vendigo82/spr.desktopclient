﻿using Newtonsoft.Json;
using SPR.DocClient.Dto;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SPR.DocClient
{
    public class DocHttpClient : IDocHttpClient
    {
        private readonly HttpClient httpClient;

        public DocHttpClient(HttpClient httpClient)
        {
            this.httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<IEnumerable<GroupDto>> GetGroupsAsync(RequestModel request)
            => (await GetAsync<GroupsListDto>(request, $"groups")).Items;

        public async Task<IEnumerable<FunctionTitleDto>> GetFunctionsAsync(RequestModel request, string filterGroup = null)
        {
            var response = await GetAsync<FunctionsTitleListDto>(request, $"functions" + GetFunctionsQueryArguments(filterGroup));
            return response.Items;
        }

        private string GetFunctionsQueryArguments(string filterGroup)
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(filterGroup))
                sb.AppendFormat("groupFilter").Append("=").Append(Uri.EscapeDataString(filterGroup));

            if (sb.Length > 0)
                sb.Insert(0, "?");

            return sb.ToString();
        }

        public Task<FunctionDto> GetFunctionAsync(RequestModel request, string functionSystemName) 
            => GetAsync<FunctionDto>(request, $"functions/{functionSystemName}");

        private string GetUrlPrefix(RequestModel request) => $"{request.Language}";
        //private string GetUrlPrefix(RequestModel request) => $"/doc/{request.Language}/{request.Project}/{request.Version}";

        private async Task<TResponse> GetAsync<TResponse>(RequestModel request, string postfix) where TResponse : class
        {
            var httpRequest = new HttpRequestMessage(HttpMethod.Get, $"{GetUrlPrefix(request)}/{postfix}");
            var response = await httpClient.SendAsync(httpRequest);
            response.EnsureSuccessStatusCode();

            if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
                return null;

            var content = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<TResponse>(content);
            return result;
        }
    }
}
