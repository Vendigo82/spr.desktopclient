﻿using AutoMapper;
using SPR.DocClient.Dto;
using SPR.DocClient.Exceptions;
using SPR.DocClient.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.DocClient
{
    public class DocRepository : IDocRepository
    {
        //public interface IOptions
        //{
        //    //string Project { get; }

        //    //string Version { get; }

        //    string Language { get; }
        //}

        private readonly IDocHttpClient client;
       // private readonly IOptions options;
        private readonly IMapper mapper;

        public DocRepository(IDocHttpClient client, IMapper mapper)
        {
            //this.options = options ?? throw new ArgumentNullException(nameof(options));
            this.client = client ?? throw new ArgumentNullException(nameof(client));
            this.mapper = mapper;
        }

        public async Task<GroupInfoList> GetGroupsAsync()
        {
            var response = await client.GetGroupsAsync(GetRequestModel());
            var items = response.Select(i => new GroupInfo(i.Group, i.Title)).ToArray();
            return new GroupInfoList(items);
        }

        public async Task<FunctionsBriefInfoList> GetFunctionsAsync(FunctionsFilter filter = null)
        {
            var response = await client.GetFunctionsAsync(GetRequestModel(), filterGroup: filter?.Group);
            var items = response.Select(i => new FunctionBriefInfo(i.SystemName, i.Title)).ToArray();
            return new FunctionsBriefInfoList(items);
        }

        public async Task<FunctionInfo> GetFunctionAsync(FunctionName name)
        {
            var response = await client.GetFunctionAsync(GetRequestModel(), name)
                ?? throw new FunctionDocumentationNotFoundException(name);
            var result = mapper.Map<FunctionInfo>(response);
            return result;
        }

        private RequestModel GetRequestModel() => new RequestModel("ru");//, options.Project, options.Version);
    }
}
