﻿
namespace SPR.DocClient.Models
{
    public class GroupName : ValueType<string>
    {
        public GroupName(string value) : base(value)
        {
        }

        public static readonly GroupName Empty = new GroupName(string.Empty);
    }
}
