﻿using System;
using System.Collections.Generic;

namespace SPR.DocClient.Models
{
    public class GroupInfoList
    {
        public GroupInfoList(IEnumerable<GroupInfo> items)
        {
            Items = items ?? throw new ArgumentNullException(nameof(items));
        }

        public IEnumerable<GroupInfo> Items { get; }
    }
}
