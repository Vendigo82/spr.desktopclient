﻿using System.Collections.Generic;

namespace SPR.DocClient.Models
{
    public class FunctionInfo : FunctionBriefInfo
    {
        public FunctionInfo(FunctionName name, Title title) : base(name, title)
        {
        }

        public IEnumerable<FunctionImplementationInfo> Implementations { get; set; }
    }

    public class FunctionImplementationInfo
    {
        public string Description { get; set; }

        public IEnumerable<ArgumentInfo> Args { get; set; }

        public ReturnValueInfo Return { get; set; }
    }

    public class ReturnValueInfo
    {
        public string Type { get; set; }
    }

    public class ArgumentInfo
    {
        public string Type { get; set; }

        public string Name { get; set; }
    }
}
