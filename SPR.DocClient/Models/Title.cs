﻿using System;

namespace SPR.DocClient.Models
{
    public class Title
    {
        private readonly string title;

        public Title(string title)
        {
            this.title = title ?? throw new ArgumentNullException(nameof(title));
        }

        public static implicit operator string(Title t) => t.title;
        public static explicit operator Title(string b) => new Title(b);

        public override bool Equals(object obj)
        {
            if (obj is Title t)
                return t.title == this.title;
            else
                return false;
        }

        public override int GetHashCode() => title.GetHashCode();

        public override string ToString() => title;
    }
}
