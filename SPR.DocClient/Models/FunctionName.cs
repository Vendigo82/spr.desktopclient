﻿using System;

namespace SPR.DocClient.Models
{
    public class FunctionName
    {
        private readonly string functionName;

        public FunctionName(string functionName)
        {
            this.functionName = functionName ?? throw new ArgumentNullException(nameof(functionName));
        }

        public static implicit operator string(FunctionName t) => t != null ? t.functionName : default;
        public static explicit operator FunctionName(string b) => new FunctionName(b);

        public override string ToString() => functionName;

        public override bool Equals(object obj)
        {
            if (obj is FunctionName f)
                return f.functionName == functionName;
            else
                return false;
        }

        public override int GetHashCode() => functionName.GetHashCode();

        public static readonly FunctionName Empty = new FunctionName(string.Empty);
    }
}
