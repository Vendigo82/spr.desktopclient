﻿using System;
using System.Collections.Generic;

namespace SPR.DocClient.Models
{
    public class FunctionsBriefInfoList
    {
        public FunctionsBriefInfoList(IEnumerable<FunctionBriefInfo> items)
        {
            Items = items ?? throw new ArgumentNullException(nameof(items));
        }

        public IEnumerable<FunctionBriefInfo> Items { get; }
    }
}
