﻿using System;

namespace SPR.DocClient.Models
{
    public class ValueType<T> 
    {
        private readonly T value;

        public ValueType(T value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            this.value = value;
        }

        public static implicit operator T(ValueType<T> t) => t != null ? t.value : default;
        //public static explicit operator ValueType<T>(T b) => new ValueType<T>(b);

        public override string ToString() => value.ToString();

        public override bool Equals(object obj)
        {
            if (obj is ValueType<T> f)
                return f.value.Equals(value);
            else
                return false;
        }

        public override int GetHashCode() => value.GetHashCode();
    }
}
