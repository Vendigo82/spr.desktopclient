﻿using System;

namespace SPR.DocClient.Models
{
    public class GroupInfo
    {
        public GroupInfo(GroupName name, Title title)
        {
            Group = name ?? throw new ArgumentNullException(nameof(name));
            Title = title ?? throw new ArgumentNullException(nameof(title));
        }

        internal GroupInfo(string systemName, string title)
        {
            Group = new GroupName(systemName);
            Title = new Title(title);
        }

        public GroupName Group { get; }

        public Title Title { get; }
    }
}
