﻿
using System;

namespace SPR.DocClient.Models
{
    public class FunctionBriefInfo
    {
        public FunctionBriefInfo(FunctionName name, Title title)
        {
            SystemName = name ?? throw new ArgumentNullException(nameof(name));
            Title = title ?? throw new ArgumentNullException(nameof(title));
        }

        internal FunctionBriefInfo(string systemName, string title)
        {
            SystemName = new FunctionName(systemName);
            Title = new Title(title);
        }

        public FunctionName SystemName { get; }

        public Title Title { get; }
    }
}
