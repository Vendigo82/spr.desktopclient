﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SPR.DocClient.Dto
{
    public class GroupsListDto
    {
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<GroupDto> Items { get; set; }
    }
}
