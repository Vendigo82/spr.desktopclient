﻿using Newtonsoft.Json;

namespace SPR.DocClient.Dto
{
    public class GroupDto
    {
        [JsonProperty(Required = Required.Always)]
        public string Group { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Title { get; set; }
    }
}
