﻿using System;

namespace SPR.DocClient.Dto
{
    public struct RequestModel
    {
        public RequestModel(string language)
        {
            Language = language ?? throw new ArgumentNullException(nameof(language));
            //Project = project ?? throw new ArgumentNullException(nameof(project));
            //Version = version ?? throw new ArgumentNullException(nameof(version));
        }

        public string Language { get; }

        //public string Project { get; }

        //public string Version { get; }
    }
}
