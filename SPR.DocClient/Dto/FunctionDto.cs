﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SPR.DocClient.Dto
{
    public class FunctionDto : FunctionTitleDto
    {
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<FunctionImplementationDto> Implementations { get; set; }
    }

    public class FunctionImplementationDto
    {
        [JsonProperty(Required = Required.Always)]
        public string Description { get; set; }

        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ArgumentDto> Args { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public ReturnValueDto Return { get; set; }
    }

    public class ReturnValueDto
    {
        [JsonProperty(Required = Required.Always)]
        public string Type { get; set; }
    }

    public class ArgumentDto
    {
        [JsonProperty(Required = Required.Always)]
        public string Type { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }
    }

}
