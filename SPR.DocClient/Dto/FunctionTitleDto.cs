﻿using Newtonsoft.Json;

namespace SPR.DocClient.Dto
{
    public class FunctionTitleDto
    {
        [JsonProperty(Required = Required.Always)]
        public string SystemName { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Title { get; set; }
    }
}
