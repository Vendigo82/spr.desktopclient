﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SPR.DocClient.Dto
{
    public class FunctionsTitleListDto
    {
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<FunctionTitleDto> Items { get; set; }
    }
}
