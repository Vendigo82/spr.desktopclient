﻿using SPR.DocClient.Models;
using System.Threading.Tasks;

namespace SPR.DocClient
{
    public interface IDocRepository
    {
        Task<GroupInfoList> GetGroupsAsync();

        Task<FunctionInfo> GetFunctionAsync(FunctionName name);

        Task<FunctionsBriefInfoList> GetFunctionsAsync(FunctionsFilter filter = null);
    }
}