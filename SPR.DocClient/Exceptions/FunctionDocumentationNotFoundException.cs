﻿using SPR.DocClient.Models;
using System;

namespace SPR.DocClient.Exceptions
{
    public class FunctionDocumentationNotFoundException : Exception
    {
        public FunctionDocumentationNotFoundException(FunctionName name) : base($"Documentation for function {name} does not found")
        {
        }
    }
}
