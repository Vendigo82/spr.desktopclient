﻿using AutoMapper;

namespace SPR.DocClient.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Dto.ReturnValueDto, Models.ReturnValueInfo>();
            CreateMap<Dto.ArgumentDto, Models.ArgumentInfo>();
            CreateMap<Dto.FunctionImplementationDto, Models.FunctionImplementationInfo>();
            CreateMap<Dto.FunctionDto, Models.FunctionInfo>()
                .ConstructUsing(c => new Models.FunctionInfo((Models.FunctionName)c.SystemName, (Models.Title)c.Title));
        }
    }
}
