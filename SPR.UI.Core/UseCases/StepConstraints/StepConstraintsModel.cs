﻿using System.Collections.Generic;

namespace SPR.UI.Core.UseCases.StepConstraints
{
    public class StepConstraintsModel
    {
        public StepConstraintsHeaderModel Header { get; set; }

        public IEnumerable<StepConstraintItemModel> Items { get; set; }
    }
}
