﻿using System;

namespace SPR.UI.Core.UseCases.StepConstraints
{
    public class StepConstraintItemModel
    {
        public Guid Id { get; set; }

        public bool Error { get; set; }

        public string Expression { get; set; }

        public string Message { get; set; }
    }
}
