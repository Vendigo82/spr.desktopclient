﻿using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.Core.UseCases.StepConstraints
{
    public interface IStepConstraintsRepository
    {
        Task<StepConstraintsModel> LoadLatestsAsync(string stepName, StepConstraintType type, CancellationToken ct = default);
        Task SaveAsync(string stepName, StepConstraintType type, StepConstraintsModel model, CancellationToken ct = default);
    }
}
