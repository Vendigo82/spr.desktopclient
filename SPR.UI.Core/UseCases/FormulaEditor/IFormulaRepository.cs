﻿using SPR.UI.Core.Shared;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.Core.UseCases.FormulaEditor
{
    /// <summary>
    /// Provide access to formulas
    /// </summary>
    public interface IFormulaRepository : IDataSource<FormulaModel>
    {
        Task<FormulaUsageResponse> GetUsage(Guid id, CancellationToken ct = default);

        Task<IEnumerable<FormulaBriefModel>> GetRegisterNameUsage(string registerName, CancellationToken ct = default);
    }
}
