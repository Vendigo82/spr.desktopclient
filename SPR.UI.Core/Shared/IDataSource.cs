﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SPR.UI.WebService.DataContract;

namespace SPR.UI.Core.Shared
{
    public interface IDataSource<TItem> : IDataVersionsSource
    {
        /// <summary>
        /// Returns working version of item
        /// Returns null item if working version does not exist
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<ObjectContainer<TItem>> GetMain(Guid id, CancellationToken ct);

        /// <summary>
        /// Save working version
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        Task<ObjectContainer<TItem>> Save(TItem item, string comment, CancellationToken ct);

        /// <summary>
        /// Get archive version
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ver"></param>
        /// <returns></returns>
        Task<TItem> GetArchiveVersionAsync(Guid id, int ver, CancellationToken ct);

        /// <summary>
        /// Restore specific version from archive
        /// </summary>
        /// <param name="id">Object's id</param>
        /// <param name="version">Version for restore</param>
        /// <param name="comment">Optional comment</param>
        /// <param name="ct"></param>
        /// <returns>Working version of formula after restoring</returns>
        Task<TItem> RestoreAsync(Guid id, int version, string comment, CancellationToken ct);
    }
}
