﻿using SPR.UI.WebService.DataContract;
using SPR.UI.WebService.DataContract.Common;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.UI.Core.Shared
{
    public interface IDataVersionsSource
    {
        /// <summary>
        /// List of archive versions numbers
        /// </summary>
        Task<ItemsList<VersionInfo>> GetArchiveVersionsListAsync(Guid id, CancellationToken ct);
    }
}
