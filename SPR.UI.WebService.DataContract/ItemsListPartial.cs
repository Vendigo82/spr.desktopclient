﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract
{
    /// <summary>
    /// Items list with partial data information
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ItemsListPartial<T> : ItemsOrderedList<T>
    {
        /// <summary>
        /// There a now other records
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Exhausted { get; set; }

        /// <summary>
        /// Request next part's id
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public long? NextRequestId { get; set; }

        /// <summary>
        /// Maximum count of records by request
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public int MaxCount { get; set; }
    }
}
