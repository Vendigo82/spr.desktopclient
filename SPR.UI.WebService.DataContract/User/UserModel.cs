﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace SPR.UI.WebService.DataContract.User
{
    /// <summary>
    /// User's info
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class UserModel
    {        
        public Guid Id { get; set; }

        public string Login { get; set; }

        public string Name { get; set; }
    }
}
