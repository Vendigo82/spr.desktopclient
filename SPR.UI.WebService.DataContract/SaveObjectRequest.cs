﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SaveObjectRequest<T> : ObjectContainer<T>
    {
        public string Comment { get; set; }
    }
}
