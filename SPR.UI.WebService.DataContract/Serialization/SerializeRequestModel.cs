﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Serialization
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SerializeRequestModel
    {
        [JsonProperty(Required = Required.Default)]
        public IEnumerable<Guid> Formulas { get; set; }

        [JsonProperty(Required = Required.Default)]
        public IEnumerable<Guid> Scenarios { get; set; }
    }
}
