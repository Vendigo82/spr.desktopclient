﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Serialization
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ObjectExistanceModel<T> : ItemExistanceModel<T>
    {
        [JsonProperty(Required = Required.AllowNull)]
        public T Existed { get; set; }
    }
}
