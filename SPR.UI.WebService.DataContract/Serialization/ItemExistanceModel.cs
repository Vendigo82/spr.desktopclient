﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Serialization
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ItemExistanceModel<T>
    {
        [JsonProperty(Required = Required.Always)]
        public T Item { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool Exists { get; set; }
    }
}
