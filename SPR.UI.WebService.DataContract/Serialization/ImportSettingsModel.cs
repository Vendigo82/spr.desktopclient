﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.ComponentModel;

namespace SPR.UI.WebService.DataContract.Serialization
{
    [JsonConverter(typeof(StringEnumConverter), typeof(SnakeCaseNamingStrategy))]
    public enum ImportUpdateKind
    {
        /// <summary>
        /// don't update already existed object
        /// </summary>
        Skip,

        /// <summary>
        /// Update already existed object
        /// </summary>
        Rewrite,

        /// <summary>
        /// Create dublicate for already existed object
        /// </summary>
        Dublicate
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ImportSettingsModel
    {      
        /// <summary>
        /// Update already existed SqlServer or Server objects
        /// If false update for this type of objects will skip
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        public bool RewriteServers { get; set; } = false;

        /// <summary>
        /// Update already existed sql servers
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        public bool RewriteSqlServers { get; set; } = false;

        /// <summary>
        /// Update already existed google bq connections
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        public bool RewriteGoogleBqConnections { get; set; } = false;

        /// <summary>
        /// Update already existed step objects
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        [DefaultValue(true)]
        public bool RewriteSteps { get; set; } = true;

        /// <summary>
        /// update already existed reject reasons
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        [DefaultValue(true)]
        public bool RewriteRejectReasons { get; set; } = true;

        /// <summary>
        /// update already existed standard results
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        [DefaultValue(true)]
        public bool RewriteStdResults { get; set; } = true;

        /// <summary>
        /// Update already
        /// </summary>
        [JsonProperty(PropertyName = "update_nested_formulas", Required = Required.DisallowNull)]
        [DefaultValue(ImportUpdateKind.Rewrite)]
        public ImportUpdateKind UpdateNestedFormulasKind { get; set; } = ImportUpdateKind.Rewrite;

        /// <summary>
        /// Create dublicate or update main objects in DataContainer
        /// </summary>
        [JsonProperty(PropertyName = "create_dublicates", Required = Required.DisallowNull)]
        public bool CreateDublicateForMainObjects { get; set; } = false;
    }
}
