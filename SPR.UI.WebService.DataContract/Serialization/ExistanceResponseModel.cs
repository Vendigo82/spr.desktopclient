﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Serialization
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ExistanceResponseModel
    {
        /// <summary>
        /// list of existed reject reasons
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ItemExistanceModel<int>> RejectReasons { get; set; }

        /// <summary>
        /// list of existed steps
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ItemExistanceModel<string>> Steps { get; set; }

        /// <summary>
        /// list of existed servers
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ItemExistanceModel<string>> Servers { get; set; }

        /// <summary>
        /// list of existed sql servers
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ItemExistanceModel<string>> SqlServers { get; set; }

        /// <summary>
        /// list of existed google bq connections
        /// </summary>
        [JsonProperty(propertyName: "google_bq_connections", Required = Required.Always)]
        public IEnumerable<ItemExistanceModel<string>> GoogleBQConnections { get; set; }

        /// <summary>
        /// list of existed standard results
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ObjectExistanceModel<StdResultBriefModel>> StdResults { get; set; }

        /// <summary>
        /// list of existed formulas
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ObjectExistanceModel<FormulaBriefModel>> Formulas { get; set; }

        /// <summary>
        /// list of existed scenarious
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ObjectExistanceModel<ScenarioSlimModel>> Scenarios { get; set; }

        [JsonProperty(Required = Required.Always)]
        public IEnumerable<Guid> FormulasIds { get; set; }
    }
}
