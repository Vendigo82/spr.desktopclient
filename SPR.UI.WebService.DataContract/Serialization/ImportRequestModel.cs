﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Serialization
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ImportRequestModel
    {
        [JsonProperty(Required = Required.Always)]
        public string Data { get; set; }

        [JsonProperty(Required = Required.Always)]
        public ImportSettingsModel Settings { get; set; }

        [JsonProperty(Required = Required.Default)]
        public string Comment { get; set; }
    }
}
