﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace SPR.UI.WebService.DataContract.Serialization
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ImportDictionaryItemResultModel<TKey>
    {
        [JsonProperty(Required = Required.Always)]
        public TKey Key { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool NewCreated { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ImportObjectResultModel
    {
        [JsonProperty(Required = Required.Always)]
        public long Code { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool NewCreated { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Guid OriginGuid { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Guid ImportedGuid { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ImportResultModel
    {
        /// <summary>
        /// list of imported reject reasons
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ImportDictionaryItemResultModel<int>> RejectReasons { get; set; }

        /// <summary>
        /// list of imported steps
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ImportDictionaryItemResultModel<string>> Steps { get; set; }

        /// <summary>
        /// list of imported servers
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ImportDictionaryItemResultModel<string>> Servers { get; set; }

        /// <summary>
        /// list of imported sql servers
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ImportDictionaryItemResultModel<string>> SqlServers { get; set; }

        /// <summary>
        /// list of imported google bq connections
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ImportDictionaryItemResultModel<string>> GoogleBQConnections { get; set; }

        /// <summary>
        /// List of imported standard results
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ImportDictionaryItemResultModel<Guid>> StdResults { get; set; }

        /// <summary>
        /// list of imported formulas
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ImportObjectResultModel> Formulas { get; set; }

        /// <summary>
        /// list of imported scenarious
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ImportObjectResultModel> Scenarios { get; set; }
    }
}
