﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract
{
    public class QueryParams
    {
        public const string UriDateTimeFormat = "yyyy-MM-ddTHH:mm:ss";
        public const string UriDateFormat = "yyyy-MM-dd";

        #region pagination and date
        public const string NextId = "next_id";

        public const string Fresh = "fresh";

        public const string From = "from";

        public const string To = "to";

        public const string Count = "count";
        #endregion

        #region journal records
        public const string SerializedObjectType = "type";

        public const string Guid = "guid";
        #endregion

        #region transactions
        /// <summary>
        /// With errors
        /// </summary>
        public const string WithError = "with_errors";

        /// <summary>
        /// Input step
        /// </summary>
        public const string Step = "step";

        /// <summary>
        /// Request's ip-address
        /// </summary>
        public const string IpAddress = "ip_address";

        /// <summary>
        /// Scenario code
        /// </summary>
        public const string ScenarioCode = "scenario";
        #endregion

        #region formula and scenario
        public const string Name = "name";
        public const string Type = "type";
        public const string Code = "code";
        public const string Checkup = "checkup";
        public const string NotUsed = "notused";
        #endregion
    }
}
