﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

using SPR.DAL.Enums;

namespace SPR.UI.WebService.DataContract.Profile
{
    /// <summary>
    /// Information about user's profile
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class Profile
    {
        /// <summary>
        /// user's name. Can be null
        /// </summary>
        public string Name { get; set; }

        ///// <summary>
        ///// User's login type
        ///// </summary>
        //[JsonProperty(Required = Required.Always)]
        //[JsonConverter(typeof(StringEnumConverter), typeof(SnakeCaseNamingStrategy))]
        //public UserLoginTypeEnum LoginType { get; set; }

        /// <summary>
        /// User's login
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Login { get; set; }

        ///// <summary>
        ///// Project which user belongs to
        ///// </summary>
        //public string Project { get; set; }

        ///// <summary>
        ///// Current selected project
        ///// </summary>
        //public string CurrentProject { get; set; }

        ///// <summary>
        ///// User's roles
        ///// </summary>
        //[JsonProperty(Required = Required.Always)]
        //public IEnumerable<Role> Roles { get; set; }
    }
}
