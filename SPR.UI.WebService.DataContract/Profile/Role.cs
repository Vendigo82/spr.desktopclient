﻿//using System;
//using System.Collections.Generic;
//using System.Text;

//using Newtonsoft.Json;
//using Newtonsoft.Json.Serialization;
//using Newtonsoft.Json.Converters;

//using SPR.DAL.Enums;

//namespace SPR.UI.WebService.DataContract.Profile
//{
//    /// <summary>
//    /// Information about user's roles
//    /// </summary>
//    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
//    public class Role
//    {
//        /// <summary>
//        /// User's role
//        /// </summary>
//        [JsonProperty(PropertyName = "role")]
//        [JsonConverter(typeof(StringEnumConverter), typeof(SnakeCaseNamingStrategy))]
//        public UserRoleEnum UserRole { get; set; }

//        /// <summary>
//        /// Project name
//        /// </summary>
//        public string Project { get; set; }
//    }
//}
