﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace SPR.UI.WebService.DataContract.Profile
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ProfileEditRequest
    {
        [JsonProperty(Required = Required.AllowNull)]
        public string Name { get; set; }
    }
}
