﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace SPR.UI.WebService.DataContract.Profile
{
    /// <summary>
    /// Change current user's password request
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ChangePasswordRequest
    {
        /// <summary>
        /// Old user password
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string OldPassword { get; set; }

        /// <summary>
        /// New user password
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string NewPassword { get; set; }
    }
}
