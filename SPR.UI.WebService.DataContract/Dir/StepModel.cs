﻿using System.ComponentModel;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using SPR.DAL.Enums;

namespace SPR.UI.WebService.DataContract.Dir
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class StepModel : UsedBaseModel
    {
        [JsonProperty(Required = Required.Always)]
        public int Id { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        public string Descr { get; set ; }

        public int Order { get; set; }

        /// <summary>
        /// Step visibility scope
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        [DefaultValue(StepScopeEnum.InOut)]
        [JsonConverter(typeof(StringEnumConverter))]
        public StepScopeEnum Scope { get; set; }

    }
}
