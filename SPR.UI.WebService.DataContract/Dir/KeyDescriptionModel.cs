﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Dir
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class KeyDescriptionModel
    {
        [JsonProperty(Required = Required.Always)]
        public string Key { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Descr { get; set; }
    }
}
