﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Dir
{
    /// <summary>
    /// Data type for bunch update operation request
    /// </summary>
    /// <typeparam name="T">Type of entity recod</typeparam>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class BunchUpdateRequest<T> : ItemsList<BunchUpdateItemModel<T>> where T : class
    {
    }
}
