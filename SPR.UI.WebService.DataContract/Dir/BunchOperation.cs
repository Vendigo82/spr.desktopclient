﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Dir
{
    /// <summary>
    /// Kind of bunch operation
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter), typeof(SnakeCaseNamingStrategy))]
    public enum BunchOperation
    {
        Insert,

        Update,

        Delete
    }
}
