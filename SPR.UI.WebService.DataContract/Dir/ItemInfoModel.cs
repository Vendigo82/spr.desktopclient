﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Dir
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ItemInfoModel<T> where T : class
    {
        [JsonProperty(Required = Required.Always)]
        public T Item { get; set; }

        [JsonProperty(Required = Required.Always)]
        public ItemUsageModel Usage { get; set; }
    }
}
