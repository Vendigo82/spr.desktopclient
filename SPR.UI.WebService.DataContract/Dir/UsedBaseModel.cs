﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Dir
{
    /// <summary>
    /// Item with Used field
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class UsedBaseModel
    {
        /// <summary>
        /// Used or not
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public bool Used { get; set; }
    }
}
