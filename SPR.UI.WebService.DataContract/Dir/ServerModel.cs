﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SPR.UI.WebService.DataContract.Converters;

namespace SPR.UI.WebService.DataContract.Dir
{
    /// <summary>
    /// Rest server dictionary item (table [Dir].[Server])
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ServerModel : UsedBaseModel
    {
        [JsonProperty(Required = Required.Always)]
        public int Id { get; set; }

        /// <summary>
        /// Server's unique name, can't be null
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        /// <summary>
        /// Server's url, can't be null
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Url { get; set; }

        /// <summary>
        /// Server's timeout. Can be null
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        [JsonConverter(typeof(TimeSpanConverter))]
        public TimeSpan? Timeout { get; set; }
    }
}
