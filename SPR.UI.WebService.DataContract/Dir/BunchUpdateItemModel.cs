﻿using System.Runtime.Serialization;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Dir
{
    /// <summary>
    /// Data type for item of bunch update operation request
    /// </summary>
    /// <typeparam name="T">Type of entity recod</typeparam>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class BunchUpdateItemModel<T> where T : class
    {
        /// <summary>
        /// Kind of bunch operation, required
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public BunchOperation Operation { get; set; }

        /// <summary>
        /// key of record, require for <see cref="BunchOperation.Delete"/> and <see cref="BunchOperation.Update"/> operations
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public int Id { get; set; }

        /// <summary>
        /// Item, require for <see cref="BunchOperation.Insert"/> and <see cref="BunchOperation.Update"/> operations
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public T Item { get; set; }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context) {
            if (Operation == BunchOperation.Insert && Item == null)
                throw new JsonReaderException("item can't null for insert operation");
            if (Operation == BunchOperation.Update && Item == null)
                throw new JsonReaderException("item can't null for update operation");
        }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class BunchUpdateItemModel<TKey, T> where T : class
    {
        /// <summary>
        /// Kind of bunch operation, required
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public BunchOperation Operation { get; set; }

        /// <summary>
        /// key of record, require for <see cref="BunchOperation.Delete"/> and <see cref="BunchOperation.Update"/> operations
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public TKey Id { get; set; }

        /// <summary>
        /// Item, require for <see cref="BunchOperation.Insert"/> and <see cref="BunchOperation.Update"/> operations
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public T Item { get; set; }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            if (Operation == BunchOperation.Insert && Item == null)
                throw new JsonReaderException("item can't null for insert operation");
            if (Operation == BunchOperation.Update && Item == null)
                throw new JsonReaderException("item can't null for update operation");
        }
    }
}
