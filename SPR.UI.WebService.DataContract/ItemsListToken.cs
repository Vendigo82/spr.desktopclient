﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract
{

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ItemsListToken<T> : ItemsList<T>
    {
        /// <summary>
        /// Token for requesting next part of data
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string NextToken { get; set; }
    }
}
