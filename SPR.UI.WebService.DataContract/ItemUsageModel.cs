﻿using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using SPR.UI.WebService.DataContract.Formula;
using SPR.UI.WebService.DataContract.Scenario;

namespace SPR.UI.WebService.DataContract
{
    /// <summary>
    /// Information about item's usage
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ItemUsageModel
    {
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<FormulaBriefModel> Formulas { get; set; }

        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ScenarioSlimModel> Scenarios { get; set; }
    }
}
