﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService
{
    [JsonConverter(typeof(StringEnumConverter), typeof(SnakeCaseNamingStrategy))]
    public enum ErrorKind
    {
        /// <summary>
        /// Can't verify password or login, etc
        /// </summary>
        InvalidClient,

        /// <summary>
        /// Unauthorized access
        /// </summary>
        Unauthorized,

        /// <summary>
        /// Incorrect input data
        /// </summary>
        InputDataError,

        /// <summary>
        /// On change password operation. New password is not good enough
        /// </summary>
        BadNewPassword,

        /// <summary>
        /// Can't perform this operation
        /// </summary>
        InvalidOperation,

        /// <summary>
        /// Operation not support
        /// </summary>
        Unsupported,

        /// <summary>
        /// Some object has not found
        /// </summary>
        NotFound,

        /// <summary>
        /// Server's error
        /// </summary>
        InternalServerError,

        /// <summary>
        /// Item can't be deleted
        /// </summary>
        CanNotDelete,
    }
}
