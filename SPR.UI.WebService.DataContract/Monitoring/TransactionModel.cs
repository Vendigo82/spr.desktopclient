﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace SPR.UI.WebService.DataContract.Monitoring
{
    public class TransactionModel
    {
        [JsonProperty(PropertyName = "ID", Required = Required.Always)]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "Timestamp", Required = Required.Always)]
        public DateTime Timestamp { get; set; }

        [JsonProperty(PropertyName = "ip_address")]
        public string IpAddress { get; set; }

        [JsonProperty(PropertyName = "step")]
        public string Step { get; set; }

        [JsonProperty(PropertyName = "ScenarioID")]
        public long? ScenarioId { get; set; }

        [JsonProperty(PropertyName = "ScenarioVer")]
        public int? ScenarioVer { get; set; }

        [JsonProperty(PropertyName = "step_out")]
        public string StepOut { get; set; }

        [JsonProperty(PropertyName = "Finished")]
        public bool? Finished { get; set; }

        [JsonProperty(PropertyName = "Error")]
        public bool? Error { get; set; }

        [JsonProperty(PropertyName = "Passed")]
        public bool? Passed { get; set; }

        [JsonProperty(PropertyName = "CostTimeTotal")]
        public int? CostTimeTotal { get; set; }

        [JsonProperty(PropertyName = "CostTimeCreate")]
        public int? CostTimeCreate { get; set; }

        [JsonProperty(PropertyName = "CostTimePerform")]
        public int? CostTimePerform { get; set; }

        [JsonProperty(PropertyName = "CostTimeLoad")]
        public int? CostTimeLoad { get; set; }

        [JsonProperty(PropertyName = "CostTimeLog")]
        public int? CostTimeLog { get; set; }

        [JsonProperty(PropertyName = "CostTimeSelectScenario")]
        public int? CostTimeSelectScenario { get; set; }

        [JsonExtensionData]
        public Dictionary<string, object> AdditionalData { get; set; }
        //                "application_id": null,
        //        "visa_id": null,
        //        "product_ID": null,

    }
}
