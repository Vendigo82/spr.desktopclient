﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Monitoring
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class TransactionDataModel
    {
        [JsonProperty(Required = Required.AllowNull)]
        public string Input { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string Output { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public int? StatusCode { get; set; }
    }
}
