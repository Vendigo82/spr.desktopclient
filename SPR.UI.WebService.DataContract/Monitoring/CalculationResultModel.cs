﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Monitoring
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class CalculationResultModel
    {
        [JsonProperty(Required = Required.Always)]
        public long FormulaId { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int FormulaVer { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public long? ParentFormulaId { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public int? ParentFormulaVer { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string ValueType { get; set; }

        [JsonProperty(Required = Required.Default)]
        public string Result { get; set; }

        [JsonProperty(Required = Required.Default)]
        public int PerformTime { get; set; }
    }
}
