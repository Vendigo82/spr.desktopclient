﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SPR.UI.WebService.DataContract.Scenario;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Monitoring
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SummaryRecordModel
    {
        public ScenarioSlimModel Scenario { get; set; }

        public string Step { get; set; }

        public int TotalCount { get; set; }

        public int Error { get; set; }

        public int Passed { get; set; }

        public int Rejected { get; set; }

        public int AvgTimeTotal { get; set; }

        public int AvgTimeSelectScenario { get; set; }

        public int AvgTimeLoad { get; set; }

        public int AvgTimeCreate { get; set; }

        public int AvgTimePerform { get; set; }

        public int AvgTimeLog { get; set; }
    }
}
