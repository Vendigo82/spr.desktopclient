﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Monitoring
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class TransactionDataResponseModel<T>
    {
        [JsonProperty(Required = Required.Default)]
        public T Transaction { get; set; }

        [JsonProperty(Required = Required.Default)]
        public TransactionDataModel Data { get; set; }

        [JsonProperty(Required = Required.Default)]
        public IEnumerable<TransactionErrorModel> Errors { get; set; }
    }

    public class TransactionDataResponseModel : TransactionDataResponseModel<TransactionModel>
    {

    }
}
