﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Monitoring
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class TransactionErrorContextModel
    {
        [JsonProperty(Required = Required.AllowNull)]
        public TransactionModel Transaction { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public TransactionDataModel Data { get; set; }
    }
}
