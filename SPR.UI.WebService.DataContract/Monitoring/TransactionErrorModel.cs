﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace SPR.UI.WebService.DataContract.Monitoring
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class TransactionErrorModel
    {
        [JsonProperty(Required = Required.Always)]
        public long Id { get; set; }

        [JsonProperty(PropertyName = "Timestamp", Required = Required.Always)]
        public DateTime Timestamp { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public long? TransId { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string ErrorType { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string Name { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string Message { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string Trace { get; set; }

        public long? FormulaId { get; set; }

        public int? FormulaVer { get; set; }

        public long? ParentFormulaId { get; set; }

        public int? ParentFormulaVer { get; set; }
    }
}
