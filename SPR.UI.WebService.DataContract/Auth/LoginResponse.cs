﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Auth
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class LoginResponse
    {
        [JsonProperty(Required = Required.Always)]
        public string AccessToken { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string TokenType { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int ExpiresIn { get; set; }

        [JsonProperty(Required = Required.Default)]
        public string Project { get; set; }
    }
}
