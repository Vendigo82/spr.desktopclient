﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Auth
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class LoginRequest
    {
        /// <summary>
        /// User name (Login)
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Username { get; set; }

        /// <summary>
        /// user password, can be nullm but can't omitted
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string Password { get; set; }
    }
}
