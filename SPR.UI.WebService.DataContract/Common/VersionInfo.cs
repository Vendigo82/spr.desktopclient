﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace SPR.UI.WebService.DataContract.Common
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class VersionInfo
    {
        public int Version { get; set; }

        public DateTime DateTime { get; set; }
    }
}
