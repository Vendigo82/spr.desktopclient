﻿
namespace SPR.UI.WebService.DataContract.Configuration
{
    /// <summary>
    /// Data type for GET configuration/instance response
    /// </summary>
    public class InstanceResponse
    {
        /// <summary>
        /// Minimal available app version
        /// </summary>
        public int MinVersion { get; set; }

        /// <summary>
        /// Maximum available app version
        /// </summary>
        public int MaxVersion { get; set; }

        /// <summary>
        /// Name of server instance
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Url of spr test module service
        /// </summary>
        public string TestServiceUrl { get; set; }     
        
        /// <summary>
        /// Url of external profile web site
        /// </summary>
        public string ProfileUrl { get; set; }

        /// <summary>
        /// Usign external or internal profile
        /// </summary>
        public bool UsingProfileWebsite { get; set; }

        /// <summary>
        /// Documentation service settings. 
        /// If null, then documentation service is off
        /// </summary>
        public DocumentationSettings DocumentationSettings { get; set; }
    }
}
