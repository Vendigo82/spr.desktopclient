﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Configuration
{
    /// <summary>
    /// Information about transaction table column
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class TransactionColumnModel
    {
        /// <summary>
        /// Database column name
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string DbName { get; set; }

        /// <summary>
        /// User's column title
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Title { get; set; }

        /// <summary>
        /// User's column position
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public short? Position { get; set; }

        /// <summary>
        /// Enable filter for this column
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool EnableFilter { get; set; }
    }
}
