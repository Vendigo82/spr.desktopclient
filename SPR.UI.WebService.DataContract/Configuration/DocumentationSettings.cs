﻿using System;

namespace SPR.UI.WebService.DataContract.Configuration
{
    /// <summary>
    /// Information about documentation service
    /// </summary>
    public class DocumentationSettings
    {
        public Uri BaseUrl { get; set; }
    }
}
