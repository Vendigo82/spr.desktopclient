﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract
{
    /// <summary>
    /// Contains list of objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ItemsList<T>
    {
        /// <summary>
        /// List of objects, can't be null
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<T> Items { get; set; }
    }
}
