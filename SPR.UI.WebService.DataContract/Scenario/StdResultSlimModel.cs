﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Scenario
{
    /// <summary>
    /// Standard scenatio results group
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class StdResultSlimModel : StdResultModelBase
    {
        /// <summary>
        /// Is this results using
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Usage { get; set; }
    }
}
