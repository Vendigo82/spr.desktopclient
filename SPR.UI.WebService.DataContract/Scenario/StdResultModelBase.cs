﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace SPR.UI.WebService.DataContract.Scenario
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class StdResultModelBase : StdResultBriefModel
    {
        /// <summary>
        /// Is this group default
        /// </summary>
        public bool Default { get; set; }
    }
}
