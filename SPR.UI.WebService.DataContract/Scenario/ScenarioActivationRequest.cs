﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Scenario
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ScenarioActivationRequest
    {
        [JsonProperty(Required = Required.Always)]
        public bool Enable { get; set; }
    }
}
