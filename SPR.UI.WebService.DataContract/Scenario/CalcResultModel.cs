﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SPR.UI.WebService.DataContract.Formula;
using SPR.DAL.Enums;
using Newtonsoft.Json.Converters;

namespace SPR.UI.WebService.DataContract.Scenario
{
    /// <summary>
    /// Scenario calculation result data model
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class CalcResultModel
    {
        /// <summary>
        /// Parameter name
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        /// <summary>
        /// Formula information
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public FormulaBriefModel Formula { get; set; }

        /// <summary>
        /// Enabled or disabled callculation result
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Enabled { get; set; }

        /// <summary>
        /// Calculation order
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public int Order { get; set; }

        /// <summary>
        /// Using as web-service response field
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Out { get; set; }

        /// <summary>
        /// Moment of calculation
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CalculateTime CalcTime { get; set; }
    }
}
