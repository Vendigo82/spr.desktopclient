﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SPR.UI.WebService.DataContract.Formula;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Scenario
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ScenarioModel : ScenarioBaseModel
    {
        [JsonProperty(Required = Required.Always)]
        public int Version { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int MinorVersion { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string Descr { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public FormulaBriefModel FilterFormula { get; set; }

        [JsonProperty(Required = Required.Always)]
        public List<CheckupModel> Checkups { get; set; }

        [JsonProperty(Required = Required.Always)]
        public List<CalcResultModel> CalcResults { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool PerformAllCheckups { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public StdResultBriefModel StdResult { get; set; }
    }
}
