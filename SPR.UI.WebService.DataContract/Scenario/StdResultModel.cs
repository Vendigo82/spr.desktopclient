﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace SPR.UI.WebService.DataContract.Scenario
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class StdResultModel : StdResultModelBase
    {
        [JsonProperty(Required = Required.Always)]
        public List<StdResultValueModel> Values { get; set; }
    }
}
