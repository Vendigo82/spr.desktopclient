﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace SPR.UI.WebService.DataContract.Scenario
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ScenarioSlimModel
    {
        /// <summary>
        /// Scenario enabled flag
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Enabled { get; set; }

        /// <summary>
        /// Unique formula code
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public long Code { get; set; }

        /// <summary>
        /// Unique scenario id
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public Guid Guid { get; set; }

        /// <summary>
        /// Name of the formula
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string Name { get; set; }

        /// <summary>
        /// Input step for scenario
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Step { get; set; }        
    }
}
