﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using SPR.DAL.Enums;

namespace SPR.UI.WebService.DataContract.Scenario
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class StdResultValueModel
    {
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Expression { get; set; }

        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CalculateTime CalcTime { get; set; }
    }
}
