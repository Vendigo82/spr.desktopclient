﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace SPR.UI.WebService.DataContract.Scenario
{
    /// <summary>
    /// Very brief information about standard results.
    /// Contains only id and name fields
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class StdResultBriefModel
    {
        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public Guid Id { get; set; }

        /// <summary>
        /// Name of the group
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }
    }
}
