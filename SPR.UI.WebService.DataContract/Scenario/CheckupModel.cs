﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SPR.UI.WebService.DataContract.Formula;

namespace SPR.UI.WebService.DataContract.Scenario
{
    /// <summary>
    /// Scenario checkup data model
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class CheckupModel
    {
        /// <summary>
        /// Information about formula
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public FormulaScenarioCheckupModel Formula { get; set; }

        /// <summary>
        /// Is checkup enabled
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Enabled { get; set; }

        /// <summary>
        /// Checkup order
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public int Order { get; set; }
    }
}
