﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Scenario
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ScenarioBaseModel : ScenarioSlimModel
    {
        /// <summary>
        /// Scenario next step on success
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string StepSuccess { get; set; }

        /// <summary>
        /// Scenario next step on reject
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string StepReject { get; set; }

        /// <summary>
        /// Scenario quota
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public byte Quota { get; set; }

        /// <summary>
        /// Scenario priority
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public short Prior { get; set; }
    }
}
