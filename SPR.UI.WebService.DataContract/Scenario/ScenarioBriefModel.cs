﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Scenario
{
    /// <summary>
    /// Brief information about scenario
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ScenarioBriefModel : ScenarioBaseModel
    {
        /// <summary>
        /// Scenario has filter or not
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool HasFilter { get; set; }
    }
}
