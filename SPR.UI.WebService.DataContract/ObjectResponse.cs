﻿using System;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ObjectResponse<T> : ObjectContainer<T>
    {
        [JsonProperty(Required = Required.Always)]
        public ObjectVersionKind Kind { get; set; }

        //[JsonProperty(Required = Required.Always)]
        //public T Item { get; set; }
    }
}
