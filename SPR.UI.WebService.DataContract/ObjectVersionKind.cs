﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract
{
    /// <summary>
    /// Kind of object data
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter), typeof(SnakeCaseNamingStrategy))]
    public enum ObjectVersionKind
    {
        /// <summary>
        /// Working version
        /// </summary>
        Main,

        /// <summary>
        /// Draft version
        /// </summary>
        Draft,

        /// <summary>
        /// Archive version
        /// </summary>
        Archive
    }
}
