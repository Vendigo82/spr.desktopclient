﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Enums
{
    public enum SPRDataType
    {
        /// <summary>
        /// Auto detect data type
        /// </summary>
        Auto = 0,

        Integer = 1,

        Float = 2,

        String = 3,

        Boolean = 4,
    }
}
