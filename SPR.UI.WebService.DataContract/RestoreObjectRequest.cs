﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class RestoreObjectRequest
    {
        /// <summary>
        /// Journal comment
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string Comment { get; set; }
    }
}
