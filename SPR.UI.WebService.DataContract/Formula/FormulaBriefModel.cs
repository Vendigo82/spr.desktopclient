﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using SPR.DAL.Enums;
using System;

namespace SPR.UI.WebService.DataContract.Formula
{
    /// <summary>
    /// Brief information about formula
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class FormulaBriefModel
    {
        /// <summary>
        /// Unique formula code
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public long Code { get; set; }

        /// <summary>
        /// Unique formula id
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public Guid Guid { get; set; }

        /// <summary>
        /// Name of the formula
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        /// <summary>
        /// Type of the formula
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public FormulaType Type { get; set; }
    }
}
