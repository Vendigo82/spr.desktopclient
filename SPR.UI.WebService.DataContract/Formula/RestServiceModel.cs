﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace SPR.UI.WebService.DataContract.Formula
{
    /// <summary>
    /// REST service formula model
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class RestServiceModel
    {
        /// <summary>
        /// REST server name
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Server { get; set; }

        /// <summary>
        /// Field name which contains result
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string Result { get; set; }

        /// <summary>
        /// Include all input fields to request
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool AllFields { get; set; }

        /// <summary>
        /// Include all simple fields to request
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool AllSimpleFields { get; set; }

        /// <summary>
        /// List of fields for include
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string Fields { get; set; }

        /// <summary>
        /// request should inlude input data form
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        [DefaultValue(true)]
        public bool IncludeInputData { get; set; } = true;

        /// <summary>
        /// path to include input data form
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string RequestInputDataPath { get; set; }

        /// <summary>
        /// request should include context values
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        [DefaultValue(true)]
        public bool IncludeAllContextValues { get; set; } = true;

        /// <summary>
        /// raise error on not successfull http status code
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        [DefaultValue(true)]
        public bool EnsureStatusCodeIsSuccess { get; set; } = true;

        /// <summary>
        /// raise error on invalid json
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        [DefaultValue(true)]
        public bool ThrowsExceptionOnInvalidJson { get; set; } = true;

        /// <summary>
        /// save response body to specific property
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string ResponsePropertyName { get; set; }

        /// <summary>
        /// save response code to specific property
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string StatusCodePropertyName { get; set; }

        /// <summary>
        /// raise error if [Result] path does not found in response body
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        [DefaultValue(true)]
        public bool ThrowExceptionIfResultPathNotFound { get; set; } = true;

        /// <summary>
        /// save raw data property if json was invalid
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string ResponseRawDataProperty { get; set; }

        /// <summary>
        /// Local service url
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string LocalUrl { get; set; }

        /// <summary>
        /// List of json properties
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public List<SqlQueryParamModel> Properties { get; set; }

        /// <summary>
        /// Http method: GET, POST etc
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        public string HttpVerb { get; set; }

        /// <summary>
        /// Perform request with body
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        public bool? WithBody { get; set; }

        /// <summary>
        /// Ignore network errors
        /// </summary>
        public bool IgnoreNetworkErrors { get; set; }

        /// <summary>
        /// add 'success' and 'response_received' properties to result
        /// </summary>
        public bool AddSuccessProperties { get; set; }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            if (Properties == null)
                Properties = new List<SqlQueryParamModel>();
        }
    }
}
