﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using SPR.DAL.Enums;
using SPR.UI.WebService.DataContract.Converters;
using System;
using System.Collections.Generic;

namespace SPR.UI.WebService.DataContract.Formula
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class GoogleBQModel
    {
        /// <summary>
        /// Name of the Google BQ Connection
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string ConnectionName { get; set; }

        /// <summary>
        /// Query 
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Query { get; set; }

        /// <summary>
        /// Query result type
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public QueryResultType ResultType { get; set; }

        /// <summary>
        /// Query timeout
        /// </summary>
        [JsonConverter(typeof(TimeSpanConverter))]
        public TimeSpan? Timeout { get; set; }

        /// <summary>
        /// Query parameters
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public List<SqlQueryParamModel> SqlParams { get; set; }
    }
}
