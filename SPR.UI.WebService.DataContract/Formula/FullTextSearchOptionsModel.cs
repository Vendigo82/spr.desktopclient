﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Formula
{
    /// <summary>
    /// Full text search options
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class FullTextSearchOptionsModel
    {
        /// <summary>
        /// Minimal length of full text search fragment
        /// </summary>
        public int MinLength { get; set; }
    }
}
