﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Formula
{
    [JsonObject(NamingStrategyType = typeof(Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy))]
    public class ExcludeList
    {
        [JsonProperty(Required = Required.Default)]
        public IEnumerable<Guid> Exclude { get; set; }
    }
}
