﻿using System.Collections.Generic;
using System.Data;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

using SPR.DAL.Enums;

namespace SPR.UI.WebService.DataContract.Formula
{
    /// <summary>
    /// Sql query formula model
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class SqlQueryModel
    {        
        /// <summary>
        /// SQL server's name
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Server { get; set; }

        /// <summary>
        /// Sql query
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Sql { get; set; }

        /// <summary>
        /// Type of result
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public QueryResultType ResultType { get; set; }

        /// <summary>
        /// Transaction isolation level
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        [JsonConverter(typeof(StringEnumConverter))]
        public IsolationLevel? IsolationLevel { get; set; }

        /// <summary>
        /// List of sql params. Required always
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public List<SqlQueryParamModel> SqlParams { get; set; }
    }
}
