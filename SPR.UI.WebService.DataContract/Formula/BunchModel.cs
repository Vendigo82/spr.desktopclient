﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Formula
{
    [JsonObject(NamingStrategyType = typeof(Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy))]
    public class BunchModel
    {
        [JsonProperty(Required = Required.Always)]
        public bool PerformAll { get; set; }

        [JsonProperty(Required = Required.Always)]
        public List<BunchRowModel> Rows { get; set; }
    }

    
}
