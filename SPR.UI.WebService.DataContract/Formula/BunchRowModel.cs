﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Formula
{
    /// <summary>
    /// Bunch formula's row data model 
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy))]
    public class BunchRowModel
    {
        /// <summary>
        /// Row order
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public int Order { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Body { get; set; }

        public int? RejectCode { get; set; }

        public int? BlackList { get; set; }

        public string RejectStep { get; set; }

        public bool AbortScenario { get; set; }
    }
}
