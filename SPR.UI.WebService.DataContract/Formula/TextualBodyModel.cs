﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Formula
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class TextualBodyModel
    {
        [JsonProperty(Required = Required.Always)]
        public string Body { get; set; }
    }
}
