﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Formula
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class NestedFormulaModel
    {
        /// <summary>
        /// Parameter name
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        /// <summary>
        /// Nested formula enabled flag
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Enable { get; set; }

        /// <summary>
        /// Nested formula info
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public FormulaBriefModel Formula { get; set; }

        /// <summary>
        /// RunIf instruction enabled flag
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool RunIfEnabled { get; set; }

        /// <summary>
        /// RunIf expression
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string RunIfExpression { get; set; }

        /// <summary>
        /// Nested formula order
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull)]
        public int Order { get; set; }
    }
}
