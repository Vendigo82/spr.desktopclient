﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Formula
{
    /// <summary>
    /// Type of usage formula in scenario
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FormulaUsageType
    {
        Checkup,

        CalcResult,

        Filter,
    }

}
