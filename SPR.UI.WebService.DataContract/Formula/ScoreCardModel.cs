﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SPR.UI.WebService.DataContract.Formula
{
    /// <summary>
    /// Score card formula's data model
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ScoreCardModel
    {
        /// <summary>
        /// Expression which applied to final summ
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string FinalExpression { get; set; }

        /// <summary>
        /// Initial value
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public double Initial { get; set; }

        /// <summary>
        /// List of variables
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public List<ScoreCardVarModel> Variables { get; set; }        
    }

    /// <summary>
    /// Score card variable data model
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ScoreCardVarModel
    {
        /// <summary>
        /// Enabled flag
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Enable { get; set; }

        /// <summary>
        /// Field name or expression. Can be null
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string Expression { get; set; }

        /// <summary>
        /// Variable description
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string Descr { get; set; }

        /// <summary>
        /// Default value, if all conditions was failed
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public double Otherwise { get; set; }

        /// <summary>
        /// List of conditions
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public List<ScoreCardConditionModel> Conditions { get; set; }
    }

    /// <summary>
    /// Score card condition data model
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ScoreCardConditionModel
    {
        /// <summary>
        /// Enabled flag
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Enable { get; set; }

        /// <summary>
        /// Expression of codition
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public string Expression { get; set; }

        /// <summary>
        /// Condition's order
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public short Order { get; set; }

        /// <summary>
        /// Value which will used if condition passed
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public double Value { get; set; }
    }
}
