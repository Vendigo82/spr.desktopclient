﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Formula
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class FormulaScenarioCheckupModel : FormulaBriefModel
    {
        [JsonProperty(Required = Required.AllowNull)]
        public int? RejectCode { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public string RejectStep { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool AbortScenario { get; set; }

        /// <summary>
        /// List of nested reject codes (bunch formula), can be null
        /// </summary>
        [JsonProperty(Required = Required.Default, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public IEnumerable<int> NestedRejectCodes { get; set; }

        /// <summary>
        /// List of nested reject steps (bunch formula), can be null
        /// </summary>
        [JsonProperty(Required = Required.Default, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public IEnumerable<string> NestedSteps { get; set; }

        /// <summary>
        /// has nested abort scenario properties
        /// </summary>
        [JsonProperty(Required = Required.Default, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool NestedAbortScenario { get; set; }
    }
}
