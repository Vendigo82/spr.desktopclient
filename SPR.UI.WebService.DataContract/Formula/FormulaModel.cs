﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using SPR.DAL.Enums;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SPR.UI.WebService.DataContract.Formula
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class FormulaModel //: FormulaCalcModel
    {
        [JsonProperty(Required = Required.Always)]
        public long Code { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Guid Guid { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int Version { get; set; }

        [JsonProperty(Required = Required.Always)]
        public int MinorVersion { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Name { get; set; }

        public string Descr { get; set; }

        public int? RejectCode { get; set; }

        public string RejectStep { get; set; }

        public bool IsCheckup { get; set; }

        public int? BlackList { get; set; }

        public bool AbortScenario { get; set; }

        public string RegisterName { get; set; }

        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public FormulaType Type { get; set; }

        /// <summary>
        /// List of parameters
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public List<ParamModel> Params { get; set; }

        /// <summary>
        /// List of child formulas
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public List<NestedFormulaModel> ChildFormulas { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
        public TextualBodyModel TextualBody { get; set; }

        /// <summary>
        /// Bunch formula model's part
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
        public BunchModel Bunch { get; set; }

        /// <summary>
        /// Rest service formula model's part
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
        public RestServiceModel Rest { get; set; }

        /// <summary>
        /// Score card formula model's part
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
        public ScoreCardModel ScoreCard { get; set; }

        /// <summary>
        /// Sql query model's part
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
        public SqlQueryModel Sql { get; set; }

        /// <summary>
        /// Google big query data
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
        public GoogleBQModel GoogleBQ { get; set; }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            switch (Type)
            {
                case FormulaType.Expression:
                case FormulaType.Script:
                    if (TextualBody == null)
                        throw new JsonSerializationException("Missed TextualBody section");
                    break;
                case FormulaType.BunchFormula:
                    if (Bunch == null)
                        throw new JsonSerializationException("Missed Bunch section");
                    break;
                case FormulaType.RESTService:
                    if (Rest == null)
                        throw new JsonSerializationException("Missed Rest section");
                    break;
                case FormulaType.ScoreCard:
                    if (ScoreCard == null)
                        throw new JsonSerializationException("Missed ScoreCard section");
                    break;
                case FormulaType.SqlQuery:
                    if (Sql == null)
                        throw new JsonSerializationException("Missed Sql section");
                    break;
                case FormulaType.GoogleBQ:
                    if (GoogleBQ == null)
                        throw new JsonSerializationException("Missed Google BQ section");
                    break;
                default:
                    throw new NotImplementedException($"Unknown formula type {Type} in FormulaModel.OnDeserializedMethod");
            }
        }
    }
}
