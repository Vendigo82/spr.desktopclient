﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using SPR.UI.WebService.DataContract.Scenario;
using System.Collections.Generic;

namespace SPR.UI.WebService.DataContract.Formula
{
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class UsageScenarioPair
    {
        [JsonProperty(Required = Required.Always)]
        public FormulaUsageType Key { get; set; }

        [JsonProperty(Required = Required.Always)]
        public IEnumerable<ScenarioSlimModel> Value { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class FormulaUsageResponse
    {
        /// <summary>
        /// List of formulas where formula is using
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<FormulaBriefModel> Formulas { get; set; }

        /// <summary>
        /// List of scenarios where formula is using
        /// </summary>
        [JsonProperty(Required = Required.Always)]        
        public IEnumerable<UsageScenarioPair> Scenarios { get; set; }
    }
}
