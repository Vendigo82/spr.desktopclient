﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SPR.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SPR.UI.WebService.DataContract.Journal
{
    /// <summary>
    /// Journal record model with object's information
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class JournalRecordModel : JournalRecordSlimModel
    {
        /// <summary>
        /// Type of object
        /// </summary>
        [JsonProperty(PropertyName = "obj_type", Required = Required.Always)]
        public SerializedObjectType ObjectType { get; set; }

        /// <summary>
        /// Object's GUID
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public Guid ObjectGuid { get; set; }

        /// <summary>
        /// Formula's or scenario's code
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public long? ObjectCode { get; set; }

        /// <summary>
        /// Object's name
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string ObjectName { get; set; }
    }
}
