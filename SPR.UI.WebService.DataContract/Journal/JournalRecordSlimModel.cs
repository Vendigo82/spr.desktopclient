﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using SPR.DAL.Enums;
using SPR.UI.WebService.DataContract.User;
using System;

namespace SPR.UI.WebService.DataContract.Journal
{
    /// <summary>
    /// Journal record data model without object's information
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class JournalRecordSlimModel
    {
        /// <summary>
        /// Record id
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public long Id { get; set; }

        /// <summary>
        /// Record data time in UTC zone
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public DateTime DateTime { get; set; }

        [JsonIgnore]
        public Guid? UserId { get; set; }

        /// <summary>
        /// Information about user
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public UserModel User { get; set; }

        /// <summary>
        /// Object's version
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public int Version { get; set; }

        /// <summary>
        /// Action
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(StringEnumConverter))]
        public JournalAction Action { get; set; }

        /// <summary>
        /// Version which restored for <see cref="JournalAction.Rollback"/> action
        /// </summary>
        [JsonProperty(Required = Required.Default, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int? RestoredVersion { get; set; }

        /// <summary>
        /// Ip address which perform action
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string IpAddress { get; set; }

        [JsonProperty(Required = Required.Default)]
        public string Comment { get; set; }
    }
}
