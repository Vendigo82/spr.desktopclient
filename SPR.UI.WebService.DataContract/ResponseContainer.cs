﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace SPR.UI.WebService.DataContract
{
    /// <summary>
    /// Container for web-service response data
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(SnakeCaseNamingStrategy))]
    public class ResponseContainer<T> where T : class
    {
        /// <summary>
        /// Operation was successefully complete and field data containts response
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool Success { get; set; }

        /// <summary>
        /// Server response data
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public T Data { get; set; }

        /// <summary>
        /// Kind of error. Omit if success = true
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull, DefaultValueHandling = DefaultValueHandling.Ignore)]
        //[JsonConverter(typeof(StringEnumConverter), typeof(SnakeCaseNamingStrategy))]
        public ErrorKind? Error { get; set; }

        /// <summary>
        /// Error message. Omit if success = true
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Message { get; set; }

        /// <summary>
        /// Error additional information. Omit if success = true
        /// </summary>
        [JsonProperty(Required = Required.DisallowNull, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Trace { get; set; }
    }
}
